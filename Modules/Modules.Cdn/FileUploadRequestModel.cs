﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Modules.Cdn
{
    public class FileUploadRequestModel
    {
        public Guid Token { get; set; }
        public string FileName { get; set; }
        public string Module { get; set; }
        public byte[] Data { get; set; }
    }
}