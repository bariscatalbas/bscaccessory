﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace Modules.Cdn
{
    /// <summary>
    /// Summary description for FileUploader
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class FileUploader : System.Web.Services.WebService
    {

        private const string UploadDirectoryRoot = "~/Uploads/";

        [WebMethod]
        public FileUploadResponseModel UploadFile(FileUploadRequestModel model)
        {
            try
            {
                var token = new Guid(ConfigurationManager.AppSettings["Token"]);

                if (model.Token != token)
                {
                    return new FileUploadResponseModel
                    {
                        Success = false,
                        Message = "Token is not valid"
                    };
                }

                var fileFolderPath = UploadDirectoryRoot + model.Module;
                _.Io.CheckAndCreateFolder(fileFolderPath);

                var fileName = _.Io.FindFileNameToUpload(fileFolderPath, model.FileName);
                var fullFilePath = Server.MapPath(fileFolderPath + "/" + fileName);

                File.WriteAllBytes(fullFilePath, model.Data);
                return new FileUploadResponseModel
                {
                    Success = true,
                    FileName = fileName
                };
            }
            catch (Exception ex)
            {
                var message = ex.ToString();
                if (ex.InnerException != null)
                {
                    message += " || " + ex.InnerException.ToString();
                }
                return new FileUploadResponseModel
                {
                    Success = false,
                    Message = message
                };
            }
        }




    }
}
