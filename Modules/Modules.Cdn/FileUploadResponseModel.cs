﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Modules.Cdn
{
    public class FileUploadResponseModel
    {
        public bool Success { get; set; }
        public string FileName { get; set; }
        public string Message { get; set; }
    }
}