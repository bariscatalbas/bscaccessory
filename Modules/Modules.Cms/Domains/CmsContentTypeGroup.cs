﻿using Core.Data.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modules.Cms.Domains
{
    public class CmsContentTypeGroup : DeletableEntity
    {
        [MaxLength(250)]
        public virtual string Name { get; set; }
        [MaxLength(250)]
        public virtual string Code { get; set; }
        [MaxLength(500)]
        public virtual string AllowedEntities { get; set; }

        public virtual int DisplayOrder { get; set; }
    }
}
