﻿using Core.Data.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modules.Cms.Domains
{
    public class CmsVersion : EntityBase
    {
        public virtual long EntityId { get; set; }
        [MaxLength(50)]
        public virtual string EntityName { get; set; }
        public virtual int Version { get; set; }
    }
}
