﻿using Core.Data.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modules.Cms.Domains
{
    public interface ICmsDbContext
    {
        DbSet<CmsContentType> CmsContentTypes { get; set; }
        DbSet<CmsContentTypeGroup> CmsContentTypeGroups { get; set; }
        DbSet<CmsHistory> CmsHistories { get; set; }
        DbSet<CmsVersion> CmsVersions { get; set; }
        DbSet<CmsVersionContent> CmsVersionContents { get; set; }
    }
}
