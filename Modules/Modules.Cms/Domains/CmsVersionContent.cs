﻿using Core.Data.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modules.Cms.Domains
{
    public class CmsVersionContent : EntityBase
    {
        public virtual long CmsVersionId { get; set; }
        [ForeignKey("CmsVersionId")]
        public virtual CmsVersion CmsVersion { get; set; }

        public virtual long CmsContentTypeId { get; set; }
        [ForeignKey("CmsContentTypeId")]
        public virtual CmsContentType CmsContentType { get; set; }

        public virtual long? ParentId { get; set; }
        [ForeignKey("ParentId")]
        public virtual CmsVersionContent Parent { get; set; }

        public virtual int DisplayOrder { get; set; }
        public virtual string Data { get; set; }
    }
}
