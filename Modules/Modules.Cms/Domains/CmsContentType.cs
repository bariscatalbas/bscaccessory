﻿using Core.Data.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modules.Cms.Domains
{
    public class CmsContentType : DeletableEntity
    {
        public virtual long CmsContentTypeGroupId { get; set; }
        [ForeignKey("CmsContentTypeGroupId")]
        public virtual CmsContentTypeGroup CmsContentTypeGroup { get; set; }

        [MaxLength(50)]
        public virtual string Name { get; set; }
        [MaxLength(50)]
        public virtual string Code { get; set; }
        [MaxLength(500)]
        public virtual string DataSystemName { get; set; }
        [MaxLength(500)]
        public virtual string AllowedEntities { get; set; }
        [MaxLength(50)]
        public virtual string ParentKey { get; set; }
        [MaxLength(50)]
        public virtual string ChildrenKey { get; set; }
    }
}
