﻿using Core.Data.Interfaces;
using Core.Data.Models;
using Core.Ioc;
using Modules.Cms.AdminModels;
using Modules.Cms.Domains;
using Modules.Cms.Interfaces;
using Modules.Cms.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modules.Cms.Services
{
    public class CmsDesignService
    {
        private readonly IRepository<CmsContentType> _cmsContentTypeRepository;

        private readonly ICmsEntityAdminService _entityAdminService;
        private readonly IRepository<CmsVersion> _cmsVersionRepository;
        private readonly IRepository<CmsVersionContent> _cmsVersionContentRepository;
        private readonly IRepository<CmsHistory> _cmsHistoryRepository;
        private readonly CmsHistoryService _cmsHistoryService;
        private readonly string _entityName;


        public CmsDesignService(ICmsEntityAdminService entityAdminService)
        {
            _cmsContentTypeRepository = AcEngine.Resolve<IRepository<CmsContentType>>();

            _entityAdminService = entityAdminService;
            _cmsVersionRepository = AcEngine.Resolve<IRepository<CmsVersion>>();
            _cmsVersionContentRepository = AcEngine.Resolve<IRepository<CmsVersionContent>>();
            _cmsHistoryRepository = AcEngine.Resolve<IRepository<CmsHistory>>();

            _cmsHistoryService = AcEngine.Resolve<CmsHistoryService>();
            _entityName = entityAdminService.GetEntityName();
        }

        public long? CreateCmsVersion(long entityId, long identityId)
        {
            var entity = _entityAdminService.GetEntity(entityId);

            if (entity != null && !entity.PendingApproval)
            {
                if (entity.ActiveVersion == entity.DesignedVersion)
                {
                    entity.DesignedVersion = entity.LastVersion + 1;
                    entity.LastVersion++;
                    entity.LockedById = identityId;
                    _entityAdminService.UpdateEntity(entity, identityId);
                }

                var cmsVersion = _cmsVersionRepository.Table.FirstOrDefault(p => p.EntityId == entityId && p.EntityName == _entityName && p.Version == entity.DesignedVersion);
                if (cmsVersion == null)
                {
                    cmsVersion = new CmsVersion
                    {
                        EntityId = entityId,
                        Version = entity.DesignedVersion,
                        EntityName = _entityName
                    };
                    _cmsVersionRepository.Insert(cmsVersion, identityId);

                    if (cmsVersion.Version == 1)
                    {
                        _cmsHistoryService.CmsVersionCreated(_entityName, entityId, identityId, cmsVersion.Version);
                    }
                    else
                    {
                        _cmsHistoryService.CmsVersionUpdated(_entityName, entityId, identityId, cmsVersion.Version);
                    }

                    var cloneHelpers = new List<CmsDesignCloneModel>();
                    var cloneContents = new List<CmsVersionContent>();

                    var activeVersion = _cmsVersionRepository.Table.FirstOrDefault(q => q.EntityId == entity.Id && q.EntityName == _entityName && q.Version == entity.ActiveVersion);
                    if (activeVersion != null)
                    {
                        var cmsContents = _cmsVersionContentRepository.Table.Where(p => p.CmsVersionId == activeVersion.Id).ToList();

                        foreach (var cmsContent in cmsContents)
                        {
                            var cloneContent = new CmsVersionContent
                            {
                                Data = cmsContent.Data,
                                DisplayOrder = cmsContent.DisplayOrder,
                                CmsContentTypeId = cmsContent.CmsContentTypeId,
                                CmsVersionId = cmsVersion.Id,
                                ParentId = cmsContent.ParentId
                            };
                            _cmsVersionContentRepository.Insert(cloneContent, identityId);

                            cloneHelpers.Add(new CmsDesignCloneModel
                            {
                                Id = cloneContent.Id,
                                OldId = cmsContent.Id,
                            });
                            cloneContents.Add(cloneContent);
                        }
                    }

                    foreach (var cloneContent in cloneContents)
                    {
                        var cloneHelper = cloneHelpers.FirstOrDefault(p => p.OldId == cloneContent.ParentId);
                        if (cloneHelper != null)
                        {
                            cloneContent.ParentId = cloneHelper.Id;
                            _cmsVersionContentRepository.Update(cloneContent, identityId);
                        }
                    }
                }

                return cmsVersion.Id;
            }
            return null;
        }

        public CmsDesignCmsVersionModel GetCmsVersion(long cmsVersionId)
        {
            var cmsVersion = _cmsVersionRepository
                        .Table
                        .Where(p => p.Id == cmsVersionId)
                        .Select(p => new CmsDesignCmsVersionModel
                        {
                            CmsVersionId = p.Id,
                            EntityId = p.EntityId,
                            EntityName = p.EntityName,
                            Version = p.Version,
                        }).FirstOrDefault();

            return cmsVersion;
        }

        public List<CmsDesignContentModel> GetCmsVersionContents(long cmsVersionId)
        {
            var list = _cmsVersionContentRepository
                        .Table
                        .Where(p => p.CmsVersionId == cmsVersionId)
                        .Select(p => new CmsDesignContentModel
                        {
                            ParentId = p.ParentId,
                            Id = p.Id,
                            CmsContentTypeId = p.CmsContentTypeId,
                            DisplayOrder = p.DisplayOrder,
                            Data = p.Data,
                            CmsContentTypeCode = p.CmsContentType.Code,
                            CmsContentTypeName = p.CmsContentType.Name,
                            CmsContentChildrenKey = p.CmsContentType.ChildrenKey,
                            CmsContentTypeGroupCode = p.CmsContentType.CmsContentTypeGroup.Code,
                        })
                        .OrderBy(p => p.DisplayOrder)
                        .ToList();

            return GenerateCmsContentListRecursive(list, null);
        }

        private List<CmsDesignContentModel> GenerateCmsContentListRecursive(List<CmsDesignContentModel> list, long? parentId)
        {
            var items = list.Where(p => p.ParentId == parentId).ToList();
            foreach (var item in items)
            {
                item.Children = GenerateCmsContentListRecursive(list, item.Id);
            }
            return items;
        }

        public object GetCreateModel(long contentTypeId, out string contentTypeDataSystemName)
        {
            contentTypeDataSystemName = _cmsContentTypeRepository.Table.Where(p => p.Id == contentTypeId).Select(p => p.DataSystemName).FirstOrDefault();
            if (!string.IsNullOrWhiteSpace(contentTypeDataSystemName))
            {
                var type = Type.GetType(contentTypeDataSystemName);
                if (type != null)
                {
                    var model = Activator.CreateInstance(type);
                    return model;
                }
            }
            return null;
        }

        public long Create(object model, long cmsVersionId, long? parentId, long contentTypeId, string contentTypeDataSystemName, long identityId)
        {
            var id = ((CmsDataModelBase)model).Id;
            var data = JsonConvert.SerializeObject(model);

            var cmsVersionContent = new CmsVersionContent
            {
                CmsVersionId = cmsVersionId,
                ParentId = parentId,
                CmsContentTypeId = contentTypeId,
                DisplayOrder = 100,
                Data = data,
            };
            _cmsVersionContentRepository.Insert(cmsVersionContent, identityId);
            return cmsVersionContent.Id;
        }

        public List<CmsDesignContentTypeSelectModel> GetContentTypes(string parentKey)
        {
            var leftString = _entityName + ",";
            var rightString = "," + _entityName;
            var centerString = "," + _entityName + ",";

            return _cmsContentTypeRepository
                        .Table
                        .Where(p => p.ParentKey == parentKey && !p.Deleted &&
                        (string.IsNullOrEmpty(p.AllowedEntities) || p.AllowedEntities.StartsWith(leftString) || p.AllowedEntities.EndsWith(rightString) || p.AllowedEntities.Contains(centerString)) &&
                        (string.IsNullOrEmpty(p.CmsContentTypeGroup.AllowedEntities) || p.CmsContentTypeGroup.AllowedEntities.StartsWith(leftString) || p.CmsContentTypeGroup.AllowedEntities.EndsWith(rightString) || p.CmsContentTypeGroup.AllowedEntities.Contains(centerString))
                        )
                        .OrderBy(p => p.Name)
                        .Select(p => new CmsDesignContentTypeSelectModel
                        {
                            Id = p.Id,
                            Name = p.Name,
                            Code = p.Code,
                            GroupId = p.CmsContentTypeGroupId,
                            GroupName = p.CmsContentTypeGroup.Name,
                            GroupOrder = p.CmsContentTypeGroup.DisplayOrder
                        }).ToList();
        }

        public object GetUpdateModel(long id, out string contentTypeDataSystemName)
        {
            var cmsContent = _cmsVersionContentRepository
                                    .Table
                                    .Where(p => p.Id == id)
                                    .Select(p => new
                                    {
                                        p.ParentId,
                                        p.CmsContentType.DataSystemName,
                                        p.Data
                                    }).FirstOrDefault();
            if (cmsContent != null)
            {
                contentTypeDataSystemName = cmsContent.DataSystemName;
                var type = Type.GetType(cmsContent.DataSystemName);
                var secureString = cmsContent.Data; // _.Html.CleanHtmlBehaviour(pageContent.Data);
                var model = JsonConvert.DeserializeObject(secureString, type);
                ((CmsDataModelBase)model).Id = id;

                return model;
            }
            contentTypeDataSystemName = "";
            return null;
        }

        public bool Update(object model, long cmsVersionId, long identityId)
        {
            var id = ((CmsDataModelBase)model).Id;
            var data = JsonConvert.SerializeObject(model);

            var cmsContent = _cmsVersionContentRepository.Table.FirstOrDefault(p => p.Id == id && p.CmsVersionId == cmsVersionId);
            if (cmsContent != null)
            {
                cmsContent.Data = data;
                _cmsVersionContentRepository.Update(cmsContent, identityId);
                return true;
            }
            return false;
        }

        public void Delete(long cmsVersionContentId, long identityId)
        {
            var cmsContent = _cmsVersionContentRepository.Table.FirstOrDefault(p => p.Id == cmsVersionContentId);
            if (cmsContent != null)
            {
                DeleteChildren(cmsContent.Id, identityId);
                _cmsVersionContentRepository.Delete(cmsContent, identityId);
            }
        }

        private void DeleteChildren(long parentId, long identityId)
        {
            var children = _cmsVersionContentRepository.Table.Where(p => p.ParentId == parentId).ToList();
            foreach (var child in children)
            {
                DeleteChildren(child.Id, identityId);
                _cmsVersionContentRepository.Delete(child, identityId);
            }
        }

        public void UpdateOrder(string orderList, int cmsVersionId, long identityId)
        {
            var index = 0;
            var list = _cmsVersionContentRepository.Table.Where(p => p.CmsVersionId == cmsVersionId).OrderBy(p => p.DisplayOrder).ToList();
            var orders = orderList.Replace("item[]=", "").Split('&').ToList();

            foreach (var order in orders)
            {
                index++;
                foreach (var item in list.Where(p => p.Id == int.Parse(order)))
                {
                    item.DisplayOrder = index;
                    _cmsVersionContentRepository.Update(item, identityId);
                }
            }
        }

        public bool ApproveCmsDesign(long entityId, long identityId)
        {
            var entity = _entityAdminService.GetEntity(entityId);
            if (entity != null)
            {
                entity.ActiveVersion = entity.DesignedVersion;
                entity.PendingApproval = false;
                entity.LockedById = null;
                entity.Published = true;

                _entityAdminService.UpdateEntity(entity, identityId);
                return true;
            }
            return false;
        }

        public bool RejectCmsDesign(long entityId, long identityId)
        {
            var entity = _entityAdminService.GetEntity(entityId);
            if (entity != null)
            {
                entity.PendingApproval = false;
                entity.LockedById = null;
                entity.DesignedVersion = entity.ActiveVersion;
                _entityAdminService.UpdateEntity(entity, identityId);

                return true;
            }
            return false;
        }

        public bool CancelCmsVersion(long entityId, long identityId)
        {
            var entity = _entityAdminService.GetEntity(entityId);
            if (entity != null)
            {
                _cmsHistoryService.CmsVersionCanceled(_entityName, entityId, identityId, entity.DesignedVersion);

                entity.DesignedVersion = entity.ActiveVersion;
                entity.LockedById = null;
                _entityAdminService.UpdateEntity(entity, identityId);

                return true;
            }
            return false;
        }

        public bool CmsVersionSendToApproval(long entityId, long identityId)
        {
            var entity = _entityAdminService.GetEntity(entityId);
            if (entity != null)
            {
                if (entity.LockedById == identityId)
                {
                    _cmsHistoryService.CmsVersionSendToApproval(_entityName, entityId, identityId, entity.LastVersion);

                    entity.PendingApproval = true;
                    entity.LockedById = identityId;
                    _entityAdminService.UpdateEntity(entity, identityId);

                    return true;
                }
            }
            return false;
        }

        public bool CmsVersionPublish(long entityId, long identityId)
        {
            var entity = _entityAdminService.GetEntity(entityId);
            if (entity != null)
            {
                _cmsHistoryService.CmsVersionPublish(_entityName, entityId, identityId, entity.DesignedVersion);

                entity.ActiveVersion = entity.DesignedVersion;
                entity.LockedById = null;
                entity.PendingApproval = false;
                entity.Published = true;

                _entityAdminService.UpdateEntity(entity, identityId);
                return true;
            }
            return false;
        }

        public bool CmsVersionUnpublish(long entityId, long identityId)
        {
            var entity = _entityAdminService.GetEntity(entityId);
            if (entity != null)
            {
                _cmsHistoryService.CmsVersionPublish(_entityName, entityId, identityId, entity.DesignedVersion);

                entity.Published = false;

                _entityAdminService.UpdateEntity(entity, identityId);
                return true;
            }
            return false;
        }

        public List<CmsHistoryModel> GetCmsHistory(string entityName, long entityId)
        {
            return _cmsHistoryRepository
                        .Table
                        .Where(p => p.EntityName == entityName && p.EntityId == entityId)
                        .Select(p => new CmsHistoryModel
                        {
                            AccountId = p.IdentityId,
                            EntityId = p.EntityId,
                            EntityName = p.EntityName,
                            CmsVersion = p.CmsVersion,
                            Operation = p.Operation,
                            Date = p.Date
                        }).ToList();
        }

        public bool UndoCmsVersion(long entityId, int cmsVersion, long identityId)
        {
            var entity = _entityAdminService.GetEntity(entityId);
            if (entity != null)
            {
                entity.ActiveVersion = cmsVersion;
                _entityAdminService.UpdateEntity(entity, identityId);

                _cmsHistoryService.CmsVersionUndo(_entityName, entityId, identityId, cmsVersion);
                return true;
            }
            return false;
        }

    }
}
