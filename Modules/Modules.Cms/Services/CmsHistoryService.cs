﻿using Core.Data.Interfaces;
using Core.Ioc;
using Modules.Cms.Domains;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modules.Cms.Services
{
    public class CmsHistoryService : IInjectAsSelf
    {
        private readonly IRepository<CmsHistory> _cmsHistoryRepository;

        public CmsHistoryService(IRepository<CmsHistory> cmsHistoryRepository)
        {
            _cmsHistoryRepository = cmsHistoryRepository;
        }


        public void CmsVersionCreated(string entityName, long entityId, long identityId, int cmsVersion)
        {
            AddHistory(entityName, entityId, identityId, cmsVersion, "Create");
        }

        public void CmsVersionUpdated(string entityName, long entityId, long identityId, int cmsVersion)
        {
            AddHistory(entityName, entityId, identityId, cmsVersion, "Update");
        }

        public void CmsVersionCanceled(string entityName, long entityId, long identityId, int cmsVersion)
        {
            AddHistory(entityName, entityId, identityId, cmsVersion, "Cancel");
        }

        public void CmsVersionSendToApproval(string entityName, long entityId, long identityId, int cmsVersion)
        {
            AddHistory(entityName, entityId, identityId, cmsVersion, "SendToApproval");
        }

        public void CmsVersionPublish(string entityName, long entityId, long identityId, int cmsVersion)
        {
            AddHistory(entityName, entityId, identityId, cmsVersion, "Publish");
        }

        public void CmsVersionUndo(string entityName, long entityId, long identityId, int cmsVersion)
        {
            AddHistory(entityName, entityId, identityId, cmsVersion, "UndoVersion");
        }

        private void AddHistory(string entityName, long entityId, long identityId, int cmsVersion, string operation)
        {
            _cmsHistoryRepository
                    .Insert(new CmsHistory
                    {
                        EntityName = entityName,
                        EntityId = entityId,
                        IdentityId = identityId,
                        CmsVersion = cmsVersion,
                        Date = DateTime.UtcNow,
                        Operation = operation,
                    }, identityId);
        }
    }
}
