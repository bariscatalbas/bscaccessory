﻿using Core.Cache;
using Core.Data.Interfaces;
using Core.Ioc;
using Modules.Cms.Domains;
using Modules.Cms.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Modules.Cms.Services
{
    public class CmsService : IInjectAsSelf
    {
        private readonly IRepository<CmsVersion> _cmsVersionRepository;
        private readonly IRepository<CmsVersionContent> _cmsVersionContentRepository;

        public CmsService(IRepository<CmsVersionContent> cmsVersionContentRepository, IRepository<CmsVersion> cmsVersionRepository)
        {
            _cmsVersionRepository = cmsVersionRepository;
            _cmsVersionContentRepository = cmsVersionContentRepository;
        }

        public List<CmsContentModel> GetContents(string entityName, long entityId, int cmsVersion)
        {
            var versionId = _cmsVersionRepository.Table.Where(p => p.EntityId == entityId && p.EntityName == entityName && p.Version == cmsVersion).Select(p => p.Id).FirstOrDefault();

            var allContents = _cmsVersionContentRepository
                                                     .Table
                                                     .Where(p => p.CmsVersionId == versionId)
                                                     .OrderBy(p => p.DisplayOrder)
                                                     .Select(p => new CmsContentModel
                                                     {
                                                         Id = p.Id,
                                                         ParentId = p.ParentId,
                                                         CmsContentTypeCode = p.CmsContentType.Code,
                                                         CmsContentTypeGroupCode = p.CmsContentType.CmsContentTypeGroup.Code,
                                                         DataSystemName = p.CmsContentType.DataSystemName,
                                                         DataJson = p.Data,
                                                     }).ToList();

            foreach (var content in allContents)
            {
                var type = Type.GetType(content.DataSystemName);
                content.Data = JsonConvert.DeserializeObject(content.DataJson, type);
            }

            return OrderContents(null, allContents);
        }

        private List<CmsContentModel> OrderContents(CmsContentModel parent, List<CmsContentModel> allContents)
        {
            var parentId = parent?.Id;

            var children = allContents.Where(p => p.ParentId == parentId).ToList();
            foreach (var child in children)
            {
                child.Parent = parent;
                child.Children = OrderContents(child, allContents);
            }

            return children;
        }


    }
}
