﻿using Core.Data.Interfaces;
using Core.Mvc.Base;
using Core.Pagination;
using Modules.Cms.AdminModels;
using Modules.Cms.Domains;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Modules.Cms.Services
{
    public class CmsContentTypeGroupService : DataServiceBase<CmsContentTypeGroup, CmsContentTypeGroupModelsCreate, CmsContentTypeGroupModelsEdit, CmsContentTypeGroupModelsView, CmsContentTypeGroupModelsFilter, CmsContentTypeGroupModelsExcelExport>
    {
        public CmsContentTypeGroupService(IRepository<CmsContentTypeGroup> repository) : base(repository)
        {
        }

        public override AcPager<CmsContentTypeGroupModelsView> Index(CmsContentTypeGroupModelsFilter filter)
        {
            return Repository.Table.Where(p => !p.Deleted)
                    .Select(p => new CmsContentTypeGroupModelsView
                    {
                        Id = p.Id,
                        DisplayOrder = p.DisplayOrder,
                        Code = p.Code,
                        Name = p.Name
                    })
                    .OrderBy(p => p.Id)
                    .ToAcPagerSorted(filter);
        }

        public override AcPager<CmsContentTypeGroupModelsExcelExport> ExportIndex(CmsContentTypeGroupModelsFilter filter)
        {
            return Repository.Table.Where(p => !p.Deleted)
                    .Select(p => new CmsContentTypeGroupModelsExcelExport
                    {
                        Id = p.Id,
                        DisplayOrder = p.DisplayOrder,
                        Code = p.Code,
                        Name = p.Name
                    })
                    .OrderBy(p => p.Id)
                    .ToAcPagerSorted(filter);
        }

        public List<SelectListItem> SelectList()
        {
            return Repository
                        .Table
                        .Where(p => !p.Deleted)
                        .Select(p => new SelectListItem
                        {
                            Text = p.Name,
                            Value = p.Id.ToString()
                        }).ToList();
        }
    }
}
