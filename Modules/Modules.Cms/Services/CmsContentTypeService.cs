﻿using Core.Data.Interfaces;
using Core.Ioc;
using Core.Mvc.Base;
using Core.Pagination;
using Modules.Cms.AdminModels;
using Modules.Cms.Domains;
using Modules.Cms.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modules.Cms.Services
{
    public class CmsContentTypeService : DataServiceBase<CmsContentType, CmsContentTypeModelsCreate, CmsContentTypeModelsEdit, CmsContentTypeModelsView, CmsContentTypeModelsFilter, CmsContentTypeModelsExcelExport>
    {
        private readonly IRepository<CmsContentTypeGroup> _cmsContentTypeGroupRepository;
        public CmsContentTypeService(IRepository<CmsContentType> repository, IRepository<CmsContentTypeGroup> cmsContentTypeGroupRepository) : base(repository)
        {
            _cmsContentTypeGroupRepository = cmsContentTypeGroupRepository;
        }

        public override AcPager<CmsContentTypeModelsView> Index(CmsContentTypeModelsFilter filter)
        {
            return Repository
                        .Table
                        .Where(p => !p.Deleted && !p.CmsContentTypeGroup.Deleted)
                        .Select(p => new CmsContentTypeModelsView
                        {
                            CmsContentTypeGroupId = p.CmsContentTypeGroupId,
                            Code = p.Code,
                            ChildrenKey = p.ChildrenKey,
                            DataSystemName = p.DataSystemName,
                            GroupName = p.CmsContentTypeGroup.Name,
                            Id = p.Id,
                            Name = p.Name,
                            ParentKey = p.ParentKey
                        })
                        .OrderBy(p => p.GroupName)
                        .ThenBy(p => p.Code)
                        .ToAcPagerSorted(filter);
        }

        public override AcPager<CmsContentTypeModelsExcelExport> ExportIndex(CmsContentTypeModelsFilter filter)
        {
            return Repository
                        .Table
                        .Where(p => !p.Deleted && !p.CmsContentTypeGroup.Deleted)
                        .Select(p => new CmsContentTypeModelsExcelExport
                        {
                            CmsContentTypeGroupId = p.CmsContentTypeGroupId,
                            Code = p.Code,
                            ChildrenKey = p.ChildrenKey,
                            DataSystemName = p.DataSystemName,
                            GroupName = p.CmsContentTypeGroup.Name,
                            Id = p.Id,
                            Name = p.Name,
                            ParentKey = p.ParentKey
                        })
                        .OrderBy(p => p.GroupName)
                        .ThenBy(p => p.Code)
                        .ToAcPagerSorted(filter);
        }

        public void LoadAllFromAssembly(long identityId)
        {
            var list = new List<CmsContentTypeModelsView>();

            var types = AcEngine.FindClassesOfType<CmsDataModelBase>();
            foreach (var type in types)
            {
                var model = Activator.CreateInstance(type) as CmsDataModelBase;
                if (model != null)
                {
                    list.Add(new CmsContentTypeModelsView
                    {
                        Code = model.CmsBaseCode,
                        Name = model.CmsBaseName,
                        AllowedEntities = model.CmsBaseAllowedEntities,
                        ChildrenKey = model.CmsBaseChildrenKey,
                        ParentKey = model.CmsBaseParentKey,
                        DataSystemName = type.AssemblyQualifiedName,
                        GroupCode = model.CmsBaseGroupCode
                    });
                }
            }

            var cmsContentTypes = Repository.Table.Where(p => !p.Deleted).ToList();
            var cmsContentTypeGroups = _cmsContentTypeGroupRepository.Table.Where(p => !p.Deleted).ToList();

            foreach (var contentType in cmsContentTypes)
            {
                var listItem = list.FirstOrDefault(p => p.Code == contentType.Code);
                if (listItem == null)
                {
                    Repository.Delete(contentType, identityId);
                }
                else
                {
                    listItem.Id = contentType.Id;
                    contentType.Code = listItem.Code;
                    contentType.Name = listItem.Name;
                    contentType.AllowedEntities = listItem.AllowedEntities;
                    contentType.ChildrenKey = listItem.ChildrenKey;
                    contentType.ParentKey = listItem.ParentKey;
                    contentType.DataSystemName = listItem.DataSystemName;
                    var contentTypeGroup = cmsContentTypeGroups.Where(p => p.Code == listItem.GroupCode).FirstOrDefault();
                    if (contentTypeGroup == null)
                    {
                        contentTypeGroup = new CmsContentTypeGroup
                        {
                            AllowedEntities = "",
                            Code = listItem.Code,
                            Name = listItem.Code,
                            DisplayOrder = 0
                        };
                        _cmsContentTypeGroupRepository.Insert(contentTypeGroup, identityId);
                    }
                    contentType.CmsContentTypeGroupId = contentTypeGroup.Id;
                    Repository.Update(contentType, identityId);
                }
            }

            foreach (var newContentType in list.Where(p => p.Id == 0))
            {
                CmsContentType contentType = new CmsContentType
                {
                    Code = newContentType.Code,
                    Name = newContentType.Name,
                    AllowedEntities = newContentType.AllowedEntities,
                    ChildrenKey = newContentType.ChildrenKey,
                    ParentKey = newContentType.ParentKey,
                    DataSystemName = newContentType.DataSystemName
                };

                var contentTypeGroup = cmsContentTypeGroups.Where(p => p.Code == newContentType.GroupCode).FirstOrDefault();
                if (contentTypeGroup == null)
                {
                    contentTypeGroup = new CmsContentTypeGroup
                    {
                        AllowedEntities = "",
                        Code = newContentType.GroupCode,
                        Name = newContentType.GroupCode,
                        DisplayOrder = 0
                    };
                    _cmsContentTypeGroupRepository.Insert(contentTypeGroup, identityId);
                }
                contentType.CmsContentTypeGroupId = contentTypeGroup.Id;
                Repository.Insert(contentType, identityId);
            }
        }
    }
}
