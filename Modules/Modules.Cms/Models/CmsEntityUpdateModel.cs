﻿using Core.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modules.Cms.Models
{
    public class CmsEntityUpdateModel : EntityBase
    {
        public virtual long? LockedById { get; set; }

        public virtual bool Published { get; set; }
        public virtual bool PendingApproval { get; set; }

        public virtual int ActiveVersion { get; set; }
        public virtual int DesignedVersion { get; set; }
        public virtual int LastVersion { get; set; }
    }
}
