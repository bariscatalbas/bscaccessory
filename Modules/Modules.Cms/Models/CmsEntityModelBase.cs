﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modules.Cms.Models
{
    public class CmsEntityModelBase
    {
        public long EntityId { get; set; }
        public string EntityName { get; set; }
        public int CmsVersion { get; set; }
    }
}
