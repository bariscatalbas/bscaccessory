﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modules.Cms.Models
{
    public class CmsContentModel
    {
        public CmsContentModel()
        {
            Children = new List<CmsContentModel>();
        }

        public CmsContentModel Parent { get; set; }

        public long Id { get; set; }
        public long? ParentId { get; set; }
        public string CmsContentTypeCode { get; set; }
        public string CmsContentTypeGroupCode { get; set; }
        public string DataSystemName { get; set; }
        public string DataJson { get; set; }
        public object Data { get; set; }

        public List<CmsContentModel> Children { get; set; }
    }
}
