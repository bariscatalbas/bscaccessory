﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modules.Cms.Models
{
    public class CmsHistoryModel
    {
        public long AccountId { get; set; }
        public string EntityName { get; set; }
        public long EntityId { get; set; }
        public int CmsVersion { get; set; }
        public string Operation { get; set; }
        public DateTime Date { get; set; }
    }
}
