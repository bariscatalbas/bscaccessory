﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modules.Cms.Models
{
    public abstract class CmsDataModelBase
    {
        public CmsDataModelBase()
        {
            Show = true;
        }

        [UIHint("HiddenInput")]
        public long Id { get; set; }

        public bool Show { get; set; }
        public DateTime? ShowStartDate { get; set; }
        public DateTime? ShowEndDate { get; set; }


        [UIHint("HiddenInput")]
        public abstract string CmsBaseName { get; }
        [UIHint("HiddenInput")]
        public abstract string CmsBaseCode { get; }
        [UIHint("HiddenInput")]
        public abstract string CmsBaseAllowedEntities { get; }
        [UIHint("HiddenInput")]
        public abstract string CmsBaseParentKey { get; }
        [UIHint("HiddenInput")]
        public abstract string CmsBaseChildrenKey { get; }
        [UIHint("HiddenInput")]
        public abstract string CmsBaseGroupCode { get; }
    }
}
