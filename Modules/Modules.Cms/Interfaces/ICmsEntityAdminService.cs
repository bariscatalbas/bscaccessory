﻿using Modules.Cms.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modules.Cms.Interfaces
{
    public interface ICmsEntityAdminService
    {
        CmsEntityUpdateModel GetEntity(long entityId);
        void UpdateEntity(CmsEntityUpdateModel entity, long identityId);
        string GetEntityName();

    }
}
