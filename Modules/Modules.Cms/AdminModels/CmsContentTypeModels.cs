﻿using Core.Data.Models;
using Core.Mvc.Attributes;
using Core.Pagination;
using Core.Validation;
using Modules.Cms.Domains;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modules.Cms.AdminModels
{
    public class CmsContentTypeModelsCreate : EntityBase
    {
        [AcPreventOnExcelExport]
        [DisplayName("Cms Content Type Group")]
        [UIHint("DropdownLong")]
        [AcDropdownParameter("CmsContentTypeGroupList")]
        public long CmsContentTypeGroupId { get; set; }

        [DisplayName("Name")]
        [AcValidateRequired]
        [AcValidateMaxLength(50)]
        public string Name { get; set; }

        [DisplayName("Code")]
        [AcValidateRequired]
        [AcValidateMaxLength(50)]
        public string Code { get; set; }

        [DisplayName("Data System Name")]
        [AcValidateRequired]
        [AcValidateMaxLength(500)]
        public string DataSystemName { get; set; }

        [DisplayName("Allowed Entities")]
        [AcValidateMaxLength(500)]
        public string AllowedEntities { get; set; }

        [DisplayName("Parent Key")]
        [AcValidateRequired]
        [AcValidateMaxLength(50)]
        public string ParentKey { get; set; }

        [DisplayName("Child Key")]
        [AcValidateMaxLength(50)]
        public string ChildrenKey { get; set; }
    }

    public class CmsContentTypeModelsEdit : CmsContentTypeModelsCreate
    {

    }

    public class CmsContentTypeModelsView : CmsContentTypeModelsCreate
    {
        [DisplayName("Group Name")]
        public string GroupName { get; set; }
        
        [AcPreventOnExcelExport]
        public string GroupCode { get; set; }
    }

    public class CmsContentTypeModelsFilter : AcFilter
    {

    }

    public class CmsContentTypeModelsExcelExport : CmsContentTypeModelsView
    {

    }

}
