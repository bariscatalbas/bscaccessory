﻿using Core.Data.Models;
using Core.Pagination;
using Core.Validation;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modules.Cms.AdminModels
{
    public class CmsContentTypeGroupModelsCreate : EntityBase
    {
        [DisplayName("Name")]
        [AcValidateMaxLength(250)]
        [AcValidateRequired]
        public string Name { get; set; }

        [DisplayName("Code")]
        [AcValidateMaxLength(250)]
        [AcValidateRequired]
        public string Code { get; set; }

        [DisplayName("Allowed Entities")]
        [AcValidateMaxLength(500)]
        public string AllowedEntities { get; set; }

        [DisplayName("Display Order")]
        public int DisplayOrder { get; set; }
    }

    public class CmsContentTypeGroupModelsEdit : CmsContentTypeGroupModelsCreate
    {
    }

    public class CmsContentTypeGroupModelsView : CmsContentTypeGroupModelsCreate
    {
    }

    public class CmsContentTypeGroupModelsFilter : AcFilter
    {

    }

    public class CmsContentTypeGroupModelsExcelExport : CmsContentTypeGroupModelsView
    {

    }
}
