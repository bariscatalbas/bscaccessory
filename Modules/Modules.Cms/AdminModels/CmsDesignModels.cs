﻿using Core.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modules.Cms.AdminModels
{
    public class CmsDesignCloneModel
    {
        public long Id { get; set; }
        public long OldId { get; set; }
    }

    public class CmsDesignCmsVersionModel
    {
        public long EntityId { get; set; }
        public string EntityName { get; set; }
        public long CmsVersionId { get; set; }
        public int Version { get; set; }
    }

    public class CmsDesignContentModel
    {
        public CmsDesignContentModel()
        {
            Children = new List<CmsDesignContentModel>();
        }

        public long Id { get; set; }
        public long? ParentId { get; set; }

        public long CmsContentTypeId { get; set; }
        public string CmsContentTypeName { get; set; }
        public string CmsContentTypeCode { get; set; }
        public string CmsContentChildrenKey { get; set; }
        public string CmsContentTypeGroupCode { get; set; }

        public int DisplayOrder { get; set; }
        public string Data { get; set; }

        public List<CmsDesignContentModel> Children { get; set; }
    }


    public class CmsDesignContentTypeSelectModel : EntityBase
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public long GroupId { get; set; }
        public int GroupOrder { get; set; }
        public string GroupName { get; set; }
    }
}
