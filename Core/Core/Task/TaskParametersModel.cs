﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Task
{
    public class TaskParametersModel
    {
        public bool CanIStart { get; set; }
        public string SettingsJson { get; set; }

    }
}
