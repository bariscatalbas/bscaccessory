﻿using Core.Provider;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Task
{
    public interface ITaskProvider : IProvider
    {
        void Start(List<AcTaskModel> tasks);
        void Stop();
        void Run<T>(params object[] parameters) where T : AcAsyncRunBase;
    }
}
