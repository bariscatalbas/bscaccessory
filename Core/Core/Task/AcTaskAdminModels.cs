﻿using Core.Audit;
using Core.Data.Interfaces;
using Core.Data.Models;
using Core.Pagination;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Task
{
    public class AcTaskAdminModelsList : EntityBase
    {
        public string Code { get; set; }
        public bool Enabled { get; set; }
        public string Name { get; set; }
        public string RunAtWhere { get; set; }
        public string RunAtWhichApp { get; set; }
        public int Seconds { get; set; }
        public string SystemName { get; set; }
        public string SettingsSystemName { get; set; }
    }
    public class AcTaskAdminModelsDetails : EntityBase
    {
        public AcTaskAdminModelsDetails()
        {
            Histories = new List<AcTaskAdminModelsDetailsHistory>();
        }
        public bool Enabled { get; set; }
        public bool SkipIfLocked { get; set; }

        public string RunAtWhere { get; set; }
        public string RunAtWhichApp { get; set; }

        public string Code { get; set; }
        public string Name { get; set; }
        public string SystemName { get; set; }
        public string SettingsSystemName { get; set; }
        public string SettingsJson { get; set; }

        public int Seconds { get; set; }

        public List<AcTaskAdminModelsDetailsHistory> Histories { get; set; }
    }

    public class AcTaskAdminModelsDetailsHistory : EntityBase
    {
        public string RunAtWhere { get; set; }
        public string RunAtWhichApp { get; set; }

        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }

        public bool Error { get; set; }
        public DateTime? ErrorDate { get; set; }

        public string Details { get; set; }
    }

    public class AcTaskAdminModelsFilter : AcFilter
    {
    }
}
