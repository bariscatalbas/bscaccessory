﻿using Core.Data.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Task
{
    public class AcTaskHistory : EntityBase
    {
        public virtual long AcTaskId { get; set; }
        [ForeignKey("AcTaskId")]
        public virtual AcTask AcTask { get; set; }

        public virtual string RunAtWhere { get; set; }
        public virtual string RunAtWhichApp { get; set; }

        public virtual DateTime StartDate { get; set; }
        public virtual DateTime? EndDate { get; set; }

        public virtual bool Error { get; set; }
        public virtual DateTime? ErrorDate { get; set; }

        public virtual string Details { get; set; }
    }
}
