﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Task
{
    public class AcTaskModel
    {
        public string SystemName { get; set; }
        public int Seconds { get; set; }
        public bool SkipIfLocked { get; set; }
    }
}
