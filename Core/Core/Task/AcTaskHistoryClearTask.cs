﻿using Core.Data.Interfaces;
using Core.Ioc;
using Core.Provider;
using Core.Task;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace Core.Audit
{
    public class AcTaskHistoryClearTask : AcTaskBase
    {
        private readonly IRepository<AcTaskHistory> _taskHistoryRepository;
        public AcTaskHistoryClearTask()
        {
            _taskHistoryRepository = AcEngine.Resolve<IRepository<AcTaskHistory>>();
        }

        public override string Code => "AcTaskHistoryClearTask";

        public override string Name => "Ac Task History Clear Task";

        public override string SettingsSystemName { get { return typeof(AcTaskHistoryClearTaskSettings).AssemblyQualifiedName; } }

        private AcTaskHistoryClearTaskSettings Settings { get; set; }

        public override bool CanIStartLocal(TaskPrepareModel task, out string logThisAtHistory)
        {
            logThisAtHistory = "";
            Settings = JsonConvert.DeserializeObject<AcTaskHistoryClearTaskSettings>(task.SettingsJson);

            if (!string.IsNullOrWhiteSpace(Settings.RunningHours))
            {
                List<int> runningHours = Settings.RunningHours.Split(',').Select(p => _.To.Int(p.Trim(), -1)).ToList();

                if (runningHours.Contains(DateTime.UtcNow.Hour))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

            return true;
        }

        public override bool Execute(TaskPrepareModel task, out string logThisAtHistory)
        {
            logThisAtHistory = "";
            var utcNow = DateTime.UtcNow;
            var deleteBeforeUtc = utcNow.AddMonths(-1 * Settings.MonthsToKeepTaskHistory);

            var taskHistories = _taskHistoryRepository.Table.Where(p => p.StartDate < deleteBeforeUtc).ToList();

            foreach(var item in taskHistories)
            {
                _taskHistoryRepository.Delete(item, 0);
            }

            return true;
        }

        public class AcTaskHistoryClearTaskSettings
        {
            [DisplayName("Running Hours (In UTC, comma seperated)")]
            public string RunningHours { get; set; }

            [DisplayName("How many months to keep task history?")]
            public int MonthsToKeepTaskHistory { get; set; }
        }
    }
}
