﻿using Core.Data.Models;
using Core.Ioc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Task
{
    public abstract class AcAsyncRunBase : IInjectAsSelf
    {
        public abstract void Run(params object[] parameters);
    }
}
