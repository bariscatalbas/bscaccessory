﻿using Core.Cache;
using Core.Data.Interfaces;
using Core.Data.Services;
using Core.Ioc;
using Core.Pagination;
using Core.Provider;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using System.Web.Mvc;

namespace Core.Task
{
    public class AcTaskHelper : IInjectAsSelf
    {
        private readonly IRepository<AcTask> _taskRepository;
        private readonly IRepository<AcTaskHistory> _taskHistoryRepository;

        public AcTaskHelper(IRepository<AcTask> taskRepository, IRepository<AcTaskHistory> taskHistoryRepository)
        {
            _taskRepository = taskRepository;
            _taskHistoryRepository = taskHistoryRepository;
        }

        private ITaskProvider GetProvider(string code = "")
        {
            if (string.IsNullOrWhiteSpace(code))
            {
                code = AcProviderHelper.GetCoreSettings().TaskProviderCode;
            }

            return AcProviderHelper.GetProvider<ITaskProvider>("task", code);
        }

        public void Start()
        {
            var tasks = _taskRepository
                            .Table
                            .Select(p => new AcTaskModel
                            {
                                Seconds = p.Seconds,
                                SystemName = p.SystemName,
                                SkipIfLocked = p.SkipIfLocked
                            }).ToList();

            var provider = GetProvider();
            provider.Start(tasks);
        }
        public void Run<T>(params object[] parameters) where T : AcAsyncRunBase
        {
            var provider = GetProvider();
            provider.Run<T>(parameters);
        }

        public void Stop()
        {
            var provider = GetProvider();
            provider.Stop();
        }

        public TaskPrepareModel PrepareMe(AcTaskBase task)
        {
            return _taskRepository
                            .Table
                            .Where(p => p.Code == task.Code)
                            .Select(p => new TaskPrepareModel
                            {
                                Id = p.Id,
                                Enabled = p.Enabled,
                                SkipIfLocked = p.SkipIfLocked,
                                RunAtWhere = p.RunAtWhere,
                                RunAtWhichApp = p.RunAtWhichApp,
                                SettingsJson = p.SettingsJson,
                            }).FirstOrDefault();
        }

        public long IAmStarting(TaskPrepareModel task)
        {
            var taskHistory = new AcTaskHistory
            {
                AcTaskId = task.Id,
                StartDate = DateTime.UtcNow,
                RunAtWhere = _.Configuration.WhereAmI,
                RunAtWhichApp = _.Configuration.WhichAppAmI,
            };
            _taskHistoryRepository.Insert(taskHistory, 0);
            return taskHistory.Id;
        }

        public void IFinished(long historyId, bool success, string logThisAtHistory)
        {
            var history = _taskHistoryRepository.Table.FirstOrDefault(p => p.Id == historyId);
            if (history != null)
            {
                if (success)
                {
                    history.EndDate = DateTime.UtcNow;
                }
                else
                {
                    history.Error = true;
                    history.ErrorDate = DateTime.UtcNow;
                }
                history.Details = logThisAtHistory;

                _taskHistoryRepository.Update(history, 0);
            }
        }

        public void ICouldntStart(TaskPrepareModel task, string logThisAtHistory)
        {
            _taskHistoryRepository.Insert(new AcTaskHistory
            {
                AcTaskId = task.Id,
                StartDate = DateTime.UtcNow,
                ErrorDate = DateTime.UtcNow,
                Error = true,
                Details = logThisAtHistory,
                RunAtWhere = _.Configuration.WhereAmI,
                RunAtWhichApp = _.Configuration.WhichAppAmI,
            }, 0);
        }

        #region Admin Functions
        public void LoadAllFromAssembly(long identityId)
        {
            var list = new List<AcTaskAdminModelsList>();

            var types = AcEngine.FindClassesOfType<AcTaskBase>();
            foreach (var type in types)
            {
                var model = Activator.CreateInstance(type) as AcTaskBase;
                if (model != null)
                {
                    list.Add(new AcTaskAdminModelsList
                    {
                        Code = model.Code,
                        Name = model.Name,
                        SystemName = type.AssemblyQualifiedName,
                        SettingsSystemName = model.SettingsSystemName
                    });
                }
            }

            var repository = AcEngine.Resolve<IRepository<AcTask>>();
            var tasks = repository.Table.Select(p => p).ToList();
            foreach (var task in tasks)
            {
                var listItem = list.FirstOrDefault(p => p.Code == task.Code);
                if (listItem == null)
                {
                    repository.Delete(task, identityId);
                }
                else
                {
                    listItem.Id = task.Id;
                }
            }

            foreach (var newTask in list.Where(p => p.Id == 0))
            {
                repository.Insert(new AcTask
                {
                    Enabled = false,
                    RunAtWhere = "",
                    RunAtWhichApp = "",
                    Seconds = 600,
                    SkipIfLocked = false,
                    SettingsJson = "",
                    Code = newTask.Code,
                    Name = newTask.Name,
                    SettingsSystemName = newTask.SettingsSystemName,
                    SystemName = newTask.SystemName
                }, identityId);
            }
        }

        public object GetSettings(long taskId, out string systemName)
        {
            systemName = "";
            var task = _taskRepository.Table.Where(p => p.Id == taskId).FirstOrDefault();

            if (task != null)
            {
                var type = Type.GetType(task.SettingsSystemName);
                if (type != null)
                {
                    systemName = task.SettingsSystemName;
                    if (!string.IsNullOrWhiteSpace(task.SettingsJson))
                    {
                        return JsonConvert.DeserializeObject(task.SettingsJson, type);
                    }
                    else
                    {
                        var model = Activator.CreateInstance(type);
                        return model;
                    }
                }
            }
            return null;
        }

        public bool SetSettings(long taskId, object model, long identityId)
        {
            var task = _taskRepository.Table.Where(p => p.Id == taskId).FirstOrDefault();

            if (task != null)
            {
                if (model == null)
                {
                    task.SettingsJson = "";
                }
                else
                {
                    task.SettingsJson = JsonConvert.SerializeObject(model);
                }

                _taskRepository.Update(task, identityId);
                return true;
            }
            return false;
        }

        public AcPager<AcTaskAdminModelsList> GetTasks(AcTaskAdminModelsFilter filter)
        {
            return _taskRepository
                            .Table
                            .Select(p => new AcTaskAdminModelsList
                            {
                                Id = p.Id,
                                Code = p.Code,
                                Enabled = p.Enabled,
                                Name = p.Name,
                                RunAtWhere = p.RunAtWhere,
                                RunAtWhichApp = p.RunAtWhichApp,
                                Seconds = p.Seconds,
                                SystemName = p.SystemName,
                                SettingsSystemName = p.SettingsSystemName
                            })
                            .ToAcPager(filter);
        }

        public AcTaskAdminModelsDetails GetTaskDetails(long taskId)
        {
            return _taskRepository
                            .Table
                            .Where(p => p.Id == taskId)
                            .Select(p => new AcTaskAdminModelsDetails
                            {
                                Id = p.Id,
                                Enabled = p.Enabled,
                                Name = p.Name,
                                RunAtWhere = p.RunAtWhere,
                                RunAtWhichApp = p.RunAtWhichApp,
                                Seconds = p.Seconds,
                                SystemName = p.SystemName,
                                Code = p.Code,
                                SkipIfLocked = p.SkipIfLocked,
                                SettingsJson = p.SettingsJson,
                                SettingsSystemName = p.SettingsSystemName,
                                Histories = p.Histories.Select(q => new AcTaskAdminModelsDetailsHistory
                                {
                                    Id = q.Id,
                                    RunAtWhere = q.RunAtWhere,
                                    RunAtWhichApp = q.RunAtWhichApp,
                                    EndDate = q.EndDate,
                                    Error = q.Error,
                                    ErrorDate = q.ErrorDate,
                                    StartDate = q.StartDate,
                                    Details = q.Details
                                }).ToList()
                            })
                            .FirstOrDefault();
        }

        public bool EnableTask(long taskId)
        {
            var task = _taskRepository.Table.Where(p => p.Id == taskId).FirstOrDefault();
            if(task != null)
            {
                task.Enabled = true;
                _taskRepository.Update(task, 0);

                return true;
            }

            return false;
        }

        public bool DisableTask(long taskId)
        {
            var task = _taskRepository.Table.Where(p => p.Id == taskId).FirstOrDefault();
            if (task != null)
            {
                task.Enabled = false;
                _taskRepository.Update(task, 0);

                return true;
            }

            return false;
        }

        #endregion

    }
}
