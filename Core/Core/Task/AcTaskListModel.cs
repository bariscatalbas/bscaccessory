﻿using Core.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Task
{
    public class AcTaskListModel : EntityBase
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string SystemName { get; set; }
        public string SettingsSystemName { get; set; }
    }
}
