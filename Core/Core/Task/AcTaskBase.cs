﻿using Core.Ioc;
using System;
using System.Linq;

namespace Core.Task
{
    public abstract class AcTaskBase : IInjectAsSelf
    {
        public abstract string Code { get; }
        public abstract string Name { get; }
        public abstract string SettingsSystemName { get; }


        private readonly AcTaskHelper _acTaskHelper;
        public AcTaskBase()
        {
            _acTaskHelper = AcEngine.Resolve<AcTaskHelper>();
        }

        public void Run()
        {
            var task = _acTaskHelper.PrepareMe(this);
            if (task.Enabled)
            {
                var whichAppAmI = (_.Configuration.WhichAppAmI ?? "").ToLowerInvariant();
                var whereAmI = (_.Configuration.WhereAmI ?? "").ToLowerInvariant();

                var selectedWhere = (task.RunAtWhere ?? "").Split(',').Where(p => !string.IsNullOrWhiteSpace(p)).Select(p => p.Trim().ToLowerInvariant()).ToList();
                var selectedWhich = (task.RunAtWhichApp ?? "").Split(',').Where(p => !string.IsNullOrWhiteSpace(p)).Select(p => p.Trim().ToLowerInvariant()).ToList();

                if (!string.IsNullOrWhiteSpace(whichAppAmI) && !string.IsNullOrWhiteSpace(whereAmI) && (!selectedWhich.Any() || selectedWhich.Contains(whichAppAmI)) && (!selectedWhere.Any() || selectedWhere.Contains(whereAmI)))
                {
                    long historyId = 0;

                    try
                    {
                        string logThisAtHistory;
                        if (CanIStartLocal(task, out logThisAtHistory))
                        {
                            historyId = _acTaskHelper.IAmStarting(task);
                            var success = Execute(task, out logThisAtHistory);
                            _acTaskHelper.IFinished(historyId, success, logThisAtHistory);
                        }
                        else if (!string.IsNullOrWhiteSpace(logThisAtHistory))
                        {
                            _acTaskHelper.ICouldntStart(task, logThisAtHistory);
                        }
                    }
                    catch (Exception ex)
                    {
                        _acTaskHelper.IFinished(historyId, false, _.Web.OpenException(ex));
                    }
                }
            }
        }

        public abstract bool CanIStartLocal(TaskPrepareModel task, out string logThisAtHistory);
        public abstract bool Execute(TaskPrepareModel task, out string logThisAtHistory);


    }
}
