﻿using Core.Data.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Task
{
    public class AcTask : EntityBase
    {
        public virtual bool Enabled { get; set; }
        public virtual bool SkipIfLocked { get; set; }

        public virtual string RunAtWhere { get; set; }
        public virtual string RunAtWhichApp { get; set; }

        [MaxLength(50)]
        public virtual string Code { get; set; }
        [MaxLength(50)]
        public virtual string Name { get; set; }
        [MaxLength(500)]
        public virtual string SystemName { get; set; }
        [MaxLength(500)]
        public virtual string SettingsSystemName { get; set; }
        public virtual string SettingsJson { get; set; }

        public virtual int Seconds { get; set; }

        public virtual IList<AcTaskHistory> Histories { get; set; }
    }
}
