﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Task
{
    public class TaskPrepareModel
    {
        public long Id { get; set; }
        public bool Enabled { get; set; }
        public bool SkipIfLocked { get; set; }
        public string RunAtWhere { get; set; }
        public string RunAtWhichApp { get; set; }
        public string SettingsJson { get; set; }
    }
}
