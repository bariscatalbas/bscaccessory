﻿using Core.Cache;
using Core.Data.Interfaces;
using Core.Ioc;
using Core.UrlRecord;
using System;
using System.ComponentModel;
using System.Linq;

namespace Core.Resources
{
    public class AcResourceHelper : IInjectAsSelf
    {
        private readonly IRepository<AcResource> _resourceRepository;
        private readonly AcUrlRecordHelper _urlRecordHelper;

        public AcResourceHelper(IRepository<AcResource> resourceRepository, AcUrlRecordHelper urlRecordHelper)
        {
            _resourceRepository = resourceRepository;
            _urlRecordHelper = urlRecordHelper;
        }

        public TResource LoadResource<TResource>(long containerId) where TResource : class, IResource
        {
            var resource = Activator.CreateInstance<TResource>();
            var keys = typeof(TResource).GetProperties()
                                        .Where(p => p.CanRead && p.CanWrite)
                                        .Select(p => (typeof(TResource).Name + "." + p.Name).ToLowerInvariant())
                                        .ToList();

            var foundResources = _resourceRepository
                                .Table
                                .Where(p => p.ContainerId == containerId && keys.Contains(p.Name))
                                .ToList();

            foreach (var prop in typeof(TResource).GetProperties())
            {
                if (!prop.CanRead || !prop.CanWrite || prop.Name == "ContainerId")
                    continue;

                var key = (typeof(TResource).Name + "." + prop.Name).ToLowerInvariant();
                var resourceObj = foundResources.FirstOrDefault(p => p.Name == key.ToLowerInvariant());
                if (resourceObj == null)
                    continue;

                if (!TypeDescriptor.GetConverter(prop.PropertyType).CanConvertFrom(typeof(string)))
                    continue;

                if (!TypeDescriptor.GetConverter(prop.PropertyType).IsValid(resourceObj.Value))
                    continue;

                var value = TypeDescriptor.GetConverter(prop.PropertyType).ConvertFromInvariantString(resourceObj.Value);
                prop.SetValue(resource, value, null);
            }
            resource.ContainerId = containerId;
            return resource;
        }

        public void SaveResource<TResource>(TResource resource, long identityId) where TResource : class, IResource
        {
            foreach (var prop in typeof(TResource).GetProperties())
            {
                if (!prop.CanRead || !prop.CanWrite || prop.Name == "ContainerId")
                    continue;

                if (!TypeDescriptor.GetConverter(prop.PropertyType).CanConvertFrom(typeof(string)))
                    continue;

                var key = typeof(TResource).Name + "." + prop.Name;
                dynamic value = prop.GetValue(resource, null) ?? "";
                var valueStr = ConvertToString(value);

                var entityName = "";
                object[] attrs = prop.GetCustomAttributes(true);
                foreach (object attr in attrs)
                {
                    AcUrlRecordAttribute urlRecordAttribute = attr as AcUrlRecordAttribute;
                    if (urlRecordAttribute != null)
                    {
                        entityName = urlRecordAttribute.EntityName;
                    }
                }
                if (!string.IsNullOrWhiteSpace(entityName))
                {
                    valueStr = _urlRecordHelper.SetUrl(resource.ContainerId, entityName, valueStr, identityId);
                }

                Set(key, valueStr, resource.ContainerId, identityId);
            }
        }

        public TResource LoadResourceCached<TResource>(long containerId) where TResource : class, IResource
        {
            var cacheHelper = AcEngine.Resolve<AcCacheHelper>();
            var cacheKey = typeof(TResource).Name.ToLowerInvariant() + "_" + containerId;
            return cacheHelper.GetVersioned<TResource>(cacheKey, () => { return LoadResource<TResource>(containerId); });
        }

        public void SaveResourceCached<TResource>(TResource resource, long identityId) where TResource : class, IResource
        {
            SaveResource<TResource>(resource, identityId);

            var cacheHelper = AcEngine.Resolve<AcCacheHelper>();
            var cacheKey = typeof(TResource).Name.ToLowerInvariant() + "_" + resource.ContainerId;
            cacheHelper.VersionUpdated(cacheKey, identityId);
        }

        private string ConvertToString<T>(T value)
        {
            return TypeDescriptor.GetConverter(typeof(T)).ConvertToInvariantString(value);
        }

        private void Set(string key, string value, long containerId, long identityId)
        {
            if (key == null) return;

            key = key.Trim().ToLowerInvariant();

            var resource = _resourceRepository.Table.FirstOrDefault(p => p.Name == key && p.ContainerId == containerId);
            if (resource != null)
            {
                resource.Value = value;
                _resourceRepository.Update(resource, identityId);
            }
            else
            {
                resource = new AcResource(containerId, key, value);
                _resourceRepository.Insert(resource, identityId);
            }
        }
    }
}
