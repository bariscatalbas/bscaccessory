﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Resources
{
     public interface IResource
    {
        long ContainerId { get; set; }
    }
}
