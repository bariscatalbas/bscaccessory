﻿using Core.Data.Models;
using System.ComponentModel.DataAnnotations;

namespace Core.Resources
{
    public class AcResource : EntityBase
    {
        public AcResource()
        {
        }

        public AcResource(long containerId, string name, string value)
        {
            ContainerId = containerId;
            Name = name;
            Value = value;
        }

        public virtual long ContainerId { get; set; }

        [MaxLength(500)]
        public virtual string Name { get; set; }
        public virtual string Value { get; set; }

        public override string ToString()
        {
            return Name;
        }

    }
}
