﻿using System.Collections.Generic;

namespace Core.Pagination
{
    public interface IPager
    {
        int PageSize { get; }
        int PageNumber { get; }
        int PageCount { get; }
        int TotalItemCount { get; }
        bool HasPreviousPage { get; }
        bool HasNextPage { get; }
        bool IsFirstPage { get; }
        bool IsLastPage { get; }
        int FirstItemOnPage { get; }
        int LastItemOnPage { get; }
        int PreviousPageNumber { get; }
        int NextPageNumber { get; }
        List<int> ShowedPages { get; }
    }
}
