﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Core.Pagination
{
    public class AcFilter
    {
        public AcFilter()
        {
            PageNumber = 1;
            PageSize = 50;
            SortPropertyOrderByDescending = true;
        }

        [UIHint("HiddenInput")]
        public int PageNumber { get; set; }

        [UIHint("PageSize")]
        [DisplayName("Sayfa Boyutu")]
        [UIHint("HiddenInput")]
        public int PageSize { get; set; }

        [UIHint("HiddenInput")]
        public bool Export { get; set; }

        [UIHint("HiddenInput")]
        public string SelectedExportProperties { get; set; }

        [UIHint("HiddenInput")]
        public string SortPropertyName { get; set; }

        [UIHint("HiddenInput")]
        public bool SortPropertyOrderByDescending { get; set; }
    }
}
