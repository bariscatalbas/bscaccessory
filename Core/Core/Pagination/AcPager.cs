﻿using Core.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Core.Pagination
{
    public class AcPager<T> : List<T>, IPager where T : EntityBase
    {
        public int PageSize { get; }
        public int PageNumber { get; }
        public int PageCount { get; }
        public int TotalItemCount { get; }

        public bool HasPreviousPage { get; }
        public bool HasNextPage { get; }
        public bool IsFirstPage { get; }
        public bool IsLastPage { get; }

        public int FirstItemOnPage { get; }
        public int LastItemOnPage { get; }

        public int PreviousPageNumber => PageNumber - 1;
        public int NextPageNumber => PageNumber + 1;

        public List<int> ShowedPages
        {
            get
            {
                var list = new List<int>();
                if (PageCount > 0)
                {
                    if (PageNumber != 1)
                    {
                        list.Add(1);
                    }
                    if (PageNumber > 2)
                    {
                        for (var i = PageNumber - 5; i < PageNumber; i++)
                        {
                            if (i > 1)
                                list.Add(i);
                        }
                    }
                    list.Add(PageNumber);
                    if (PageNumber < PageCount - 1)
                    {
                        for (var i = PageNumber + 1; i <= PageNumber + 5; i++)
                        {
                            if (i < PageCount)
                                list.Add(i);
                        }
                    }
                    if (PageCount > 1 && PageNumber < PageCount)
                    {
                        list.Add(PageCount);
                    }
                }
                return list;
            }
        }

        public AcPager(IQueryable<T> list, int pageNumber, int pageSize, bool sortByDefault = true, string sortPropertyName = "", bool sortPropertyNameOrderByDesc = false)
        {
            if (list != null)
            {
                TotalItemCount = list?.Count() ?? 0;
                PageSize = pageSize <= 0 ? TotalItemCount : pageSize;
                PageNumber = pageSize <= 0 ? 1 : pageNumber;
                PageCount = TotalItemCount > 0 ? (int)Math.Ceiling(TotalItemCount / (double)PageSize) : 0;

                HasPreviousPage = PageNumber > 1;
                HasNextPage = PageNumber < PageCount;
                IsFirstPage = PageNumber == 1;
                IsLastPage = PageNumber >= PageCount;
                FirstItemOnPage = (PageNumber - 1) * PageSize + 1;
                var numberOfLastItemOnPage = FirstItemOnPage + PageSize - 1;
                LastItemOnPage = numberOfLastItemOnPage > TotalItemCount ? TotalItemCount : numberOfLastItemOnPage;

                if (!string.IsNullOrWhiteSpace(sortPropertyName))
                {
                    var param = Expression.Parameter(typeof(T), "p");
                    var prop = Expression.Property(param, sortPropertyName);
                    var exp = Expression.Lambda(prop, param);
                    string method = sortPropertyNameOrderByDesc ? "OrderByDescending" : "OrderBy";
                    Type[] types = new Type[] { list.ElementType, exp.Body.Type };
                    var mce = Expression.Call(typeof(Queryable), method, types, list.Expression, exp);
                    list = list.Provider.CreateQuery<T>(mce);
                }
                else if (sortByDefault)
                {
                    list = list.OrderByDescending(p => p.Id);
                }

                AddRange(list.Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList());
            }
        }

        public AcPager(IEnumerable<T> list, int pageNumber, int pageSize, int totalItemCount)
        {
            TotalItemCount = totalItemCount;
            PageSize = pageSize <= 0 ? TotalItemCount : pageSize;
            PageNumber = pageSize <= 0 ? 1 : pageNumber;
            PageCount = TotalItemCount > 0 ? (int)Math.Ceiling(TotalItemCount / (double)PageSize) : 0;

            HasPreviousPage = PageNumber > 1;
            HasNextPage = PageNumber < PageCount;
            IsFirstPage = PageNumber == 1;
            IsLastPage = PageNumber >= PageCount;
            FirstItemOnPage = (PageNumber - 1) * PageSize + 1;
            var numberOfLastItemOnPage = FirstItemOnPage + PageSize - 1;
            LastItemOnPage = numberOfLastItemOnPage > TotalItemCount ? TotalItemCount : numberOfLastItemOnPage;

            AddRange(list);
        }
    }
}
