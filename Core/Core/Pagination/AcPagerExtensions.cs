﻿using Core.Data.Models;
using Omu.ValueInjecter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Core.Pagination
{
    public static class AcPagerExtensions
    {
        public static AcPager<TK> ToPager<T, TK>(this IQueryable<T> query, AcFilter filter)
            where T : EntityBase
            where TK : EntityBase, new()
        {
            var itemCount = query.Count();

            if (!string.IsNullOrWhiteSpace(filter.SortPropertyName))
            {
                var param = Expression.Parameter(typeof(T), "p");
                var prop = Expression.Property(param, filter.SortPropertyName);
                var exp = Expression.Lambda(prop, param);
                string method = filter.SortPropertyOrderByDescending ? "OrderByDescending" : "OrderBy";
                Type[] types = new Type[] { query.ElementType, exp.Body.Type };
                var mce = Expression.Call(typeof(Queryable), method, types, query.Expression, exp);
                query = query.Provider.CreateQuery<T>(mce);
            }
            else
            {
                query = query.OrderByDescending(p => p.Id);
            }

            var list = query.Skip((filter.PageNumber - 1) * filter.PageSize).Take((int)filter.PageSize).ToList();

            var otherList = new List<TK>();
            foreach (var item in list)
            {
                var otherItem = new TK();
                otherItem.InjectFrom(item);
                otherList.Add(otherItem);
            }

            return new AcPager<TK>(otherList, filter.PageNumber, (int)filter.PageSize, itemCount);
        }

        public static AcPager<T> ToAcPager<T>(this IQueryable<T> query, AcFilter filter) where T : EntityBase
        {
            return new AcPager<T>(query, filter.PageNumber, filter.PageSize, true, filter.SortPropertyName, filter.SortPropertyOrderByDescending);
        }

        public static AcPager<T> ToAcPagerSorted<T>(this IQueryable<T> query, AcFilter filter) where T : EntityBase
        {
            return new AcPager<T>(query, filter.PageNumber, filter.PageSize, false, filter.SortPropertyName, filter.SortPropertyOrderByDescending);
        }
    }
}
