﻿using Core.Data.Models;
using Core.Pagination;
using System;
using System.ComponentModel;

namespace Core.Audit
{
    public class AcAuditLogAdminModelsDetails : EntityBase
    {
        public string WhereAmI { get; set; }
        public string WhichAppAmI { get; set; }

        public DateTime DateUtc { get; set; }
        public long IdentityId { get; set; }
        public long ParentEntityId { get; set; }
        public long EntityId { get; set; }
        public string EntityName { get; set; }
        public string ParentEntityName { get; set; }
        public string Operation { get; set; }
        
        public string Details { get; set; }

        public string Url { get; set; }
        public string ReferrerUrl { get; set; }
        public string Browser { get; set; }
        public string UserAgent { get; set; }
        public string HttpMethod { get; set; }
        public string UserHostAddress { get; set; }
        public string UserHostName { get; set; }
    }
    public class AcAuditLogAdminModelsList : EntityBase
    {
        public DateTime DateUtc { get; set; }
        public string Operation { get; set; }

        public long EntityId { get; set; }
        public string EntityName { get; set; }
        public long ParentEntityId { get; set; }
        public string ParentEntityName { get; set; }
    }

    public class AcAuditLogAdminModelsFilter : AcFilter
    {
        [DisplayName("Start Date")]
        public DateTime? StartDate { get; set; }

        [DisplayName("End Date")]
        public DateTime? EndDate { get; set; }

        [DisplayName("Entity Name")]
        public string EntityName { get; set; }
    }
}
