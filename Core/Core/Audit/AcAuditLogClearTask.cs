﻿using Core.Provider;
using Core.Task;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace Core.Audit
{
    public class AcAuditLogClearTask : AcTaskBase
    {
        public AcAuditLogClearTask()
        {
        }

        public override string Code => "AcAuditLogClearTask";

        public override string Name => "Ac Audit Log Clear Task";

        public override string SettingsSystemName { get { return typeof(AcAuditLogClearTaskSettings).AssemblyQualifiedName; } }

        private AcAuditLogClearTaskSettings Settings { get; set; }

        public override bool CanIStartLocal(TaskPrepareModel task, out string logThisAtHistory)
        {
            logThisAtHistory = "";
            Settings = JsonConvert.DeserializeObject<AcAuditLogClearTaskSettings>(task.SettingsJson);

            if (!string.IsNullOrWhiteSpace(Settings.RunningHours))
            {
                List<int> runningHours = Settings.RunningHours.Split(',').Select(p => _.To.Int(p.Trim(), -1)).ToList();

                if (runningHours.Contains(DateTime.UtcNow.Hour))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

            return true;
        }

        public override bool Execute(TaskPrepareModel task, out string logThisAtHistory)
        {
            logThisAtHistory = "";
            var utcNow = DateTime.UtcNow;
            var deleteBeforeUtc = utcNow.AddMonths(-1 * Settings.MonthsToKeepAuditLogs);

            var code = AcProviderHelper.GetCoreSettings().AuditProviderCode;
            var provider = AcProviderHelper.GetProvider<IAuditProvider>("audit", code);
            provider.ClearOldLogs(deleteBeforeUtc);

            return true;
        }

        public class AcAuditLogClearTaskSettings
        {
            [DisplayName("Running Hours (In UTC, comma seperated)")]
            public string RunningHours { get; set; }

            [DisplayName("How many months to keep audit logs?")]
            public int MonthsToKeepAuditLogs { get; set; }
        }
    }
}
