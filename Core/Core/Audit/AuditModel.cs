﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Audit
{
    public class AuditModel
    {
        public long ParentEntityId { get; set; }
        public long EntityId { get; set; }
        public string EntityName { get; set; }
        public string ParentEntityName { get; set; }
        public object Details { get; set; }
    }
}
