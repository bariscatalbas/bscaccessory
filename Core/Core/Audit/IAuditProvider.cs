﻿using Core.Pagination;
using Core.Provider;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Audit
{
    public interface IAuditProvider : IProvider
    {
        void LogInsert(AuditModel model, long identityId);
        void LogUpdate(AuditModel model, long identityId);
        void LogDelete(AuditModel model, long identityId);

        void ClearOldLogs(DateTime deleteBeforeUtc);
        AcPager<AcAuditLogAdminModelsList> GetAuditLogs(AcAuditLogAdminModelsFilter filter);
        AcAuditLogAdminModelsDetails GetAuditLogDetails(long id);
    }
}
