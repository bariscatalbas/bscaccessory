﻿using Core.Data.Interfaces;
using Core.Mvc.Base;
using Core.Pagination;
using Core.SmsTemplates;
using System.Linq;

namespace Core.SmsTemplates
{
    public class AcSmsTemplateAdminService : DataServiceBase<AcSmsTemplate, AcSmsTemplateAdminModelsCreate, AcSmsTemplateAdminModelsEdit, AcSmsTemplateAdminModelsView, AcSmsTemplateAdminModelsFilter, AcSmsTemplateAdminModelsExcelExport>
    {
        public AcSmsTemplateAdminService(IRepository<AcSmsTemplate> repository) : base(repository)
        {
        }

        public override AcPager<AcSmsTemplateAdminModelsView> Index(AcSmsTemplateAdminModelsFilter filter)
        {
            return Repository
                        .Table
                        .Where(p => p.ContainerId == filter.ContainerId)
                        .Select(p => new AcSmsTemplateAdminModelsView
                        {
                            Id = p.Id,
                            Message = p.Message,
                            ContainerId = p.ContainerId,
                            SmsProviderCode = p.SmsProviderCode,
                            IsActive = p.IsActive,
                            Type = p.Type
                        }).ToAcPager(filter);
        }

        public override AcPager<AcSmsTemplateAdminModelsExcelExport> ExportIndex(AcSmsTemplateAdminModelsFilter filter)
        {
            return Repository
                        .Table
                        .Where(p => p.ContainerId == filter.ContainerId)
                        .Select(p => new AcSmsTemplateAdminModelsExcelExport
                        {
                            Id = p.Id,
                            Message = p.Message,
                            ContainerId = p.ContainerId,
                            SmsProviderCode = p.SmsProviderCode,
                            IsActive = p.IsActive,
                            Type = p.Type
                        }).ToAcPager(filter);
        }

    }
}
