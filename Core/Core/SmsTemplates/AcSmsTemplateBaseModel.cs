﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.SmsTemplates
{
    public abstract class AcSmsTemplateBaseModel
    {
        public string To { get; set; }
        public string Website { get; set; }
        public string AdminSite { get; set; }
    }
}
