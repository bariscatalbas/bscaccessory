﻿using Core.Data.Models;
using Core.Mvc.Attributes;
using Core.Pagination;
using Core.Validation;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Core.SmsTemplates
{
    public class AcSmsTemplateAdminModelsCreate : EntityBase
    {
        [AcValidateRequired]
        [AcDropdownParameter("SmsProviderCodeList")]
        [UIHint("Dropdown")]
        public string SmsProviderCode { get; set; }

        [UIHint("HiddenInput")]
        public long ContainerId { get; set; }

        [AcValidateRequired]
        [AcValidateMaxLength(50)]
        [AcDropdownParameter("TypeList")]
        [UIHint("Dropdown")]
        public string Type { get; set; }

        [AllowHtml]
        [UIHint("TextArea")]
        public string Message { get; set; }

        public bool IsActive { get; set; }
    }

    public class AcSmsTemplateAdminModelsEdit : AcSmsTemplateAdminModelsCreate
    {

    }

    public class AcSmsTemplateAdminModelsView : AcSmsTemplateAdminModelsCreate
    {

    }

    public class AcSmsTemplateAdminModelsFilter : AcFilter
    {
        [UIHint("HiddenInput")]
        public long ContainerId { get; set; }
    }
    public class AcSmsTemplateAdminModelsExcelExport : AcSmsTemplateAdminModelsView
    {

    }
}
