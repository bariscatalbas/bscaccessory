﻿using Core.Data.Interfaces;
using Core.Email;
using Core.Ioc;
using Core.Sms;
using Omu.ValueInjecter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.SmsTemplates
{
    public class AcSmsTemplateHelper : IInjectAsSelf
    {
        private readonly IRepository<AcSmsTemplate> _acSmsTemplateRepository;

        public AcSmsTemplateHelper(IRepository<AcSmsTemplate> acSmsTemplateRepository)
        {
            _acSmsTemplateRepository = acSmsTemplateRepository;
        }


        public QueuedSmsModel FillTemplateWithModel<T>(T model, long containerId, string templateType, string masterTemplateType) where T : AcSmsTemplateBaseModel
        {
            var template = _acSmsTemplateRepository.Table.FirstOrDefault(p => p.Type == templateType && p.IsActive && p.ContainerId == containerId);
            if (template != null)
            {
                var result = new QueuedSmsModel
                {
                    AcSmsProviderCode = template.SmsProviderCode,
                    To = model.To
                };

                var content = FillTemplateWithModel(template.Message, model);

                if (!string.IsNullOrWhiteSpace(masterTemplateType))
                {
                    var masterTemplate = _acSmsTemplateRepository.Table.FirstOrDefault(p => p.Type == masterTemplateType && p.IsActive && p.ContainerId == containerId);
                    if (masterTemplate != null)
                    {
                        var masterModel = new AcSmsTemplateMasterModel();
                        masterModel.InjectFrom(model);
                        masterModel.Content = content;

                        content = FillTemplateWithModel(masterTemplate.Message, masterModel);
                    }
                }

                result.Message = content;

                return result;
            }

            return null;
        }

        private string FillTemplateWithModel(string template, object model)
        {
            var result = template ?? "";

            if (model != null)
            {
                var properties = model.GetType().GetProperties();
                foreach (var propertyInfo in properties)
                {
                    result = result.Replace("{" + propertyInfo.Name + "}", propertyInfo.GetValue(model) + "");
                }
            }

            return result;
        }

    }
}
