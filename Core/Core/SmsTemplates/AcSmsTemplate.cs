﻿using Core.Audit;
using Core.Data.Interfaces;
using Core.Data.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.SmsTemplates
{
    public class AcSmsTemplate : EntityBase, IAuditable
    {
        [MaxLength(50)]
        public virtual string SmsProviderCode { get; set; }

        public virtual long ContainerId { get; set; }

        [MaxLength(50)]
        public virtual string Type { get; set; }

        public virtual string Message { get; set; }

        public virtual bool IsActive { get; set; }

        public AuditModel SelectLog(object repository)
        {
            var model = new
            {
                this.Id,
                this.IsActive,
                this.Message,
                this.Type,
                this.ContainerId,
                this.SmsProviderCode
            };

            return new AuditModel
            {
                EntityId = this.Id,
                EntityName = "SmsTemplate",
                ParentEntityId = this.ContainerId,
                ParentEntityName = "SmsTemplateContainer",
                Details = JsonConvert.SerializeObject(model)
            };
        }
    }
}
