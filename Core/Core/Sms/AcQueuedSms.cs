﻿using Core.Data.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Sms
{
    public class AcQueuedSms : EntityBase
    {
        [MaxLength(50)]
        public virtual string AcSmsProviderCode { get; set; }
        public virtual DateTime CreationDate { get; set; }

        public virtual bool Sent { get; set; }
        public virtual DateTime? SendDate { get; set; }

        public virtual bool Error { get; set; }
        public virtual DateTime? ErrorDate { get; set; }

        [MaxLength(500)]
        public virtual string To { get; set; }
        public virtual string Message { get; set; }

        public virtual string ErrorText { get; set; }
        public virtual string SuccessText { get; set; }

    }
}
