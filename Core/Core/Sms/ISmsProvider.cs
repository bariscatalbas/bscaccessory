﻿using Core.Provider;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Sms
{
    public interface ISmsProvider : IProvider
    {
        string SendSms(QueuedSmsModel sms);
    }
}
