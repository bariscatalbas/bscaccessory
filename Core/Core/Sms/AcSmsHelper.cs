﻿using Core.Data.Interfaces;
using Core.Ioc;
using Core.Pagination;
using Core.Provider;
using Core.Sms;
using Omu.ValueInjecter;
using System;
using System.Linq;

namespace Core.Sms
{
    public class AcSmsHelper : IInjectAsSelf
    {
        private readonly IRepository<AcQueuedSms> _queuedSmsRepository;

        public AcSmsHelper(IRepository<AcQueuedSms> queuedSmsRepository)
        {
            _queuedSmsRepository = queuedSmsRepository;
        }

        private ISmsProvider GetProvider(string code = "")
        {
            if (string.IsNullOrWhiteSpace(code))
            {
                code = AcProviderHelper.GetCoreSettings().SmsProviderCode;
            }

            return AcProviderHelper.GetProvider<ISmsProvider>("sms", code);
        }

        public long AddSmsToQueue(QueuedSmsModel sms, long identityId)
        {
            var queuedSms = new AcQueuedSms();
            queuedSms.InjectFrom(sms);
            queuedSms.CreationDate = DateTime.UtcNow;
            _queuedSmsRepository.Insert(queuedSms, identityId);
            return queuedSms.Id;
        }

        public bool SendSms(long id)
        {
            var sms = _queuedSmsRepository.Table.FirstOrDefault(p => p.Id == id);
            if (sms != null)
            {
                var exceptionText = "";
                var model = new QueuedSmsModel();
                model.InjectFrom(sms);

                var provider = GetProvider(model.AcSmsProviderCode);
                if (provider != null)
                {
                    try
                    {
                        var result = provider.SendSms(model);

                        sms.Sent = true;
                        sms.SendDate = DateTime.UtcNow;
                        sms.SuccessText = result;
                        _queuedSmsRepository.Update(sms, 0);
                        return true;
                    }
                    catch (Exception ex)
                    {
                        exceptionText = _.Web.OpenException(ex);
                    }
                }
                else
                {
                    exceptionText = "Sms Provider couldn't found: " + model.AcSmsProviderCode;
                }

                sms.Error = true;
                sms.ErrorText = exceptionText;
                sms.ErrorDate = DateTime.UtcNow;
                _queuedSmsRepository.Update(sms, 0);

                return false;
            }
            return false;
        }

        #region Admin Functions
        public AcPager<AcQueuedSmsAdminModelsList> GetQueuedSms(AcQueuedSmsAdminModelsFilter filter)
        {
            var query = _queuedSmsRepository.Table;

            if (filter.StartDate.HasValue)
            {
                query = query.Where(p => p.CreationDate >= filter.StartDate.Value);
            }
            if (filter.EndDate.HasValue)
            {
                query = query.Where(p => p.CreationDate >= filter.EndDate.Value);
            }
            if (!string.IsNullOrWhiteSpace(filter.To))
            {
                query = query.Where(p => p.To.Contains(filter.To));
            }
            if (filter.Error.HasValue)
            {
                query = query.Where(p => p.Error == true);
            }

            return query
                .Select(p => new AcQueuedSmsAdminModelsList
                {
                    Id = p.Id,
                    To = p.To,
                    CreationDate = p.CreationDate,
                    ErrorText = p.ErrorText,
                    Sent = p.Sent
                }).ToAcPager(filter);
        }

        public AcQueuedSmsAdminModelsDetails GetQueuedSmsDetails(long queuedSmsId)
        {
            return _queuedSmsRepository.Table.Where(p => p.Id == queuedSmsId)
                        .Select(p => new AcQueuedSmsAdminModelsDetails
                        {
                            Id = p.Id,
                            AcSmsProviderCode = p.AcSmsProviderCode,
                            CreationDate = p.CreationDate,
                            Sent = p.Sent,
                            SendDate = p.SendDate,
                            Error = p.Error,
                            ErrorDate = p.ErrorDate,
                            To = p.To,
                            Message = p.Message,
                            ErrorText = p.ErrorText
                        }).FirstOrDefault();
        }

        public bool SendQueuedSmsAgain(long queuedSmsId, long identityId)
        {
            var queuedSms = _queuedSmsRepository.Table.Where(p => p.Id == queuedSmsId).FirstOrDefault();

            if (queuedSms != null)
            {
                queuedSms.Sent = false;
                queuedSms.SendDate = null;
                queuedSms.Error = false;
                queuedSms.ErrorDate = null;
                queuedSms.ErrorText = "";

                _queuedSmsRepository.Update(queuedSms, identityId);

                return true;
            }

            return false;
        }

        #endregion

    }
}
