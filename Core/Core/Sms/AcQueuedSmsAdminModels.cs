﻿using Core.Data.Models;
using Core.Pagination;
using System;
using System.ComponentModel;

namespace Core.Sms
{
    public class AcQueuedSmsAdminModelsList : EntityBase
    {
        public string To { get; set; }
        public DateTime CreationDate { get; set; }
        public bool Sent { get; set; }
        public string ErrorText { get; set; }
    }

    public class AcQueuedSmsAdminModelsDetails : EntityBase
    {
        public string AcSmsProviderCode { get; set; }
        public DateTime CreationDate { get; set; }

        public bool Sent { get; set; }
        public DateTime? SendDate { get; set; }

        public bool Error { get; set; }
        public DateTime? ErrorDate { get; set; }

        public string To { get; set; }
        public string Message { get; set; }

        public string ErrorText { get; set; }

    }

    public class AcQueuedSmsAdminModelsFilter : AcFilter
    {
        [DisplayName("Start Date")]
        public DateTime? StartDate { get; set; }
        [DisplayName("End Date")]
        public DateTime? EndDate { get; set; }
        [DisplayName("To")]
        public string To { get; set; }
        [DisplayName("Is Error?")]
        public bool? Error { get; set; }
    }
}
