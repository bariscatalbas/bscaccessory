﻿using Core.Data.Interfaces;
using Core.Ioc;
using Core.Task;
using Newtonsoft.Json;
using System.Linq;

namespace Core.Sms
{
    public class AcSmsTask : AcTaskBase
    {
        private readonly IRepository<AcQueuedSms> _queuedSmsRepository;
        private readonly AcSmsHelper _smsHelper;

        public AcSmsTask()
        {
            _queuedSmsRepository = AcEngine.Resolve<IRepository<AcQueuedSms>>();
            _smsHelper = AcEngine.Resolve<AcSmsHelper>();
        }

        public override string Code => "AcSmsTask";

        public override string Name => "Ac Sms Task";

        public override string SettingsSystemName { get { return typeof(AcSmsTaskSettings).AssemblyQualifiedName; } }

        private AcSmsTaskSettings Settings { get; set; }

        public override bool CanIStartLocal(TaskPrepareModel task, out string logThisAtHistory)
        {
            logThisAtHistory = "";
            Settings = JsonConvert.DeserializeObject<AcSmsTaskSettings>(task.SettingsJson);
            return true;
        }

        public override bool Execute(TaskPrepareModel task, out string logThisAtHistory)
        {
            logThisAtHistory = "";
            var smsIds = _queuedSmsRepository.Table.Where(p => !p.Sent && !p.Error).Select(p => p.Id).ToList();

            foreach (var smsId in smsIds)
            {
                _smsHelper.SendSms(smsId);
            }

            return true;
        }
    }

    public class AcSmsTaskSettings
    {
    }
}
