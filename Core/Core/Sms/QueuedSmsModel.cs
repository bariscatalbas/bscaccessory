﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Sms
{
    public class QueuedSmsModel
    {
        public string AcSmsProviderCode { get; set; }
        public string To { get; set; }
        public string Message { get; set; }
    }
}
