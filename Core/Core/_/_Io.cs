﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

public partial class _
{
    public static class Io
    {
        public static void CheckAndCreateFolder(string folderPath)
        {
            var folderFullPath = HttpContext.Current.Server.MapPath(folderPath);
            DirectoryInfo dir = new DirectoryInfo(folderFullPath);
            if (!dir.Exists)
            {
                dir.Create();
            }
        }

        public static string FindFileNameToUpload(string folder, string fileName)
        {
            var fileNameWithoutExtension = fileName.Substring(0, fileName.LastIndexOf('.'));
            fileNameWithoutExtension = _.String.StripNonAlphaNumeric(fileNameWithoutExtension).Replace(" ", "");

            var fileNameExtension = fileName.Substring(fileName.LastIndexOf('.') + 1);
            var index = 0;

            while (true)
            {
                var newFileName = fileNameWithoutExtension + (index == 0 ? "" : "_" + index + "") + "." + fileNameExtension;
                var path = folder + "/" + newFileName;
                var fullPath = HttpContext.Current.Server.MapPath(path);

                if (!File.Exists(fullPath))
                {
                    return newFileName;
                }
                index++;
            }
        }

        public static string AppendTextToFileName(string fileName, string appendText, string prependText = "")
        {
            if (string.IsNullOrWhiteSpace(fileName))
            {
                return "";
            }

            var lastIndexOfTre = fileName.LastIndexOf('/');
            var lastIndexOfDot = fileName.LastIndexOf('.');
            if (lastIndexOfTre > 0 && !string.IsNullOrWhiteSpace(prependText))
            {
                var folder = lastIndexOfTre > 0 ? fileName.Substring(0, fileName.LastIndexOf('/')) : "";
                var fileNameWithoutExtension = fileName.Substring(lastIndexOfTre + 1, lastIndexOfDot - lastIndexOfTre - 1);
                var fileNameExtension = fileName.Substring(fileName.LastIndexOf('.') + 1);

                return (!string.IsNullOrWhiteSpace(folder) ? folder + "/" + prependText : "")
                    + fileNameWithoutExtension + appendText + "." + fileNameExtension;
            }
            else
            {
                var fileNameWithoutExtension = fileName.Substring(0, lastIndexOfDot);
                var fileNameExtension = fileName.Substring(fileName.LastIndexOf('.') + 1);
                return fileNameWithoutExtension + appendText + "." + fileNameExtension;
            }
        }
    }
}
