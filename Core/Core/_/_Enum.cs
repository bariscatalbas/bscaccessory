﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

public partial class _
{
    public static class Enum
    {
        public static string Description<T>(T item) where T : struct, IConvertible
        {
            var type = typeof(T);
            var uType = Nullable.GetUnderlyingType(type);
            if (uType != null)
            {
                type = uType;
            }
            if (type.IsEnum)
            {
                var fieldInfo = typeof(T).GetField(item.ToString());
                var attribute = fieldInfo.GetCustomAttributes(typeof(DescriptionAttribute), true).FirstOrDefault();
                return attribute == null ? _.String.MakeReadable(item.ToString()) : ((DescriptionAttribute)attribute).Description;
            }

            return string.Empty;
        }

        public static string Description(Type type, object item)
        {
            var uType = Nullable.GetUnderlyingType(type);
            if (uType != null)
            {
                type = uType;
            }
            if (type.IsEnum)
            {
                var fieldInfo = type.GetField(item.ToString());
                var attribute = fieldInfo.GetCustomAttributes(typeof(DescriptionAttribute), true).FirstOrDefault();
                return attribute == null ? _.String.MakeReadable(item.ToString()) : ((DescriptionAttribute)attribute).Description;
            }

            return string.Empty;
        }


        public static List<SelectListItem> SelectList(Type type, object item)
        {
            var uType = Nullable.GetUnderlyingType(type);
            if (uType != null)
            {
                type = uType;
            }
            if (type.IsEnum)
            {
                return System.Enum.GetValues(type).Cast<object>()
                .Select(v => new SelectListItem
                {
                    Selected = v.Equals(item),
                    Text = Description(type, v),
                    Value = v.ToString()
                }).ToList();
            }

            return null;


        }

        public static List<SelectListItem> SelectList<T>() where T : struct, IConvertible
        {

            var type = typeof(T);
            var uType = Nullable.GetUnderlyingType(type);
            if (uType != null)
            {
                type = uType;
            }
            if (type.IsEnum)
            {
                return System.Enum.GetValues(typeof(T)).Cast<object>()
                .Select(v => new SelectListItem
                {
                    Text = Description(typeof(T), v),
                    Value = ((int)v).ToString(CultureInfo.InvariantCulture)
                }).ToList();
            }

            return null;
        }
    }
}
