﻿using System;
using System.Globalization;

public partial class _
{
    public static class To
    {
        public static DateTime? Date(string value)
        {
            if (!string.IsNullOrWhiteSpace(value))
            {
                DateTime date;
                if (DateTime.TryParse(value, out date))
                {
                    return date;
                }
            }
            return null;
        }

        public static Guid Guidd(string value)
        {
            if (!string.IsNullOrWhiteSpace(value))
            {
                Guid result;
                if (Guid.TryParse(value, out result))
                {
                    return result;
                }
            }
            return Guid.Empty;
        }

        public static long Long(string value, long defaultValue = 0)
        {
            if (!string.IsNullOrWhiteSpace(value))
            {
                long result;
                if (long.TryParse(value, out result))
                {
                    return result;
                }
            }
            return defaultValue;
        }

        public static long? LongNullable(string value)
        {
            if (!string.IsNullOrWhiteSpace(value))
            {
                long result;
                if (long.TryParse(value, out result))
                {
                    return result;
                }
            }
            return (long?)null;
        }

        public static int Int(string value, int defaultValue = 0)
        {
            if (!string.IsNullOrWhiteSpace(value))
            {
                int result;
                if (int.TryParse(value, out result))
                {
                    return result;
                }
            }
            return defaultValue;
        }

        public static Decimal Decimal(object parseValue, Decimal defaultValue = 0)
        {
            Decimal returnValue;
            try
            {
                if (!System.Decimal.TryParse(parseValue.ToString(), out returnValue))
                {
                    returnValue = defaultValue;
                }
            }
            catch
            {
                returnValue = defaultValue;
            }
            return returnValue;
        }
    }
}
