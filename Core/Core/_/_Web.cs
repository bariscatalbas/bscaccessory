﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

public partial class _
{
    public static class Web
    {
        public static string OpenException(Exception exception)
        {
            var properties = exception.GetType().GetProperties();
            var fields = properties
                             .Select(property => new
                             {
                                 Name = property.Name,
                                 Value = property.GetValue(exception, null)
                             })
                             .Select(x => string.Format(
                                 "{0} = {1}",
                                 x.Name,
                                 x.Value != null ? x.Value.ToString() : ""
                             ));
            var result = string.Join("\n", fields);

            if (exception.InnerException != null)
            {
                result += "\n\n--------INNER EXCEPTION---------\n\n" + OpenException(exception.InnerException);
            }

            return result;
        }

        public static string ApplicationUrl()
        {
            var httpHost = HttpContext.Current.Request.ServerVariables["HTTP_HOST"];
            var urlScheme = HttpContext.Current.Request.Url.Scheme;
            return urlScheme + "://" + httpHost;
        }

        public static MvcHtmlString AddQueryString(string url, string parameter, string value)
        {
            var httpHost = HttpContext.Current.Request.ServerVariables["HTTP_HOST"];
            var urlScheme = HttpContext.Current.Request.Url.Scheme;

            if (!url.StartsWith("http") && !url.StartsWith(httpHost))
            {
                url = urlScheme + "://" + httpHost + url;
            }
            var uriBuilder = new UriBuilder(url);
            var query = HttpUtility.ParseQueryString(uriBuilder.Query);
            query[parameter] = value;
            uriBuilder.Query = query.ToString();
            return new MvcHtmlString(uriBuilder.ToString());
        }

        public static string RemovePagingQueryString(Uri uri, params string[] keys)
        {
            return RemoveQueryStringByKey(uri, "pageIndex");
        }
        public static string RemoveQueryStringByKey(string url, params string[] keys)
        {
            var uri = new Uri(url);
            return RemoveQueryStringByKey(uri, keys);
        }
        public static string RemoveQueryStringByKey(Uri uri, params string[] keys)
        {
            var newQueryString = HttpUtility.ParseQueryString(uri.Query);
            foreach (var key in keys)
            {
                newQueryString.Remove(key);
            }
            string pagePathWithoutQueryString = uri.GetLeftPart(UriPartial.Path);
            return newQueryString.Count > 0 ? string.Format("{0}?{1}", pagePathWithoutQueryString, newQueryString) : pagePathWithoutQueryString;
        }

        public static string CaptchaImage(string captchaString, bool noisy = true)
        {
            int i, x, y;
            var rand = new Random((int)DateTime.Now.Ticks);
            var captcha = captchaString;
            string img = "";
            using (var mem = new MemoryStream())
            using (var bmp = new Bitmap(90, 40))
            using (var gfx = Graphics.FromImage((Image)bmp))
            {
                gfx.TextRenderingHint = TextRenderingHint.ClearTypeGridFit;
                gfx.SmoothingMode = SmoothingMode.AntiAlias;

                gfx.FillRectangle(new SolidBrush(Color.FromArgb(255, (byte)130, (byte)223, (byte)180)), new Rectangle(0, 0, bmp.Width, bmp.Height));
                if (noisy)
                {
                    var pen = new Pen(Color.Black);
                    pen.Color = Color.FromArgb(255, (byte)8, (byte)154, (byte)87);
                    for (i = 1; i <= 30; i++)
                    {
                        x = rand.Next(0, 90);
                        y = rand.Next(0, 40);

                        gfx.DrawRectangle(pen, x, y, 1, 1);
                    }
                }

                var fontName = new DirectoryInfo(string.Format("{0}AdminTheme\\subset-RidleyGrotesk-Black.ttf", HttpContext.Current.Server.MapPath(@"\"))).ToString();


                PrivateFontCollection pfc = new PrivateFontCollection();
                pfc.AddFontFile(fontName);

                var color = new SolidBrush(Color.FromArgb(255, (byte)8, (byte)154, (byte)87));

                var count = 0;
                var randomizer = new Random();

                foreach (var item in captcha.ToCharArray())
                {
                    gfx.ResetTransform();
                    gfx.RotateTransform(randomizer.Next(-8, 8));
                    gfx.DrawString(item.ToString(), new Font(pfc.Families[0], 16, FontStyle.Regular), color, 0 + count * randomizer.Next(13, 15), 0);
                    count++;
                }

                bmp.Save(mem, System.Drawing.Imaging.ImageFormat.Png);
                img = Convert.ToBase64String(mem.GetBuffer());
            }

            return img;
        }

        public static string CreatePost(Dictionary<string, string> formElements, string url, string formName = "postform", string method = "POST")
        {
            var sb = new StringBuilder();
            sb.AppendLine("<html><head>");
            sb.AppendLine(string.Format("</head><body onload=\"document.{0}.submit()\">", formName));
            sb.AppendLine(string.Format("<form name=\"{0}\" method=\"{1}\" action=\"{2}\" >", formName, method, url));
            foreach (var item in formElements)
            {
                sb.AppendLine(string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">", HttpUtility.HtmlEncode(item.Key), HttpUtility.HtmlEncode(item.Value)));
            }
            sb.AppendLine("</form>");
            sb.AppendLine("</body></html>");
            return sb.ToString();
        }

        public static void PostHtml(string html)
        {
            HttpContext context = HttpContext.Current;
            context.Response.Clear();
            context.Response.Write(html);
            context.Response.End();
        }

        public static Dictionary<string, string> FormToDictionary()
        {
            var dictionary = new Dictionary<string, string>();

            foreach (var formElementKey in HttpContext.Current.Request.Form.AllKeys)
            {
                dictionary[formElementKey] = HttpContext.Current.Request.Form[formElementKey];
            }

            return dictionary;
        }

        public static string IpAddress()
        {
            var result = "";
            if (HttpContext.Current != null && HttpContext.Current.Handler != null && HttpContext.Current.Request != null && HttpContext.Current.Request.Headers != null)
            {
                //The X-Forwarded-For (XFF) HTTP header field is a de facto standard
                //for identifying the originating IP address of a client
                //connecting to a web server through an HTTP proxy or load balancer.
                var forwardedHttpHeader = "X-FORWARDED-FOR";
                if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["ForwardedHTTPheader"]))
                {
                    //but in some cases server use other HTTP header
                    //in these cases an administrator can specify a custom Forwarded HTTP header
                    //e.g. CF-Connecting-IP, X-FORWARDED-PROTO, etc
                    forwardedHttpHeader = ConfigurationManager.AppSettings["ForwardedHTTPheader"];
                }

                //it's used for identifying the originating IP address of a client connecting to a web server
                //through an HTTP proxy or load balancer. 
                string xff = HttpContext.Current.Request.Headers.AllKeys
                    .Where(x => forwardedHttpHeader.Equals(x, StringComparison.InvariantCultureIgnoreCase))
                    .Select(k => HttpContext.Current.Request.Headers[k])
                    .FirstOrDefault();

                //if you want to exclude private IP addresses, then see http://stackoverflow.com/questions/2577496/how-can-i-get-the-clients-ip-address-in-asp-net-mvc
                if (!string.IsNullOrEmpty(xff))
                {
                    string lastIp = xff.Split(new[] { ',' }).FirstOrDefault();
                    result = lastIp;
                }
            }

            try
            {
                if (string.IsNullOrEmpty(result) && HttpContext.Current.Request.UserHostAddress != null)
                    result = HttpContext.Current.Request.UserHostAddress;
            }
            catch (Exception)
            {
                // Do nothing, IP address cannot be retrieved.
            }

            //some validation
            if (result == "::1")
                result = "127.0.0.1";
            //remove port
            if (!string.IsNullOrEmpty(result))
            {
                int index = result.IndexOf(":", StringComparison.InvariantCultureIgnoreCase);
                if (index > 0)
                    result = result.Substring(0, index);
            }
            return result;
        }

        public static string GetHeaderValue(string key)
        {
            if (HttpContext.Current != null && HttpContext.Current.Handler != null && HttpContext.Current.Request != null && HttpContext.Current.Request.Headers != null)
            {
                return HttpContext.Current.Request.Headers
                                   .AllKeys
                                   .Where(x => key.Equals(x, StringComparison.InvariantCultureIgnoreCase))
                                   .Select(k => HttpContext.Current.Request.Headers[k])
                                   .FirstOrDefault();
            }
            return "";
        }

        public static bool SetHeaderValue(string key, string value)
        {
            if (HttpContext.Current != null && HttpContext.Current.Handler != null && HttpContext.Current.Response != null && HttpContext.Current.Response.Headers != null)
            {
                var existingKeys = HttpContext.Current.Response.Headers.AllKeys.Where(x => key.Equals(x, StringComparison.InvariantCultureIgnoreCase)).ToList();
                foreach (var existingKey in existingKeys)
                {
                    HttpContext.Current.Response.Headers.Remove(existingKey);
                }

                HttpContext.Current.Response.Headers.Add(key, value);
            }
            return false;
        }

    }
}
