﻿using System;
using System.Collections.ObjectModel;

public partial class _
{
    public static class Date
    {
        public static ReadOnlyCollection<TimeZoneInfo> GetSystemTimeZones()
        {
            return TimeZoneInfo.GetSystemTimeZones();
        }

        public static TimeZoneInfo FindTimeZoneById(string id)
        {
            return TimeZoneInfo.FindSystemTimeZoneById(id);
        }

        public static TimeZoneInfo TurkeyStandardTime()
        {
            return FindTimeZoneById("Turkey Standard Time");
        }

        public static DateTime ConvertUtcToTrTime(DateTime dt)
        {
            return ConvertToUserTime(dt, DateTimeKind.Utc, TurkeyStandardTime());
        }

        public static DateTime ConvertToUserTime(DateTime dt, DateTimeKind sourceDateTimeKind, TimeZoneInfo targetTimeZone)
        {
            dt = DateTime.SpecifyKind(dt, sourceDateTimeKind);
            return TimeZoneInfo.ConvertTime(dt, targetTimeZone);
        }

        public static DateTime ConvertToUtcTime(DateTime dt)
        {
            return ConvertToUtcTime(dt, dt.Kind);
        }

        public static DateTime ConvertToUtcTime(DateTime dt, DateTimeKind sourceDateTimeKind)
        {
            dt = DateTime.SpecifyKind(dt, sourceDateTimeKind);
            return TimeZoneInfo.ConvertTimeToUtc(dt);
        }

        public static void LocalizeObject<T>(T item)
        {
            if (item != null)
            {
                var properties = item.GetType().GetProperties();
                foreach (var propertyInfo in properties)
                {
                    if (propertyInfo.PropertyType == typeof(DateTime))
                    {
                        var date = (DateTime)propertyInfo.GetValue(item);
                        propertyInfo.SetValue(item, ConvertUtcToTrTime(date));
                    }
                    else if (propertyInfo.PropertyType == typeof(DateTime?))
                    {
                        var date = (DateTime?)propertyInfo.GetValue(item);
                        if (date.HasValue)
                        {
                            propertyInfo.SetValue(item, ConvertUtcToTrTime(date.Value));
                        }
                    }
                }
            }
        }

        public static void DeLocalizeObject<T>(T item)
        {
            if (item != null)
            {
                var properties = item.GetType().GetProperties();
                foreach (var propertyInfo in properties)
                {
                    if (propertyInfo.PropertyType == typeof(DateTime))
                    {
                        var date = (DateTime)propertyInfo.GetValue(item);
                        propertyInfo.SetValue(item, ConvertToUtcTime(date));
                    }
                    else if (propertyInfo.PropertyType == typeof(DateTime?))
                    {
                        var date = (DateTime?)propertyInfo.GetValue(item);
                        if (date.HasValue)
                        {
                            propertyInfo.SetValue(item, ConvertToUtcTime(date.Value));
                        }
                    }
                }
            }
        }

        public static DateTime? Min(DateTime? firstDate, DateTime? secondDate, bool nullIsMinimum = false)
        {
            if (!firstDate.HasValue || !secondDate.HasValue)
            {
                return nullIsMinimum ? (DateTime?)null : (firstDate ?? secondDate);
            }

            return new DateTime(Math.Min(firstDate.Value.Ticks, secondDate.Value.Ticks));
        }

        public static DateTime? Max(DateTime? firstDate, DateTime? secondDate, bool nullIsMaximum = false)
        {
            if (!firstDate.HasValue || !secondDate.HasValue)
            {
                return nullIsMaximum ? (DateTime?)null : (firstDate ?? secondDate);
            }

            return new DateTime(Math.Max(firstDate.Value.Ticks, secondDate.Value.Ticks));
        }

    }
}
