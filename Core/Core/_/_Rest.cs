﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

public partial class _
{
    public static class Rest
    {
        public static bool Download(string url, out string result, out string exception)
        {
            try
            {
                var client = new HttpClient();
                var uri = new Uri(url);

                var response = client.GetAsync(uri).Result;
                result = response.Content.ReadAsStringAsync().Result;
                exception = "";
                return true;
            }
            catch (Exception ex)
            {
                result = "";
                exception = ex.ToString();
                if (ex.InnerException != null)
                {
                    exception += " | " + ex.InnerException.ToString();
                }
                return false;
            }
        }

        public static bool Get<T>(string url, out T result, out string exception, out string responseData)
        {
            responseData = "";
            try
            {
                var client = new HttpClient();
                var uri = new Uri(url);

                var response = client.GetAsync(uri).Result;
                responseData = response.Content.ReadAsStringAsync().Result;

                result = JsonConvert.DeserializeObject<T>(responseData);
                exception = "";
                return true;
            }
            catch (Exception ex)
            {
                result = default(T);
                exception = ex.ToString();
                if (ex.InnerException != null)
                {
                    exception += " | " + ex.InnerException.ToString();
                }
                return false;
            }
        }

        public static bool Post<T, TK>(string url, TK model, out T result, out string exception, out string responseData, string type = "application/json")
        {
            responseData = "";
            try
            {
                var requestData = JsonConvert.SerializeObject(model);

                var client = new HttpClient();
                var uri = new Uri(url);
                var content = new StringContent(requestData, Encoding.UTF8, type);

                var response = client.PostAsync(uri, content).Result;
                responseData = response.Content.ReadAsStringAsync().Result;

                result = JsonConvert.DeserializeObject<T>(responseData);
                exception = "";
                return true;
            }
            catch (Exception ex)
            {
                result = default(T);
                exception = ex.ToString();
                if (ex.InnerException != null)
                {
                    exception += " | " + ex.InnerException.ToString();
                }
                return false;
            }
        }

        public static HttpResponseMessage PostResponse<TK>(string url, TK model, out string exception)
        {
            try
            {
                var requestData = JsonConvert.SerializeObject(model);

                var client = new HttpClient();
                var uri = new Uri(url);
                var content = new StringContent(requestData, Encoding.UTF8, "application/json");
                exception = "";
                return client.PostAsync(uri, content).Result;
            }
            catch (Exception ex)
            {
                exception = ex.ToString();
                return null;
            }
        }
    }
}
