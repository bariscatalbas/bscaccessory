﻿using System;
using System.Linq;
using System.Web;

public partial class _
{
    public static class Cookie
    {
        public static string GetCookie(string key, bool updateCookie = false, int updateMin = 300)
        {
            if (HttpContext.Current != null && HttpContext.Current.Handler != null && HttpContext.Current.Request != null && HttpContext.Current.Request.Cookies != null)
            {
                var cookie = HttpContext.Current.Request.Cookies[key];
                if (cookie != null)
                {
                    var value = cookie.Value;
                    if (updateCookie)
                    {
                        var updateDate = updateMin <= 0 ? DateTime.Now.AddYears(1) : DateTime.Now.AddMinutes(updateMin);
                        if (cookie.Expires <= updateDate)
                        {
                            SetCookie(key, value, updateDate);
                        }
                    }
                    return value;
                }

                cookie = HttpContext.Current.Response.Cookies[key];
                if (cookie != null)
                {
                    return cookie.Value;
                }
            }
            return null;
        }

        /// <summary>
        /// Set Cookie For 1 Year
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public static void SetCookiePermenant(string key, string value)
        {
            SetCookie(key, value, DateTime.Now.AddYears(1));
        }

        /// <summary>
        /// Create New Cookie, If cookie exist, override existant one.
        /// </summary>
        /// <param name="key">Cookie Key</param>
        /// <param name="value">Cookie Value</param>
        /// <param name="expirationDate">Default 30min</param>
        public static void SetCookie(string key, string value, int updateMin)
        {
            if (HttpContext.Current != null && HttpContext.Current.Handler != null && HttpContext.Current.Response != null && HttpContext.Current.Response.Cookies != null)
            {
                var expDate = DateTime.Now.AddMinutes(updateMin);

                var cookie = new HttpCookie(key) { Value = value, Expires = expDate };

                if (HttpContext.Current.Response.Cookies.AllKeys.Contains(key))
                    HttpContext.Current.Response.Cookies.Remove(key);

                var cookieDomain = _.Configuration.AppSettings("CookieDomain");
                if (!string.IsNullOrWhiteSpace(cookieDomain))
                {
                    cookie.Domain = cookieDomain;
                }

                cookie.Secure = _.Configuration.IsLive;
                cookie.HttpOnly = true;
                cookie.SameSite = _.Configuration.IsLive ? SameSiteMode.None : SameSiteMode.Lax;

                HttpContext.Current.Response.Cookies.Add(cookie);
            }
        }

        /// <summary>
        /// Create New Cookie, If cookie exist, override existant one.
        /// </summary>
        /// <param name="key">Cookie Key</param>
        /// <param name="value">Cookie Value</param>
        /// <param name="expirationDate">Default 30min</param>
        public static void SetCookie(string key, string value, DateTime? expirationDate = null)
        {
            if (HttpContext.Current != null && HttpContext.Current.Handler != null && HttpContext.Current.Response != null && HttpContext.Current.Response.Cookies != null)
            {
                var expDate = expirationDate ?? DateTime.Now.AddMinutes(30);

                if (expDate < DateTime.Now.AddMinutes(5))
                    expDate = DateTime.Now.AddMinutes(10);

                var cookie = new HttpCookie(key) { Value = value, Expires = expDate };

                if (HttpContext.Current.Response.Cookies.AllKeys.Contains(key))
                    HttpContext.Current.Response.Cookies.Remove(key);

                var cookieDomain = _.Configuration.AppSettings("CookieDomain");
                if (!string.IsNullOrWhiteSpace(cookieDomain))
                {
                    cookie.Domain = cookieDomain;
                }

                cookie.Secure = _.Configuration.IsLive;
                cookie.HttpOnly = true;
                cookie.SameSite = _.Configuration.IsLive ? SameSiteMode.None : SameSiteMode.Lax;

                HttpContext.Current.Response.Cookies.Add(cookie);
            }
        }

        public static void RemoveCookie(string key)
        {
            if (HttpContext.Current != null && HttpContext.Current.Handler != null && HttpContext.Current.Request != null && HttpContext.Current.Request.Cookies != null)
            {
                if (HttpContext.Current.Request.Cookies.AllKeys.Contains(key))
                {
                    if (HttpContext.Current.Response.Cookies.AllKeys.Contains(key))
                        HttpContext.Current.Response.Cookies.Remove(key);

                    var cookie = new HttpCookie(key) { Value = "", Expires = DateTime.Now.AddDays(-1) };
                    var cookieDomain = _.Configuration.AppSettings("CookieDomain");
                    if (!string.IsNullOrWhiteSpace(cookieDomain))
                    {
                        cookie.Domain = cookieDomain;
                    }

                    cookie.Secure = _.Configuration.IsLive;
                    cookie.HttpOnly = true;
                    cookie.SameSite = _.Configuration.IsLive ? SameSiteMode.None : SameSiteMode.Lax;

                    HttpContext.Current.Response.Cookies.Add(cookie);
                }
            }
        }
    }
}
