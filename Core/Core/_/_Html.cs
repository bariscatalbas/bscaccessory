﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

public partial class _
{
    public static class Html
    {
        public static string AddHttp(string url)
        {
            return string.IsNullOrWhiteSpace(url) ? "" : (url.Contains("://") ? url : "http://" + url);
        }
        public static string TextToHtmlFragment(string value)
        {
            if (!string.IsNullOrWhiteSpace(value))
            {
                value = Regex.Replace(value, "\n", "<br />");
                value = Regex.Replace(value, @"\b(https?|ftp|file)://[-A-Z0-9+&@#/%?=~_|!:,.;]*[-A-Z0-9+&@#/%=~_|]", "<a href=\"$0\" rel=\"nofollow\" target=\"_blank\">$0</a>", RegexOptions.IgnoreCase);
            }
            return value;
        }
        public static string ClearTags(string html)
        {
            if(!string.IsNullOrWhiteSpace(html))
            {
                RegexOptions options = RegexOptions.Singleline | RegexOptions.IgnoreCase;

                html = Regex.Replace(html, @"<script.*?</script>", string.Empty, options);
                html = Regex.Replace(html, @"<a(.|\n)*?>", string.Empty, options);
                html = Regex.Replace(html, @"</a>", string.Empty, options);
                html = Regex.Replace(html, @"<ul(.|\n)*?>", string.Empty, options);
                html = Regex.Replace(html, @"</ul>", string.Empty, options);
                html = Regex.Replace(html, @"<ol(.|\n)*?>", string.Empty, options);
                html = Regex.Replace(html, @"</ol>", string.Empty, options);
                html = Regex.Replace(html, @"<li(.|\n)*?>", string.Empty, options);
                html = Regex.Replace(html, @"</li>", string.Empty, options);
                html = Regex.Replace(html, @"<p(.|\n)*?>", string.Empty, options);
                html = Regex.Replace(html, @"</p>", string.Empty, options);
                html = Regex.Replace(html, @"<i(.|\n)*?>", string.Empty, options);
                html = Regex.Replace(html, @"</i>", string.Empty, options);
                html = Regex.Replace(html, @"<em(.|\n)*?>", string.Empty, options);
                html = Regex.Replace(html, @"</em>", string.Empty, options);
                html = Regex.Replace(html, @"<b(.|\n)*?>", string.Empty, options);
                html = Regex.Replace(html, @"</b>", string.Empty, options);
                html = Regex.Replace(html, @"<div(.|\n)*?>", string.Empty, options);
                html = Regex.Replace(html, @"</div>", string.Empty, options);
                html = Regex.Replace(html, @"<form.*?</form>", string.Empty, options);
                html = Regex.Replace(html, @"<img(.|\n)*?>", string.Empty, options);
                html = Regex.Replace(html, @"<br(.|\n)*?>", string.Empty, options);
                html = Regex.Replace(html, "&#(\\d{2,3});", AsciiReplaceMatchEvaluator);

                html = html.Replace(@"<input class=""check-box"" disabled=""disabled"" type=""checkbox"" />", "");
                html = html.Replace(@"<input checked=""checked"" class=""check-box"" disabled=""disabled"" type=""checkbox"" />", "X");

                return html;
            }

            return "";
        }
        private static string AsciiReplaceMatchEvaluator(Match match)
        {
            int chrCode = Convert.ToInt32(match.Groups[1].Value);
            char chr = (char)chrCode;
            return chr.ToString();
        }
        public static string AddStyle(string html)
        {
            var newHtml = "<html> <head> <meta http-equiv='Content-Type' content='text/html; charset=utf-8' />";
            var style = @"<style type='text/css'>
                                    .list-table
                                    {
	                                    border: solid 1px #e8eef4;
	                                    border-collapse: collapse;
                                    }
                                    .list-table td
                                    {
	                                    padding: 5px;
	                                    border: solid 1px #e8eef4;
                                    }
                                    .list-table th
                                    {
	                                    padding: 6px 5px;
	                                    text-align: left;
	                                    background-color: #e8eef4;
	                                    border: solid 1px #e8eef4;
                                    }
                                    .pagingTr
                                    {
                                        display:none;
                                    }
                             </style> ";

            newHtml += style;
            newHtml += "</head>";
            newHtml += "<body>";
            newHtml += html;
            newHtml += "</body>";
            newHtml += "</html>";
            return newHtml;
        }
        public static string RemoveTags(string value)
        {
            if (!string.IsNullOrWhiteSpace(value))
            {
                value = CleanHtmlComments(value);
                value = CleanHtmlBehaviour(value);
                value = Regex.Replace(value, @"</[^>]+?>", " ");
                value = Regex.Replace(value, @"<[^>]+?>", "");
                value = value.Trim();
                return value;
            }
            return "";
        }
        public static string CleanHtmlBehaviour(string value)
        {
            return string.IsNullOrWhiteSpace(value) ? "" : Regex.Replace(value, "(<style.+?</style>)|(<script.+?</script>)", "", RegexOptions.IgnoreCase | RegexOptions.Singleline);
        }
        public static string CleanHtmlComments(string value)
        {
            return string.IsNullOrWhiteSpace(value) ? "" : Regex.Replace(value, "<!--.+?-->", "", RegexOptions.IgnoreCase | RegexOptions.Singleline);
        }

    }
}
