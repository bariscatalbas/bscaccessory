﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web;

public partial class _
{
    public static class Crypto
    {
        public static string EncryptPassword(string password)
        {
            return Algorithms.RijndaelEncrypt(password);
        }
        public static string DecryptPassword(string password)
        {
            return Algorithms.RijndaelDecrypt(password);
        }

        public static string EncryptCode(string code)
        {
            return Algorithms.RijndaelEncrypt(code, new Guid("4BD416E6-6CAB-40C1-B169-8C5E617CD3DA"), new Guid("CFA2EB30-BD0D-4152-B535-6BB89265D1EE"));
        }
        public static string DecryptCode(string encryptedCode)
        {
            return Algorithms.RijndaelDecrypt(encryptedCode, new Guid("4BD416E6-6CAB-40C1-B169-8C5E617CD3DA"), new Guid("CFA2EB30-BD0D-4152-B535-6BB89265D1EE"));
        }

        public static string TokenizeText(string text)
        {
            if (string.IsNullOrEmpty(text))
                return text;

            var key = new Guid("BE23EF62-2222-4EC1-BD5F-FB38C23EA53D");
            var iv = new Guid("FA4AD203-3333-4F6D-B625-B50CABD944C4");

            var value = new UTF8Encoding(false).GetBytes(text);
            using (var stm = new MemoryStream())
            {
                var rij = Rijndael.Create();
                rij.Key = key.ToByteArray();
                rij.IV = iv.ToByteArray();
                using (var cs = new CryptoStream(stm, rij.CreateEncryptor(), CryptoStreamMode.Write))
                {
                    cs.Write(value, 0, value.Length);
                    cs.FlushFinalBlock();
                    cs.Close();
                }
                return HttpServerUtility.UrlTokenEncode(stm.ToArray());
            }
        }
        public static string UntokenizeText(string text)
        {
            try
            {
                var value = HttpServerUtility.UrlTokenDecode(text);
                var key = new Guid("BE23EF62-2222-4EC1-BD5F-FB38C23EA53D");
                var iv = new Guid("FA4AD203-3333-4F6D-B625-B50CABD944C4");

                using (var stm = new MemoryStream())
                {
                    var rij = Rijndael.Create();
                    rij.Key = key.ToByteArray();
                    rij.IV = iv.ToByteArray();
                    using (var cs = new CryptoStream(stm, rij.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(value, 0, value.Length);
                        cs.Flush();
                        cs.Close();
                    }
                    var byResult = stm.ToArray();
                    return new UTF8Encoding(false).GetString(byResult, 0, byResult.Length);
                }
            }
            catch (Exception)
            {
                return "";
            }
        }

        public static string EncryptText(string plainText, string encryptionPrivateKey = "")
        {
            if (string.IsNullOrEmpty(plainText))
                return plainText;

            if (string.IsNullOrEmpty(encryptionPrivateKey))
                encryptionPrivateKey = _.Configuration.AppSettings("EncryptionKey");
            try
            {
                var tDESalg = new TripleDESCryptoServiceProvider();
                tDESalg.Key = new ASCIIEncoding().GetBytes(encryptionPrivateKey.Substring(0, 16));
                tDESalg.IV = new ASCIIEncoding().GetBytes(encryptionPrivateKey.Substring(8, 8));

                byte[] encryptedBinary = EncryptTextToMemory(plainText, tDESalg.Key, tDESalg.IV);
                return Convert.ToBase64String(encryptedBinary);
            }
            catch (Exception)
            {
                return plainText;
            }
        }
        public static string DecryptText(string cipherText, string encryptionPrivateKey = "")
        {
            try
            {
                if (string.IsNullOrEmpty(cipherText))
                    return cipherText;

                if (string.IsNullOrEmpty(encryptionPrivateKey))
                    encryptionPrivateKey = _.Configuration.AppSettings("EncryptionKey");

                var tDESalg = new TripleDESCryptoServiceProvider();
                tDESalg.Key = new ASCIIEncoding().GetBytes(encryptionPrivateKey.Substring(0, 16));
                tDESalg.IV = new ASCIIEncoding().GetBytes(encryptionPrivateKey.Substring(8, 8));

                byte[] buffer = Convert.FromBase64String(cipherText);
                return DecryptTextFromMemory(buffer, tDESalg.Key, tDESalg.IV);
            }
            catch (Exception)
            {
                return cipherText;
            }
        }

        public static string GenerateRandomInt(int length = 2)
        {
            return Algorithms.GenerateRandomInt(length, length);
        }
        public static string GenerateRandomString(int length = 8)
        {
            return GenerateRandomString(length, length);
        }
        public static string GenerateRandomString(int minLength = 8, int maxLength = 8)
        {
            return Algorithms.GenerateRandomString(minLength, maxLength);
        }
        public static int RandomNumber(int min, int max)
        {
            var rnd = new Random();
            return rnd.Next(min, max);
        }

        private static byte[] EncryptTextToMemory(string data, byte[] key, byte[] iv)
        {
            using (var ms = new MemoryStream())
            {
                using (var cs = new CryptoStream(ms, new TripleDESCryptoServiceProvider().CreateEncryptor(key, iv), CryptoStreamMode.Write))
                {
                    byte[] toEncrypt = new UnicodeEncoding().GetBytes(data);
                    cs.Write(toEncrypt, 0, toEncrypt.Length);
                    cs.FlushFinalBlock();
                }

                return ms.ToArray();
            }
        }
        private static string DecryptTextFromMemory(byte[] data, byte[] key, byte[] iv)
        {
            using (var ms = new MemoryStream(data))
            {
                using (var cs = new CryptoStream(ms, new TripleDESCryptoServiceProvider().CreateDecryptor(key, iv), CryptoStreamMode.Read))
                {
                    var sr = new StreamReader(cs, new UnicodeEncoding());
                    return sr.ReadToEnd();
                }
            }
        }

        public static class Algorithms
        {
            private const string PasswordCharsLcase = "abcdefgijkmnopqrstwxyz";
            private const string PasswordCharsUcase = "ABCDEFGHJKLMNPQRSTWXYZ";
            private const string PasswordCharsNumeric = "0123456789";

            public static string GenerateRandomString(int minLength, int maxLength)
            {
                if (minLength <= 0 || maxLength <= 0 || minLength > maxLength)
                    return null;

                var charGroups = new[]
                             {
            PasswordCharsLcase.ToCharArray(),
            PasswordCharsUcase.ToCharArray(),
            PasswordCharsNumeric.ToCharArray()
        };

                var charsLeftInGroup = new int[charGroups.Length];
                for (var i = 0; i < charsLeftInGroup.Length; i++) charsLeftInGroup[i] = charGroups[i].Length;
                var leftGroupsOrder = new int[charGroups.Length];
                for (var i = 0; i < leftGroupsOrder.Length; i++) leftGroupsOrder[i] = i;
                var randomBytes = new byte[4];
                var rng = new RNGCryptoServiceProvider();
                rng.GetBytes(randomBytes);
                var seed = (randomBytes[0] & 0x7f) << 24 | randomBytes[1] << 16 | randomBytes[2] << 8 | randomBytes[3];
                var random = new Random(seed);
                var password = minLength < maxLength ? new char[random.Next(minLength, maxLength + 1)] : new char[minLength];
                var lastLeftGroupsOrderIdx = leftGroupsOrder.Length - 1;
                for (var i = 0; i < password.Length; i++)
                {
                    var nextLeftGroupsOrderIdx = lastLeftGroupsOrderIdx == 0 ? 0 : random.Next(0, lastLeftGroupsOrderIdx);
                    var nextGroupIdx = leftGroupsOrder[nextLeftGroupsOrderIdx];
                    var lastCharIdx = charsLeftInGroup[nextGroupIdx] - 1;
                    var nextCharIdx = lastCharIdx == 0 ? 0 : random.Next(0, lastCharIdx + 1);
                    password[i] = charGroups[nextGroupIdx][nextCharIdx];
                    if (lastCharIdx == 0)
                        charsLeftInGroup[nextGroupIdx] = charGroups[nextGroupIdx].Length;
                    else
                    {
                        if (lastCharIdx != nextCharIdx)
                        {
                            var temp = charGroups[nextGroupIdx][lastCharIdx];
                            charGroups[nextGroupIdx][lastCharIdx] = charGroups[nextGroupIdx][nextCharIdx];
                            charGroups[nextGroupIdx][nextCharIdx] = temp;
                        }
                        charsLeftInGroup[nextGroupIdx]--;
                    }

                    if (lastLeftGroupsOrderIdx == 0)
                        lastLeftGroupsOrderIdx = leftGroupsOrder.Length - 1;
                    else
                    {
                        if (lastLeftGroupsOrderIdx != nextLeftGroupsOrderIdx)
                        {
                            var temp = leftGroupsOrder[lastLeftGroupsOrderIdx];
                            leftGroupsOrder[lastLeftGroupsOrderIdx] = leftGroupsOrder[nextLeftGroupsOrderIdx];
                            leftGroupsOrder[nextLeftGroupsOrderIdx] = temp;
                        }
                        lastLeftGroupsOrderIdx--;
                    }
                }
                return new string(password);
            }
            public static string GenerateRandomInt(int minLength, int maxLength)
            {
                if (minLength <= 0 || maxLength <= 0 || minLength > maxLength)
                    return null;

                var charGroups = new[]
                             {
                    PasswordCharsNumeric.ToCharArray()
                };

                var charsLeftInGroup = new int[charGroups.Length];
                for (var i = 0; i < charsLeftInGroup.Length; i++) charsLeftInGroup[i] = charGroups[i].Length;
                var leftGroupsOrder = new int[charGroups.Length];
                for (var i = 0; i < leftGroupsOrder.Length; i++) leftGroupsOrder[i] = i;
                var randomBytes = new byte[4];
                var rng = new RNGCryptoServiceProvider();
                rng.GetBytes(randomBytes);
                var seed = (randomBytes[0] & 0x7f) << 24 | randomBytes[1] << 16 | randomBytes[2] << 8 | randomBytes[3];
                var random = new Random(seed);
                var password = minLength < maxLength ? new char[random.Next(minLength, maxLength + 1)] : new char[minLength];
                var lastLeftGroupsOrderIdx = leftGroupsOrder.Length - 1;
                for (var i = 0; i < password.Length; i++)
                {
                    var nextLeftGroupsOrderIdx = lastLeftGroupsOrderIdx == 0 ? 0 : random.Next(0, lastLeftGroupsOrderIdx);
                    var nextGroupIdx = leftGroupsOrder[nextLeftGroupsOrderIdx];
                    var lastCharIdx = charsLeftInGroup[nextGroupIdx] - 1;
                    var nextCharIdx = lastCharIdx == 0 ? 0 : random.Next(0, lastCharIdx + 1);
                    password[i] = charGroups[nextGroupIdx][nextCharIdx];
                    if (lastCharIdx == 0)
                        charsLeftInGroup[nextGroupIdx] = charGroups[nextGroupIdx].Length;
                    else
                    {
                        if (lastCharIdx != nextCharIdx)
                        {
                            var temp = charGroups[nextGroupIdx][lastCharIdx];
                            charGroups[nextGroupIdx][lastCharIdx] = charGroups[nextGroupIdx][nextCharIdx];
                            charGroups[nextGroupIdx][nextCharIdx] = temp;
                        }
                        charsLeftInGroup[nextGroupIdx]--;
                    }

                    if (lastLeftGroupsOrderIdx == 0)
                        lastLeftGroupsOrderIdx = leftGroupsOrder.Length - 1;
                    else
                    {
                        if (lastLeftGroupsOrderIdx != nextLeftGroupsOrderIdx)
                        {
                            var temp = leftGroupsOrder[lastLeftGroupsOrderIdx];
                            leftGroupsOrder[lastLeftGroupsOrderIdx] = leftGroupsOrder[nextLeftGroupsOrderIdx];
                            leftGroupsOrder[nextLeftGroupsOrderIdx] = temp;
                        }
                        lastLeftGroupsOrderIdx--;
                    }
                }
                return new string(password);
            }

            public static string Md5Hash(string text)
            {
                var value = new UTF8Encoding().GetBytes(text);
                var binMd5 = new MD5CryptoServiceProvider().ComputeHash(value);
                return HexEncode(binMd5);
            }

            public static string RijndaelEncrypt(string text)
            {
                return RijndaelEncrypt(text, new Guid("BE23EF62-6920-4EC1-BD5F-FB38C23EA53D"), new Guid("FA4AD203-5498-4F6D-B625-B50CABD944C4"));
            }
            public static string RijndaelEncrypt(string text, Guid key, Guid iv)
            {
                var value = new UTF8Encoding(false).GetBytes(text);

                using (var stm = new MemoryStream())
                {
                    var rij = Rijndael.Create();
                    rij.Key = key.ToByteArray();
                    rij.IV = iv.ToByteArray();
                    using (var cs = new CryptoStream(stm, rij.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(value, 0, value.Length);
                        cs.FlushFinalBlock();
                        cs.Close();
                    }
                    return Convert.ToBase64String(stm.ToArray());
                }
            }
            public static string RijndaelDecrypt(string text)
            {
                return RijndaelDecrypt(text, new Guid("BE23EF62-6920-4EC1-BD5F-FB38C23EA53D"), new Guid("FA4AD203-5498-4F6D-B625-B50CABD944C4"));
            }
            public static string RijndaelDecrypt(string text, Guid key, Guid iv)
            {
                try
                {
                    var value = Convert.FromBase64String(text);

                    using (var stm = new MemoryStream())
                    {
                        var rij = Rijndael.Create();
                        rij.Key = key.ToByteArray();
                        rij.IV = iv.ToByteArray();
                        using (var cs = new CryptoStream(stm, rij.CreateDecryptor(), CryptoStreamMode.Write))
                        {
                            cs.Write(value, 0, value.Length);
                            cs.Flush();
                            cs.Close();
                        }
                        var byResult = stm.ToArray();
                        return new UTF8Encoding(false).GetString(byResult, 0, byResult.Length);
                    }
                }
                catch (Exception ex)
                {
                    return "";
                }
            }

            public static string TripleDesEncrypt(string text)
            {
                return TripleDesEncrypt(text, new Guid("BE23EF62-6920-4EC1-BD5F-FB38C23EA53D"), new Guid("FA4AD203-5498-4F6D-B625-B50CABD944C4"));
            }
            public static string TripleDesEncrypt(string text, Guid key, Guid iv)
            {
                byte[] value = Encoding.ASCII.GetBytes(text);

                var des = new TripleDESCryptoServiceProvider { Key = key.ToByteArray(), IV = iv.ToByteArray() };
                return Convert.ToBase64String(des.CreateEncryptor().TransformFinalBlock(value, 0, value.Length));
            }
            public static string TripleDesDecrypt(string text)
            {
                return TripleDesDecrypt(text, new Guid("BE23EF62-6920-4EC1-BD5F-FB38C23EA53D"), new Guid("FA4AD203-5498-4F6D-B625-B50CABD944C4"));
            }
            public static string TripleDesDecrypt(string text, Guid key, Guid iv)
            {
                var value = Convert.FromBase64String(text);

                var des = new TripleDESCryptoServiceProvider { Key = key.ToByteArray(), IV = iv.ToByteArray() };
                return Encoding.ASCII.GetString(des.CreateDecryptor().TransformFinalBlock(value, 0, value.Length));
            }

            public static string AesEncrypt(string text)
            {
                return AesEncrypt(text, new Guid("BE23EF62-6920-4EC1-BD5F-FB38C23EA53D").ToString(), new Guid("FA4AD203-5498-4F6D-B625-B50CABD944C4").ToString(), "SHA1", 2, "!@_#^7+-*,Cp@sTe", 256);
            }
            public static string AesEncrypt(string plainText, string password, string salt, string hashAlgorithm, int passwordIterations, string initialVector, int keySize)
            {
                var initialVectorBytes = Encoding.ASCII.GetBytes(initialVector);
                var saltValueBytes = Encoding.ASCII.GetBytes(salt);
                var plainTextBytes = Encoding.UTF8.GetBytes(plainText);
                var derivedPassword = new PasswordDeriveBytes(password, saltValueBytes, hashAlgorithm, passwordIterations);
#pragma warning disable 618,612
                var keyBytes = derivedPassword.GetBytes(keySize / 8);
#pragma warning restore 618,612
                var symmetricKey = new RijndaelManaged { Mode = CipherMode.CBC };
                var encryptor = symmetricKey.CreateEncryptor(keyBytes, initialVectorBytes);
                var memStream = new MemoryStream();
                var cryptoStream = new CryptoStream(memStream, encryptor, CryptoStreamMode.Write);
                cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
                cryptoStream.FlushFinalBlock();
                var cipherTextBytes = memStream.ToArray();
                memStream.Close();
                cryptoStream.Close();
                return Convert.ToBase64String(cipherTextBytes);
            }
            public static string AesDecrypt(string text)
            {
                return AesDecrypt(text, new Guid("BE23EF62-6920-4EC1-BD5F-FB38C23EA53D").ToString(), new Guid("FA4AD203-5498-4F6D-B625-B50CABD944C4").ToString(), "SHA1", 2, "!@_#^7+-*,Cp@sTe", 256);
            }
            public static string AesDecrypt(string cipherText, string password, string salt, string hashAlgorithm, int passwordIterations, string initialVector, int keySize)
            {
                var initialVectorBytes = Encoding.ASCII.GetBytes(initialVector);
                var saltValueBytes = Encoding.ASCII.GetBytes(salt);
                var cipherTextBytes = Convert.FromBase64String(cipherText);
                var derivedPassword = new PasswordDeriveBytes(password, saltValueBytes, hashAlgorithm, passwordIterations);
#pragma warning disable 618,612
                var keyBytes = derivedPassword.GetBytes(keySize / 8);
#pragma warning restore 618,612
                var symmetricKey = new RijndaelManaged { Mode = CipherMode.CBC };
                var decryptor = symmetricKey.CreateDecryptor(keyBytes, initialVectorBytes);
                var memStream = new MemoryStream(cipherTextBytes);
                var cryptoStream = new CryptoStream(memStream, decryptor, CryptoStreamMode.Read);
                var plainTextBytes = new byte[cipherTextBytes.Length];
                var byteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);
                memStream.Close();
                cryptoStream.Close();
                return Encoding.UTF8.GetString(plainTextBytes, 0, byteCount);
            }

            public static string HexEncode(IEnumerable<byte> value)
            {
                const string hex = "0123456789abcdef";
                var sb = new StringBuilder();
                foreach (byte t in value)
                {
                    sb.Append(hex[(t & 0xf0) >> 4]);
                    sb.Append(hex[t & 0x0f]);
                }
                return sb.ToString();
            }

        }

        public static class Password
        {
            public static string CreateSaltKey(int size)
            {
                // Generate a cryptographic random number
                var rng = new RNGCryptoServiceProvider();
                var buff = new byte[size];
                rng.GetBytes(buff);

                // Return a Base64 string representation of the random number
                return Convert.ToBase64String(buff);
            }


            public static string CreatePasswordHash(string password, string saltkey, string passwordFormat = "SHA1")
            {
                if (string.IsNullOrEmpty(passwordFormat))
                    passwordFormat = "SHA1";
                string saltAndPassword = string.Concat(password, saltkey);

                //return FormsAuthentication.HashPasswordForStoringInConfigFile(saltAndPassword, passwordFormat);
                var algorithm = HashAlgorithm.Create(passwordFormat);
                if (algorithm == null)
                    throw new ArgumentException("Unrecognized hash name");

                var hashByteArray = algorithm.ComputeHash(Encoding.UTF8.GetBytes(saltAndPassword));
                return BitConverter.ToString(hashByteArray).Replace("-", "");
            }
        }
    }
}
