﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public partial class _
{
    public static class Configuration
    {

        public static string ConnectionString(string code)
        {
            var connectionStringSettings = ConfigurationManager.ConnectionStrings[code];
            return connectionStringSettings != null ? connectionStringSettings.ConnectionString : "";
        }

        public static string AppSettings(string code)
        {
            return ConfigurationManager.AppSettings[code];
        }

        public static bool IsLive { get { return AppSettings("IsLive") == "TRUE"; } }
        public static bool IsSecure { get { return AppSettings("IsLive") == "TRUE"; } }
        public static string WebSiteUrl { get { return AppSettings("WebSiteUrl"); } }
        public static string AdminSiteUrl { get { return AppSettings("AdminSiteUrl"); } }

        public static string CookieDomain { get { return AppSettings("CookieDomain"); } }
        public static string WhereAmI { get { return AppSettings("WhereAmI"); } }
        public static string WhichAppAmI { get { return AppSettings("WhichAppAmI"); } }
    }
}
