﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

public partial class _
{
    public static class String
    {
        public static string MakeReadable(string value)
        {
            var text = value.Trim();
            var length = text.Length;
            var chars = text.ToArray();
            var result = string.Empty;

            for (var i = 0; i < length; i++)
            {
                var letter = chars[i];
                if (i == 0)
                {
                    result += letter;
                    continue;
                }

                if (char.IsUpper(letter))
                    result = result + " " + letter;
                else
                    result += letter;
            }

            result = result.Trim();
            if (result.EndsWith(" Id"))
            {
                result = result.Substring(0, result.Length - 3);
            }
            return result;
        }

        public static string MakeSearchable(string value)
        {
            return (value ?? "").Trim()
                            .Replace("ö", "o").Replace("ı", "i").Replace("ğ", "g").Replace("ş", "s").Replace("ü", "u").Replace("ç", "c")
                            .Replace("Ö", "o").Replace("İ", "i").Replace("Ğ", "g").Replace("Ş", "s").Replace("Ü", "u").Replace("Ç", "c")
                            .ToLowerInvariant();
        }

        public static string LastNcharacters(string value, int characterCount, char addForMissingChars = ' ')
        {
            value = value ?? "";
            if (value.Length >= characterCount)
            {
                return value.Substring(value.Length - characterCount);
            }

            var missingCharacterCount = characterCount - value.Length;
            for (int i = 0; i < missingCharacterCount; i++)
            {
                value = addForMissingChars + value;
            }
            return value;
        }

        public static string StripNonAlphaNumeric(string value, bool toLower = true, params string[] leaveValues)
        {
            var regex = leaveValues.Aggregate(@"[^a-zA-Z0-9_\- ", (current, s) => current + s) + "]";
            var rgx = new Regex(regex);
            value = (value ?? "").Trim().Replace("  ", " ");
            var newValue = toLower
                            ? value.ToLower().Replace("ö", "o").Replace("ı", "i").Replace("ğ", "g").Replace("ş", "s").Replace("ü", "u").Replace("ç", "c").Replace(" ", "-")
                            : value.ToUpper().Replace("Ö", "O").Replace("İ", "I").Replace("Ğ", "G").Replace("Ş", "S").Replace("Ü", "U").Replace("Ç", "C").Replace(" ", "-");
            newValue = rgx.Replace(newValue, "");
            return newValue.Replace("--", "-");
        }

        public static string Summarize(string value, int maxChars, string appendIfCropped = "", int minChars = 0, char appendIfMissing = ' ')
        {
            value = value ?? "";
            if (!string.IsNullOrWhiteSpace(value) && value.Length > maxChars)
            {
                value = value.Substring(0, maxChars);
                if (!string.IsNullOrWhiteSpace(appendIfCropped))
                {
                    value += appendIfCropped;
                }
            }
            var missingCharacterCount = minChars - value.Length;
            for (int i = 0; i < missingCharacterCount; i++)
            {
                value = value + appendIfMissing;
            }

            return value;
        }

        public static string CleanPhoneNumber(string phone)
        {
            phone = phone ?? "";
            if (phone.StartsWith("0"))
            {
                return phone.Substring(1, phone.Length - 1).Replace("(", "").Replace(")", "").Replace(" ", "").Replace("-", "").Replace("_", "");
            }
            else if (phone.StartsWith("+90"))
            {
                return phone.Substring(3, phone.Length - 3).Replace("(", "").Replace(")", "").Replace(" ", "").Replace("-", "").Replace("_", "");
            }
            else
            {
                return phone.Replace("(", "").Replace(")", "").Replace(" ", "").Replace("-", "").Replace("_", "");
            }

        }

        public static List<decimal> SplitToDecimal(string value, char seperator)
        {
            var list = new List<decimal>();
            if (!string.IsNullOrWhiteSpace(value))
            {
                var array = value.Split(seperator);
                foreach (var item in array)
                {
                    decimal converted;
                    if (decimal.TryParse(item.Trim(), out converted))
                    {
                        list.Add(converted);
                    }
                }
            }
            return list;
        }
    }
}
