﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;

public partial class _
{
    public static class Validation
    {
        public static bool ValidateNumerical(string text, bool emptyIsValid = false)
        {
            if (string.IsNullOrWhiteSpace(text))
            {
                return emptyIsValid;
            }

            var regex = new Regex(@"^[0-9]+$");
            var match = regex.Match(text);
            return match.Success;
        }

        public static bool ValidateAlphaNumerical(string text, bool emptyIsValid = true)
        {
            if (string.IsNullOrWhiteSpace(text))
            {
                return emptyIsValid;
            }

            var regex = new Regex(@"^[0-9a-zA-Z _-]+$");
            var match = regex.Match(text);
            return match.Success;
        }

        public static int PasswordScore(string password)
        {
            if (string.IsNullOrWhiteSpace(password)) return -1;
            if (password.Length < 4) return 0;

            var score = 0;

            if (password.Length >= 8) score++;
            if (password.Length >= 12) score++;
            if (Regex.IsMatch(password, @"/\d+/", RegexOptions.ECMAScript)) score++;
            if (Regex.IsMatch(password, @"/[a-z]/", RegexOptions.ECMAScript) && Regex.IsMatch(password, @"/[A-Z]/", RegexOptions.ECMAScript))
                score++;
            if (Regex.IsMatch(password, @"/.[!,@,#,$,%,^,&,*,?,_,~,-,£,(,)]/", RegexOptions.ECMAScript))
                score++;

            return score;
        }

        public static bool ValidateDate(string date, CultureInfo culture, bool emptyIsValid = true)
        {
            if (string.IsNullOrWhiteSpace(date))
            {
                return emptyIsValid;
            }
            try
            {
                Convert.ToDateTime(date, culture);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static bool ValidateTurkishDate(string date, bool emptyIsValid = true)
        {
            return ValidateDate(date, CultureInfo.CreateSpecificCulture("tr-TR"), emptyIsValid);
        }

        public static bool AreSame(IEnumerable<long> list1, IEnumerable<long> list2, bool ignoreMultipleOccurance = false)
        {
            if (list1 == null)
            {
                list1 = new List<long>();
            }
            if (list2 == null)
            {
                list2 = new List<long>();
            }

            if (!list1.Any() && !list2.Any())
            {
                return true;
            }

            if (ignoreMultipleOccurance)
            {
                list1 = list1.Distinct().ToList();
                list2 = list2.Distinct().ToList();
            }

            if (list1.Count() != list2.Count())
            {
                return false;
            }

            bool same = true;
            foreach (var item in list1)
            {
                same = same && list1.Count(t => t == item) == list2.Count(t => t == item);
            }
            return same;
        }

        public static bool ValidateEmail(string email, bool emptyIsValid = true)
        {
            if (string.IsNullOrWhiteSpace(email))
            {
                return emptyIsValid;
            }
            try
            {
                var validEmailPattern = @"^(?!\.)(""([^""\r\\]|\\[""\r\\])*""|"
                                        + @"([-a-z0-9!#$%&'*+/=?^_`{|}~]|(?<!\.)\.)*)(?<!\.)"
                                        + @"@[a-z0-9][\w\.-]*[a-z0-9]\.[a-z][a-z\.]*[a-z]$";
                var regex = new Regex(validEmailPattern, RegexOptions.IgnoreCase);
                return regex.IsMatch(email);
            }
            catch (FormatException)
            {
                return false;
            }
        }

        public static bool ValidateTckn(string tcNo, bool emptyIsValid = true)
        {
            if (string.IsNullOrWhiteSpace(tcNo))
            {
                return emptyIsValid;
            }

            long tc = 0;

            bool isNumber = long.TryParse(tcNo, out tc);
            bool hasElevenChar = tcNo.Length == 11;
            bool isNotStartWithZero = tcNo.StartsWith("0") == false;


            // şartlar sağlanmıyorsa devam etme
            if (!isNumber || !hasElevenChar || !isNotStartWithZero)
            {
                return false;
            }

            bool isValid = false;

            #region compute sums
            var first10NumberSum = 0;
            var evenNumbersSum = 0;
            var oddNumbersSum = 0;
            for (int i = 0; i < tcNo.Length; i++)
            {
                int n = (int)Char.GetNumericValue(tcNo[i]); // değerini al

                var isLastNo = i == 10;
                if (!isLastNo)
                {
                    first10NumberSum += n;
                }

                var isEven = i % 2 == 0;
                if (isEven && i <= 8) // i = 0,2,4,6,8 
                {
                    evenNumbersSum += n;
                }

                var isOdd = i % 2 != 0;
                if (isOdd && i < 8) // i = 1,3,5,7
                {
                    oddNumbersSum += n;
                }
            }
            #endregion

            var numberBeforeLastNumber = (int)Char.GetNumericValue(tcNo[9]);
            var lastNumber = (int)Char.GetNumericValue(tcNo[10]);

            // ilk 10 rakamın toplamının birler basamağı, son rakama eşittir
            bool isFirst10NumberSumEqualLastNumber = ((first10NumberSum % 10) == lastNumber);

            // ( çift haneler * 7 + tek haneler * 9 ) mod 10 = sondan bir önceki
            bool isRuleTrue = ((evenNumbersSum * 7 + oddNumbersSum * 9) % 10) == numberBeforeLastNumber;

            // çift haneler toplamının 8 katının birler basamağı(mod10) son rakama eşittir
            bool isEvenRuleTrue = (evenNumbersSum * 8) % 10 == lastNumber;

            // extra
            bool rule = (evenNumbersSum * 7 - oddNumbersSum) % 10 == numberBeforeLastNumber;
            bool isLastNumberEven = lastNumber % 2 == 0;

            if (isFirst10NumberSumEqualLastNumber && isEvenRuleTrue && isRuleTrue && rule && isLastNumberEven)
            {
                isValid = true;
            }

            return isValid;
        }

        public static bool ValidatePhone(string phone, bool emptyIsValid = true)
        {
            if (string.IsNullOrWhiteSpace(phone))
            {
                return emptyIsValid;
            }
            try
            {
                var validPhone = @"^((\+90)|0)?\(?\d{3}\)?-? *\d{3}-? *-?\d{4}$";
                var regex = new Regex(validPhone, RegexOptions.IgnoreCase);
                return regex.IsMatch(phone);
            }
            catch (FormatException)
            {
                return false;
            }
        }
    }
}
