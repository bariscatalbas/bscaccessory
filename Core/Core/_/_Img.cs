﻿using ImageResizer;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public partial class _
{
    public static class Img
    {

        public static byte[] ResizeImage(byte[] imageBytes, string size)
        {
            int height = 0, width = 0;
            if (size.StartsWith("h"))
            {
                int.TryParse(size.Substring(1), out height);
            }
            else if (size.StartsWith("w"))
            {
                int.TryParse(size.Substring(1), out width);
            }

            if (height > 0 || width > 0)
            {
                byte[] resizedImageBytes = null;
                using (var stream = new MemoryStream(imageBytes))
                {
                    Bitmap b = null;
                    try
                    {
                        //try-catch to ensure that picture binary is really OK. Otherwise, we can get "Parameter is not valid" exception if binary is corrupted for some reasons
                        b = new Bitmap(stream);
                    }
                    catch (ArgumentException exc)
                    {
                    }
                    if (b != null)
                    {
                        using (var destStream = new MemoryStream())
                        {
                            //var newSize = CalculateDimensions(b.Size, Math.Max(height, width), width > 0);
                            if (width > 0)
                            {
                                ImageBuilder.Current.Build(b, destStream, new ResizeSettings
                                {
                                    Width = width,
                                    Scale = ScaleMode.Both,
                                });
                            }
                            else
                            {
                                ImageBuilder.Current.Build(b, destStream, new ResizeSettings
                                {
                                    Height = height,
                                    Scale = ScaleMode.Both,
                                });
                            }

                            resizedImageBytes = destStream.ToArray();
                        }
                    }
                }
                return resizedImageBytes;
            }

            return null;
        }

        public static int GetImageWidth(byte[] imageBytes)
        {
            using (var stream = new MemoryStream(imageBytes))
            {
                Bitmap b = null;
                try
                {
                    //try-catch to ensure that picture binary is really OK. Otherwise, we can get "Parameter is not valid" exception if binary is corrupted for some reasons
                    b = new Bitmap(stream);
                }
                catch (ArgumentException exc)
                {
                }
                if (b != null)
                {
                    return b.Width;
                }
            }

            return 0;
        }

        public static int GetImageHeight(byte[] imageBytes)
        {
            using (var stream = new MemoryStream(imageBytes))
            {
                Bitmap b = null;
                try
                {
                    //try-catch to ensure that picture binary is really OK. Otherwise, we can get "Parameter is not valid" exception if binary is corrupted for some reasons
                    b = new Bitmap(stream);
                }
                catch (ArgumentException exc)
                {
                }
                if (b != null)
                {
                    return b.Height;
                }
            }

            return 0;
        }
    }
}
