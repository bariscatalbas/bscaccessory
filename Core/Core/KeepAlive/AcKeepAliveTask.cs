﻿using Core.Task;
using Newtonsoft.Json;
using System.Net;

namespace Core.KeepAlive
{
    public class AcKeepAliveTask : AcTaskBase
    {
        public AcKeepAliveTask()
        {
        }

        public override string Code => "AcKeepAliveTask";

        public override string Name => "Ac Keep Alive Task";

        public override string SettingsSystemName { get { return typeof(AcKeepAliveTaskSettings).AssemblyQualifiedName; } }

        private AcKeepAliveTaskSettings Settings { get; set; }

        public override bool CanIStartLocal(TaskPrepareModel task, out string logThisAtHistory)
        {
            logThisAtHistory = "";
            Settings = JsonConvert.DeserializeObject<AcKeepAliveTaskSettings>(task.SettingsJson);

            return true;
        }

        public override bool Execute(TaskPrepareModel task, out string logThisAtHistory)
        {
            logThisAtHistory = "";
            var keepAliveUrl = _.Configuration.AppSettings("KeepAliveUrl");
            if (!string.IsNullOrWhiteSpace(keepAliveUrl))
            {
                using (var webClient = new WebClient())
                {
                    webClient.DownloadString(keepAliveUrl);
                }

                return true;
            }

            logThisAtHistory = "No Keep Alive Url Found";
            return false;
        }

        public class AcKeepAliveTaskSettings
        {
        }
    }
}
