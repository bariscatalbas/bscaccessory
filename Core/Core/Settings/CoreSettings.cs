﻿using Core.Mvc.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Settings
{
    public class CoreSettings : ISetting
    {
        [UIHint("dropdown")]
        [AcDropdownParameter("AuditProviderList")]
        [DisplayName("Audit Provider")]
        public string AuditProviderCode { get; set; }

        [UIHint("dropdown")]
        [AcDropdownParameter("MediaProviderList")]
        [DisplayName("Media Provider")]
        public string MediaProviderCode { get; set; }

        [UIHint("dropdown")]
        [AcDropdownParameter("LogProviderList")]
        [DisplayName("Log Provider")]
        public string LogProviderCode { get; set; }

        [UIHint("dropdown")]
        [AcDropdownParameter("EmailProviderList")]
        [DisplayName("Email Provider")]
        public string EmailProviderCode { get; set; }

        [UIHint("dropdown")]
        [AcDropdownParameter("SmsProviderList")]
        [DisplayName("Sms Provider")]
        public string SmsProviderCode { get; set; }

        [UIHint("dropdown")]
        [AcDropdownParameter("TaskProviderList")]
        [DisplayName("Task Provider")]
        public string TaskProviderCode { get; set; }

    }
}
