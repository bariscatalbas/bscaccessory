﻿using Core.Cache;
using Core.Data.Interfaces;
using Core.Ioc;
using System;
using System.ComponentModel;
using System.Linq;

namespace Core.Settings
{
    public class AcSettingsHelper : IInjectAsSelf
    {
        private readonly IRepository<AcSetting> _settingRepository;

        public AcSettingsHelper(IRepository<AcSetting> settingRepository)
        {
            _settingRepository = settingRepository;
        }

        public TSetting LoadSettings<TSetting>() where TSetting : class, ISetting
        {
            var settings = Activator.CreateInstance<TSetting>();
            var keys = typeof(TSetting).GetProperties()
                                        .Where(p => p.CanRead && p.CanWrite)
                                        .Select(p => (typeof(TSetting).Name + "." + p.Name).ToLowerInvariant())
                                        .ToList();

            var foundSettings = _settingRepository
                                .Table
                                .Where(p => keys.Contains(p.Name))
                                .ToList();

            foreach (var prop in typeof(TSetting).GetProperties())
            {
                if (!prop.CanRead || !prop.CanWrite)
                    continue;

                var key = (typeof(TSetting).Name + "." + prop.Name).ToLowerInvariant();
                var setting = foundSettings.FirstOrDefault(p => p.Name == key.ToLowerInvariant());
                if (setting == null)
                    continue;

                if (!TypeDescriptor.GetConverter(prop.PropertyType).CanConvertFrom(typeof(string)))
                    continue;

                if (!TypeDescriptor.GetConverter(prop.PropertyType).IsValid(setting.Value))
                    continue;

                var value = TypeDescriptor.GetConverter(prop.PropertyType).ConvertFromInvariantString(setting.Value);
                prop.SetValue(settings, value, null);
            }

            return settings;
        }

        public void SaveSettings<TSetting>(TSetting setting, long identityId) where TSetting : class, ISetting
        {
            foreach (var prop in typeof(TSetting).GetProperties())
            {
                if (!prop.CanRead || !prop.CanWrite)
                    continue;

                if (!TypeDescriptor.GetConverter(prop.PropertyType).CanConvertFrom(typeof(string)))
                    continue;

                var key = typeof(TSetting).Name + "." + prop.Name;
                dynamic value = prop.GetValue(setting, null);
                Set(key, value ?? "", identityId);
            }
        }

        public TSetting LoadSettingsCached<TSetting>() where TSetting : class, ISetting
        {
            var acCacheHelper = AcEngine.Resolve<AcCacheHelper>();
            var cacheKey = typeof(TSetting).Name.ToLowerInvariant();
            return acCacheHelper.GetVersioned<TSetting>(cacheKey, () => { return LoadSettings<TSetting>(); });
        }

        public void SaveSettingsCached<TSetting>(TSetting setting, long identityId) where TSetting : class, ISetting
        {
            SaveSettings<TSetting>(setting, identityId);

            var acCacheHelper = AcEngine.Resolve<AcCacheHelper>();
            var cacheKey = typeof(TSetting).Name.ToLowerInvariant();
            acCacheHelper.VersionUpdated(cacheKey, identityId);
        }

        private void Set<T>(string key, T value, long identityId)
        {
            if (key == null) return;

            key = key.Trim().ToLowerInvariant();
            var valueStr = TypeDescriptor.GetConverter(typeof(T)).ConvertToInvariantString(value);

            var setting = _settingRepository.Table.FirstOrDefault(p => p.Name == key);
            if (setting != null)
            {
                setting.Value = valueStr;
                _settingRepository.Update(setting, identityId);
            }
            else
            {
                setting = new AcSetting(key, valueStr);
                _settingRepository.Insert(setting, identityId);
            }
        }
    }
}
