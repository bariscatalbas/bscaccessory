﻿using Core.Data.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Settings
{
    public class AcSetting : EntityBase
    {
        public AcSetting()
        {
        }

        public AcSetting(string name, string value)
        {
            Name = name;
            Value = value;
        }

        [MaxLength(250)]
        public virtual string Name { get; set; }
        public virtual string Value { get; set; }

        public override string ToString()
        {
            return Name;
        }

    }
}
