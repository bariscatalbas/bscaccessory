﻿using System;
using System.Collections.Generic;

namespace Core.Ioc
{
    public interface IDependencyRegistrar
    {
        IList<Tuple<Type, Type>> TypesToRegister(ITypeFinder typeFinder);
        IList<Tuple<Type, Type>> TypesToRegisterGeneric(ITypeFinder typeFinder);
        IList<Tuple<Type, Type>> TypesToRegisterPerRequest(ITypeFinder typeFinder);

        int Order { get; }

    }
}
