﻿namespace Core.Ioc.Helpers
{
    internal enum ComponentLifeStyle
    {
        Singleton = 0,
        Transient = 1,
        LifetimeScope = 2
    }
}
