﻿using Autofac;
using System;
using System.Web;

namespace Core.Ioc.Helpers
{
    internal class AutofacRequestLifetimeHttpModule : IHttpModule
    {
        public static readonly object HttpRequestTag = "AutofacWebRequest";

        private static ILifetimeScope LifetimeScope
        {
            get { return (ILifetimeScope)HttpContext.Current.Items[typeof(ILifetimeScope)]; }
            set { HttpContext.Current.Items[typeof(ILifetimeScope)] = value; }
        }

        public void Init(HttpApplication context)
        {
            context.EndRequest += ContextEndRequest;
        }

        public void Dispose()
        {
        }

        private static ILifetimeScope InitializeLifetimeScope(Action<ContainerBuilder> configurationAction,
            ILifetimeScope container)
        {
            return configurationAction == null
                ? container.BeginLifetimeScope(HttpRequestTag)
                : container.BeginLifetimeScope(HttpRequestTag, configurationAction);
        }

        public static ILifetimeScope GetLifetimeScope(ILifetimeScope container,
            Action<ContainerBuilder> configurationAction)
        {
            if (HttpContext.Current != null)
                return LifetimeScope ?? (LifetimeScope = InitializeLifetimeScope(configurationAction, container));

            return InitializeLifetimeScope(configurationAction, container);
        }

        public static void ContextEndRequest(object sender, EventArgs e)
        {
            ILifetimeScope lifetimeScope = LifetimeScope;
            if (lifetimeScope != null)
                lifetimeScope.Dispose();
        }
    }
}
