﻿using Core.Data.Interfaces;
using Core.Data.Services;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Ioc.Registrars
{
    public class DataDependencyRegistrar : IDependencyRegistrar
    {
        public int Order => 0;

        public IList<Tuple<Type, Type>> TypesToRegister(ITypeFinder typeFinder)
        {
            var injectAsDbContext = typeFinder.FindClassesOfType<IInjectAsDbContext>().FirstOrDefault();
            if (injectAsDbContext != null)
            {
                return new List<Tuple<Type, Type>>
                {
                    new Tuple<Type, Type>(injectAsDbContext, typeof(IInjectAsDbContext)),
                    new Tuple<Type, Type>(injectAsDbContext, typeof(DbContext)),
                    new Tuple<Type, Type>(injectAsDbContext, injectAsDbContext)
                };
            }
            return new List<Tuple<Type, Type>>();
        }

        public IList<Tuple<Type, Type>> TypesToRegisterGeneric(ITypeFinder typeFinder)
        {
            return new List<Tuple<Type, Type>>
            {
                new Tuple<Type, Type>(typeof(AcRepository<>), typeof(IRepository<>)),
            };
        }

        public IList<Tuple<Type, Type>> TypesToRegisterPerRequest(ITypeFinder typeFinder)
        {
            return new List<Tuple<Type, Type>>();
        }
    }
}
