﻿using Autofac;
using Autofac.Integration.Mvc;
using System.Linq;
using System.Web.Mvc;

namespace Core.Ioc.Mvc
{
    public class AcIocMvcEngine : AcIocEngine
    {

        public override void AdditionalConfiguration(ContainerBuilder builder, ITypeFinder typeFinder)
        {
            builder.RegisterControllers(typeFinder.GetAssemblies().ToArray());
        }

        public override void InitializeAll()
        {
            AcEngine.SetEngine(this);

            var dependencyResolver = new MvcDependencyResolver();
            DependencyResolver.SetResolver(dependencyResolver);
        }

    }
}
