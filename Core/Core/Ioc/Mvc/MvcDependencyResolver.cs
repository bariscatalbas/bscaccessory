﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Core.Ioc.Mvc
{
    public class MvcDependencyResolver : IDependencyResolver
    {
        public object GetService(Type serviceType)
        {
            return AcEngine.GetService(serviceType);
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            return AcEngine.GetServices(serviceType);
        }
    }
}
