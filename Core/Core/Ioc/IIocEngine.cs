﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Ioc
{
    public interface IIocEngine
    {
        T Resolve<T>() where T : class;
        object GetService(Type serviceType);
        IEnumerable<object> GetServices(Type serviceType);
        IEnumerable<Type> FindClassesOfType<T>();
    }
}
