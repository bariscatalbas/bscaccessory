﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.Dependencies;

namespace Core.Ioc.Api
{
    public class ConfigurationDependencyResolver : IDependencyResolver
    {
        public IDependencyScope BeginScope()
        {
            return new ConfigurationDependencyScope();
        }

        public void Dispose()
        {

        }

        public object GetService(Type serviceType)
        {
            return AcEngine.GetService(serviceType);
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            return AcEngine.GetServices(serviceType);
        }
    }
}
