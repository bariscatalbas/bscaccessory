﻿using Autofac;
using Autofac.Builder;
using Core.Ioc.Helpers;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Web;


namespace Core.Ioc
{
    public class AcIocEngine : IIocEngine
    {
        private readonly IContainer _container;
        private ContainerBuilder builder;

        public AcIocEngine()
        {
            builder = new ContainerBuilder();
            Configure();
            InitializeAll();
            _container = builder.Build();
        }

        public IEnumerable<Type> FindClassesOfType<T>()
        {
            var typeFinder = GetTypeFinder();
            return typeFinder.FindClassesOfType<T>();
        }

        private ITypeFinder GetTypeFinder()
        {

            return new WebAppTypeFinder();

            //if (HttpContext.Current != null)
            //{
            //    return new WebAppTypeFinder();
            //}
            //else
            //{
            //    return new AppDomainTypeFinder();
            //}
        }
        private void Configure()
        {
            var typeFinder = GetTypeFinder();
            //UpdateContainer(builder =>
            //{
                var injectsAsSelf = typeFinder.FindClassesOfType<IInjectAsSelf>();
                foreach (var item in injectsAsSelf)
                {
                    builder.RegisterType(item).AsSelf().InstancePerRequest();
                }

                var injectsAsInterfacePerRequest = typeFinder.FindClassesOfType<IInjectAsInterface>();
                foreach (var item in injectsAsInterfacePerRequest)
                {
                    var targetInterfaces = item.GetInterfaces().Where(p => p != typeof(IInjectAsInterface) && p != typeof(IInjectAsSelf));
                    foreach (var targetInterface in targetInterfaces)
                    {
                        builder.RegisterType(item).As(targetInterface).InstancePerRequest();
                    }
                }

                var dependencyRegistrars = typeFinder.FindClassesOfType<IDependencyRegistrar>();
                var dependencyRegistrarInstances = new List<IDependencyRegistrar>();
                foreach (var dependencyRegistrar in dependencyRegistrars)
                {
                    dependencyRegistrarInstances.Add((IDependencyRegistrar)Activator.CreateInstance(dependencyRegistrar));
                }

                dependencyRegistrarInstances = dependencyRegistrarInstances.OrderBy(t => t.Order).ToList();
                foreach (var dependencyRegistrar in dependencyRegistrarInstances)
                {
                    foreach (var generic in dependencyRegistrar.TypesToRegisterGeneric(typeFinder))
                    {
                        builder.RegisterGeneric(generic.Item1).As(generic.Item2).InstancePerRequest();
                    }

                    foreach (var generic in dependencyRegistrar.TypesToRegisterPerRequest(typeFinder))
                    {
                        builder.RegisterType(generic.Item1).As(generic.Item2).InstancePerRequest();
                    }

                    foreach (var generic in dependencyRegistrar.TypesToRegister(typeFinder))
                    {
                        builder.RegisterType(generic.Item1).As(generic.Item2);
                    }
                }

                AdditionalConfiguration(builder, typeFinder);
            //});
        }

        public virtual void AdditionalConfiguration(ContainerBuilder builder, ITypeFinder typeFinder)
        {

        }
        public virtual void InitializeAll()
        {

        }

        public object GetService(Type serviceType)
        {
            var scope = Scope();
            return scope.ResolveOptional(serviceType);
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            Type type = typeof(IEnumerable<>).MakeGenericType(serviceType);
            return (IEnumerable<object>)Resolve(type);
        }

        public T Resolve<T>() where T : class
        {
            return Resolve<T>("");
        }

        #region Resolver Functions

        private T Resolve<T>(string key, ILifetimeScope scope = null) where T : class
        {
            if (scope == null)
            {
                //no scope specified
                scope = Scope();
            }

            if (string.IsNullOrEmpty(key))
            {
                var isRegistered = scope.IsRegistered<T>();
                return isRegistered ? scope.Resolve<T>() : ResolveUnregistered<T>(scope);
            }

            return scope.ResolveKeyed<T>(key);
        }

        private object Resolve(Type type, ILifetimeScope scope = null)
        {
            if (scope == null)
            {
                //no scope specified
                scope = Scope();
            }

            var isRegistered = scope.IsRegistered(type);
            return isRegistered ? scope.Resolve(type) : ResolveUnregistered(type, scope);
        }

        private T[] ResolveAll<T>(string key = "", ILifetimeScope scope = null)
        {
            if (scope == null)
            {
                //no scope specified
                scope = Scope();
            }
            if (string.IsNullOrEmpty(key))
            {
                return scope.Resolve<IEnumerable<T>>().ToArray();
            }
            return scope.ResolveKeyed<IEnumerable<T>>(key).ToArray();
        }

        private T ResolveUnregistered<T>(ILifetimeScope scope = null) where T : class
        {
            return ResolveUnregistered(typeof(T), scope) as T;
        }

        private object ResolveUnregistered(Type type, ILifetimeScope scope = null)
        {
            if (scope == null)
            {
                //no scope specified
                scope = Scope();
            }
            ConstructorInfo[] constructors = type.GetConstructors();
            foreach (ConstructorInfo constructor in constructors)
            {
                try
                {
                    ParameterInfo[] parameters = constructor.GetParameters();
                    var parameterInstances = new List<object>();
                    foreach (ParameterInfo parameter in parameters)
                    {
                        object service = Resolve(parameter.ParameterType, scope);
                        if (service == null) throw new Exception("Unkown dependency");
                        parameterInstances.Add(service);
                    }
                    return Activator.CreateInstance(type, parameterInstances.ToArray());
                }
                catch (Exception exception)
                {
                    Debug.WriteLine(exception.Message);
                }
            }
            return null;
            //throw new Exception("No contructor was found that had all the dependencies satisfied.");
        }

        private bool TryResolve(Type serviceType, out object instance)
        {
            var scope = Scope();
            return scope.TryResolve(serviceType, out instance);
        }

        private bool TryResolve(Type serviceType, ILifetimeScope scope, out object instance)
        {
            if (scope == null)
            {
                //no scope specified
                scope = Scope();
            }
            return scope.TryResolve(serviceType, out instance);
        }

        private bool IsRegistered(Type serviceType, ILifetimeScope scope = null)
        {
            if (scope == null)
            {
                //no scope specified
                scope = Scope();
            }
            return scope.IsRegistered(serviceType);
        }

        private object ResolveOptional(Type serviceType, ILifetimeScope scope = null)
        {
            if (scope == null)
            {
                //no scope specified
                scope = Scope();
            }

            return scope.ResolveOptional(serviceType);
        }

        private ILifetimeScope Scope()
        {
            try
            {
                return AutofacRequestLifetimeHttpModule.GetLifetimeScope(_container, null);
            }
            catch
            {
                return _container;
            }
        }

        #endregion

        #region Add Component Functions

        private void AddComponent<TService>(string key = "", ComponentLifeStyle lifeStyle = ComponentLifeStyle.Singleton)
        {
            AddComponent<TService, TService>(key, lifeStyle);
        }

        private void AddComponent(Type service, string key = "",
            ComponentLifeStyle lifeStyle = ComponentLifeStyle.Singleton)
        {
            AddComponent(service, service, key, lifeStyle);
        }

        private void AddComponent<TService, TImplementation>(string key = "",
            ComponentLifeStyle lifeStyle = ComponentLifeStyle.Singleton)
        {
            AddComponent(typeof(TService), typeof(TImplementation), key, lifeStyle);
        }

        private void AddComponent(Type service, Type implementation, string key = "",
            ComponentLifeStyle lifeStyle = ComponentLifeStyle.Singleton)
        {
            //UpdateContainer(x =>
            //{
                var serviceTypes = new List<Type> { service };

                if (service.IsGenericType)
                {
                    IRegistrationBuilder<object, ReflectionActivatorData, DynamicRegistrationStyle> temp = builder
                        .RegisterGeneric(implementation).As(
                            serviceTypes.ToArray()).PerLifeStyle(lifeStyle);
                    if (!string.IsNullOrEmpty(key))
                    {
                        temp.Keyed(key, service);
                    }
                }
                else
                {
                    IRegistrationBuilder<object, ConcreteReflectionActivatorData, SingleRegistrationStyle> temp = builder
                        .RegisterType(implementation).As(
                            serviceTypes.ToArray()).PerLifeStyle(lifeStyle);
                    if (!string.IsNullOrEmpty(key))
                    {
                        temp.Keyed(key, service);
                    }
                }
            //});
        }

        private void AddComponentInstance<TService>(object instance, string key = "",
            ComponentLifeStyle lifeStyle = ComponentLifeStyle.Singleton)
        {
            AddComponentInstance(typeof(TService), instance, key, lifeStyle);
        }

        private void AddComponentInstance(Type service, object instance, string key = "",
            ComponentLifeStyle lifeStyle = ComponentLifeStyle.Singleton)
        {
            //UpdateContainer(x => x.RegisterInstance(instance).Keyed(key, service).As(service).PerLifeStyle(lifeStyle));
            builder.RegisterInstance(instance).Keyed(key, service).As(service).PerLifeStyle(lifeStyle);
        }

        private void AddComponentInstance(object instance, string key = "",
            ComponentLifeStyle lifeStyle = ComponentLifeStyle.Singleton)
        {
            AddComponentInstance(instance.GetType(), instance, key, lifeStyle);
        }

        private void AddComponentWithParameters<TService, TImplementation>(IDictionary<string, string> properties,
            string key = "", ComponentLifeStyle lifeStyle = ComponentLifeStyle.Singleton)
        {
            AddComponentWithParameters(typeof(TService), typeof(TImplementation), properties);
        }

        private void AddComponentWithParameters(Type service, Type implementation, IDictionary<string, string> properties,
            string key = "", ComponentLifeStyle lifeStyle = ComponentLifeStyle.Singleton)
        {
            //UpdateContainer(x =>
            //{
                var serviceTypes = new List<Type> { service };

                IRegistrationBuilder<object, ConcreteReflectionActivatorData, SingleRegistrationStyle> temp = builder
                    .RegisterType(implementation).As(serviceTypes.ToArray()).
                    WithParameters(properties.Select(y => new NamedParameter(y.Key, y.Value)));
                if (!string.IsNullOrEmpty(key))
                {
                    temp.Keyed(key, service);
                }

            //});
        }

        #endregion
    }
}
