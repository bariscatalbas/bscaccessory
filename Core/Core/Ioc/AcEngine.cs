﻿using System;
using System.Collections.Generic;

namespace Core.Ioc
{
    public static class AcEngine
    {
        private static IIocEngine Engine;

        public static void SetEngine(IIocEngine engine)
        {
            Engine = engine;
        }

        public static T Resolve<T>() where T : class
        {
            return Engine.Resolve<T>();
        }

        public static object GetService(Type serviceType)
        {
            return Engine.GetService(serviceType);
        }

        public static IEnumerable<object> GetServices(Type serviceType)
        {
            return Engine.GetServices(serviceType);
        }

        public static IEnumerable<Type> FindClassesOfType<T>()
        {
            return Engine.FindClassesOfType<T>();
        }

    }
}
