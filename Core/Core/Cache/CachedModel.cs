﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Cache
{
    internal class CachedModel<T> where T : class
    {
        public int Version { get; set; }
        public T Data { get; set; }
    }
}
