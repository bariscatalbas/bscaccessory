﻿using Core.Data.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Cache
{
    public class AcCacheVersion : EntityBase
    {
        [MaxLength(500)]
        public virtual string Key { get; set; }

        public virtual DateTime CreatedUtc { get; set; }
        public virtual int Version { get; set; }
    }
}
