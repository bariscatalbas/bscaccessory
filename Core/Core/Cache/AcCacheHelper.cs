﻿using Core.Data.Interfaces;
using Core.Ioc;
using System;
using System.Linq;
using System.Runtime.Caching;
using System.Web;

namespace Core.Cache
{
    public class AcCacheHelper : IInjectAsSelf
    {
        private readonly IRepository<AcCacheVersion> _cacheVersionRepository;

        public AcCacheHelper(IRepository<AcCacheVersion> cacheVersionRepository)
        {
            _cacheVersionRepository = cacheVersionRepository;
        }

        public T Get<T>(string cacheKey, Func<T> getItemCallback, int seconds = 86400, bool skipCache = false) where T : class
        {
            if (!skipCache)
            {
                T item = MemoryCache.Default.Get(cacheKey) as T;
                if (item == null)
                {
                    item = getItemCallback();
                    MemoryCache.Default.Set(cacheKey, item, DateTime.Now.AddSeconds(seconds));
                }
                return item;
            }

            return getItemCallback();
        }

        public T GetVersioned<T>(string cacheKey, Func<T> getItemCallback, bool skipCache = false) where T : class
        {
            if (!skipCache)
            {
                int version = (MemoryCache.Default.Get("VERSION_" + cacheKey) as int?) ?? 0;
                if (version == 0)
                {
                    version = _cacheVersionRepository.Table.Where(p => p.Key == cacheKey).Select(p => p.Version).FirstOrDefault();
                    MemoryCache.Default.Set("VERSION_" + cacheKey, version, DateTime.Now.AddSeconds(10));
                }

                var cachedItem = MemoryCache.Default.Get(cacheKey) as CachedModel<T>;
                if (cachedItem == null || cachedItem.Version < version || version == 0)
                {
                    var result = getItemCallback();
                    cachedItem = new CachedModel<T>
                    {
                        Version = version,
                        Data = result
                    };

                    MemoryCache.Default.Set(cacheKey, cachedItem, DateTime.Now.AddDays(1));
                }

                return cachedItem.Data;
            }
            return getItemCallback();
        }

        public void VersionUpdated(string cacheKey, long identityId)
        {
            var version = _cacheVersionRepository.Table.FirstOrDefault(p => p.Key == cacheKey);
            if (version != null)
            {
                version.Version++;
                _cacheVersionRepository.Update(version, identityId);
            }
            else
            {
                version = new AcCacheVersion
                {
                    Key = cacheKey,
                    Version = 1,
                    CreatedUtc = DateTime.UtcNow
                };
                _cacheVersionRepository.Insert(version, identityId);
            }
        }

        public void VersionRemoved(string cacheKey, long identityId)
        {
            var version = _cacheVersionRepository.Table.FirstOrDefault(p => p.Key == cacheKey);
            if (version != null)
            {
                _cacheVersionRepository.Delete(version, identityId);
            }
        }

        public void SetPerRequest<T>(string cacheKey, T item) where T : class
        {
            HttpContext.Current.Items[cacheKey] = item;
        }

        public T GetPerRequest<T>(string cacheKey, Func<T> getItemCallback) where T : class
        {
            T item = HttpContext.Current.Items[cacheKey] as T;
            if (item == null)
            {
                item = getItemCallback();
                HttpContext.Current.Items[cacheKey] = item;
            }
            return item;
        }

        public T SetPerRequest<T>(string cacheKey, Func<T> getItemCallback) where T : class
        {
            var item = getItemCallback();
            HttpContext.Current.Items[cacheKey] = item;
            return item;
        }

        public void RemovePerRequest(string cacheKey)
        {
            if (HttpContext.Current.Items.Contains(cacheKey))
            {
                HttpContext.Current.Items.Remove(cacheKey);
            }
        }
    }
}
