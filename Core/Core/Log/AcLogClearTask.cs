﻿using Core.Provider;
using Core.Task;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace Core.Log
{
    public class AcLogClearTask : AcTaskBase
    {
        public AcLogClearTask()
        {
        }

        public override string Code => "AcLogClearTask";

        public override string Name => "Ac Log Clear Task";

        public override string SettingsSystemName { get { return typeof(AcLogClearTaskSettings).AssemblyQualifiedName; } }

        private AcLogClearTaskSettings Settings { get; set; }

        public override bool CanIStartLocal(TaskPrepareModel task, out string logThisAtHistory)
        {
            logThisAtHistory = "";
            Settings = JsonConvert.DeserializeObject<AcLogClearTaskSettings>(task.SettingsJson);

            if (!string.IsNullOrWhiteSpace(Settings.RunningHours))
            {
                List<int> runningHours = Settings.RunningHours.Split(',').Select(p => _.To.Int(p.Trim(), -1)).ToList();

                if (runningHours.Contains(DateTime.UtcNow.Hour))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

            return true;
        }

        public override bool Execute(TaskPrepareModel task, out string logThisAtHistory)
        {
            logThisAtHistory = "";
            var utcNow = DateTime.UtcNow;
            var deleteBeforeUtc = utcNow.AddMonths(-1 * Settings.MonthsToKeepLogs);

            var code = AcProviderHelper.GetCoreSettings().LogProviderCode;
            var provider = AcProviderHelper.GetProvider<ILogProvider>("log", code);
            provider.ClearOldLogs(deleteBeforeUtc);

            return true;
        }

        public class AcLogClearTaskSettings
        {
            [DisplayName("Running Hours (In UTC, comma seperated)")]
            public string RunningHours { get; set; }

            [DisplayName("How many months to keep logs?")]
            public int MonthsToKeepLogs { get; set; }
        }
    }
}
