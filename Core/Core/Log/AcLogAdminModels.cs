﻿using Core.Data.Models;
using Core.Pagination;
using System;
using System.ComponentModel;

namespace Core.Log
{
    public class AcLogAdminModelsDetails : EntityBase
    {
        public DateTime DateUtc { get; set; }

        public string Server { get; set; }
        public string WhereAmI { get; set; }
        public string WhichAppAmI { get; set; }
        public string Level { get; set; }

        public string Module { get; set; }
        public string Action { get; set; }
        public string Function { get; set; }

        public long IdentityId { get; set; }

        public string Searchable { get; set; }
        public string Request { get; set; }
        public string Response { get; set; }
        public string Details { get; set; }

        public string Url { get; set; }
        public string ReferrerUrl { get; set; }
        public string Browser { get; set; }
        public string UserAgent { get; set; }
        public string HttpMethod { get; set; }
        public string UserHostAddress { get; set; }
        public string UserHostName { get; set; }
    }
    
    public class AcLogAdminModelsList : EntityBase
    {
        public DateTime DateUtc { get; set; }
        public string Module { get; set; }
        public string Action { get; set; }
    }

    public partial class AcLogAdminModelsFilter : AcFilter
    {
        [DisplayName("Start Date")]
        public DateTime? StartDate { get; set; }

        [DisplayName("End Date")]
        public DateTime? EndDate { get; set; }

        [DisplayName("Module")]
        public string Module { get; set; }
    }
}
