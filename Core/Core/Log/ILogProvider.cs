﻿using Core.Data.Models;
using Core.Pagination;
using Core.Provider;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Log
{
    public interface ILogProvider : IProvider
    {
        void LogImportant<TIdentity, TRequest, TResponse>(string module, string action, string function, TIdentity identity, string searchable,
            TRequest request, TResponse response, string details)
            where TIdentity : EntityBase
            where TRequest : class
            where TResponse : class;

        void LogInfo<TIdentity, TRequest, TResponse>(string module, string action, string function, TIdentity identity, string searchable,
        TRequest request, TResponse response, string details)
            where TIdentity : EntityBase
            where TRequest : class
            where TResponse : class;

        void LogWarning<TIdentity, TRequest, TResponse>(string module, string action, string function, TIdentity identity, string searchable,
        TRequest request, TResponse response, string details)
            where TIdentity : EntityBase
            where TRequest : class
            where TResponse : class;

        void LogAction<TIdentity, TRequest, TResponse>(string module, string action, string function, TIdentity identity, string searchable,
        TRequest request, TResponse response, string details)
            where TIdentity : EntityBase
            where TRequest : class
            where TResponse : class;

        void LogError<TIdentity, TRequest, TResponse>(string module, string action, string function, TIdentity identity, string searchable,
            TRequest request, TResponse response, string details)
            where TIdentity : EntityBase
            where TRequest : class
            where TResponse : class;

        void LogError<TIdentity, TRequest, TResponse>(string module, string action, string function, TIdentity identity, string searchable,
            TRequest request, TResponse response, Exception exception)
            where TIdentity : EntityBase
            where TRequest : class
            where TResponse : class;


        void ClearOldLogs(DateTime deleteBeforeUtc);

        AcPager<AcLogAdminModelsList> GetLogs(AcLogAdminModelsFilter filter);
        AcLogAdminModelsDetails GetLogDetails(long id);

    }
}
