﻿using System.Web.Mvc;

namespace Core.Mvc.Models
{
    public class ModuleWrapper
    {
        private readonly ViewDataDictionary _dictionary;
        public ModuleWrapper(ViewDataDictionary dictionary)
        {
            _dictionary = dictionary;
        }

        public string MainKey
        {
            get { return (_dictionary["__MODULE_MAIN_KEY__"] ?? "").ToString(); }
            set { _dictionary["__MODULE_MAIN_KEY__"] = value; }
        }
        public string SubKey
        {
            get { return (_dictionary["__MODULE_SUB_KEY__"] ?? "").ToString(); }
            set { _dictionary["__MODULE_SUB_KEY__"] = value; }
        }
        public string Title
        {
            get { return (_dictionary["__MODULE_TITLE__"] ?? "").ToString(); }
            set { _dictionary["__MODULE_TITLE__"] = value; }
        }
        public string Url
        {
            get { return (_dictionary["__MODULE_URL__"] ?? "").ToString(); }
            set { _dictionary["__MODULE_URL__"] = value; }
        }
    }

    public class SetModuleAttribute : ActionFilterAttribute
    {
        private readonly string _mainKey;
        private readonly string _subKey;
        private readonly string _moduleTitle;

        public SetModuleAttribute(string mainKey, string subKey, string moduleTitle)
        {
            _mainKey = mainKey;
            _subKey = subKey;
            _moduleTitle = moduleTitle;
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            filterContext.Controller.ViewData["__MODULE_MAIN_KEY__"] = _mainKey;
            filterContext.Controller.ViewData["__MODULE_SUB_KEY__"] = _subKey;
            filterContext.Controller.ViewData["__MODULE_TITLE__"] = _moduleTitle;

            base.OnActionExecuting(filterContext);
        }

    }
}
