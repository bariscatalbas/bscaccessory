﻿using System.Web.Mvc;

namespace Core.Mvc.Models
{
    public class MessageWrapper
    {
        private readonly TempDataDictionary _tempDictionary;

        public MessageWrapper(TempDataDictionary tempDictionary)
        {
            _tempDictionary = tempDictionary;
        }

        public bool HasSuccess { get { return !string.IsNullOrWhiteSpace(Success); } }
        public bool HasError { get { return !string.IsNullOrWhiteSpace(Error); } }

        public string Success
        {
            get { return (_tempDictionary["__SUCCESS_MESSAGE__"] ?? "").ToString(); }
            set { _tempDictionary["__SUCCESS_MESSAGE__"] = value; }
        }
        public string Error
        {
            get { return (_tempDictionary["__ERROR_MESSAGE__"] ?? "").ToString(); }
            set { _tempDictionary["__ERROR_MESSAGE__"] = value; }
        }
    }
}
