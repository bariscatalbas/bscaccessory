﻿using Core.Mvc.Models;
using System.Web.Mvc;

namespace Core.Mvc.Base
{
    public abstract class MvcViewBase<T> : WebViewPage<T>
    {
        public string ControllerName { get { return ViewContext.RouteData.Values["controller"].ToString(); } }
        public string ActionName { get { return ViewContext.RouteData.Values["action"].ToString(); } }
        
        private MessageWrapper _messageWrapper;
        public MessageWrapper Message { get { return _messageWrapper ?? (_messageWrapper = new MessageWrapper(TempData)); } }
        private ModuleWrapper _moduleWrapper;
        public ModuleWrapper Module { get { return _moduleWrapper ?? (_moduleWrapper = new ModuleWrapper(ViewData)); } }

        public TK GetValue<TK>(string name, TK defaultValue)
        {
            try
            {
                var value = ViewContext.Controller.ValueProvider.GetValue(name);
                if (value != null)
                {
                    return (TK)value.ConvertTo(typeof(TK));
                }
            }
            catch
            {
            }
            return defaultValue;
        }

    }
}
