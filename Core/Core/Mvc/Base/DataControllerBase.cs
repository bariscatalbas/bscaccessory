﻿using ClosedXML.Excel;
using Core.Data.Models;
using Core.Ioc;
using Core.Media;
using Core.Mvc.Attributes;
using Core.Mvc.Extensions;
using Core.Mvc.Services;
using Core.Pagination;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web.Mvc;

namespace Core.Mvc.Base
{
    public abstract class DataControllerBase<TDomain, TCreateModel, TEditModel, TViewModel, TFilterModel, TExcelExportModel> : MvcControllerBase
        where TDomain : EntityBase, new()
        where TCreateModel : EntityBase, new()
        where TEditModel : EntityBase, new()
        where TViewModel : EntityBase, new()
        where TFilterModel : AcFilter, new()
        where TExcelExportModel : EntityBase, new()
    {
        private readonly EntityBase _identity;
        public readonly DataServiceBase<TDomain, TCreateModel, TEditModel, TViewModel, TFilterModel, TExcelExportModel> _service;

        public DataControllerBase(DataServiceBase<TDomain, TCreateModel, TEditModel, TViewModel, TFilterModel, TExcelExportModel> service, EntityBase identity)
        {
            _service = service;
            _identity = identity;
        }

        public virtual ActionResult Index(TFilterModel filter)
        {
            if (filter.Export)
            {
                filter.PageNumber = 1;
                filter.PageSize = int.MaxValue;

                var items = _service.ExportIndex(filter);
                ModifyIndexExportData(items);

                DataTable datatable = new DataTable();

                var selectedExportProperties = filter.SelectedExportProperties.Split(',').ToList();
                var selectedExportPropertyNames = selectedExportProperties.Select(p => p.Split(':')[0]).ToList();
                var selectedExportPropertyDisplayNames = selectedExportProperties.Select(p => p.Split(':')[1]).ToList();
                foreach (var propertyDisplayName in selectedExportPropertyDisplayNames)
                {
                    datatable.Columns.Add(propertyDisplayName);
                }

                if (items.Any())
                {
                    var allProperties = items.FirstOrDefault().GetType().GetProperties();
                    var propertiesOrdered = allProperties.Where(p => selectedExportPropertyNames.Contains(p.Name)).ToList();

                    foreach (var item in items)
                    {
                        DataRow row = datatable.NewRow();
                        foreach (var propertyInfo in propertiesOrdered)
                        {
                            MemberInfo propertyMemberInfo = typeof(TExcelExportModel).GetProperty(propertyInfo.Name);

                            string displayName = "";
                            var displayNameAttribute = propertyMemberInfo.GetCustomAttribute(typeof(DisplayNameAttribute)) as DisplayNameAttribute;
                            if (displayNameAttribute != null)
                            {
                                displayName = displayNameAttribute.DisplayName;
                            }
                            if (string.IsNullOrWhiteSpace(displayName))
                            {
                                displayName = propertyInfo.Name;
                            }

                            row[displayName] = propertyInfo.GetValue(item);
                        }

                        datatable.Rows.Add(row);
                    }
                }

                var fileName = Module.Title + "_" + DateTime.Now.ToString("yyyyMMdd_HHmmss") + ".xlsx";

                var workbook = new XLWorkbook();
                var worksheet = workbook.Worksheets.Add(datatable, "Data");
                worksheet.Columns().AdjustToContents();

                using (MemoryStream stream = new MemoryStream())
                {
                    workbook.SaveAs(stream);

                    return new ExcelResult
                    {
                        FileName = fileName,
                        Stream = stream
                    };
                }
            }

            Module.Url = _.Web.RemovePagingQueryString(Request.Url);
            var model = _service.Index(filter);
            ModifyIndexData(model);
            ViewBag.Data = model;
            LoadViewData(null);
            return View(filter);
        }

        public virtual void ModifyIndexData(AcPager<TViewModel> data) { }
        public virtual void ModifyIndexExportData(AcPager<TExcelExportModel> data) { }


        public virtual void LoadViewData(object objectModel) { }

        [HttpGet]
        public virtual ActionResult Create()
        {
            var model = _service.CreateModel();
            LoadViewData(model);
            return View(model);
        }

        [HttpPost, AcParameterBasedOnFormName("save-continue", "continueEditing")]
        public virtual ActionResult Create(TCreateModel model, bool continueEditing)
        {
            model = ModifyCreate(model);

            UploadMedia(model);
            ModelState.ValidateAndLoadErrors(model, p => p);
            if (ModelState.IsValid)
            {
                var id = _service.Insert(model, _identity.Id);
                model.Id = id;

                Message.Success = "Module created successfully.";
                if (continueEditing)
                {
                    return RedirectToAction("Edit", new { id = id });
                }

                var modifiedReturn = CreateReturn(id);
                if (modifiedReturn != null)
                {
                    return modifiedReturn;
                }

                return RedirectToAction("Index");
            }

            if (!Message.HasError)
            {
                Message.Error = "Please check errors.";
            }
            LoadViewData(model);
            return View(model);
        }

        public virtual ActionResult Edit(long id)
        {
            var model = _service.Single(id);
            if (model == null)
            {
                Message.Error = "Module not found.";
                return RedirectToAction("Index");
            }

            LoadViewData(model);
            LoadMedia(model);
            return View(model);
        }

        [HttpPost, AcParameterBasedOnFormName("save-continue", "continueEditing")]
        public virtual ActionResult Edit(TEditModel model, bool continueEditing)
        {
            model = ModifyEdit(model);

            UploadMedia(model);
            ModelState.ValidateAndLoadErrors(model, p => p);
            if (ModelState.IsValid)
            {
                if (_service.Update(model, _identity.Id))
                {
                    Message.Success = "Module updated successfully.";

                    if (continueEditing)
                    {
                        return RedirectToAction("Edit", new { id = model.Id });
                    }

                    var modifiedReturn = EditReturn(model.Id);
                    if (modifiedReturn != null)
                    {
                        return modifiedReturn;
                    }
                }
                else
                {
                    Message.Error = "Module not found";
                }

                return RedirectToAction("Index");
            }

            if (!Message.HasError)
            {
                Message.Error = "Please check errors.";
            }

            LoadViewData(model);

            LoadMedia(model);
            return View(model);
        }

        [HttpPost]
        public virtual ActionResult Delete(long id)
        {
            var message = _service.IsAllowedToDelete(id);
            if (string.IsNullOrWhiteSpace(message))
            {
                if (_service.Delete(id, _identity.Id))
                {
                    Message.Success = "Module deleted successfully.";
                }
                else
                {
                    Message.Error = "Module not found";
                }
            }
            else
            {
                Message.Error = message;
            }

            var modifiedReturn = DeleteReturn();
            if (modifiedReturn != null)
            {
                return modifiedReturn;
            }

            return RedirectToAction("Index");
        }

        public virtual ActionResult SelectExcelExportColumns()
        {
            List<Tuple<string, string>> model = new List<Tuple<string, string>>();

            var excelExportModel = new TExcelExportModel();

            var properties = excelExportModel.GetType().GetProperties();

            foreach (var propertyInfo in properties)
            {
                var propertyInfoType = propertyInfo.PropertyType;
                var propertyInfoUType = Nullable.GetUnderlyingType(propertyInfoType);
                if (propertyInfoUType != null)
                {
                    propertyInfoType = propertyInfoUType;
                }
                var isPropertyEnum = propertyInfoType.IsEnum;
                if ((_.Prop.GetEnumerableType(propertyInfo.PropertyType) == null || _.Prop.GetEnumerableType(propertyInfo.PropertyType) == typeof(char)) && (propertyInfo.PropertyType.Namespace.StartsWith("System") || isPropertyEnum))
                {
                    MemberInfo propertyMemberInfo = typeof(TExcelExportModel).GetProperty(propertyInfo.Name);

                    bool preventOnExcelExport = false;
                    var preventOnExcelExportAttribute = propertyMemberInfo.GetCustomAttribute(typeof(AcPreventOnExcelExportAttribute)) as AcPreventOnExcelExportAttribute;
                    if (preventOnExcelExportAttribute != null)
                    {
                        preventOnExcelExport = true;
                    }

                    if (!preventOnExcelExport)
                    {
                        string displayName = "";
                        var displayNameAttribute = propertyMemberInfo.GetCustomAttribute(typeof(DisplayNameAttribute)) as DisplayNameAttribute;
                        if (displayNameAttribute != null)
                        {
                            displayName = displayNameAttribute.DisplayName;
                        }
                        if (string.IsNullOrWhiteSpace(displayName))
                        {
                            displayName = propertyInfo.Name;
                        }

                        var propertyName = new Tuple<string, string>(propertyInfo.Name, displayName);
                        model.Add(propertyName);
                    }
                }
            }

            return View(model);
        }

        public virtual TCreateModel ModifyCreate(TCreateModel model) { return model; }
        public virtual TEditModel ModifyEdit(TEditModel model) { return model; }

        public virtual ActionResult CreateReturn(long id) { return null; }
        public virtual ActionResult EditReturn(long id) { return null; }
        public virtual ActionResult DeleteReturn() { return null; }
    }
}
