﻿using Core.Ioc;
using Core.Media;
using Core.Mvc.Extensions;
using Core.Mvc.Models;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;

namespace Core.Mvc.Base
{
    public abstract class MvcControllerBase : Controller
    {
        private readonly AcMediaHelper _mediaHelper;
        public MvcControllerBase()
        {
            _mediaHelper = AcEngine.Resolve<AcMediaHelper>();
        }

        private MessageWrapper _messageWrapper;
        public MessageWrapper Message { get { return _messageWrapper ?? (_messageWrapper = new MessageWrapper(TempData)); } }

        private ModuleWrapper _moduleWrapper;
        public ModuleWrapper Module { get { return _moduleWrapper ?? (_moduleWrapper = new ModuleWrapper(ViewData)); } }

        public TK GetValue<TK>(string name, TK defaultValue)
        {
            try
            {
                var value = ValueProvider.GetValue(name);
                if (value != null)
                {
                    return (TK)value.ConvertTo(typeof(TK));
                }
            }
            catch
            {
            }
            return defaultValue;
        }

        #region Render Partial View To String
        protected string RenderPartialViewToString()
        {
            return RenderPartialViewToString(null, null);
        }
        protected string RenderPartialViewToString(string viewName)
        {
            return RenderPartialViewToString(viewName, null);
        }
        protected string RenderPartialViewToString(object model)
        {
            return RenderPartialViewToString(null, model);
        }
        protected string RenderPartialViewToString(string viewName, object model)
        {
            if (string.IsNullOrEmpty(viewName))
                viewName = ControllerContext.RouteData.GetRequiredString("action");

            ViewData.Model = model;

            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                var viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);

                return sw.GetStringBuilder().ToString();
            }
        }
        protected string RenderPartialViewToString(string viewName, object model, string key, object value)
        {
            if (string.IsNullOrEmpty(viewName))
                viewName = ControllerContext.RouteData.GetRequiredString("action");

            ViewData.Model = model;
            ViewData.Add(key, value);

            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                var viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);
                return sw.GetStringBuilder().ToString();
            }
        }

        #endregion

        #region media

        public virtual void LoadMedia<T>(T model, string itemPrefix = "")
        {
            var properties = model.GetType().GetProperties();
            foreach (var propertyInfo in properties)
            {
                if (typeof(IEnumerable<object>).IsAssignableFrom(propertyInfo.PropertyType))
                {
                    var currentParentName = propertyInfo.Name;
                    var items = (IEnumerable<object>)propertyInfo.GetValue(model);

                    if (items != null && items.Any())
                    {
                        var firstItem = (object)items.FirstOrDefault();
                        var firstItemType = firstItem.GetType();

                        if (!firstItemType.Namespace.StartsWith("System"))
                        {
                            var index = 0;
                            foreach (var item in items)
                            {
                                LoadMedia(item, currentParentName + "_" + index + "__");
                                index++;
                            }
                        }
                    }
                }
                else if (!propertyInfo.PropertyType.Namespace.StartsWith("System"))
                {
                    var currentParentName = propertyInfo.Name;
                    var item = propertyInfo.GetValue(model);
                    LoadMedia(item, currentParentName + "_");
                }

                if (!propertyInfo.IsUiHint("Image"))
                    continue;

                var mediaId = (long?)propertyInfo.GetValue(model);
                if (mediaId.HasValue && mediaId > 0)
                {
                    string url, altText;
                    if (_mediaHelper.LoadMedia(mediaId.Value, out url, out altText))
                    {
                        ViewData[itemPrefix + propertyInfo.Name + "_Url"] = url;
                        ViewData[itemPrefix + propertyInfo.Name + "_AltText"] = altText;
                    }
                }
            }
        }

        public virtual void UploadMedia<T>(T model, string itemPrefix = "")
        {
            var properties = model.GetType().GetProperties();
            foreach (var propertyInfo in properties)
            {
                if (_.Prop.GetEnumerableType(propertyInfo.PropertyType) != null && _.Prop.GetEnumerableType(propertyInfo.PropertyType) != typeof(char))
                {
                    var itemType = _.Prop.GetEnumerableType(propertyInfo.PropertyType);
                    if (!itemType.Namespace.StartsWith("System"))
                    {
                        var currentParentName = propertyInfo.Name;

                        var valuesEnumerator = ((System.Collections.IEnumerable)propertyInfo.GetValue(model)).GetEnumerator();
                        var index = 0;
                        while (valuesEnumerator.MoveNext())
                        {
                            UploadMedia(valuesEnumerator.Current, currentParentName + "_" + index + "__");
                            index++;
                        }
                    }
                }
                else if (!propertyInfo.PropertyType.Namespace.StartsWith("System"))
                {
                    var currentParentName = propertyInfo.Name;
                    var item = propertyInfo.GetValue(model);
                    UploadMedia(item, currentParentName + "_");
                }

                if (!propertyInfo.IsUiHint("Image") && !propertyInfo.IsUiHint("MultipleImage")
                    && !propertyInfo.IsUiHint("Video") && !propertyInfo.IsUiHint("File")
                    && !propertyInfo.IsUiHint("ImageString"))
                    continue;

                var fileInputName = propertyInfo.Name + "_File";

                if (propertyInfo.IsUiHint("Image"))
                {
                    var file = Request.Files[itemPrefix + fileInputName];
                    var module = Request.Form[itemPrefix + propertyInfo.Name + "_Module"];
                    var resize = Request.Form[itemPrefix + propertyInfo.Name + "_Resize"];
                    var thumb = Request.Form[itemPrefix + propertyInfo.Name + "_Thumb"];
                    var sizeValidation = Request.Form[itemPrefix + propertyInfo.Name + "_SizeValidation"];
                    var altText = Request.Form[itemPrefix + propertyInfo.Name + "_AltText"];
                    var isRemove = Request.Form[itemPrefix + propertyInfo.Name + "_Remove"] == "true";
                    if (file != null && file.ContentLength > 0)
                    {
                        var validExtensions = new List<string> { "jpg", "jpeg", "png", "gif", "svg" };
                        if (!validExtensions.Any(file.FileName.ToLowerInvariant().EndsWith))
                        {
                            ModelState.AddModelError(itemPrefix + propertyInfo.Name, "You can only upload (" + string.Join(", ", validExtensions) + ") extensions.");
                            continue;
                        }

                        var mediaId = _mediaHelper.UploadMedia(file, module, altText, 0, thumb, resize, sizeValidation);

                        if (mediaId == -1)
                        {
                            ModelState.AddModelError(itemPrefix + propertyInfo.Name, "You can only upload (" + sizeValidation + ") valid sizes.");
                            continue;
                        }

                        propertyInfo.SetValue(model, mediaId, null);

                        string fieldUrl, fieldAltText;
                        _mediaHelper.LoadMedia(mediaId, out fieldUrl, out fieldAltText);

                        var urlProperty = properties.FirstOrDefault(p => p.Name == itemPrefix + propertyInfo.Name + "_Url");
                        if (urlProperty != null)
                        {
                            urlProperty.SetValue(model, fieldUrl);
                        }

                        var altTextProperty = properties.FirstOrDefault(p => p.Name == itemPrefix + propertyInfo.Name + "_AltText");
                        if (altTextProperty != null)
                        {
                            altTextProperty.SetValue(model, altText);
                        }
                    }
                    else if (isRemove)
                    {
                        propertyInfo.SetValue(model, null, null);
                    }
                }
                else if (propertyInfo.IsUiHint("ImageString"))
                {
                    var file = Request.Files[itemPrefix + fileInputName];
                    var module = Request.Form[itemPrefix + propertyInfo.Name + "_Module"];
                    var isRemove = Request.Form[itemPrefix + propertyInfo.Name + "_Remove"] == "true";
                    if (file != null && file.ContentLength > 0)
                    {
                        var validExtensions = new List<string> { "jpg", "jpeg", "png", "gif", "svg" };
                        if (!validExtensions.Any(file.FileName.ToLowerInvariant().EndsWith))
                        {
                            ModelState.AddModelError(itemPrefix + propertyInfo.Name, "You can only upload (" + string.Join(", ", validExtensions) + ") extensions.");
                            continue;
                        }

                        var uploadedFileName = _mediaHelper.UploadMediaGetFileName(file, module, "", 0, "");
                        propertyInfo.SetValue(model, uploadedFileName, null);
                    }
                    else if (isRemove)
                    {
                        propertyInfo.SetValue(model, null, null);
                    }
                }
                else if (propertyInfo.IsUiHint("MultipleImage"))
                {
                    var mediaIds = new List<long>();
                    var isRemove = Request.Form[itemPrefix + propertyInfo.Name + "_Remove"] == "true";

                    for (int i = 0; i < Request.Files.Count; i++)
                    {
                        if (Request.Files.GetKey(i) == fileInputName)
                        {
                            var file = Request.Files[i];

                            var module = Request.Form[itemPrefix + propertyInfo.Name + "_Module"];
                            var resize = Request.Form[itemPrefix + propertyInfo.Name + "_Resize"];
                            var thumb = Request.Form[itemPrefix + propertyInfo.Name + "_Thumb"];
                            var sizeValidation = Request.Form[itemPrefix + propertyInfo.Name + "_SizeValidation"];
                            var altText = Request.Form[itemPrefix + propertyInfo.Name + "_AltText"];
                            if (file != null && file.ContentLength > 0)
                            {
                                var validExtensions = new List<string> { "jpg", "jpeg", "png", "gif", "svg", "mp4" };
                                if (!validExtensions.Any(file.FileName.ToLowerInvariant().EndsWith))
                                {
                                    ModelState.AddModelError(itemPrefix + propertyInfo.Name, "You can only upload (" + string.Join(", ", validExtensions) + ") extensions.");
                                    continue;
                                }

                                var mediaId = _mediaHelper.UploadMedia(file, module, altText, 0, thumb, resize, sizeValidation);

                                if (mediaId == -1)
                                {
                                    ModelState.AddModelError(itemPrefix + propertyInfo.Name, "You can only upload (" + sizeValidation + ") valid sizes.");
                                    continue;
                                }

                                mediaIds.Add(mediaId);

                                string fieldUrl, fieldAltText;
                                _mediaHelper.LoadMedia(mediaId, out fieldUrl, out fieldAltText);

                                var urlProperty = properties.FirstOrDefault(p => p.Name == itemPrefix + propertyInfo.Name + "_Url");
                                if (urlProperty != null)
                                {
                                    urlProperty.SetValue(model, fieldUrl);
                                }

                                var altTextProperty = properties.FirstOrDefault(p => p.Name == itemPrefix + propertyInfo.Name + "_AltText");
                                if (altTextProperty != null)
                                {
                                    altTextProperty.SetValue(model, altText);
                                }
                            }
                        }
                    }

                    propertyInfo.SetValue(model, mediaIds, null);

                    if (isRemove)
                    {
                        propertyInfo.SetValue(model, null, null);
                    }
                }
                else if (propertyInfo.IsUiHint("Video"))
                {
                    var file = Request.Files[itemPrefix + fileInputName];
                    var module = Request.Form[itemPrefix + propertyInfo.Name + "_Module"];
                    var altText = Request.Form[itemPrefix + propertyInfo.Name + "_AltText"];
                    var isRemove = Request.Form[itemPrefix + propertyInfo.Name + "_Remove"] == "true";
                    if (file != null && file.ContentLength > 0)
                    {
                        var validExtensions = new List<string> { "mp4" };
                        if (!validExtensions.Any(file.FileName.ToLowerInvariant().EndsWith))
                        {
                            ModelState.AddModelError(itemPrefix + propertyInfo.Name, "You can only upload (" + string.Join(", ", validExtensions) + ") extensions.");
                            continue;
                        }

                        var mediaId = _mediaHelper.UploadMedia(file, module, altText, 0);
                        propertyInfo.SetValue(model, mediaId, null);

                        string fieldUrl, fieldAltText;
                        _mediaHelper.LoadMedia(mediaId, out fieldUrl, out fieldAltText);

                        var urlProperty = properties.FirstOrDefault(p => p.Name == itemPrefix + propertyInfo.Name + "_Url");
                        if (urlProperty != null)
                        {
                            urlProperty.SetValue(model, fieldUrl);
                        }

                        var altTextProperty = properties.FirstOrDefault(p => p.Name == itemPrefix + propertyInfo.Name + "_AltText");
                        if (altTextProperty != null)
                        {
                            altTextProperty.SetValue(model, altText);
                        }
                    }
                    else if (isRemove)
                    {
                        propertyInfo.SetValue(model, null, null);
                    }
                }
                else if (propertyInfo.IsUiHint("File"))
                {
                    var file = Request.Files[itemPrefix + fileInputName];
                    var module = Request.Form[itemPrefix + propertyInfo.Name + "_Module"];
                    var altText = Request.Form[itemPrefix + propertyInfo.Name + "_AltText"];
                    var isRemove = Request.Form[itemPrefix + propertyInfo.Name + "_Remove"] == "true";
                    if (file != null && file.ContentLength > 0)
                    {
                        var validExtensions = new List<string> { "pdf", "docx", "doc", "xls", "xlsx", "wav", "mp3" };
                        if (!validExtensions.Any(file.FileName.ToLowerInvariant().EndsWith))
                        {
                            ModelState.AddModelError(itemPrefix + propertyInfo.Name, "You can only upload (" + string.Join(", ", validExtensions) + ") extensions.");
                            continue;
                        }

                        var mediaId = _mediaHelper.UploadMedia(file, module, altText, 0);
                        propertyInfo.SetValue(model, mediaId, null);

                        string fieldUrl, fieldAltText;
                        _mediaHelper.LoadMedia(mediaId, out fieldUrl, out fieldAltText);

                        var urlProperty = properties.FirstOrDefault(p => p.Name == itemPrefix + propertyInfo.Name + "_Url");
                        if (urlProperty != null)
                        {
                            urlProperty.SetValue(model, fieldUrl);
                        }

                        var altTextProperty = properties.FirstOrDefault(p => p.Name == itemPrefix + propertyInfo.Name + "_AltText");
                        if (altTextProperty != null)
                        {
                            altTextProperty.SetValue(model, altText);
                        }
                    }
                    else if (isRemove)
                    {
                        propertyInfo.SetValue(model, null, null);
                    }
                }
            }
        }
        #endregion
    }
}
