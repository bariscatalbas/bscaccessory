﻿using Core.Data.Interfaces;
using Core.Data.Models;
using Core.Ioc;
using Core.Pagination;
using Omu.ValueInjecter;

namespace Core.Mvc.Base
{
    public abstract class DataServiceBase<TDomain, TCreateModel, TEditModel, TViewModel, TFilterModel, TExcelExportModel> : IInjectAsSelf
        where TDomain : EntityBase, new()
        where TCreateModel : new()
        where TEditModel : EntityBase, new()
        where TViewModel : EntityBase, new()
        where TFilterModel : AcFilter, new()
        where TExcelExportModel : EntityBase, new()
    {
        public readonly IRepository<TDomain> Repository;

        public DataServiceBase(IRepository<TDomain> repository)
        {
            Repository = repository;
        }

        public virtual AcPager<TViewModel> Index(TFilterModel filter)
        {
            var query = Repository.TableAsNoTracking;
            return query.ToPager<TDomain, TViewModel>(filter);
        }

        public virtual AcPager<TExcelExportModel> ExportIndex(TFilterModel filter)
        {
            var query = Repository.TableAsNoTracking;
            return query.ToPager<TDomain, TExcelExportModel>(filter);
        }

        public virtual TCreateModel CreateModel() { return new TCreateModel(); }

        public virtual long Insert(TCreateModel model, long identityId)
        {
            var item = new TDomain();
            item.InjectFrom(model);
            ModifyInsert(item, model, identityId);
            Repository.Insert(item, identityId);
            return item.Id;
        }

        public virtual void ModifyInsert(TDomain item, TCreateModel model, long identityId) { }

        public virtual TEditModel Single(long id)
        {
            var item = Repository.Single(id);
            if (item != null)
            {
                var model = new TEditModel();
                model.InjectFrom(item);
                return model;
            }
            return default(TEditModel);
        }

        public virtual bool Update(TEditModel model, long identityId)
        {
            var item = Repository.Single(model.Id);
            if (item != null)
            {
                item.InjectFrom(model);
                ModifyUpdate(item, model, identityId);
                Repository.Update(item, identityId);
                return true;
            }
            return false;
        }
        public virtual void ModifyUpdate(TDomain item, TEditModel model, long identityId) { }

        public virtual bool Delete(long id, long identityId)
        {
            var item = Repository.Single(id);
            if (item != null)
            {
                ModifyDelete(item, identityId);
                Repository.Delete(item, identityId);
                return true;
            }
            return false;
        }

        public virtual string IsAllowedToDelete(long id) { return ""; }
        public virtual void ModifyDelete(TDomain item, long identityId) { }
    }
}
