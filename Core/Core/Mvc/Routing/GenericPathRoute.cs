﻿using Core.Ioc;
using Core.UrlRecord;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Core.Mvc.Routing
{
    public class GenericPathRoute : Route
    {
        public GenericPathRoute(string url, IRouteHandler routeHandler)
            : base(url, routeHandler)
        {
        }
        public GenericPathRoute(string url, RouteValueDictionary defaults, IRouteHandler routeHandler)
            : base(url, defaults, routeHandler)
        {
        }
        public GenericPathRoute(string url, RouteValueDictionary defaults, RouteValueDictionary constraints, IRouteHandler routeHandler)
            : base(url, defaults, constraints, routeHandler)
        {
        }
        public GenericPathRoute(string url, RouteValueDictionary defaults, RouteValueDictionary constraints, RouteValueDictionary dataTokens, IRouteHandler routeHandler)
            : base(url, defaults, constraints, dataTokens, routeHandler)
        {
        }

        public override RouteData GetRouteData(HttpContextBase httpContext)
        {
            RouteData data = base.GetRouteData(httpContext);
            if (data == null)
            {
                data = new RouteData(this, RouteHandler);
            }

            var urlText = (data == null ? "" : data.Values["url"] as string) ?? "";
            var routeProvider = AcEngine.Resolve<IRouteProvider>();
            var urlRecordHelper = AcEngine.Resolve<AcUrlRecordHelper>();

            long containerId;
            urlText = routeProvider.DecideContainer(urlText, out containerId);
            if (containerId == 0)
            {
                var values = routeProvider.DecideNoContainerPage();
                foreach (var value in values)
                {
                    data.Values[value.Key] = value.Value;
                }

                return data;
            }

            if (string.IsNullOrWhiteSpace(urlText))
            {
                urlText = routeProvider.DecideHomePage(containerId);
            }

            var record = urlRecordHelper.DecideUrl(containerId, urlText);
            if (record == null)
            {
                urlText = routeProvider.Decide404Page(containerId);
                record = urlRecordHelper.DecideUrl(containerId, urlText);
                if (record != null)
                {
                    data.Values["StatusCode"] = 404;
                }
            }

            if (record == null)
            {
                var values = routeProvider.DecideErrorPage(containerId);
                foreach (var value in values)
                {
                    data.Values[value.Key] = value.Value;
                }
                return data;
            }
            else
            {
                var parameters = urlText.Length > record.Url.Length ? urlText.Substring(record.Url.Length + 1) : "";
                var values = routeProvider.DecideRoute(containerId, record.EntityId, record.EntityName, urlText, parameters);
                foreach (var value in values)
                {
                    data.Values[value.Key] = value.Value;
                }

                return data;
            }
        }

    }
}
