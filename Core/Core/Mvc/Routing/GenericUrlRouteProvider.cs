﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Routing;

namespace Core.Mvc.Routing
{
    public class GenericUrlRouteProvider
    {
        public void RegisterRoutes(RouteCollection routes)
        {
            routes.MapGenericPathRoute("GenericUrl",
                                       "{*url}",
                                       new { controller = "Home", action = "GenericUrl" },
                                       new[] { "Web.Controllers" });
        }
    }
}
