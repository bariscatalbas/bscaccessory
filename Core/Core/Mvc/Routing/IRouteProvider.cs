﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Mvc.Routing
{
    public interface IRouteProvider
    {
        string DecideContainer(string url, out long containerId);
        string DecideHomePage(long containerId);
        string Decide404Page(long containerId);
        Dictionary<string, object> DecideErrorPage(long containerId);
        Dictionary<string, object> DecideRoute(long containerId, long entityId, string entityName, string url, string parameters);
        Dictionary<string, object> DecideNoContainerPage();
    }
}
