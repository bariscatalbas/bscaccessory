﻿using System;
using System.Web.Mvc;

namespace Core.Mvc.Attributes
{
    public class AcMediaResizeAttribute : Attribute, IMetadataAware
    {
        private string Value { get; set; }

        public AcMediaResizeAttribute(string value)
        {
            Value = value;
        }

        public void OnMetadataCreated(ModelMetadata metadata)
        {
            metadata.AdditionalValues["AcMediaResize"] = Value;
        }
    }
}
