﻿using System;
using System.Web.Mvc;

namespace Core.Mvc.Attributes
{
    public class AcMediaThumbAttribute : Attribute, IMetadataAware
    {
        private string Value { get; set; }

        public AcMediaThumbAttribute(string value)
        {
            Value = value;
        }

        public void OnMetadataCreated(ModelMetadata metadata)
        {
            metadata.AdditionalValues["AcMediaThumb"] = Value;
        }
    }
}
