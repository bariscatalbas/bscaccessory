﻿using System;
using System.Web.Mvc;

namespace Core.Mvc.Attributes
{
    public class AcDropdownCustomValueAttribute : Attribute, IMetadataAware
    {
        private string Value { get; set; }

        public AcDropdownCustomValueAttribute(string value)
        {
            Value = value;
        }

        public void OnMetadataCreated(ModelMetadata metadata)
        {
            metadata.AdditionalValues["AcDropdownCustomValue"] = Value;
        }
    }
}
