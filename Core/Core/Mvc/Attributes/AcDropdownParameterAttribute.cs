﻿using System;
using System.Web.Mvc;

namespace Core.Mvc.Attributes
{
    public class AcDropdownParameterAttribute : Attribute, IMetadataAware
    {
        private string Value { get; set; }

        public AcDropdownParameterAttribute(string value)
        {
            Value = value;
        }

        public void OnMetadataCreated(ModelMetadata metadata)
        {
            metadata.AdditionalValues["AcDropdownParameter"] = Value;
        }
    }
}
