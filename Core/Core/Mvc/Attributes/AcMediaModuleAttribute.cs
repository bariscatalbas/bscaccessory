﻿using System;
using System.Web.Mvc;

namespace Core.Mvc.Attributes
{
    public class AcMediaModuleAttribute : Attribute, IMetadataAware
    {
        private string Value { get; set; }

        public AcMediaModuleAttribute(string value)
        {
            Value = value;
        }

        public void OnMetadataCreated(ModelMetadata metadata)
        {
            metadata.AdditionalValues["AcMediaModule"] = Value;
        }
    }
}
