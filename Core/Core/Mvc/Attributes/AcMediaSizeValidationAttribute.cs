﻿using System;
using System.Web.Mvc;

namespace Core.Mvc.Attributes
{
    public class AcMediaSizeValidationAttribute : Attribute, IMetadataAware
    {
        private string Value { get; set; }

        public AcMediaSizeValidationAttribute(string value)
        {
            Value = value;
        }

        public void OnMetadataCreated(ModelMetadata metadata)
        {
            metadata.AdditionalValues["AcMediaSizeValidation"] = Value;
        }
    }
}
