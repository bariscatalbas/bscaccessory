﻿using System;
using System.Web.Mvc;

namespace Core.Mvc.Attributes
{
    public class AcCustomParameterAttribute : Attribute, IMetadataAware
    {
        private string Value { get; set; }

        public AcCustomParameterAttribute(string value)
        {
            Value = value;
        }

        public void OnMetadataCreated(ModelMetadata metadata)
        {
            metadata.AdditionalValues["AcCustomParameter"] = Value;
        }
    }
}
