﻿using System;
using System.Web.Mvc;

namespace Core.Mvc.Attributes
{
    public class AcCustomAlternativeParameterAttribute : Attribute, IMetadataAware
    {
        private string Value { get; set; }

        public AcCustomAlternativeParameterAttribute(string value)
        {
            Value = value;
        }

        public void OnMetadataCreated(ModelMetadata metadata)
        {
            metadata.AdditionalValues["AcCustomAlternativeParameter"] = Value;
        }
    }
}
