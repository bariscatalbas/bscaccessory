﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Core.Mvc.Services
{
    public class ExcelResult : ActionResult
    {
        public string FileName { get; set; }
        public StringWriter Sw { get; set; }
        public MemoryStream Stream { get; set; }

        public override void ExecuteResult(ControllerContext context)
        {
            context.HttpContext.Response.Buffer = true;
            context.HttpContext.Response.Clear();
            context.HttpContext.Response.ClearHeaders();
            context.HttpContext.Response.ClearContent();
            context.HttpContext.Response.ContentEncoding = System.Text.Encoding.GetEncoding("utf-8");
            context.HttpContext.Response.Charset = "utf-8";

            context.HttpContext.Response.AddHeader("content-disposition", "attachment; filename=" + FileName);
            context.HttpContext.Response.ContentType = "application/vnd.ms-excel";
            
            if (Sw != null)
            {
                context.HttpContext.Response.Write(Sw);
            }
            else if (Stream != null)
            {
                context.HttpContext.Response.BinaryWrite(Stream.ToArray());
                context.HttpContext.Response.Flush();
                context.HttpContext.Response.Close();
                context.HttpContext.Response.End();
            }
            else
            {
                throw new Exception("No Source");
            }
        }
    }
}
