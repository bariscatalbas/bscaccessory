﻿using System;

namespace Core.Mvc.Services
{
    public class CookieBasedMembershipProvider
    {

        private readonly string _cookieKey;
        private readonly int _sessionInterval;

        public CookieBasedMembershipProvider(string cookieKey, int sessionInterval)
        {
            _cookieKey = cookieKey;
            _sessionInterval=sessionInterval;
        }


        public Guid GetSessionKey
        {
            get
            {
                var sessionKey = _.Cookie.GetCookie(_cookieKey, true, _sessionInterval);
                if (sessionKey == null)
                {
                    return Guid.Empty;
                }

                if (!string.IsNullOrWhiteSpace(sessionKey))
                {
                    sessionKey = _.Crypto.DecryptText(sessionKey);
                    return _.To.Guidd(sessionKey);
                }

                return Guid.Empty;
            }
        }

        public void SignIn(Guid token)
        {
            var encKey = _.Crypto.EncryptText(token.ToString());
            _.Cookie.SetCookie(_cookieKey, encKey, _sessionInterval);
        }

        public void SignOut()
        {
            _.Cookie.RemoveCookie(_cookieKey);
        }

    }
}
