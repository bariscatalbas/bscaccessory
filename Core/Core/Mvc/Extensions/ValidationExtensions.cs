﻿using Core.CommonModels;
using Core.Validation;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

public static class ValidationExtensions
{
    public static void LoadErrors(this ModelStateDictionary modelState, List<KeyValue> errors)
    {
        foreach (var error in errors)
        {
            modelState.AddModelError(error.Key, error.Value);
        }
    }

    public static bool ValidateAndLoadErrors<T>(this ModelStateDictionary modelState, T model, Func<string, string> localize)
    {
        List<KeyValue> errors;
        if (AcValidationProvider.Validate(model, localize, out errors))
        {
            return true;
        }
        LoadErrors(modelState, errors);
        return false;
    }
}
