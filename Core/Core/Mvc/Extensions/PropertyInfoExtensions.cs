﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Core.Mvc.Extensions
{
    public static class PropertyInfoExtensions
    {
        public static bool IsUiHint(this PropertyInfo property, string uiHint)
        {
            return string.CompareOrdinal(ModelMetadataProviders.Current.GetMetadataForProperty(null, property.DeclaringType, property.Name).TemplateHint, uiHint) == 0;
        }
    }
}
