﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.Routing;

public static class HtmlExtensions
{
    private const string EditButtonFormat = "<a href='{0}' {2} class='btn btn-warning btn-sm m-btn m-btn--icon m--margin-left-5 m--margin-right-5 m--margin-top-5 m--margin-bottom-5'><span><i class='la la-mail-reply'></i><span>{1}</span></span></a>";
    private const string DeleteButtonFormat = "<a href = \"javascript:void:;\" tabindex=\"0\" class=\"btn btn-danger btn-sm m-btn m-btn--icon delete-button m--margin-left-5 m--margin-right-5 m--margin-top-5 m--margin-bottom-5\"><span><i class='la la-trash'></i><span>SİL</span></span></a><form style=\"display: none\" action=\"{0}\" name=\"del{1}\" method=\"post\"></form>";
    private const string CustomButtonFormat = "<a href=\"{0}\" class=\"btn btn-sm m-btn m--margin-left-5 m--margin-right-5 m--margin-top-5 m--margin-bottom-5 {1}\">{2}</a>";

    public static MvcHtmlString EditButton(this HtmlHelper htmlHelper, object parameters, string actionName = "Edit", string controllerName = null, string title = "EDIT", bool show = true, string target = "", string hashtag = null)
    {
        var url = CreateUrl(htmlHelper, actionName, controllerName, parameters);
        if (!string.IsNullOrWhiteSpace(hashtag))
        {
            url = url + "#" + hashtag;
        }
        return show
                   ? new MvcHtmlString(string.Format(EditButtonFormat, url, title, target))
                   : new MvcHtmlString("");
    }
    public static MvcHtmlString DeleteButton(this HtmlHelper htmlHelper, object parameters, string actionName = "Delete", string controllerName = null, string title = "DELETE", bool show = true)
    {
        if (show)
        {
            var rnd = _.Crypto.GenerateRandomString(7);


            return MvcHtmlString.Create(string.Format(DeleteButtonFormat, CreateUrl(htmlHelper, actionName, controllerName, parameters), rnd, htmlHelper.AntiForgeryToken(), title));
        }
        return new MvcHtmlString("");
    }

    public static MvcHtmlString CustomButton(this HtmlHelper htmlHelper, string cssClass, object parameters, string actionName, string title, string controllerName = null, bool show = true)
    {
        return show
                   ? new MvcHtmlString(string.Format(CustomButtonFormat, CreateUrl(htmlHelper, actionName, controllerName, parameters), cssClass, title))
                   : new MvcHtmlString("");
    }

    public static string CreateUrl(this HtmlHelper helper, string actionName = null, string controllerName = null, object parameters = null, string pName = null, object pValue = null)
    {
        var rvd = new RouteValueDictionary(parameters);
        if (!string.IsNullOrWhiteSpace(pName) && pValue != null)
        {
            if (rvd.ContainsKey(pName))
            {
                rvd[pName] = pValue;
            }
            else
            {
                rvd.Add(pName, pValue);
            }
        }
        return UrlHelper.GenerateUrl(null, actionName, controllerName, rvd, helper.RouteCollection, helper.ViewContext.RequestContext, true);
    }

    public static string AcLabelTextFor<TModel, TValue>(this HtmlHelper<TModel> helper, Expression<Func<TModel, TValue>> expression)
    {
        var metadata = ModelMetadata.FromLambdaExpression(expression, helper.ViewData);

        var labelText = metadata.DisplayName;
        if (string.IsNullOrEmpty(labelText) && !string.IsNullOrWhiteSpace(metadata.PropertyName))
            labelText = _.String.MakeReadable(metadata.PropertyName);

        return labelText;
    }

    public static MvcHtmlString AcLabelText(this HtmlHelper helper, string expression)
    {
        var metadata = ModelMetadata.FromStringExpression(expression, helper.ViewData);

        var labelText = metadata.DisplayName;
        if (string.IsNullOrEmpty(labelText) && !string.IsNullOrWhiteSpace(metadata.PropertyName))
            labelText = _.String.MakeReadable(metadata.PropertyName);

        return MvcHtmlString.Create(labelText);
    }

    public static MvcHtmlString AcDropdownFor<TModel, TValue>(this HtmlHelper<TModel> helper, Expression<Func<TModel, TValue>> expression, object htmlAttributes = null)
    {
        var meta = ModelMetadata.FromLambdaExpression(expression, helper.ViewData);
        var selectList = GetSelectListForAttribute(helper, expression, meta.Model).ToList();

        selectList.Insert(0, new SelectListItem { });

        return helper.DropDownListFor(expression, selectList, htmlAttributes);
    }
    public static MvcHtmlString AcDropdownFor<TModel, TValue>(this HtmlHelper<TModel> helper, Expression<Func<TModel, TValue>> expression, List<SelectListItem> items, object htmlAttributes = null)
    {
        var meta = ModelMetadata.FromLambdaExpression(expression, helper.ViewData);
        var selectList = items;

        selectList.Insert(0, new SelectListItem { });

        return helper.DropDownListFor(expression, selectList, htmlAttributes);
    }

    public static MvcHtmlString AcDropdownMultipleFor<TModel, TValue>(this HtmlHelper<TModel> helper, Expression<Func<TModel, TValue>> expression, object htmlAttributes = null)
    {
        var meta = ModelMetadata.FromLambdaExpression(expression, helper.ViewData);
        var selectList = GetSelectListForAttribute(helper, expression, meta.Model).ToList();

        selectList.Insert(0, new SelectListItem { });

        return helper.ListBoxFor(expression, selectList, htmlAttributes);
    }

    public static MvcHtmlString AcDropdown(this HtmlHelper helper, string expression, List<SelectListItem> items, object htmlAttributes = null)
    {
        var meta = ModelMetadata.FromStringExpression(expression, helper.ViewData);
        var selectList = items;

        selectList.Insert(0, new SelectListItem { });

        return helper.DropDownList(expression, selectList, htmlAttributes);
    }

    public static string CustomParameter(this HtmlHelper helper, string expression)
    {
        var meta = ModelMetadata.FromStringExpression(expression, helper.ViewData);
        return meta.AdditionalValues.ContainsKey("AcCustomParameter") ? meta.AdditionalValues["AcCustomParameter"].ToString() : "";
    }
    public static string CustomParameter<TModel, TProperty>(this HtmlHelper<TModel> helper, Expression<Func<TModel, TProperty>> expression)
    {
        var meta = ModelMetadata.FromLambdaExpression(expression, helper.ViewData);
        return meta.AdditionalValues.ContainsKey("AcCustomParameter") ? meta.AdditionalValues["AcCustomParameter"].ToString() : "";
    }
    public static string CustomAlternativeParameter<TModel, TProperty>(this HtmlHelper<TModel> helper, string expression)
    {
        var meta = ModelMetadata.FromStringExpression(expression, helper.ViewData);
        return meta.AdditionalValues.ContainsKey("AcCustomAlternativeParameter") ? meta.AdditionalValues["AcCustomAlternativeParameter"].ToString() : "";
    }
    public static string CustomAlternativeParameter<TModel, TProperty>(this HtmlHelper<TModel> helper, Expression<Func<TModel, TProperty>> expression)
    {
        var meta = ModelMetadata.FromLambdaExpression(expression, helper.ViewData);
        return meta.AdditionalValues.ContainsKey("AcCustomAlternativeParameter") ? meta.AdditionalValues["AcCustomAlternativeParameter"].ToString() : "";
    }
    public static string MediaModule<TModel, TProperty>(this HtmlHelper<TModel> helper, string expression)
    {
        var meta = ModelMetadata.FromStringExpression(expression, helper.ViewData);
        return meta.AdditionalValues.ContainsKey("AcMediaModule") ? meta.AdditionalValues["AcMediaModule"].ToString() : "";
    }
    public static string MediaModule<TModel, TProperty>(this HtmlHelper<TModel> helper, Expression<Func<TModel, TProperty>> expression)
    {
        var meta = ModelMetadata.FromLambdaExpression(expression, helper.ViewData);
        return meta.AdditionalValues.ContainsKey("AcMediaModule") ? meta.AdditionalValues["AcMediaModule"].ToString() : "";
    }

    public static string MediaResize<TModel, TProperty>(this HtmlHelper<TModel> helper, Expression<Func<TModel, TProperty>> expression)
    {
        var meta = ModelMetadata.FromLambdaExpression(expression, helper.ViewData);
        return meta.AdditionalValues.ContainsKey("AcMediaResize") ? meta.AdditionalValues["AcMediaResize"].ToString() : "";
    }

    public static string MediaThumb<TModel, TProperty>(this HtmlHelper<TModel> helper, Expression<Func<TModel, TProperty>> expression)
    {
        var meta = ModelMetadata.FromLambdaExpression(expression, helper.ViewData);
        return meta.AdditionalValues.ContainsKey("AcMediaThumb") ? meta.AdditionalValues["AcMediaThumb"].ToString() : "";
    }
    public static string MediaSizeValidation<TModel, TProperty>(this HtmlHelper<TModel> helper, Expression<Func<TModel, TProperty>> expression)
    {
        var meta = ModelMetadata.FromLambdaExpression(expression, helper.ViewData);
        return meta.AdditionalValues.ContainsKey("AcMediaSizeValidation") ? meta.AdditionalValues["AcMediaSizeValidation"].ToString() : "";
    }
    public static string DropdownParameter(this HtmlHelper helper, string expression)
    {
        var meta = ModelMetadata.FromStringExpression(expression, helper.ViewData);
        return meta.AdditionalValues.ContainsKey("AcDropdownParameter") ? meta.AdditionalValues["AcDropdownParameter"].ToString() : "";
    }
    public static string DropdownParameter<TModel, TProperty>(this HtmlHelper<TModel> helper, Expression<Func<TModel, TProperty>> expression)
    {
        var meta = ModelMetadata.FromLambdaExpression(expression, helper.ViewData);
        return meta.AdditionalValues.ContainsKey("AcDropdownParameter") ? meta.AdditionalValues["AcDropdownParameter"].ToString() : "";
    }
    public static string DropdownCustomValue(this HtmlHelper helper, string expression)
    {
        var meta = ModelMetadata.FromStringExpression(expression, helper.ViewData);
        return meta.AdditionalValues.ContainsKey("AcDropdownCustomValue") ? meta.AdditionalValues["AcDropdownCustomValue"].ToString() : "";
    }
    public static string DropdownCustomValue<TModel, TProperty>(this HtmlHelper<TModel> helper, Expression<Func<TModel, TProperty>> expression)
    {
        var meta = ModelMetadata.FromLambdaExpression(expression, helper.ViewData);
        return meta.AdditionalValues.ContainsKey("AcDropdownCustomValue") ? meta.AdditionalValues["AcDropdownCustomValue"].ToString() : "";
    }

    public static IEnumerable<SelectListItem> GetSelectListForAttribute<TModel, TProperty>(this HtmlHelper<TModel> helper, Expression<Func<TModel, TProperty>> expression, object selectedValue)
    {
        var meta = ModelMetadata.FromLambdaExpression(expression, helper.ViewData);
        var parameter = meta.AdditionalValues.ContainsKey("AcDropdownParameter") ? meta.AdditionalValues["AcDropdownParameter"].ToString() : "";

        var list = (List<SelectListItem>)helper.ViewData[parameter] ?? new List<SelectListItem>();

        var customValues = meta.AdditionalValues.ContainsKey("AcDropdownCustomValue") ? meta.AdditionalValues["AcDropdownCustomValue"].ToString() : "";
        var customValuesList = customValues.Split(';').Where(p => !string.IsNullOrWhiteSpace(p)).Select(p => new SelectListItem { Text = p, Value = p }).ToList();
        list.AddRange(customValuesList);

        if (selectedValue != null)
        {
            if (_.Prop.GetEnumerableType(selectedValue.GetType()) != null && _.Prop.GetEnumerableType(selectedValue.GetType()) != typeof(char))
            {
                var itemType = _.Prop.GetEnumerableType(selectedValue.GetType());

                List<string> values = new List<string>();
                var valuesEnumerator = ((System.Collections.IEnumerable)selectedValue).GetEnumerator();
                while (valuesEnumerator.MoveNext())
                {
                    values.Add(valuesEnumerator.Current.ToString());
                }
                return list.Select(p => new SelectListItem { Selected = values.Any(q => string.Equals(q.ToString(), p.Value)), Text = p.Text, Value = p.Value });
            }
            else
            {
                var value = selectedValue.ToString();
                return list.Select(p => new SelectListItem { Selected = string.Equals(value, p.Value), Text = p.Text, Value = p.Value });
            }
        }
        return list;
    }

    public static MvcHtmlString TextEditor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression)
    {
        return htmlHelper.TextAreaFor(expression, new { @class = "tinymce" });
    }
    public static MvcHtmlString DateTimeFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, object htmlAttributes = null)
    {
        var metadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
        var val = metadata.Model;
        var name = htmlHelper.ViewContext.ViewData.TemplateInfo.GetFullHtmlFieldName(ExpressionHelper.GetExpressionText(expression));
        var value = val == null || !(val is DateTime) ? "" : ((DateTime)val).ToString("yyyy.MM.dd HH:mm");

        return htmlHelper.TextBox(name, value, htmlAttributes);
    }
    public static MvcHtmlString FileInputFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression)
    {
        var name = htmlHelper.ViewContext.ViewData.TemplateInfo.GetFullHtmlFieldName(ExpressionHelper.GetExpressionText(expression));
        var input = string.Format(@"<input type=""file"" id=""{0}"" name=""{0}"" />", name);
        return new MvcHtmlString(input);
    }
}
