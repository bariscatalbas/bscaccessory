﻿using Core.Audit;
using Core.Cache;
using Core.Email;
using Core.EmailTemplates;
using Core.Log;
using Core.Media;
using Core.Provider;
using Core.Resources;
using Core.Settings;
using Core.Sms;
using Core.SmsTemplates;
using Core.Task;
using Core.UrlRecord;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace Core.Data.Services
{
    public class AcContext : DbContext
    {
        public AcContext()
        {
            Initialize();
        }

        public AcContext(string connectionString) : base(connectionString)
        {
            Initialize();
        }

        private void Initialize()
        {
            var ensureDllIsCopied = System.Data.Entity.SqlServer.SqlProviderServices.Instance;
            System.Data.Entity.SqlServer.SqlProviderServices.TruncateDecimalsToScale = false;
            var connectionTimeout = Database.Connection.ConnectionTimeout;
            Database.CommandTimeout = 180;

            Configuration.LazyLoadingEnabled = false;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
        }

        // Helpers
        public DbSet<AcProvider> AcProviders { get; set; }
        public DbSet<AcUrlRecord> AcUrlRecords { get; set; }
        public DbSet<AcSetting> AcSettings { get; set; }
        public DbSet<AcResource> AcResources { get; set; }
        public DbSet<AcCacheVersion> AcCacheVersions { get; set; }
        public DbSet<AcMedia> AcMedias { get; set; }
        public DbSet<AcEmailTemplate> AcEmailTemplates { get; set; }
        public DbSet<AcSmsTemplate> AcSmsTemplates { get; set; }

        // Providers
        public DbSet<AcQueuedEmail> AcQueuedEmails { get; set; }
        public DbSet<AcQueuedSms> AcQueuedSms { get; set; }
        public DbSet<AcTask> AcTasks { get; set; }
        public DbSet<AcTaskHistory> AcTaskHistories { get; set; }

    }
}
