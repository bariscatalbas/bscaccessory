﻿using Core.Data.Interfaces;
using Core.Data.Models;
using Core.EventTrigger;
using System;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;

namespace Core.Data.Services
{
    public class AcRepository<TDomain> : IRepository<TDomain> where TDomain : EntityBase
    {
        private readonly DbContext _context;

        private IDbSet<TDomain> _entities;
        private IDbSet<TDomain> Entities => _entities ?? (_entities = _context.Set<TDomain>());

        public DbContext Context { get { return _context; } }
        public IQueryable<TDomain> Table { get { return Entities; } }
        public IQueryable<TDomain> TableAsNoTracking { get { return Entities.AsNoTracking(); } }

        public IQueryable<TEntity> GetDbSet<TEntity>() where TEntity : EntityBase
        {
            return _context.Set<TEntity>();
        }

        public AcRepository(DbContext context)
        {
            _context = context;
        }

        public TDomain Single(long id, bool includeDeleted = false)
        {
            var domain = Entities.FirstOrDefault(p => p.Id == id);
            if (domain != null && !includeDeleted)
            {
                if (domain is DeletableEntityBase)
                    if ((domain as DeletableEntityBase).Deleted)
                        return null;
            }
            return domain;
        }

        public void Insert(TDomain entity, long identityId)
        {
            try
            {
                if (entity == null)
                {
                    throw new ArgumentNullException("entity");
                }
                if (entity is DeletableEntityBase)
                {
                    (entity as DeletableEntityBase).Deleted = false;
                }
                if (entity is Entity)
                {
                    (entity as Entity).CreationDate = DateTime.UtcNow;
                    (entity as Entity).CreatedBy = identityId;
                    (entity as Entity).ModificationDate = DateTime.UtcNow;
                    (entity as Entity).ModifiedBy = identityId;
                }
                else if (entity is DeletableEntity)
                {
                    (entity as DeletableEntity).CreationDate = DateTime.UtcNow;
                    (entity as DeletableEntity).CreatedBy = identityId;
                    (entity as DeletableEntity).ModificationDate = DateTime.UtcNow;
                    (entity as DeletableEntity).ModifiedBy = identityId;
                }

                AcEventTriggerHelper.RaiseEvent(entity, new AcEventTriggerArguments { EventName = "Repository.PreInsert", EventArguments = this, IdentityId = identityId });
                Entities.Add(entity);
                _context.SaveChanges();
                AcEventTriggerHelper.RaiseEvent(entity, new AcEventTriggerArguments { EventName = "Repository.PostInsert", EventArguments = this, IdentityId = identityId });
                _context.SaveChanges();

            }
            catch (DbEntityValidationException dbEx)
            {
                var msg = dbEx.EntityValidationErrors.SelectMany(validationErrors => validationErrors.ValidationErrors).Aggregate(string.Empty, (current, validationError) => current + (string.Format("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage) + Environment.NewLine));
                throw new Exception(msg, dbEx);
            }
        }
        public void Update(TDomain entity, long identityId)
        {
            try
            {
                if (entity == null)
                {
                    throw new ArgumentNullException("entity");
                }
                if (entity is Entity)
                {
                    (entity as Entity).ModificationDate = DateTime.UtcNow;
                    (entity as Entity).ModifiedBy = identityId;
                }
                else if (entity is DeletableEntity)
                {
                    (entity as DeletableEntity).ModificationDate = DateTime.UtcNow;
                    (entity as DeletableEntity).ModifiedBy = identityId;
                }

                AcEventTriggerHelper.RaiseEvent(entity, new AcEventTriggerArguments { EventName = "Repository.PreUpdate", EventArguments = this, IdentityId = identityId });
                _context.SaveChanges();
                AcEventTriggerHelper.RaiseEvent(entity, new AcEventTriggerArguments { EventName = "Repository.PostUpdate", EventArguments = this, IdentityId = identityId });
                _context.SaveChanges();
            }
            catch (DbEntityValidationException dbEx)
            {
                var msg = dbEx.EntityValidationErrors.SelectMany(validationErrors => validationErrors.ValidationErrors).Aggregate(string.Empty, (current, validationError) => current + (Environment.NewLine + string.Format("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage)));
                throw new Exception(msg, dbEx);
            }
        }
        public void Delete(TDomain entity, long identityId)
        {
            try
            {
                if (entity == null)
                {
                    throw new ArgumentNullException("entity");
                }

                if (entity is DeletableEntityBase)
                {
                    (entity as DeletableEntityBase).Deleted = true;
                    if (entity is DeletableEntity)
                    {
                        (entity as DeletableEntity).ModificationDate = DateTime.UtcNow;
                        (entity as DeletableEntity).ModifiedBy = identityId;
                    }
                }
                else
                {
                    Entities.Remove(entity);
                }

                AcEventTriggerHelper.RaiseEvent(entity, new AcEventTriggerArguments { EventName = "Repository.PreDelete", EventArguments = this, IdentityId = identityId });
                _context.SaveChanges();
                AcEventTriggerHelper.RaiseEvent(entity, new AcEventTriggerArguments { EventName = "Repository.PostDelete", EventArguments = this, IdentityId = identityId });
                _context.SaveChanges();
            }
            catch (DbEntityValidationException dbEx)
            {
                var msg = dbEx.EntityValidationErrors.SelectMany(validationErrors => validationErrors.ValidationErrors).Aggregate(string.Empty, (current, validationError) => current + (Environment.NewLine + string.Format("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage)));
                throw new Exception(msg, dbEx);
            }
        }
        public void Delete(long entityId, long identityId)
        {
            var entity = Entities.FirstOrDefault(p => p.Id == entityId);
            if (entity != null)
            {
                Delete(entity, identityId);
            }
        }

    }
}
