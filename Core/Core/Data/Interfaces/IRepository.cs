﻿using Core.Data.Models;
using System.Data.Entity;
using System.Linq;

namespace Core.Data.Interfaces
{
    public interface IRepository<TDomain> where TDomain : EntityBase
    {
        TDomain Single(long id, bool includeDeleted = false);
        void Insert(TDomain entity, long identityId);
        void Update(TDomain entity, long identityId);
        void Delete(TDomain entity, long identityId);
        void Delete(long entityId, long identityId);

        DbContext Context { get; }
        IQueryable<TDomain> Table { get; }
        IQueryable<TDomain> TableAsNoTracking { get; }

        IQueryable<TEntity> GetDbSet<TEntity>() where TEntity : EntityBase;
    }
}
