﻿namespace Core.Data.Interfaces
{
    public interface IUrlRecordParent
    {
        long Id { get; set; }
        string EntityName();
    }
}
