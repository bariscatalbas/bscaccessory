﻿using Core.Audit;

namespace Core.Data.Interfaces
{
    public interface IAuditable
    {
        AuditModel SelectLog(object repository);
    }
}
