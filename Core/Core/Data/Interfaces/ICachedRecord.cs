﻿namespace Core.Data.Interfaces
{
    public interface ICachedRecord
    {
        string CacheKey();
    }
}
