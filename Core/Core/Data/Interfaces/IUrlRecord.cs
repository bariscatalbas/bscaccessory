﻿namespace Core.Data.Interfaces
{
    public interface IUrlRecord
    {
        string Url { get; set; }

        long UrlRecordItemId();
        long UrlRecordContainerId();
        string UrlRecordEntityName();
    }
}
