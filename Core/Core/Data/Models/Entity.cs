﻿using System;

namespace Core.Data.Models
{
    public class Entity : EntityBase
    {
        public virtual DateTime CreationDate { get; set; }
        public virtual long CreatedBy { get; set; }

        public virtual DateTime ModificationDate { get; set; }
        public virtual long ModifiedBy { get; set; }
    }
}
