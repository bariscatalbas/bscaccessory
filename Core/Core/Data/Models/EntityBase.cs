﻿using System.ComponentModel.DataAnnotations;

namespace Core.Data.Models
{
    public class EntityBase 
    {
        [Key]
        [ScaffoldColumn(false)]
        public virtual long Id { get; set; }
    }
}
