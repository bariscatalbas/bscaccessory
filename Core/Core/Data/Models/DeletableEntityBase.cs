﻿namespace Core.Data.Models
{
    public class DeletableEntityBase : EntityBase
    {
        public virtual bool Deleted { get; set; }
    }
}
