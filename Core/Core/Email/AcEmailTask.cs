﻿using Core.Data.Interfaces;
using Core.Ioc;
using Core.Provider;
using Core.Task;
using Newtonsoft.Json;
using System;
using System.Linq;

namespace Core.Email
{
    public class AcEmailTask : AcTaskBase
    {
        private readonly IRepository<AcQueuedEmail> _queuedEmailRepository;
        private readonly AcEmailHelper _emailHelper;
        public AcEmailTask()
        {
            _queuedEmailRepository = AcEngine.Resolve<IRepository<AcQueuedEmail>>();
            _emailHelper = AcEngine.Resolve<AcEmailHelper>();
        }

        public override string Code => "AcEmailTask";

        public override string Name => "Ac Email Task";

        public override string SettingsSystemName { get { return typeof(AcEmailTaskSettings).AssemblyQualifiedName; } }

        private AcEmailTaskSettings Settings { get; set; }

        public override bool CanIStartLocal(TaskPrepareModel task, out string logThisAtHistory)
        {
            logThisAtHistory = "";
            Settings = JsonConvert.DeserializeObject<AcEmailTaskSettings>(task.SettingsJson);
            return true;
        }

        public override bool Execute(TaskPrepareModel task, out string logThisAtHistory)
        {
            logThisAtHistory = "";
            var emailIds = _queuedEmailRepository.Table.Where(p => !p.Sent && !p.Error).Select(p => p.Id).ToList();

            foreach(var emailId in emailIds)
            {
                _emailHelper.SendEmail(emailId);
            }

            return true;
        }
    }

    public class AcEmailTaskSettings
    {
    }
}
