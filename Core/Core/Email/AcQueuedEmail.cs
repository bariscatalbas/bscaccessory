﻿using Core.Audit;
using Core.Data.Interfaces;
using Core.Data.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Email
{
    public class AcQueuedEmail : EntityBase
    {
        [MaxLength(50)]
        public virtual string AcEmailProviderCode { get; set; }
        public virtual DateTime CreationDate { get; set; }

        public virtual bool Sent { get; set; }
        public virtual DateTime? SendDate { get; set; }

        public virtual bool Error { get; set; }
        public virtual DateTime? ErrorDate { get; set; }

        [MaxLength(500)]
        public virtual string To { get; set; }
        [MaxLength(500)]
        public virtual string Cc { get; set; }
        [MaxLength(500)]
        public virtual string Bcc { get; set; }
        [MaxLength(500)]
        public virtual string Subject { get; set; }
        public virtual string Body { get; set; }

        public virtual string Attachments { get; set; }
        public virtual string ErrorText { get; set; }
     
    }
}
