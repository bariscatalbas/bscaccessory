﻿using Core.Provider;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Email
{
    public interface IEmailProvider : IProvider
    {
        string SendEmail(QueuedEmailModel email);
    }
}
