﻿using Core.Audit;
using Core.Data.Interfaces;
using Core.Data.Models;
using Core.Pagination;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Email
{
    public class AcQueuedEmailAdminModelsList : EntityBase
    {
        public string To { get; set; }
        public DateTime CreationDate { get; set; }
        public bool Sent { get; set; }
        public string ErrorText { get; set; }
    }
    public class AcQueuedEmailAdminModelsDetails : EntityBase
    {
        public string AcEmailProviderCode { get; set; }
        public DateTime CreationDate { get; set; }
        public bool Sent { get; set; }
        public DateTime? SendDate { get; set; }
        public bool Error { get; set; }
        public DateTime? ErrorDate { get; set; }
        public string To { get; set; }
        public string Cc { get; set; }
        public string Bcc { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public string ErrorText { get; set; }
    }
    public class AcQueuedEmailAdminModelsFilter : AcFilter
    {
        [DisplayName("Start Date")]
        public DateTime? StartDate { get; set; }
        [DisplayName("End Date")]
        public DateTime? EndDate { get; set; }
        [DisplayName("To")]
        public string To { get; set; }
        [DisplayName("Is Error?")]
        public bool? Error { get; set; }
    }
}
