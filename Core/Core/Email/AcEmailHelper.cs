﻿using Core.Cache;
using Core.Data.Interfaces;
using Core.Ioc;
using Core.Pagination;
using Core.Provider;
using Omu.ValueInjecter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Email
{
    public class AcEmailHelper : IInjectAsSelf
    {
        private readonly IRepository<AcQueuedEmail> _queuedEmailRepository;

        public AcEmailHelper(IRepository<AcQueuedEmail> queuedEmailRepository)
        {
            _queuedEmailRepository = queuedEmailRepository;
        }

        private IEmailProvider GetProvider(string code = "")
        {
            if (string.IsNullOrWhiteSpace(code))
            {
                code = AcProviderHelper.GetCoreSettings().EmailProviderCode;
            }

            return AcProviderHelper.GetProvider<IEmailProvider>("email", code);
        }

        public long AddEmailToQueue(QueuedEmailModel email, long identityId)
        {
            var queuedEmail = new AcQueuedEmail();
            queuedEmail.InjectFrom(email);
            queuedEmail.CreationDate = DateTime.UtcNow;
            _queuedEmailRepository.Insert(queuedEmail, identityId);
            return queuedEmail.Id;
        }

        public string SendEmail(long id)
        {
            var email = _queuedEmailRepository.Table.FirstOrDefault(p => p.Id == id);
            if (email != null)
            {
                var model = new QueuedEmailModel();
                model.InjectFrom(email);

                var provider = GetProvider(model.AcEmailProviderCode);
                if (provider != null)
                {
                    var result = provider.SendEmail(model);

                    if (string.IsNullOrWhiteSpace(result))
                    {
                        email.Sent = true;
                        email.SendDate = DateTime.UtcNow;
                        _queuedEmailRepository.Update(email, 0);
                    }
                    else
                    {
                        email.Error = true;
                        email.ErrorText = result;
                        email.ErrorDate = DateTime.UtcNow;
                        _queuedEmailRepository.Update(email, 0);
                    }

                    return result;
                }

                return "Email Provider couldn't found: " + model.AcEmailProviderCode;
            }
            return "Email couldn't found with id:" + id;
        }

        #region Admin Functions
        public AcPager<AcQueuedEmailAdminModelsList> GetQueuedEmails(AcQueuedEmailAdminModelsFilter filter)
        {
            var query = _queuedEmailRepository.Table;

            if (filter.StartDate.HasValue)
            {
                query = query.Where(p => p.CreationDate >= filter.StartDate.Value);
            }
            if (filter.EndDate.HasValue)
            {
                query = query.Where(p => p.CreationDate >= filter.EndDate.Value);
            }
            if (!string.IsNullOrWhiteSpace(filter.To))
            {
                query = query.Where(p => p.To.Contains(filter.To));
            }
            if (filter.Error.HasValue)
            {
                query = query.Where(p => p.Error == true);
            }

            return query
                .Select(p => new AcQueuedEmailAdminModelsList
                {
                    Id = p.Id,
                    To = p.To,
                    CreationDate = p.CreationDate,
                    ErrorText = p.ErrorText,
                    Sent = p.Sent
                }).ToAcPager(filter);
        }

        public AcQueuedEmailAdminModelsDetails GetQueuedEmailDetails(long queuedEmailId)
        {
            return _queuedEmailRepository.Table.Where(p => p.Id == queuedEmailId)
                        .Select(p => new AcQueuedEmailAdminModelsDetails
                        {
                            Id = p.Id,
                            AcEmailProviderCode = p.AcEmailProviderCode,
                            CreationDate = p.CreationDate,
                            Sent = p.Sent,
                            SendDate = p.SendDate,
                            Error = p.Error,
                            ErrorDate = p.ErrorDate,
                            To = p.To,
                            Cc = p.Cc,
                            Bcc = p.Bcc,
                            Subject = p.Subject,
                            Body = p.Body,
                            ErrorText = p.ErrorText
                        }).FirstOrDefault();
        }

        public bool SendQueuedEmailAgain(long queuedEmailId, long identityId)
        {
            var queuedEmail = _queuedEmailRepository.Table.Where(p => p.Id == queuedEmailId).FirstOrDefault();

            if (queuedEmail != null)
            {
                queuedEmail.Sent = false;
                queuedEmail.SendDate = null;
                queuedEmail.Error = false;
                queuedEmail.ErrorDate = null;
                queuedEmail.ErrorText = "";

                _queuedEmailRepository.Update(queuedEmail, identityId);

                return true;
            }

            return false;
        }

        #endregion
    }
}
