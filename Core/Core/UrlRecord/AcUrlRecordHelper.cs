﻿using Core.Data.Interfaces;
using Core.Data.Models;
using Core.Ioc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.UrlRecord
{
    public class AcUrlRecordHelper : IInjectAsSelf
    {
        private readonly IRepository<AcUrlRecord> _urlRecordRepository;

        public AcUrlRecordHelper(IRepository<AcUrlRecord> urlRecordRepository)
        {
            _urlRecordRepository = urlRecordRepository;
        }

        public void DeleteUrl<T>(T entity, long identityId) where T : IUrlRecord
        {
            var entityId = (entity as EntityBase).Id;
            var containerId = entity.UrlRecordContainerId();
            var entityName = entity.UrlRecordEntityName();

            var record = _urlRecordRepository.Table.FirstOrDefault(p => p.ContainerId == containerId && p.EntityId == entityId && p.EntityName == entityName);
            if (record != null)
            {
                _urlRecordRepository.Delete(record, identityId);
            }
        }

        public void DeleteUrlFromParent<T>(T entity, long identityId) where T : IUrlRecordParent
        {
            var entityId = entity.Id;
            var entityName = entity.EntityName();

            var records = _urlRecordRepository.Table.Where(p => p.EntityId == entityId && p.EntityName == entityName).ToList();
            foreach(var record in records)
            {
                _urlRecordRepository.Delete(record, identityId);
            }
        }

        public string SetUrl<T>(T entity, long identityId) where T : IUrlRecord
        {
            var entityId = (entity as IUrlRecord).UrlRecordItemId();
            var containerId = entity.UrlRecordContainerId();
            var entityName = entity.UrlRecordEntityName();

            var currentUrl = (entity.Url ?? "").Trim().TrimEnd('/').ToLowerInvariant();
            var subFix = 0;
            while (!ValidateUrl(containerId, entityId, entityName, currentUrl))
            {
                subFix++;
                currentUrl = entity.Url + "_" + subFix;
            }

            var record = _urlRecordRepository.Table.FirstOrDefault(p => p.ContainerId == containerId && p.EntityId == entityId && p.EntityName == entityName);
            if (record != null)
            {
                record.Url = currentUrl;
                _urlRecordRepository.Update(record, identityId);
            }
            else
            {
                record = new AcUrlRecord
                {
                    ContainerId = containerId,
                    EntityId = entityId,
                    EntityName = entityName,
                    Url = currentUrl
                };
                _urlRecordRepository.Insert(record, identityId);
            }
            return currentUrl;
        }

        public string SetUrl(long containerId, string entityName, string url, long identityId)
        {
            var entityId = 0;

            var currentUrl = (url ?? "").Trim().TrimEnd('/').ToLowerInvariant();
            var subFix = 0;
            while (!ValidateUrl(containerId, entityId, entityName, currentUrl))
            {
                subFix++;
                currentUrl = url + "_" + subFix;
            }

            var record = _urlRecordRepository.Table.FirstOrDefault(p => p.ContainerId == containerId && p.EntityId == entityId && p.EntityName == entityName);
            if (record != null)
            {
                record.Url = currentUrl;
                _urlRecordRepository.Update(record, identityId);
            }
            else
            {
                record = new AcUrlRecord
                {
                    ContainerId = containerId,
                    EntityId = entityId,
                    EntityName = entityName,
                    Url = currentUrl
                };
                _urlRecordRepository.Insert(record, identityId);
            }
            return currentUrl;
        }

        public bool ValidateUrl(long containerId, long entityId, string entityName, string url)
        {
            url = (url ?? "").Trim().TrimEnd('/').ToLowerInvariant();
            return !_urlRecordRepository.Table.Any(p => p.ContainerId == containerId && p.Url == url && (p.EntityId != entityId || p.EntityName != entityName));
        }

        public bool ValidateUrl<T>(T entity) where T : IUrlRecord
        {

            return ValidateUrl(entity.UrlRecordContainerId(), (entity as EntityBase).Id, entity.UrlRecordEntityName(), entity.Url);
        }

        public UrlRecordModel DecideUrl(long containerId, string url)
        {
            url = (url ?? "").Trim().TrimEnd('/').ToLowerInvariant();
            var record = _urlRecordRepository
                        .Table
                        .Where(p => p.ContainerId == containerId && p.Url == url)
                        .Select(p => new UrlRecordModel
                        {
                            EntityId = p.EntityId,
                            EntityName = p.EntityName,
                            Url = p.Url
                        }).FirstOrDefault();

            if (record == null)
            {
                record = _urlRecordRepository
                        .Table
                        .Where(p => p.ContainerId == containerId && url.StartsWith(p.Url + "/"))
                        .Select(p => new UrlRecordModel
                        {
                            EntityId = p.EntityId,
                            EntityName = p.EntityName,
                            Url = p.Url
                        }).FirstOrDefault();
            }

            return record;
        }
    }
}
