﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.UrlRecord
{
    public class AcUrlRecordAttribute : Attribute
    {
        public string EntityName { get; set; }

        public AcUrlRecordAttribute(string entityName)
        {
            EntityName = entityName;
        }
    }
}
