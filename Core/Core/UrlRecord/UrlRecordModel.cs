﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.UrlRecord
{
    public class UrlRecordModel
    {
        public long EntityId { get; set; }
        public string EntityName { get; set; }
        public string Url { get; set; }
    }
}
