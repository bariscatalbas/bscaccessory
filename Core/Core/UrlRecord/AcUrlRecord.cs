﻿using Core.Data.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.UrlRecord
{
    public class AcUrlRecord : EntityBase
    {
        public virtual long ContainerId { get; set; }
        public virtual long EntityId { get; set; }
        [MaxLength(250)]
        public virtual string EntityName { get; set; }

        [MaxLength(500)]
        public virtual string Url { get; set; }
    }
}
