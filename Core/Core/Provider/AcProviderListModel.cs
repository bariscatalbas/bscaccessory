﻿using Core.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Provider
{
    public class AcProviderListModel : EntityBase
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string SystemName { get; set; }
        public string SettingsSystemName { get; set; }
        public string Type { get; set; }
    }
}
