﻿using Core.Audit;
using Core.Cache;
using Core.Data.Interfaces;
using Core.Data.Models;
using Core.Data.Services;
using Core.EventTrigger;
using Core.Ioc;
using Core.Log;
using Core.Pagination;
using Core.Settings;
using Core.UrlRecord;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Core.Provider
{
    public static class AcProviderHelper
    {
        #region Generic Get Provider Functions
        public static List<T> GetProviders<T>(string type) where T : IProvider
        {
            var list = new List<T>();

            var acProviderRepository = AcEngine.Resolve<IRepository<AcProvider>>();
            var providerCodes = acProviderRepository
                                    .Table
                                    .Where(p => p.Type == type)
                                    .Select(p => p.Code)
                                    .ToList();

            foreach (var code in providerCodes)
            {
                var item = GetProvider<T>(type, code);
                if (item != null)
                {
                    list.Add(item);
                }
            }

            return list;
        }
        public static List<SelectListItem> GetProvidersSelectList(string type)
        {
            var acProviderRepository = AcEngine.Resolve<IRepository<AcProvider>>();
            return acProviderRepository
                            .Table
                            .Where(p => p.Type == type)
                            .Select(p => new SelectListItem
                            {
                                Text = p.Name,
                                Value = p.Code
                            }).ToList();
        }
        public static T GetProvider<T>(string type, string code) where T : IProvider
        {
            var acCacheHelper = AcEngine.Resolve<AcCacheHelper>();
            var providerCacheModel = acCacheHelper.GetVersioned<AcProviderCacheModel>("AC_CACHE_PROVIDER_" + type + "_" + code,
                     () =>
                     {
                         var acProviderRepository = AcEngine.Resolve<IRepository<AcProvider>>();
                         var providerModel = acProviderRepository
                                                     .Table
                                                     .Where(p => p.Type == type && p.Code == code)
                                                     .Select(p => new { p.SystemName, p.SettingsJson })
                                                     .FirstOrDefault();

                         if (providerModel != null)
                         {
                             var providerType = Type.GetType(providerModel.SystemName);
                             return new AcProviderCacheModel
                             {
                                 ProviderType = providerType,
                                 SettingsJson = providerModel.SettingsJson
                             };
                         }

                         return null;
                     });

            if (providerCacheModel != null)
            {
                var providerInstance = Activator.CreateInstance(providerCacheModel.ProviderType) as IProvider;
                if (providerInstance != null)
                {
                    providerInstance.Initialize(providerCacheModel.SettingsJson);
                }

                return providerInstance != null ? (T)providerInstance : default(T);
            }
            return default(T);
        }
        public static T GetProviderByType<T>() where T : IProvider
        {
            if (typeof(T).IsAssignableFrom(typeof(ILogProvider)))
            {
                return GetProvider<T>("log", GetCoreSettings().LogProviderCode);
            }
            if (typeof(T).IsAssignableFrom(typeof(IAuditProvider)))
            {
                return GetProvider<T>("audit", GetCoreSettings().AuditProviderCode);
            }

            return default(T);
        }
        #endregion

        #region Named Instances
        public static void CatchNamedEventsByProviders(object source, AcEventTriggerArguments arguments)
        {
            if (arguments.EventName == "Repository.PreInsert")
            {
                // No Events
            }
            else if (arguments.EventName == "Repository.PostInsert")
            {
                if (source is IUrlRecord)
                {
                    var urlRecordHelper = AcEngine.Resolve<AcUrlRecordHelper>();
                    (source as IUrlRecord).Url = urlRecordHelper.SetUrl((IUrlRecord)source, arguments.IdentityId);
                }

                if (source is IAuditable)
                {
                    var auditProvider = GetProviderByType<IAuditProvider>();
                    if (auditProvider != null)
                    {
                        var auditModel = ((IAuditable)source).SelectLog(arguments.EventArguments);
                        auditProvider.LogInsert(auditModel, arguments.IdentityId);
                    }
                }

                if (source is ICachedRecord)
                {
                    var cacheHelper = AcEngine.Resolve<AcCacheHelper>();
                    var key = ((ICachedRecord)source).CacheKey();
                    cacheHelper.VersionUpdated(key, arguments.IdentityId);
                }
            }
            else if (arguments.EventName == "Repository.PreUpdate")
            {
                if (source is IUrlRecord)
                {
                    var urlRecordHelper = AcEngine.Resolve<AcUrlRecordHelper>();
                    (source as IUrlRecord).Url = urlRecordHelper.SetUrl((IUrlRecord)source, arguments.IdentityId);
                }
            }
            else if (arguments.EventName == "Repository.PostUpdate")
            {
                if (source is IAuditable)
                {
                    var auditProvider = GetProviderByType<IAuditProvider>();
                    if (auditProvider != null)
                    {
                        var auditModel = ((IAuditable)source).SelectLog(arguments.EventArguments);
                        auditProvider.LogUpdate(auditModel, arguments.IdentityId);
                    }
                }

                if (source is ICachedRecord)
                {
                    var cacheHelper = AcEngine.Resolve<AcCacheHelper>();
                    var key = ((ICachedRecord)source).CacheKey();
                    cacheHelper.VersionUpdated(key, arguments.IdentityId);
                }
            }
            else if (arguments.EventName == "Repository.PreDelete")
            {
                if (source is IAuditable)
                {
                    var auditProvider = GetProviderByType<IAuditProvider>();
                    if (auditProvider != null)
                    {
                        var auditModel = ((IAuditable)source).SelectLog(arguments.EventArguments);
                        auditProvider.LogDelete(auditModel, arguments.IdentityId);
                    }
                }

                if (source is ICachedRecord)
                {
                    var cacheHelper = AcEngine.Resolve<AcCacheHelper>();
                    var key = ((ICachedRecord)source).CacheKey();
                    cacheHelper.VersionRemoved(key, arguments.IdentityId);
                }
            }
            else if (arguments.EventName == "Repository.PostDelete")
            {
                if (source is IUrlRecord)
                {
                    var urlRecordHelper = AcEngine.Resolve<AcUrlRecordHelper>();
                    urlRecordHelper.DeleteUrl((IUrlRecord)source, arguments.IdentityId);
                }

                if (source is IUrlRecordParent)
                {
                    var urlRecordHelper = AcEngine.Resolve<AcUrlRecordHelper>();
                    urlRecordHelper.DeleteUrlFromParent((IUrlRecordParent)source, arguments.IdentityId);
                }
            }
        }
        public static void BindEventCachers()
        {
            AcEventTriggerHelper.eventHandlers.Add(CatchNamedEventsByProviders);
        }
        public static CoreSettings GetCoreSettings()
        {
            var acSettingsHelper = AcEngine.Resolve<AcSettingsHelper>();
            return acSettingsHelper.LoadSettingsCached<CoreSettings>();
        }

        #endregion

        #region Admin Functions
        public static void LoadAllFromAssembly(long identityId)
        {
            var list = new List<AcProviderAdminModelsList>();

            var types = AcEngine.FindClassesOfType<IProvider>();
            foreach (var type in types)
            {
                var model = Activator.CreateInstance(type) as IProvider;
                if (model != null)
                {
                    list.Add(new AcProviderAdminModelsList
                    {
                        Code = model.Code,
                        Name = model.Name,
                        SystemName = type.AssemblyQualifiedName,
                        SettingsSystemName = model.SettingsSystemName,
                        Type = model.Type
                    });
                }
            }

            var acProviderRepository = AcEngine.Resolve<IRepository<AcProvider>>();
            var providers = acProviderRepository.Table.Select(p => p).ToList();
            foreach (var provider in providers)
            {
                var listItem = list.FirstOrDefault(p => p.Code == provider.Code);
                if (listItem == null)
                {
                    acProviderRepository.Delete(provider, identityId);
                }
                else
                {
                    listItem.Id = provider.Id;
                }
            }

            foreach (var newProvider in list.Where(p => p.Id == 0))
            {
                acProviderRepository.Insert(new AcProvider
                {
                    Code = newProvider.Code,
                    Name = newProvider.Name,
                    Type = newProvider.Type,
                    SettingsSystemName = newProvider.SettingsSystemName,
                    SystemName = newProvider.SystemName
                }, identityId);
            }
        }
        public static object GetSettings(long providerId, out string systemName)
        {
            systemName = "";
            var acProviderRepository = AcEngine.Resolve<IRepository<AcProvider>>();
            var providerObj = acProviderRepository.Table.FirstOrDefault(p => p.Id == providerId);

            if (providerObj != null)
            {
                var type = Type.GetType(providerObj.SettingsSystemName);
                if (type != null)
                {
                    systemName = providerObj.SettingsSystemName;
                    if (!string.IsNullOrWhiteSpace(providerObj.SettingsJson))
                    {
                        return JsonConvert.DeserializeObject(providerObj.SettingsJson, type);
                    }
                    else
                    {
                        var model = Activator.CreateInstance(type);
                        return model;
                    }
                }
            }
            return null;
        }
        public static bool SetSettings(long providerId, object model, long identityId)
        {
            var acProviderRepository = AcEngine.Resolve<IRepository<AcProvider>>();
            var providerObj = acProviderRepository.Table.FirstOrDefault(p => p.Id == providerId);
            if (providerObj != null)
            {
                if (model == null)
                {
                    providerObj.SettingsJson = "";
                }
                else
                {
                    providerObj.SettingsJson = JsonConvert.SerializeObject(model);
                }

                acProviderRepository.Update(providerObj, identityId);
                return true;
            }
            return false;
        }
        public static List<SelectListItem> SelectList(string type)
        {
            var acProviderRepository = AcEngine.Resolve<IRepository<AcProvider>>();
            return acProviderRepository
                        .Table
                        .Where(p => p.Type == type)
                        .Select(p => new SelectListItem
                        {
                            Text = p.Name,
                            Value = p.Code
                        }).ToList();
        }
        public static AcPager<AcProviderAdminModelsList> GetProviders(AcProviderAdminModelsFilter filter)
        {
            var acProviderRepository = AcEngine.Resolve<IRepository<AcProvider>>();
            var query = acProviderRepository.Table;

            if (!string.IsNullOrWhiteSpace(filter.Type))
            {
                query = query.Where(p => p.Type == filter.Type);
            }
            return query.Select(p => new AcProviderAdminModelsList
            {
                Code = p.Code,
                Id = p.Id,
                Name = p.Name,
                Type = p.Type,
                SettingsSystemName = p.SettingsSystemName,
                SystemName = p.SystemName
            }).ToAcPager(filter);
        }
        #endregion

    }
}
