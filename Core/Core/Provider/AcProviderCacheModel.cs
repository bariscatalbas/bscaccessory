﻿using Core.Audit;
using Core.Data.Interfaces;
using Core.Data.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Provider
{
    public class AcProviderCacheModel
    {
        public Type ProviderType { get; set; }
        public string SettingsJson { get; set; }
    }
}
