﻿using Core.Audit;
using Core.Data.Interfaces;
using Core.Data.Models;
using Core.Pagination;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Provider
{
    public class AcProviderAdminModelsList : EntityBase
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string SystemName { get; set; }
        public string SettingsSystemName { get; set; }
        public string Type { get; set; }
    }

    public class AcProviderAdminModelsFilter : AcFilter
    {
        public string Type { get; set; }
    }
}
