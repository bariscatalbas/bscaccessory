﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Provider
{
    public interface IProvider
    {
        string Code { get; }
        string Type { get; }
        string Name { get; }
        string SettingsSystemName { get; }

        void Initialize(string settingsJson);
    }
}
