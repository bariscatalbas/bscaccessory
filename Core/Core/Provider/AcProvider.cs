﻿using Core.Audit;
using Core.Data.Interfaces;
using Core.Data.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Provider
{
    public class AcProvider : EntityBase, ICachedRecord, IAuditable
    {
        [MaxLength(50)]
        public virtual string Code { get; set; }

        [MaxLength(50)]
        public virtual string Name { get; set; }

        [MaxLength(50)]
        public virtual string Type { get; set; }

        [MaxLength(500)]
        public virtual string SystemName { get; set; }

        [MaxLength(500)]
        public virtual string SettingsSystemName { get; set; }

        public virtual string SettingsJson { get; set; }

        public string CacheKey() => "AC_CACHE_PROVIDER_" + Type + "_" + Code;

        public AuditModel SelectLog(object repository)
        {
            return new AuditModel
            {
                EntityId = this.Id,
                EntityName = "Provider",
                ParentEntityId = 0,
                ParentEntityName = "",
                Details = new
                {
                    this.Id,
                    this.Code,
                    this.Name,
                    this.Type,
                    this.SystemName,
                    this.SettingsSystemName,
                    this.SettingsJson
                }
            };
        }
    }
}
