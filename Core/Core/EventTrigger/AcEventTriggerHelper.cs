﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.EventTrigger
{
    public static class AcEventTriggerHelper
    {
        public static List<AcEvent> eventHandlers = new List<AcEvent>();

        public static void RaiseEvent(object source, AcEventTriggerArguments arguments)
        {
            foreach (var eventHandler in eventHandlers)
            {
                eventHandler(source, arguments);
            }
        }
    }

    public delegate void AcEvent(object source, AcEventTriggerArguments arguments);
}
