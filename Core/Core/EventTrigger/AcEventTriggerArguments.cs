﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.EventTrigger
{
    public class AcEventTriggerArguments
    {
        public string EventName { get; set; }
        public object EventArguments { get; set; }
        public long IdentityId { get; set; }
    }
}
