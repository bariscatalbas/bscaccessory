﻿namespace Core.Validation
{
    public class AcValidateRequiredAttribute : AcValidationBaseAttribute
    {
        public AcValidateRequiredAttribute(string errorMessage = "AcValidation.Required") : base("Required", errorMessage)
        {
        }
    }
}
