﻿using Core.CommonModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace Core.Validation
{
    public static class AcValidationProvider
    {
        public static int ILocalizationProvider { get; private set; }

        public static bool Validate<T>(T model, Func<string, string> localize, out List<KeyValue> errors, string parentName = "")
        {
            errors = new List<KeyValue>();
            var properties = model.GetType().GetProperties();
            foreach (var propertyInfo in properties)
            {
                if (_.Prop.GetEnumerableType(propertyInfo.PropertyType) != null && _.Prop.GetEnumerableType(propertyInfo.PropertyType) != typeof(char))
                {
                    var currentParentName = propertyInfo.Name;
                    var itemType = _.Prop.GetEnumerableType(propertyInfo.PropertyType);

                    if (_.Prop.GetEnumerableType(propertyInfo.PropertyType) != null && _.Prop.GetEnumerableType(propertyInfo.PropertyType) != typeof(char))
                    {
                        var valuesEnumerator = ((System.Collections.IEnumerable)propertyInfo.GetValue(model)).GetEnumerator();
                        var index = 0;
                        while (valuesEnumerator.MoveNext())
                        {
                            List<KeyValue> subErrors;
                            Validate(valuesEnumerator.Current, localize, out subErrors, currentParentName + "[" + index + "].");
                            errors.AddRange(subErrors);
                            index++;
                        }
                    }
                }
                else if (!propertyInfo.PropertyType.Namespace.StartsWith("System"))
                {
                    var currentParentName = propertyInfo.Name;
                    var item = propertyInfo.GetValue(model);
                    List<KeyValue> subErrors;
                    Validate(item, localize, out subErrors, currentParentName + ".");
                    errors.AddRange(subErrors);
                }

                var attributes = propertyInfo.GetCustomAttributes(typeof(AcValidationBaseAttribute), true);

                foreach (var attributeObject in attributes)
                {
                    var attribute = (AcValidationBaseAttribute)attributeObject;
                    var name = propertyInfo.Name;
                    var value = propertyInfo.GetValue(model);

                    if (attribute.Type == "Required")
                    {
                        if (_.Prop.GetEnumerableType(propertyInfo.PropertyType) != null && _.Prop.GetEnumerableType(propertyInfo.PropertyType) != typeof(char))
                        {
                            var valuesEnumerator = ((System.Collections.IEnumerable)propertyInfo.GetValue(model)).GetEnumerator();
                            var index = 0;
                            while (valuesEnumerator.MoveNext())
                            {
                                index++;
                            }
                            if (index == 0)
                            {
                                errors.Add(new KeyValue { Key = parentName + name, Value = localize(attribute.ErrorMessage) });
                            }
                        }
                        else if (value == null)
                        {
                            errors.Add(new KeyValue { Key = parentName + name, Value = localize(attribute.ErrorMessage) });
                        }
                        else
                        {
                            if (value is string)
                            {
                                var valueItem = (string)value;
                                if (string.IsNullOrWhiteSpace(valueItem))
                                {
                                    errors.Add(new KeyValue { Key = parentName + name, Value = localize(attribute.ErrorMessage) });
                                }
                            }
                            else if (value is HttpPostedFileBase)
                            {
                                var valueItem = (HttpPostedFileBase)value;
                                if (valueItem.ContentLength <= 0 || string.IsNullOrWhiteSpace(valueItem.FileName))
                                {
                                    errors.Add(new KeyValue { Key = parentName + name, Value = localize(attribute.ErrorMessage) });
                                }
                            }
                        }
                    }
                    else if (attribute.Type == "Email")
                    {
                        if (value != null)
                        {
                            var valueItem = (string)value;
                            if (!string.IsNullOrWhiteSpace(valueItem))
                            {
                                if (!_.Validation.ValidateEmail(valueItem))
                                {
                                    errors.Add(new KeyValue { Key = parentName + name, Value = localize(attribute.ErrorMessage) });
                                }
                            }
                        }
                    }
                    else if (attribute.Type == "Tckn")
                    {
                        if (value != null)
                        {
                            var valueItem = (string)value;
                            if (!string.IsNullOrWhiteSpace(valueItem))
                            {
                                if (!_.Validation.ValidateTckn(valueItem))
                                {
                                    errors.Add(new KeyValue { Key = parentName + name, Value = localize(attribute.ErrorMessage) });
                                }
                            }
                        }
                    }
                    else if (attribute.Type == "MaxLength")
                    {
                        if (value != null)
                        {
                            var valueItem = (string)value;
                            if (!string.IsNullOrWhiteSpace(valueItem))
                            {
                                var maxLength = Convert.ToInt32(attribute.Parameters);
                                if (valueItem.Length > maxLength)
                                {
                                    errors.Add(new KeyValue { Key = parentName + name, Value = ((localize(attribute.ErrorMessage) ?? "").Replace("{0}", maxLength + "")) });
                                }
                            }
                        }
                    }
                    else if (attribute.Type == "MinLength")
                    {
                        if (value != null)
                        {
                            var valueItem = (string)value;
                            if (!string.IsNullOrWhiteSpace(valueItem))
                            {
                                var minLength = Convert.ToInt32(attribute.Parameters);
                                if (valueItem.Length < minLength)
                                {
                                    errors.Add(new KeyValue { Key = parentName + name, Value = ((localize(attribute.ErrorMessage) ?? "").Replace("{0}", minLength + "")) });
                                }
                            }
                        }
                    }
                    else if (attribute.Type == "BoolMustBeTrue")
                    {
                        if (value is bool)
                        {
                            var valueItem = (bool)value;
                            if (!valueItem)
                            {
                                errors.Add(new KeyValue { Key = parentName + name, Value = localize(attribute.ErrorMessage) });
                            }
                        }
                        else
                        {
                            errors.Add(new KeyValue { Key = parentName + name, Value = localize(attribute.ErrorMessage) });
                        }
                    }
                    else if (attribute.Type == "MustBeSame")
                    {
                        var valueItem = (string)value;

                        var relatedPropertyName = attribute.Parameters;
                        var relatedPropertyValue = (string)model.GetType().GetProperty(relatedPropertyName).GetValue(model);

                        if ((valueItem ?? "") != (relatedPropertyValue ?? ""))
                        {
                            errors.Add(new KeyValue { Key = parentName + name, Value = (localize(attribute.ErrorMessage)??"").Replace("{0}", relatedPropertyName) });
                        }
                    }
                    else if (attribute.Type == "NonAlphaNumeric")
                    {
                        var additionalAllowedValues = attribute.Parameters;

                        var valueItem = (string)value;

                        Regex pattern = new Regex("^[0-9a-zA-Z_" + additionalAllowedValues + "-]+$");
                        if (!pattern.IsMatch(valueItem))
                        {
                            errors.Add(new KeyValue { Key = parentName + name, Value = localize(attribute.ErrorMessage) });
                        }
                    }
                }
            }

            return !errors.Any();
        }



    }
}
