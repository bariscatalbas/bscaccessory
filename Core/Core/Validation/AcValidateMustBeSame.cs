﻿namespace Core.Validation
{
    public class AcValidateMustBeSame : AcValidationBaseAttribute
    {
        public AcValidateMustBeSame(string fieldNameToValidate, string errorMessage = "AcValidation.MustBeSame") : base("MustBeSame", errorMessage, fieldNameToValidate)
        {
        }
    }
}
