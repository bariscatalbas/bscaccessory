﻿namespace Core.Validation
{
    public class AcValidateMinLengthAttribute : AcValidationBaseAttribute
    {
        public AcValidateMinLengthAttribute(int minLength, string errorMessage = "AcValidation.MinLength") : base("MinLength", errorMessage, minLength + "")
        {
        }
    }
}
