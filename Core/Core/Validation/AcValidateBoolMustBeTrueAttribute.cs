﻿namespace Core.Validation
{
    public class AcValidateBoolMustBeTrueAttribute : AcValidationBaseAttribute
    {
        public AcValidateBoolMustBeTrueAttribute(string errorMessage = "AcValidation.BoolMustBeTrue") : base("BoolMustBeTrue", errorMessage)
        {
        }
    }
}
