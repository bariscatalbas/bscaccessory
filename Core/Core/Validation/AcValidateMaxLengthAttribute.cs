﻿namespace Core.Validation
{
    public class AcValidateMaxLengthAttribute : AcValidationBaseAttribute
    {
        public AcValidateMaxLengthAttribute(int maxLength, string errorMessage = "AcValidation.MaxLength") : base("MaxLength", errorMessage, maxLength + "")
        {
        }
    }
}
