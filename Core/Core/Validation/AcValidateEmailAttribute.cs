﻿namespace Core.Validation
{
    public class AcValidateEmailAttribute : AcValidationBaseAttribute
    {
        public AcValidateEmailAttribute(string errorMessage = "AcValidation.Email") : base("Email", errorMessage)
        {
        }
    }
}
