﻿using System;

namespace Core.Validation
{
    public class AcValidationBaseAttribute : Attribute
    {
        public string Type { get; set; }
        public string Parameters { get; set; }
        public string ErrorMessage { get; set; }

        public AcValidationBaseAttribute(string type, string errorMessage, string parameters = null)
        {
            Type = type;
            ErrorMessage = errorMessage;
            Parameters = parameters;
        }
    }
}
