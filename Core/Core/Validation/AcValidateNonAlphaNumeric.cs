﻿namespace Core.Validation
{
    public class AcValidateNonAlphaNumeric : AcValidationBaseAttribute
    {
        public AcValidateNonAlphaNumeric(string errorMessage = "AcValidation.NonAlphaNumeric", string additionalAllowedValues = "") : base("NonAlphaNumeric", errorMessage, additionalAllowedValues)
        {
        }
    }
}
