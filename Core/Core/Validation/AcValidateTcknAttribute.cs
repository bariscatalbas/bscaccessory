﻿namespace Core.Validation
{
    public class AcValidateTcknAttribute : AcValidationBaseAttribute
    {
        public AcValidateTcknAttribute(string errorMessage = "AcValidation.Tckn") : base("Tckn", errorMessage)
        {
        }
    }
}
