﻿using Core.Data.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Media
{
    public class AcMedia : EntityBase
    {
        [MaxLength(50)]
        public virtual string MediaProviderCode { get; set; }
        [MaxLength(250)]
        public virtual string IntegrationCode { get; set; }

        [MaxLength(50)]
        public virtual string MimeType { get; set; }
        [MaxLength(50)]
        public virtual string Module { get; set; }
        [MaxLength(500)]
        public virtual string FileName { get; set; }
        [MaxLength(500)]
        public virtual string AltText { get; set; }
        [MaxLength(4000)]
        public virtual string Url { get; set; }

    }
}
