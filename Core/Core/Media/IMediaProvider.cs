﻿using Core.Provider;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Media
{
    public interface IMediaProvider : IProvider
    {
        MediaProviderModel UploadMedia(string mimeType, string module, string fileName, byte[] data);
    }
}
