﻿using Core.Cache;
using Core.Data.Interfaces;
using Core.Ioc;
using Core.Provider;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Core.Media
{
    public class AcMediaHelper : IInjectAsSelf
    {
        private readonly IRepository<AcMedia> _acMediaRepository;

        public AcMediaHelper(IRepository<AcMedia> acMediaRepository)
        {
            _acMediaRepository = acMediaRepository;
        }

        private IMediaProvider GetProvider(string code = "")
        {
            if (string.IsNullOrWhiteSpace(code))
            {
                code = AcProviderHelper.GetCoreSettings().MediaProviderCode;
            }

            return AcProviderHelper.GetProvider<IMediaProvider>("media", code);
        }


        public long UploadMedia(HttpPostedFileBase file, string module, string altText, long identityId,
            string resize = "", string thumbSizes = "", string sizeValidation = "", string overrideFileName = "")
        {
            if (file != null && file.ContentLength > 0 && !string.IsNullOrWhiteSpace(file.FileName))
            {
                byte[] data = null;
                byte[] resizeData = null;
                using (var binaryReader = new BinaryReader(file.InputStream))
                {
                    data = binaryReader.ReadBytes(file.ContentLength);
                }

                var fileName = file.FileName;
                if (!string.IsNullOrWhiteSpace(overrideFileName))
                {
                    fileName = overrideFileName;
                }
                var mimeType = FindMimeType(fileName);

                if (!string.IsNullOrWhiteSpace(resize))
                {
                    resizeData = _.Img.ResizeImage(data, resize);
                }
                else
                {
                    resizeData = data;
                }

                if (!string.IsNullOrWhiteSpace(sizeValidation))
                {
                    var sizesToValidate = sizeValidation.Split(',').Where(p => !string.IsNullOrWhiteSpace(p)).Select(p => p.Trim()).ToList();
                    foreach (var size in sizesToValidate)
                    {
                        if (size.StartsWith("w"))
                        {
                            var realWidth = _.Img.GetImageWidth(resizeData);
                            var widthMustBe = 0;
                            int.TryParse(size.Substring(1), out widthMustBe);
                            if (realWidth != widthMustBe)
                            {
                                return -1;
                            }
                        }

                        if (size.StartsWith("h"))
                        {
                            var realHeight = _.Img.GetImageHeight(resizeData);
                            var heightMustBe = 0;
                            int.TryParse(size.Substring(1), out heightMustBe);
                            if (realHeight != heightMustBe)
                            {
                                return -1;
                            }
                        }
                    }
                }

                var result = UploadMedia(mimeType, module, fileName, altText, resizeData, identityId);

                if (result != null && result.Id > 0)
                {
                    if (!string.IsNullOrWhiteSpace(thumbSizes))
                    {
                        string providerCode = "";
                        var thumbSizeArray = thumbSizes.Split(',').Where(p => !string.IsNullOrWhiteSpace(p)).Select(p => p.Trim()).ToList();
                        foreach (var thumbSize in thumbSizeArray)
                        {
                            var thumbData = _.Img.ResizeImage(data, thumbSize);
                            if (thumbData != null && thumbData.Any())
                            {
                                var thumbFileName = _.Io.AppendTextToFileName(result.FileName, "_" + thumbSize);
                                var uploadedFileResult = UploadMediaToProvider(mimeType, module + "/thumbs", thumbFileName, altText, thumbData, identityId, providerCode, out providerCode);
                            }
                        }
                    }
                }

                return result.Id;
            }
            return 0;
        }

        public string UploadMediaGetUrl(HttpPostedFileBase file, string module, string altText, long identityId)
        {
            if (file != null && file.ContentLength > 0 && !string.IsNullOrWhiteSpace(file.FileName))
            {
                byte[] data = null;
                using (var binaryReader = new BinaryReader(file.InputStream))
                {
                    data = binaryReader.ReadBytes(file.ContentLength);
                }

                var fileName = file.FileName;
                var mimeType = FindMimeType(fileName);

                string providerCode;
                var result = UploadMediaToProvider(mimeType, module, fileName, altText, data, identityId, "", out providerCode);
                return result.Url;
            }
            return "";
        }

        public string UploadMediaGetFileName(HttpPostedFileBase file, string module, string altText, long identityId, string overrideFileName = "")
        {
            if (file != null && file.ContentLength > 0 && !string.IsNullOrWhiteSpace(file.FileName))
            {
                byte[] data = null;
                using (var binaryReader = new BinaryReader(file.InputStream))
                {
                    data = binaryReader.ReadBytes(file.ContentLength);
                }

                var fileName = file.FileName;
                if (!string.IsNullOrWhiteSpace(overrideFileName))
                {
                    fileName = overrideFileName;
                }
                var mimeType = FindMimeType(fileName);

                string providerCode;
                var result = UploadMediaToProvider(mimeType, module, fileName, altText, data, identityId, "", out providerCode);
                return result.Url;
            }
            return "";
        }

        public string UploadMediaGetUrl(byte[] data, string fileName, string module, string altText, long identityId)
        {
            if (data != null && data.Any() && !string.IsNullOrWhiteSpace(fileName))
            {
                var mimeType = FindMimeType(fileName);
                string providerCode;
                var result = UploadMediaToProvider(mimeType, module, fileName, altText, data, identityId, "", out providerCode);
                return result.Url;
            }
            return "";
        }

        public string UploadMediaGetFileName(byte[] data, string fileName, string module, string altText, long identityId)
        {
            if (data != null && data.Any() && !string.IsNullOrWhiteSpace(fileName))
            {
                var mimeType = FindMimeType(fileName);
                string providerCode;
                var result = UploadMediaToProvider(mimeType, module, fileName, altText, data, identityId, "", out providerCode);
                return result.FileName;
            }
            return "";
        }

        private string FindMimeType(string fileName)
        {
            var mime = "application/unknown";
            var extension = Path.GetExtension(fileName).ToLowerInvariant();
            var key = Registry.ClassesRoot.OpenSubKey(extension);
            if (key != null && key.GetValue("Content Type") != null)
            {
                mime = key.GetValue("Content Type").ToString();
            }
            return mime;
        }

        private AcMediaProviderUploadResult UploadMedia(string mimeType, string module, string fileName, string altText, byte[] data, long identityId, string providerCode = "")
        {
            var uploadedFileResult = UploadMediaToProvider(mimeType, module, fileName, altText, data, identityId, providerCode, out providerCode);

            var media = new AcMedia
            {
                AltText = altText,
                FileName = fileName,
                IntegrationCode = uploadedFileResult.IntegrationCode,
                MediaProviderCode = providerCode,
                MimeType = mimeType,
                Module = module,
                Url = uploadedFileResult.Url
            };
            _acMediaRepository.Insert(media, identityId);

            return new AcMediaProviderUploadResult
            {
                Id = media.Id,
                FileName = uploadedFileResult.FileName
            };
        }

        private MediaProviderModel UploadMediaToProvider(string mimeType, string module, string fileName, string altText, byte[] data, long identityId, string providerCode, out string selectedProviderCode)
        {
            var provider = GetProvider(providerCode);

            if (provider == null)
            {
                throw new Exception("No Media Provider Found");
            }
            selectedProviderCode = provider.Code;
            return provider.UploadMedia(mimeType, module, fileName, data);
        }

        public bool LoadMedia(long mediaId, out string url, out string altText)
        {
            var media = _acMediaRepository.Table.FirstOrDefault(p => p.Id == mediaId);
            if (media != null)
            {
                url = media.Url;
                altText = media.AltText;
                return true;
            }

            url = "";
            altText = "";
            return false;
        }

    }
}
