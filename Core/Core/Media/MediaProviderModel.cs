﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Media
{
    public class MediaProviderModel
    {
        public string IntegrationCode { get; set; }
        public string Url { get; set; }
        public string FileName { get; set; }
    }
}
