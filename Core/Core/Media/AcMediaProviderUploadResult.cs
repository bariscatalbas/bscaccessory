﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Media
{
    public class AcMediaProviderUploadResult
    {
        public long Id { get; set; }
        public string FileName { get; set; }
    }
}
