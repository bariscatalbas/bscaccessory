﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.EmailTemplates
{
    public class AcEmailTemplateMasterModel : AcEmailTemplateBaseModel
    {
        public string Content { get; set; }
    }
}
