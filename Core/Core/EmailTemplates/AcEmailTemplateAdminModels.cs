﻿using Core.Data.Models;
using Core.Mvc.Attributes;
using Core.Pagination;
using Core.Validation;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Core.EmailTemplates
{
    public class AcEmailTemplateAdminModelsCreate : EntityBase
    {
        [AcValidateRequired]
        [AcDropdownParameter("EmailProviderCodeList")]
        [UIHint("Dropdown")]
        public string EmailProviderCode { get; set; }

        [UIHint("HiddenInput")]
        public long ContainerId { get; set; }

        [AcValidateRequired]
        [AcValidateMaxLength(50)]
        [AcDropdownParameter("TypeList")]
        [UIHint("Dropdown")]
        public string Type { get; set; }

        [AllowHtml]
        [AcValidateMaxLength(250)]
        public string Subject { get; set; }

        [AllowHtml]
        [UIHint("TextArea")]
        public string Body { get; set; }

        [AcValidateMaxLength(250)]
        public string Cc { get; set; }
        [AcValidateMaxLength(250)]
        public string Bcc { get; set; }

        public bool IsActive { get; set; }
    }

    public class AcEmailTemplateAdminModelsEdit : AcEmailTemplateAdminModelsCreate
    {

    }

    public class AcEmailTemplateAdminModelsView : AcEmailTemplateAdminModelsCreate
    {

    }

    public class AcEmailTemplateAdminModelsFilter : AcFilter
    {
        [UIHint("HiddenInput")]
        public long ContainerId { get; set; }
    }
    public class AcEmailTemplateAdminModelsExcelExport : AcEmailTemplateAdminModelsView
    {

    }
}
