﻿using Core.Data.Interfaces;
using Core.Email;
using Core.Ioc;
using Omu.ValueInjecter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.EmailTemplates
{
    public class AcEmailTemplateHelper : IInjectAsSelf
    {
        private readonly IRepository<AcEmailTemplate> _acEmailTemplateRepository;

        public AcEmailTemplateHelper(IRepository<AcEmailTemplate> acEmailTemplateRepository)
        {
            _acEmailTemplateRepository = acEmailTemplateRepository;
        }


        public QueuedEmailModel FillTemplateWithModel<T>(T model, long containerId, string templateType, string masterTemplateType) where T : AcEmailTemplateBaseModel
        {
            var template = _acEmailTemplateRepository.Table.FirstOrDefault(p => p.Type == templateType && p.IsActive && p.ContainerId == containerId);
            if (template != null)
            {
                var result = new QueuedEmailModel
                {
                    AcEmailProviderCode = template.EmailProviderCode,
                    To = model.To,
                    Cc = MergeEmails(model.Cc, template.Cc),
                    Bcc = MergeEmails(model.Bcc, template.Bcc),
                    Subject = template.Subject
                };

                var content = FillTemplateWithModel(template.Body, model);

                if (!string.IsNullOrWhiteSpace(masterTemplateType))
                {
                    var masterTemplate = _acEmailTemplateRepository.Table.FirstOrDefault(p => p.Type == masterTemplateType && p.IsActive && p.ContainerId == containerId);
                    if (masterTemplate != null)
                    {
                        var masterModel = new AcEmailTemplateMasterModel();
                        masterModel.InjectFrom(model);
                        masterModel.Content = content;

                        result.Subject = result.Subject + (string.IsNullOrWhiteSpace(masterTemplate.Subject) ? "" : (" " + masterTemplate.Subject));
                        result.Cc = MergeEmails(result.Cc, masterTemplate.Cc);
                        result.Bcc = MergeEmails(result.Bcc, masterTemplate.Bcc);

                        content = FillTemplateWithModel(masterTemplate.Body, masterModel);
                    }
                }

                result.Body = content;
                return result;
            }

            return null;
        }

        private string MergeEmails(string emails, string addedEmails)
        {
            var emailList = (emails ?? "").Trim().Split(',').Where(p => !string.IsNullOrWhiteSpace(p)).Select(p => p.Trim()).ToList();
            emailList.AddRange((addedEmails ?? "").Trim().Split(',').Where(p => !string.IsNullOrWhiteSpace(p)).Select(p => p.Trim()).ToList());
            return string.Join(",", emailList.Distinct());
        }

        private string FillTemplateWithModel(string template, object model)
        {
            var result = template ?? "";

            if (model != null)
            {
                var properties = model.GetType().GetProperties();
                foreach (var propertyInfo in properties)
                {
                    result = result.Replace("{" + propertyInfo.Name + "}", propertyInfo.GetValue(model) + "");
                }
            }

            return result;
        }

    }
}
