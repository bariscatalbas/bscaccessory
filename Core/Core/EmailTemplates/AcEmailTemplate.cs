﻿using Core.Audit;
using Core.Data.Interfaces;
using Core.Data.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.EmailTemplates
{
    public class AcEmailTemplate : EntityBase, IAuditable
    {
        [MaxLength(50)]
        public virtual string EmailProviderCode { get; set; }

        public virtual long ContainerId { get; set; }

        [MaxLength(50)]
        public virtual string Type { get; set; }

        [MaxLength(250)]
        public virtual string Subject { get; set; }
        public virtual string Body { get; set; }

        [MaxLength(250)]
        public virtual string Cc { get; set; }
        [MaxLength(250)]
        public virtual string Bcc { get; set; }

        public virtual bool IsActive { get; set; }

        public AuditModel SelectLog(object repository)
        {
            var model = new
            {
                this.Id,
                this.IsActive,
                this.Subject,
                this.Type,
                this.Bcc,
                this.Cc,
                this.Body,
                this.ContainerId,
                this.EmailProviderCode,
            };

            return new AuditModel
            {
                EntityId = this.Id,
                EntityName = "EmailTemplate",
                ParentEntityId = this.ContainerId,
                ParentEntityName = "EmailTemplateContainer",
                Details = JsonConvert.SerializeObject(model)
            };
        }
    }
}
