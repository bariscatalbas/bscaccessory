﻿using Core.Data.Interfaces;
using Core.Mvc.Base;
using Core.Pagination;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.EmailTemplates
{
    public class AcEmailTemplateAdminService : DataServiceBase<AcEmailTemplate, AcEmailTemplateAdminModelsCreate, AcEmailTemplateAdminModelsEdit, AcEmailTemplateAdminModelsView, AcEmailTemplateAdminModelsFilter, AcEmailTemplateAdminModelsExcelExport>
    {
        public AcEmailTemplateAdminService(IRepository<AcEmailTemplate> repository) : base(repository)
        {
        }

        public override AcPager<AcEmailTemplateAdminModelsView> Index(AcEmailTemplateAdminModelsFilter filter)
        {
            return Repository
                        .Table
                        .Where(p => p.ContainerId == filter.ContainerId)
                        .Select(p => new AcEmailTemplateAdminModelsView
                        {
                            Id = p.Id,
                            Bcc = p.Bcc,
                            Body = p.Body,
                            Cc = p.Cc,
                            ContainerId = p.ContainerId,
                            EmailProviderCode = p.EmailProviderCode,
                            IsActive = p.IsActive,
                            Subject = p.Subject,
                            Type = p.Type
                        }).ToAcPager(filter);
        }

        public override AcPager<AcEmailTemplateAdminModelsExcelExport> ExportIndex(AcEmailTemplateAdminModelsFilter filter)
        {
            return Repository
                        .Table
                        .Where(p => p.ContainerId == filter.ContainerId)
                        .Select(p => new AcEmailTemplateAdminModelsExcelExport
                        {
                            Id = p.Id,
                            Bcc = p.Bcc,
                            Body = p.Body,
                            Cc = p.Cc,
                            ContainerId = p.ContainerId,
                            EmailProviderCode = p.EmailProviderCode,
                            IsActive = p.IsActive,
                            Subject = p.Subject,
                            Type = p.Type
                        }).ToAcPager(filter);
        }

    }
}
