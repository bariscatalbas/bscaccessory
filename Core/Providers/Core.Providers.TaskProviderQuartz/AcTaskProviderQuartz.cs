﻿using Core.KeepAlive;
using Core.Task;
using Newtonsoft.Json;
using Quartz;
using Quartz.Impl;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Threading.Tasks;

namespace Core.Providers.TaskProviderQuartz
{
    public class AcTaskProviderQuartz : ITaskProvider
    {
        public string Code => "AcTaskProviderQuartz";
        public string Type => "task";
        public string Name => "Ac Task Provider Quartz";

        public string SettingsSystemName { get { return typeof(AcTaskProviderQuartzSettingsModel).AssemblyQualifiedName; } }

        private AcTaskProviderQuartzSettingsModel Settings { get; set; }

        public Task<IScheduler> GetScheduler()
        {
            StdSchedulerFactory stdSchedulerFactory = new StdSchedulerFactory();
            var scheduler = stdSchedulerFactory.GetScheduler();
            return scheduler;
        }
        public void Initialize(string settingsJson)
        {
            Settings = JsonConvert.DeserializeObject<AcTaskProviderQuartzSettingsModel>(settingsJson);
        }

        public void Start(List<AcTaskModel> tasks)
        {
            StdSchedulerFactory factory = new StdSchedulerFactory();
            IScheduler sched = factory.GetScheduler().Result;
            sched.Start();

            foreach (var item in tasks)
            {
                Type type = System.Type.GetType(item.SystemName);
                if(type != null)
                {
                    if (item.SkipIfLocked)
                    {
                        Type genericType = typeof(AcTaskProviderQuartzTaskBasePreventSimultaneouslyRun<>).MakeGenericType(type);
                        var name = item.SystemName.ToLowerInvariant();

                        IJobDetail job = JobBuilder.Create(genericType)
                            .WithIdentity("job_" + name)
                            .Build();

                        ITrigger trigger = TriggerBuilder.Create()
                            .WithIdentity("trigger_" + name)
                            .StartNow()
                            .WithSimpleSchedule(x => x
                                .WithIntervalInSeconds(item.Seconds)
                                .RepeatForever())
                        .Build();

                        sched.ScheduleJob(job, trigger);
                    }
                    else
                    {
                        Type genericType = typeof(AcTaskProviderQuartzTaskBase<>).MakeGenericType(type);
                        var name = item.SystemName.ToLowerInvariant();

                        IJobDetail job = JobBuilder.Create(genericType)
                            .WithIdentity("job_" + name)
                            .Build();

                        ITrigger trigger = TriggerBuilder.Create()
                            .WithIdentity("trigger_" + name)
                            .StartNow()
                            .WithSimpleSchedule(x => x
                                .WithIntervalInSeconds(item.Seconds)
                                .RepeatForever())
                        .Build();

                        sched.ScheduleJob(job, trigger);
                    }
                }
            }
        }

        public void Run<T>(params object[] parameters) where T : AcAsyncRunBase
        {
            StdSchedulerFactory factory = new StdSchedulerFactory();
            IScheduler sched = factory.GetScheduler().Result;
            sched.Start();

            Type type = typeof(T);
            if (type != null)
            {
                Type genericType = typeof(AcTaskProviderQuartzAsyncRunBase<>).MakeGenericType(type);
                var name = _.Crypto.GenerateRandomString(8);

                IJobDetail job = JobBuilder.Create(genericType)
                    .WithIdentity("job_" + name)
                    .Build();

                job.JobDataMap["parameters"] = parameters;

                ITrigger trigger = TriggerBuilder.Create()
                    .WithIdentity("trigger_" + name)
                    .StartNow()
                    .Build();

                sched.ScheduleJob(job, trigger);
            }
        }

        public void Stop()
        {
            StdSchedulerFactory factory = new StdSchedulerFactory();
            IScheduler sched = factory.GetScheduler().Result;
            sched.Shutdown();
        }
    }
}
