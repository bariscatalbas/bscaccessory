﻿using Core.Ioc;
using Core.Task;
using Quartz;

namespace Core.Providers.TaskProviderQuartz
{
    [DisallowConcurrentExecution]
    public class AcTaskProviderQuartzTaskBasePreventSimultaneouslyRun<T> : IJob
        where T : AcTaskBase
    {
        public AcTaskProviderQuartzTaskBasePreventSimultaneouslyRun()
        {

        }

        public System.Threading.Tasks.Task Execute(IJobExecutionContext context)
        {
            var task = AcEngine.Resolve<T>();
            task.Run();

            return null;
        }
    }
}
