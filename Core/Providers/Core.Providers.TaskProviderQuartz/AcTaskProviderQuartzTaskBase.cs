﻿using Core.Ioc;
using Core.Task;
using Quartz;

namespace Core.Providers.TaskProviderQuartz
{
    public class AcTaskProviderQuartzTaskBase<T> : IJob
        where T : AcTaskBase
    {
        public AcTaskProviderQuartzTaskBase()
        {

        }

        public System.Threading.Tasks.Task Execute(IJobExecutionContext context)
        {
            var task = AcEngine.Resolve<T>();
            task.Run();

            return null;
        }
    }
}
