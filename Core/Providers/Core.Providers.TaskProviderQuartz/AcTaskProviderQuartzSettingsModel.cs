﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Providers.TaskProviderQuartz
{
    public class AcTaskProviderQuartzSettingsModel
    {
        public bool IsActive { get; set; }
    }
}
