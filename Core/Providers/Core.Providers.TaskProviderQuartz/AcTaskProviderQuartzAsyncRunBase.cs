﻿using Core.Ioc;
using Core.Task;
using Quartz;

namespace Core.Providers.TaskProviderQuartz
{
    public class AcTaskProviderQuartzAsyncRunBase<T> : IJob
        where T : AcAsyncRunBase
    {
        public AcTaskProviderQuartzAsyncRunBase()
        {

        }

        public System.Threading.Tasks.Task Execute(IJobExecutionContext context)
        {
            var parameters = (object[])context.MergedJobDataMap["parameters"];

            var task = AcEngine.Resolve<T>();
            task.Run(parameters);

            return null;
        }
    }
}
