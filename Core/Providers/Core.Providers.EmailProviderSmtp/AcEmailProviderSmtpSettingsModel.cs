﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Providers.EmailProviderSmtp
{
    public class AcEmailProviderSmtpSettingsModel
    {
        [DisplayName("Is Active?")]
        public bool IsActive { get; set; }

        [DisplayName("SMTP Server Address")]
        public string SmtpServer { get; set; }
        [DisplayName("SMTP Server Port")]
        public int SmtpPort { get; set; }
        [DisplayName("Is SSL Enabled?")]
        public bool SSLEnabled { get; set; }

        [DisplayName("Sender Account Name")]
        public string SenderAccountName { get; set; }
        [DisplayName("Sender Account Password")]
        public string SenderAccountPassword { get; set; }

        [DisplayName("Sender Email Address")]
        public string SenderEmailAddress { get; set; }
        [DisplayName("Senden Name")]
        public string SenderName { get; set; }

    }
}
