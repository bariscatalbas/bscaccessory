﻿using Core.Email;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Core.Providers.EmailProviderSmtp
{
    public class AcEmailProviderSmtp : IEmailProvider
    {
        public string Code => "AcEmailProviderSmtp";
        public string Type => "email";
        public string Name => "Ac Email Provider SMTP";
        public string SettingsSystemName  { get { return typeof(AcEmailProviderSmtpSettingsModel).AssemblyQualifiedName; } }


        private AcEmailProviderSmtpSettingsModel Settings { get;set;}
        public void Initialize(string settingsJson)
        {
            Settings = JsonConvert.DeserializeObject<AcEmailProviderSmtpSettingsModel>(settingsJson);
        }


        public AcEmailProviderSmtp()
        {

        }

        public string SendEmail(QueuedEmailModel email)
        {
            if(Settings.IsActive)
            {
                try
                {
                    var message = new MailMessage
                    {
                        IsBodyHtml = true,
                        SubjectEncoding = Encoding.UTF8,
                        Subject = email.Subject,
                        BodyEncoding = Encoding.UTF8,
                        Body = email.Body
                    };

                    if (email.To.Contains(",") || email.To.Contains(";"))
                    {
                        var toEmails = (email.To ?? "").Split(new[] { ',', ';' }, StringSplitOptions.RemoveEmptyEntries).Select(p => p.Trim());
                        foreach (var toEmail in toEmails.Where(toEmail => _.Validation.ValidateEmail(toEmail, false)))
                        {
                            message.To.Add(new MailAddress(toEmail));
                        }
                    }
                    else
                    {
                        var toEmail = email.To.Trim();
                        if (_.Validation.ValidateEmail(toEmail, false))
                        {
                            message.To.Add(new MailAddress(toEmail));
                        }
                    }

                    var ccEmails = (email.Cc ?? "").Split(new[] { ',', ';' }, StringSplitOptions.RemoveEmptyEntries).Select(p => p.Trim());
                    foreach (var ccEmail in ccEmails.Where(ccEmail => _.Validation.ValidateEmail(ccEmail, false)))
                    {
                        message.CC.Add(new MailAddress(ccEmail));
                    }

                    var bccEmails = (email.Bcc ?? "").Split(new[] { ',', ';' }, StringSplitOptions.RemoveEmptyEntries).Select(p => p.Trim());
                    foreach (var bccEmail in bccEmails.Where(bccEmail => _.Validation.ValidateEmail(bccEmail, false)))
                    {
                        message.Bcc.Add(new MailAddress(bccEmail));
                    }

                    var attachments = (email.Attachments ?? "").Split(new[] { ',', ';' }, StringSplitOptions.RemoveEmptyEntries).Select(p => p.Trim());
                    foreach (var attachment in attachments)
                    {
                        if (!string.IsNullOrWhiteSpace(attachment) && File.Exists(attachment))
                        {
                            message.Attachments.Add(new Attachment(attachment));
                        }
                    }

                    message.From = !string.IsNullOrWhiteSpace(Settings.SenderName)
                                      ? new MailAddress(Settings.SenderEmailAddress, Settings.SenderName)
                                      : new MailAddress(Settings.SenderEmailAddress);

                    if (string.IsNullOrWhiteSpace(Settings.SmtpServer) || Settings.SmtpPort <= 0)
                    {
                        return "SMTP server or SMTP port is not valid!";
                    }

                    var client = new SmtpClient(Settings.SmtpServer, Settings.SmtpPort);

                    if (!string.IsNullOrWhiteSpace(Settings.SenderAccountName) && !string.IsNullOrWhiteSpace(Settings.SenderAccountPassword))
                    {
                        client.UseDefaultCredentials = false;
                        client.Credentials = new NetworkCredential(Settings.SenderAccountName, Settings.SenderAccountPassword);
                    }

                    client.EnableSsl = Settings.SSLEnabled;
                    client.Send(message);

                    return "";
                }
                catch (Exception exception)
                {
                    return _.Web.OpenException(exception);
                }
            }
            else
            {
                return "Email sending is deactive!";
            }
        }
    }
}
