﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Providers.SmsProviderVerimor
{
    internal class SmsModel
    {
        public string username { get; set; }
        public string password { get; set; }
        public string source_addr { get; set; }
        public SmsModelMessage[] messages { get; set; }
    }

    internal class SmsModelMessage
    {
        public SmsModelMessage()
        {
        }
        public SmsModelMessage(string msg, string dest)
        {
            this.msg = msg;
            this.dest = dest;
        }

        public string msg { get; set; }
        public string dest { get; set; }


    }
}
