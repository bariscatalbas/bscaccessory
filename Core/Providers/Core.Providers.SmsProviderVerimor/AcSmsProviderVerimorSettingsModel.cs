﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Providers.SmsProviderVerimor
{
    public class AcSmsProviderVerimorSettingsModel
    {
        public bool IsActive { get; set; }

        public string Url { get; set; } // http://sms.verimor.com.tr/v2/send.json
        public string SourceAddress { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
