﻿using Core.Sms;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Core.Providers.SmsProviderVerimor
{
    public class AcSmsProviderVerimor : ISmsProvider
    {
        public string Code => "AcSmsProviderVerimor";
        public string Type => "sms";
        public string Name => "Ac SMS Provider Verimor";
        public string SettingsSystemName { get { return typeof(AcSmsProviderVerimorSettingsModel).AssemblyQualifiedName; } }


        private AcSmsProviderVerimorSettingsModel Settings { get; set; }
        public void Initialize(string settingsJson)
        {
            Settings = JsonConvert.DeserializeObject<AcSmsProviderVerimorSettingsModel>(settingsJson);
        }

        public string SendSms(QueuedSmsModel sms)
        {
            if (Settings.IsActive)
            {
                if (string.IsNullOrWhiteSpace(Settings.Url))
                {
                    throw new Exception("Settings URL cannot be EMPTY");
                }
                if (string.IsNullOrWhiteSpace(Settings.UserName))
                {
                    throw new Exception("Settings UserName cannot be EMPTY");
                }
                if (string.IsNullOrWhiteSpace(Settings.Password))
                {
                    throw new Exception("Settings Password cannot be EMPTY");
                }
                if (string.IsNullOrWhiteSpace(Settings.SourceAddress))
                {
                    throw new Exception("Settings SourceAddress cannot be EMPTY");
                }
                if (string.IsNullOrWhiteSpace(sms.To))
                {
                    throw new Exception("SMS To field cannot be EMPTY");
                }
                if (string.IsNullOrWhiteSpace(sms.Message))
                {
                    throw new Exception("SMS Message field cannot be EMPTY");
                }

                var phone = _.String.CleanPhoneNumber(sms.To);
                if (phone.Length != 10)
                {
                    throw new Exception("Invalid Phone: " + phone);
                }

                var smsMessage = new SmsModel
                {
                    username = Settings.UserName,
                    password = Settings.Password,
                    source_addr = Settings.SourceAddress,
                    messages = new SmsModelMessage[] { new SmsModelMessage(sms.Message, "90" + phone) }
                };

                string payload = JsonConvert.SerializeObject(smsMessage);

                WebClient wc = new WebClient();
                wc.Headers["Content-Type"] = "application/json";
                wc.Encoding = Encoding.UTF8;

                return wc.UploadString(Settings.Url, payload);
                // TODO: Catch Exception = Check is valid..
            }
            else
            {
                throw new Exception("SMS sending is deactive!");
            }
        }
    }
}
