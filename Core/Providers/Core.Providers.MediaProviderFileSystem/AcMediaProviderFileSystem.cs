﻿using Core.Data.Interfaces;
using Core.Ioc;
using Core.Media;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Core.Providers.MediaProviderFileSystem
{
    public class AcMediaProviderFileSystem : IMediaProvider
    {
        private AcMediaProviderFileSystemSettingsModel Settings { get; set; }

        public AcMediaProviderFileSystem()
        {
        }

        public string Code { get { return "AcMediaProviderFileSystem"; } }
        public string Type { get { return "media"; } }
        public string Name { get { return "Ac Media Provider File System"; } }

        public string SettingsSystemName { get { return typeof(AcMediaProviderFileSystemSettingsModel).AssemblyQualifiedName; } }

        public void Initialize(string settingsJson)
        {
            Settings = new AcMediaProviderFileSystemSettingsModel();

            if (!string.IsNullOrWhiteSpace(settingsJson))
            {
                Settings = JsonConvert.DeserializeObject<AcMediaProviderFileSystemSettingsModel>(settingsJson);
            }
        }

        public MediaProviderModel UploadMedia(string mimeType, string module, string fileName, byte[] data)
        {
            if (string.IsNullOrWhiteSpace(module))
                throw new Exception("Upload Media: Module Cannot Be NULL");
            if (string.IsNullOrWhiteSpace(fileName))
                throw new Exception("Upload Media: File Name Cannot Be NULL");
            if (data == null || data.Length <= 0)
                throw new Exception("Upload Media: Data Cannot Be NULL");

            var folder = Settings.FileSystemPrefix + module;
            _.Io.CheckAndCreateFolder(folder);
            var newFileName = _.Io.FindFileNameToUpload(folder, fileName);
            var newFilePath = folder + "/" + newFileName;
            var fullPath = HttpContext.Current.Server.MapPath(newFilePath);
            File.WriteAllBytes(fullPath, data);

            var result = new MediaProviderModel
            {
                IntegrationCode = "",
                Url = Settings.UrlPrefix + module + "/" + newFileName,
                FileName = newFileName
            };

            return result;
        }

    }
}
