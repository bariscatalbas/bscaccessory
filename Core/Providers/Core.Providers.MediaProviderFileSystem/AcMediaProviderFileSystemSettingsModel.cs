﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Providers.MediaProviderFileSystem
{
    public class AcMediaProviderFileSystemSettingsModel
    {
        [DisplayName("File System Prefix")]
        public string FileSystemPrefix { get; set; }
        [DisplayName("Url Prefix")]
        public string UrlPrefix { get; set; }
    }
}
