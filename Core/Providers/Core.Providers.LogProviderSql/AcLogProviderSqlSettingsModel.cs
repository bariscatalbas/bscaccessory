﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Providers.LogProviderSql
{
    public class AcLogProviderSqlSettingsModel
    {
        [DisplayName("Is Active?")]
        public bool IsActive { get; set; }
        
        [DisplayName("Log Important?")]
        public bool LogImportant { get; set; }
        [DisplayName("Log Info?")]
        public bool LogInfo { get; set; }
        [DisplayName("Log Warning?")]
        public bool LogWarning { get; set; }
        [DisplayName("Log Action?")]
        public bool LogAction { get; set; }
        [DisplayName("Log Error?")]
        public bool LogError { get; set; }
    }
}
