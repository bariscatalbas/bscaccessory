﻿using Core.Data.Interfaces;
using Core.Data.Models;
using Core.Ioc;
using Core.Log;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Core.Data.Services;
using Core.Pagination;

namespace Core.Providers.LogProviderSql
{
    public class AcLogProviderSql : ILogProvider
    {
        private readonly IRepository<AcLog> _logRepository;

        public AcLogProviderSql()
        {
            _logRepository = new AcRepository<AcLog>(new DbContext());
        }

        public string Code { get { return "AcLogProviderSql"; } }
        public string Type => "log";

        public string Name { get { return "Ac Log Provider SQL"; } }

        public string SettingsSystemName { get { return typeof(AcLogProviderSqlSettingsModel).AssemblyQualifiedName; } }

        private AcLogProviderSqlSettingsModel Settings { get; set; }

        public void Initialize(string settingsJson)
        {
            Settings = JsonConvert.DeserializeObject<AcLogProviderSqlSettingsModel>(settingsJson);
        }

        public void LogImportant<TIdentity, TRequest, TResponse>(string module, string action, string function, TIdentity identity, string searchable,
            TRequest request, TResponse response, string details)
            where TIdentity : EntityBase
            where TRequest : class
            where TResponse : class
        {
            if (Settings.IsActive && Settings.LogImportant)
            {
                Log("Important", module, action, function, identity, searchable, request, response, details);
            }
        }

        public void LogInfo<TIdentity, TRequest, TResponse>(string module, string action, string function, TIdentity identity, string searchable,
        TRequest request, TResponse response, string details)
            where TIdentity : EntityBase
            where TRequest : class
            where TResponse : class
        {
            if (Settings.IsActive && Settings.LogInfo)
            {
                Log("Info", module, action, function, identity, searchable, request, response, details);
            }
        }

        public void LogWarning<TIdentity, TRequest, TResponse>(string module, string action, string function, TIdentity identity, string searchable,
        TRequest request, TResponse response, string details)
            where TIdentity : EntityBase
            where TRequest : class
            where TResponse : class
        {
            if (Settings.IsActive && Settings.LogWarning)
            {
                Log("Warning", module, action, function, identity, searchable, request, response, details);
            }
        }

        public void LogAction<TIdentity, TRequest, TResponse>(string module, string action, string function, TIdentity identity, string searchable,
        TRequest request, TResponse response, string details)
            where TIdentity : EntityBase
            where TRequest : class
            where TResponse : class
        {
            if (Settings.IsActive && Settings.LogAction)
            {
                Log("Action", module, action, function, identity, searchable, request, response, details);
            }
        }

        public void LogError<TIdentity, TRequest, TResponse>(string module, string action, string function, TIdentity identity, string searchable,
            TRequest request, TResponse response, string details)
            where TIdentity : EntityBase
            where TRequest : class
            where TResponse : class
        {
            if (Settings.IsActive && Settings.LogError)
            {
                Log("Error", module, action, function, identity, searchable, request, response, details);
            }
        }

        public void LogError<TIdentity, TRequest, TResponse>(string module, string action, string function, TIdentity identity, string searchable,
            TRequest request, TResponse response, Exception exception)
            where TIdentity : EntityBase
            where TRequest : class
            where TResponse : class
        {
            if (Settings.IsActive && Settings.LogError)
            {
                var details = _.Web.OpenException(exception);
                Log("Error", module, action, function, identity, searchable, request, response, details);
            }
        }

        public void ClearOldLogs(DateTime deleteBeforeUtc)
        {
            var oldLogs = _logRepository.Table.Where(p => p.DateUtc < deleteBeforeUtc).ToList();

            foreach (var item in oldLogs)
            {
                _logRepository.Delete(item, 0);
            }

        }

        private void Log<TIdentity, TRequest, TResponse>(string level, string module, string action, string function, TIdentity identity, string searchable,
            TRequest request, TResponse response, string details)
            where TIdentity : EntityBase
            where TRequest : class
            where TResponse : class
        {
            long identityId = 0;
            if (identity != null)
            {
                identityId = identity.Id;
            }

            var requestString = "";
            if (request != null)
            {
                requestString = JsonConvert.SerializeObject(request);
            }

            var responseString = "";
            if (response != null)
            {
                responseString = JsonConvert.SerializeObject(response);
            }

            var log = new AcLog
            {
                WhereAmI = ConfigurationManager.AppSettings["WhereAmI"],
                WhichAppAmI = ConfigurationManager.AppSettings["WhichAppAmI"],
                DateUtc = DateTime.UtcNow,

                Level = level,

                Module = module,
                Action = action,
                Function = function,

                IdentityId = identityId,

                Searchable = searchable,
                Request = requestString,
                Response = requestString,
                Details = details,
            };

            if (HttpContext.Current != null && HttpContext.Current.Handler != null && HttpContext.Current.Request != null)
            {
                var httpRequest = HttpContext.Current.Request;

                log.Server = httpRequest.ServerVariables["LOCAL_ADDR"];
                log.Url = httpRequest.Url == null ? "" : httpRequest.Url.PathAndQuery;
                log.Browser = httpRequest.Browser != null ? httpRequest.Browser.Browser : "";
                log.HttpMethod = httpRequest.HttpMethod;
                log.ReferrerUrl = httpRequest.UrlReferrer == null ? "" : httpRequest.UrlReferrer.PathAndQuery;
                log.UserAgent = httpRequest.UserAgent;
                log.UserHostAddress = _.Web.IpAddress();
                log.UserHostName = httpRequest.UserHostName;
            }
            _logRepository.Insert(log, identity.Id);
        }


        public AcPager<AcLogAdminModelsList> GetLogs(AcLogAdminModelsFilter filter)
        {
            var query = _logRepository.Table;
            if (filter.StartDate.HasValue)
            {
                query = query.Where(p => p.DateUtc >= filter.StartDate.Value);
            }
            if (filter.EndDate.HasValue)
            {
                query = query.Where(p => p.DateUtc <= filter.EndDate.Value);
            }
            if (!string.IsNullOrWhiteSpace(filter.Module))
            {
                query = query.Where(p => p.Module.Contains(filter.Module));
            }

            return query.Select(p => new AcLogAdminModelsList
            {
                Id = p.Id,
                DateUtc = p.DateUtc,
                Module = p.Module,
                Action = p.Action
            }).ToAcPager(filter);
        }
        public AcLogAdminModelsDetails GetLogDetails(long id)
        {
            return _logRepository.Table.Where(p => p.Id == id)
                    .Select(p => new AcLogAdminModelsDetails
                    {
                        Id = p.Id,
                        Browser = p.Browser,
                        DateUtc = p.DateUtc,
                        Details = p.Details,
                        HttpMethod = p.HttpMethod,
                        IdentityId = p.IdentityId,
                        Module = p.Module,
                        Action = p.Action,
                        Function = p.Function,
                        Level = p.Level,
                        Request = p.Request,
                        Response = p.Response,
                        Searchable = p.Searchable,
                        Server = p.Server,
                        ReferrerUrl = p.ReferrerUrl,
                        Url = p.Url,
                        UserAgent = p.UserAgent,
                        UserHostAddress = p.UserHostAddress,
                        UserHostName = p.UserHostName,
                        WhereAmI = p.WhereAmI,
                        WhichAppAmI = p.WhichAppAmI
                    }).FirstOrDefault();
        }

    }
}
