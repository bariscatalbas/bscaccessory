﻿using Core.Data.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Providers.LogProviderSql
{
    public class AcLog: EntityBase
    {
        public virtual DateTime DateUtc { get; set; }

        [MaxLength(50)]
        public virtual string Server { get; set; }
        [MaxLength(50)]
        public virtual string WhereAmI { get; set; }
        [MaxLength(50)]
        public virtual string WhichAppAmI { get; set; }
        [MaxLength(10)]
        public virtual string Level { get; set; }

        [MaxLength(50)]
        public virtual string Module { get; set; }
        [MaxLength(50)]
        public virtual string Action { get; set; }
        [MaxLength(50)]
        public virtual string Function { get; set; }

        public virtual long IdentityId { get; set; }

        public virtual string Searchable { get; set; }
        public virtual string Request { get; set; }
        public virtual string Response { get; set; }
        public virtual string Details { get; set; }

        [MaxLength(500)]
        public virtual string Url { get; set; }
        [MaxLength(500)]
        public virtual string ReferrerUrl { get; set; }
        [MaxLength(250)]
        public virtual string Browser { get; set; }
        [MaxLength(250)]
        public virtual string UserAgent { get; set; }
        [MaxLength(50)]
        public virtual string HttpMethod { get; set; }
        [MaxLength(50)]
        public virtual string UserHostAddress { get; set; }
        [MaxLength(50)]
        public virtual string UserHostName { get; set; }
    }
}
