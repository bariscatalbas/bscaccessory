﻿using Core.Data.Services;
using System.Data.Entity;

namespace Core.Providers.LogProviderSql
{
    public class DbContext : AcContext
    {
        public DbSet<AcLog> AcLogs { get; set; }
    }
}
