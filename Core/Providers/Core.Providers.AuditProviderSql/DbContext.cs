﻿using Core.Data.Services;
using System.Data.Entity;

namespace Core.Providers.AuditProviderSql
{
    public class DbContext : AcContext
    {
        public DbSet<AcAuditLog> AcAuditLogs { get; set; }
    }
}
