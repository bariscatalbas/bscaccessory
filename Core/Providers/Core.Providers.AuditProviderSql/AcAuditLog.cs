﻿using Core.Data.Models;
using System;
using System.ComponentModel.DataAnnotations;

namespace Core.Providers.AuditProviderSql
{
    public partial class AcAuditLog : EntityBase
    {
        [MaxLength(10)]
        public virtual string WhereAmI { get; set; }
        [MaxLength(10)]
        public virtual string WhichAppAmI { get; set; }

        public virtual DateTime DateUtc { get; set; }
        public virtual long IdentityId { get; set; }

        public virtual long ParentEntityId { get; set; }
        public virtual long EntityId { get; set; }
        [MaxLength(250)]
        public virtual string EntityName { get; set; }
        [MaxLength(250)]
        public virtual string ParentEntityName { get; set; }

        [MaxLength(10)]
        public virtual string Operation { get; set; }
        public virtual string Details { get; set; }

        [MaxLength(500)]
        public virtual string Url { get; set; }
        [MaxLength(500)]
        public virtual string ReferrerUrl { get; set; }
        [MaxLength(250)]
        public virtual string Browser { get; set; }
        [MaxLength(250)]
        public virtual string UserAgent { get; set; }
        [MaxLength(50)]
        public virtual string HttpMethod { get; set; }
        [MaxLength(50)]
        public virtual string UserHostAddress { get; set; }
        [MaxLength(50)]
        public virtual string UserHostName { get; set; }
    }
}
