﻿using Core.Audit;
using Core.Data.Interfaces;
using Core.Data.Services;
using Core.Pagination;
using Newtonsoft.Json;
using System;
using System.Configuration;
using System.Linq;
using System.Web;

namespace Core.Providers.AuditProviderSql
{
    public class AcAuditProviderSql : IAuditProvider
    {
        private readonly IRepository<AcAuditLog> _auditRepository;

        public AcAuditProviderSql()
        {
            _auditRepository = new AcRepository<AcAuditLog>(new DbContext());
        }

        public string Code => "AcAuditProviderSql";
        public string Type => "audit";
        public string Name => "Ac Audit Provider SQL";
        public string SettingsSystemName { get { return typeof(AcAuditProviderSqlSettingsModel).AssemblyQualifiedName; } }

        public bool IsActive { get; set; }
        public void Initialize(string settingsJson)
        {
            var settings = JsonConvert.DeserializeObject<AcAuditProviderSqlSettingsModel>(settingsJson);
            IsActive = settings.IsActive;
        }

        public void ClearOldLogs(DateTime deleteBeforeUtc)
        {
            var oldLogs = _auditRepository.Table.Where(p => p.DateUtc < deleteBeforeUtc).ToList();

            foreach(var item in oldLogs)
            {
                _auditRepository.Delete(item, 0);
            }

        }

        public AcPager<AcAuditLogAdminModelsList> GetAuditLogs(AcAuditLogAdminModelsFilter filter)
        {
            var query = _auditRepository.Table;
            if (filter.StartDate.HasValue)
            {
                query = query.Where(p => p.DateUtc >= filter.StartDate.Value);
            }
            if (filter.EndDate.HasValue)
            {
                query = query.Where(p => p.DateUtc <= filter.EndDate.Value);
            }
            if (!string.IsNullOrWhiteSpace(filter.EntityName))
            {
                query = query.Where(p => p.EntityName.Contains(filter.EntityName));
            }

            return query.Select(p => new AcAuditLogAdminModelsList
            {
                Id = p.Id,
                EntityId = p.EntityId,
                DateUtc = p.DateUtc,
                EntityName = p.EntityName,
                ParentEntityId = p.ParentEntityId,
                ParentEntityName = p.ParentEntityName,
                Operation = p.Operation
            }).ToAcPager(filter);
        }
        public AcAuditLogAdminModelsDetails GetAuditLogDetails(long id)
        {
            return _auditRepository.Table.Where(p => p.Id == id)
                    .Select(p => new AcAuditLogAdminModelsDetails
                    {
                        Id = p.Id,
                        Browser = p.Browser,
                        DateUtc = p.DateUtc,
                        Details = p.Details,
                        EntityId = p.EntityId,
                        HttpMethod = p.HttpMethod,
                        IdentityId = p.IdentityId,
                        EntityName = p.EntityName,
                        ParentEntityName = p.ParentEntityName,
                        Operation = p.Operation,
                        ParentEntityId = p.ParentEntityId,
                        ReferrerUrl = p.ReferrerUrl,
                        Url = p.Url,
                        UserAgent = p.UserAgent,
                        UserHostAddress = p.UserHostAddress,
                        UserHostName = p.UserHostName,
                        WhereAmI = p.WhereAmI,
                        WhichAppAmI = p.WhichAppAmI
                    }).FirstOrDefault();
        }


        public void LogDelete(AuditModel model, long identityId)
        {
            Log("Delete", model, identityId);
        }
        public void LogInsert(AuditModel model, long identityId)
        {
            Log("Insert", model, identityId);
        }
        public void LogUpdate(AuditModel model, long identityId)
        {
            Log("Update", model, identityId);
        }
        private void Log(string operation, AuditModel model, long identityId)
        {
            if (!IsActive || model == null)
            {
                return;
            }

            var detailsString = "";
            if (model.Details != null)
            {
                detailsString = JsonConvert.SerializeObject(model.Details, Formatting.Indented);
            }

            var log = new AcAuditLog
            {
                WhereAmI = ConfigurationManager.AppSettings["WhereAmI"],
                WhichAppAmI = ConfigurationManager.AppSettings["WhichAppAmI"],
                DateUtc = DateTime.UtcNow,

                Operation = operation,
                IdentityId = identityId,

                EntityId = model.EntityId,
                EntityName = model.EntityName,
                ParentEntityId = model.ParentEntityId,
                ParentEntityName = model.ParentEntityName,

                Details = detailsString,
            };

            if (HttpContext.Current != null && HttpContext.Current.Handler != null && HttpContext.Current.Request != null)
            {
                var httpRequest = HttpContext.Current.Request;

                log.Url = httpRequest.Url == null ? "" : httpRequest.Url.PathAndQuery;
                log.Browser = httpRequest.Browser != null ? httpRequest.Browser.Browser : "";
                log.HttpMethod = httpRequest.HttpMethod;
                log.ReferrerUrl = httpRequest.UrlReferrer == null ? "" : httpRequest.UrlReferrer.PathAndQuery;
                log.UserAgent = httpRequest.UserAgent;
                log.UserHostAddress = _.Web.IpAddress();
                log.UserHostName = httpRequest.UserHostName;
            }

            _auditRepository.Insert(log, identityId);
        }

    }
}
