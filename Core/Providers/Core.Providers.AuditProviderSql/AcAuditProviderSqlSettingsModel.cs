﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Providers.AuditProviderSql
{
    public class AcAuditProviderSqlSettingsModel
    {
        [DisplayName("Is Audit Logging Active?")]
        public bool IsActive { get; set; }
    }
}
