﻿using Core.Media;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Providers.MediaProviderAcCdn
{
    public class AcMediaProviderAcCdn : IMediaProvider
    {
        private AcMediaProviderAcCdnSettingsModel Settings { get; set; }

        public AcMediaProviderAcCdn()
        {
        }

        public string Code { get { return "AcMediaProviderAcCdn"; } }
        public string Type { get { return "media"; } }
        public string Name { get { return "Ac Media Provider AC CDN"; } }

        public string SettingsSystemName { get { return typeof(AcMediaProviderAcCdnSettingsModel).AssemblyQualifiedName; } }

        public void Initialize(string settingsJson)
        {
            Settings = new AcMediaProviderAcCdnSettingsModel();

            if (!string.IsNullOrWhiteSpace(settingsJson))
            {
                Settings = JsonConvert.DeserializeObject<AcMediaProviderAcCdnSettingsModel>(settingsJson);
            }
        }

        public MediaProviderModel UploadMedia(string mimeType, string module, string fileName, byte[] data)
        {
            if (string.IsNullOrWhiteSpace(module))
                throw new Exception("Upload Media: Module Cannot Be NULL");
            if (string.IsNullOrWhiteSpace(fileName))
                throw new Exception("Upload Media: File Name Cannot Be NULL");
            if (data == null || data.Length <= 0)
                throw new Exception("Upload Media: Data Cannot Be NULL");

            var fileUploader = new AcCdn.FileUploader
            {
                Url = Settings.CdnUrl + "FileUploader.asmx"
            };

            var result = fileUploader.UploadFile(new AcCdn.FileUploadRequestModel
            {
                FileName = fileName,
                Module = module,
                Token = Settings.Token ?? Guid.Empty,
                Data = data
            });

            if (result.Success)
            {
                return new MediaProviderModel
                {
                    Url = Settings.CdnUrl + "Uploads/" + module + "/" + result.FileName,
                    IntegrationCode = "",
                    FileName = result.FileName
                };
            }
            else
            {
                throw new Exception(result.Message);
            }
        }
    }
}
