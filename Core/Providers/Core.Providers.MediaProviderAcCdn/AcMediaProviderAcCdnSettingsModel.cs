﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Providers.MediaProviderAcCdn
{
    public class AcMediaProviderAcCdnSettingsModel
    {
        public Guid? Token { get; set; }
        public string CdnUrl { get; set; }
    }
}
