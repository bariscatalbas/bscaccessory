﻿using AcCms.Services.Admin.Services;
using Core.Data.Models;
using Core.EventTrigger;
using Core.Ioc;
using Core.Ioc.Mvc;
using Core.Log;
using Core.Provider;
using Core.Task;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace AcCms.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            var iocEngine = new AcIocMvcEngine();
            AcProviderHelper.BindEventCachers();

            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            MvcHandler.DisableMvcResponseHeader = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;

            var taskHelper = AcEngine.Resolve<AcTaskHelper>();
            taskHelper.Start();

            AcEventTriggerHelper.eventHandlers.Add(new AcEvent(EntityEventService.HandleEvents));
        }

        protected void Application_PreSendRequestHeaders(object source, EventArgs e)
        {
            Response.Headers.Remove("Server");
        }

        protected void Application_End()
        {
            //var logProvider = AcProviderHelper.GetProviderByType<ILogProvider>();
            //if(logProvider != null)
            //{
            //    logProvider.LogInfo("Global.asax", "Application_End", "Web Application End", new EntityBase { Id = -1 }, "", "", "", "");
            //}

            var taskHelper = AcEngine.Resolve<AcTaskHelper>();
            taskHelper.Stop();
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError();

            //var logProvider = AcProviderHelper.GetProviderByType<ILogProvider>();
            //if (logProvider != null)
            //{
            //    logProvider.LogError("Global.asax", "Application_Error", "Web Unhandled Exception", new EntityBase { Id = -1 }, "", "", "", ex);
            //}
        }
    }
}
