﻿using AcCms.Models.Enumeration;
using AcCms.Services.Admin.Models;
using AcCms.Services.Admin.Mvc;
using AcCms.Services.Admin.Services;
using AcCms.Services.Common.Authentication;
using ClosedXML.Excel;
using Core.Audit;
using Core.Log;
using Core.Mvc.Models;
using Core.Mvc.Services;
using Core.Provider;
using Core.Settings;
using System;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web.Mvc;

namespace AcCms.Web.Areas.admin.Controllers
{
    [AdminAuthorize(Enums.Privilege.AdminLogs)]
    [SetModule("Settings", "Log", "Log")]

    public class LogController : AdminMvcControllerBaseWithIdentity
    {
        private readonly ILogProvider _logProvider;
        private readonly AcSettingsHelper _settingsHelper;
        private readonly CoreSettings _coreSettings;

        public LogController(WebIdentity identity, AcSettingsHelper settingsHelper) : base(identity)
        {
            _settingsHelper = settingsHelper;
            _coreSettings = _settingsHelper.LoadSettingsCached<CoreSettings>();

            _logProvider = AcProviderHelper.GetProvider<ILogProvider>("log", _coreSettings.LogProviderCode);
        }

        public ActionResult Index(AcLogAdminModelsFilter filter)
        {
            if (filter.Export)
            {
                filter.PageNumber = 1;
                filter.PageSize = int.MaxValue;

                var items = _logProvider.GetLogs(filter);

                DataTable datatable = new DataTable();

                var selectedExportProperties = filter.SelectedExportProperties.Split(',').ToList();
                var selectedExportPropertyNames = selectedExportProperties.Select(p => p.Split(':')[0]).ToList();
                var selectedExportPropertyDisplayNames = selectedExportProperties.Select(p => p.Split(':')[1]).ToList();
                foreach (var propertyDisplayName in selectedExportPropertyDisplayNames)
                {
                    datatable.Columns.Add(propertyDisplayName);
                }

                if (items.Any())
                {
                    var allProperties = items.FirstOrDefault().GetType().GetProperties();
                    var propertiesOrdered = allProperties.Where(p => selectedExportPropertyNames.Contains(p.Name)).ToList();

                    foreach (var item in items)
                    {
                        DataRow row = datatable.NewRow();
                        foreach (var propertyInfo in propertiesOrdered)
                        {
                            MemberInfo propertyMemberInfo = typeof(AcLogAdminModelsList).GetProperty(propertyInfo.Name);

                            string displayName = "";
                            var displayNameAttribute = propertyMemberInfo.GetCustomAttribute(typeof(DisplayNameAttribute)) as DisplayNameAttribute;
                            if (displayNameAttribute != null)
                            {
                                displayName = displayNameAttribute.DisplayName;
                            }
                            if (string.IsNullOrWhiteSpace(displayName))
                            {
                                displayName = propertyInfo.Name;
                            }

                            row[displayName] = propertyInfo.GetValue(item);
                        }

                        datatable.Rows.Add(row);
                    }
                }

                var fileName = Module.Title + "_" + DateTime.Now.ToString("yyyyMMdd_HHmmss") + ".xlsx";

                var workbook = new XLWorkbook();
                var worksheet = workbook.Worksheets.Add(datatable, "Data");
                worksheet.Columns().AdjustToContents();

                using (MemoryStream stream = new MemoryStream())
                {
                    workbook.SaveAs(stream);

                    return new ExcelResult
                    {
                        FileName = fileName,
                        Stream = stream
                    };
                }
            }

            Module.Url = _.Web.RemovePagingQueryString(Request.Url);
            var model = _logProvider.GetLogs(filter);
            ViewBag.Data = model;
            return View(filter);
        }

        public ActionResult Details(long id)
        {
            var model = _logProvider.GetLogDetails(id);
            return View(model);
        }
    }
}