﻿using AcCms.Models.Enumeration;
using AcCms.Services.Admin.Mvc;
using AcCms.Services.Common.Authentication;
using ClosedXML.Excel;
using Core.Mvc.Models;
using Core.Mvc.Services;
using Core.Task;
using System;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web.Mvc;

namespace AcCms.Web.Areas.admin.Controllers
{
    [AdminAuthorize(Enums.Privilege.AdminSuper)]
    [SetModule("Super", "Task", "Task")]
    public class TaskController : AdminMvcControllerBaseWithIdentity
    {
        private readonly AcTaskHelper _acTaskHelper;
        public TaskController(WebIdentity identity, AcTaskHelper acTaskHelper) : base(identity)
        {
            _acTaskHelper = acTaskHelper;
        }

        public ActionResult Index(AcTaskAdminModelsFilter filter)
        {
            if (filter.Export)
            {
                filter.PageNumber = 1;
                filter.PageSize = int.MaxValue;

                var items = _acTaskHelper.GetTasks(filter);

                DataTable datatable = new DataTable();

                var selectedExportProperties = filter.SelectedExportProperties.Split(',').ToList();
                var selectedExportPropertyNames = selectedExportProperties.Select(p => p.Split(':')[0]).ToList();
                var selectedExportPropertyDisplayNames = selectedExportProperties.Select(p => p.Split(':')[1]).ToList();
                foreach (var propertyDisplayName in selectedExportPropertyDisplayNames)
                {
                    datatable.Columns.Add(propertyDisplayName);
                }

                if (items.Any())
                {
                    var allProperties = items.FirstOrDefault().GetType().GetProperties();
                    var propertiesOrdered = allProperties.Where(p => selectedExportPropertyNames.Contains(p.Name)).ToList();

                    foreach (var item in items)
                    {
                        DataRow row = datatable.NewRow();
                        foreach (var propertyInfo in propertiesOrdered)
                        {
                            MemberInfo propertyMemberInfo = typeof(AcTaskAdminModelsList).GetProperty(propertyInfo.Name);

                            string displayName = "";
                            var displayNameAttribute = propertyMemberInfo.GetCustomAttribute(typeof(DisplayNameAttribute)) as DisplayNameAttribute;
                            if (displayNameAttribute != null)
                            {
                                displayName = displayNameAttribute.DisplayName;
                            }
                            if (string.IsNullOrWhiteSpace(displayName))
                            {
                                displayName = propertyInfo.Name;
                            }

                            row[displayName] = propertyInfo.GetValue(item);
                        }

                        datatable.Rows.Add(row);
                    }
                }

                var fileName = Module.Title + "_" + DateTime.Now.ToString("yyyyMMdd_HHmmss") + ".xlsx";

                var workbook = new XLWorkbook();
                var worksheet = workbook.Worksheets.Add(datatable, "Data");
                worksheet.Columns().AdjustToContents();

                using (MemoryStream stream = new MemoryStream())
                {
                    workbook.SaveAs(stream);

                    return new ExcelResult
                    {
                        FileName = fileName,
                        Stream = stream
                    };
                }
            }

            Module.Url = _.Web.RemovePagingQueryString(Request.Url);
            var model = _acTaskHelper.GetTasks(filter);
            ViewBag.Data = model;
            return View(filter);
        }

        public ActionResult Details(long id)
        {
            var model = _acTaskHelper.GetTaskDetails(id);
            return View(model);
        }

        public ActionResult Enable(long id)
        {
            if (_acTaskHelper.EnableTask(id))
            {
                Message.Success = "Task enabled successfully";
            }
            else
            {
                Message.Error = "An error occured, please try again later";
            }

            return RedirectToAction("Index");
        }

        public ActionResult Disable(long id)
        {
            if (_acTaskHelper.DisableTask(id))
            {
                Message.Success = "Task disabled successfully";
            }
            else
            {
                Message.Error = "An error occured, please try again later";
            }

            return RedirectToAction("Index");
        }

        public ActionResult LoadFromAssembly()
        {
            _acTaskHelper.LoadAllFromAssembly(Identity.Id);
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Settings(long id)
        {
            string settingsSystemName;
            var model = _acTaskHelper.GetSettings(id, out settingsSystemName);
            ViewBag.SettingsSystemName = settingsSystemName;
            return View(model);
        }

        [HttpPost]
        public ActionResult Settings(long id, [ModelBinder(typeof(CmsModelBinder))]object model, string contentTypeDataSystemName)
        {
            ModelState.ValidateAndLoadErrors(model, p => p);
            if (ModelState.IsValid)
            {
                _acTaskHelper.SetSettings(id, model, Identity.Id);

                return RedirectToAction("Index");
            }

            ViewBag.SettingsSystemName = contentTypeDataSystemName;
            return View(model);
        }
    }
}