﻿using AcCms.Data.Domains;
using AcCms.Models.Enumeration;
using AcCms.Services.Admin.Models;
using AcCms.Services.Admin.Mvc;
using AcCms.Services.Admin.Services;
using AcCms.Services.Common.Authentication;
using ClosedXML.Excel;
using Core.Audit;
using Core.Log;
using Core.Mvc.Attributes;
using Core.Mvc.Models;
using Core.Mvc.Services;
using Core.Provider;
using Core.Settings;
using System;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web.Mvc;

namespace AcCms.Web.Areas.admin.Controllers
{
    [AdminAuthorize(Enums.Privilege.AdminPagePartial, Enums.Privilege.AdminPagePartialAdmin)]
    [SetModule("Design", "Page Partial", "Page Partial")]
    public class PagePartialController : AdminMvcControllerBaseWithIdentity
    {
        private readonly LanguageService _languageService;
        private readonly PagePartialService _pagePartialService;
        private readonly CmsPagePartialService _cmsPagePartialService;

        public PagePartialController(WebIdentity identity, LanguageService languageService, PagePartialService pagePartialService, CmsPagePartialService cmsPagePartialService) : base(identity)
        {
            _languageService = languageService;
            _pagePartialService = pagePartialService;
            _cmsPagePartialService = cmsPagePartialService;
        }

        public ActionResult Index(PagePartialModelsFilter filter)
        {
            filter.LanguageId = Identity.LanguageId ?? 0;
            var data = _pagePartialService.Index(filter);
            ViewBag.Data = data;
            ViewBag.EntityName = _cmsPagePartialService.GetEntityName();
            return View(filter);
        }

        public ActionResult CmsInfo(long id)
        {
            var model = _pagePartialService.GetCmsInfo(id);
            ViewBag.EntityName = _cmsPagePartialService.GetEntityName();
            return View(model);
        }
    }
}