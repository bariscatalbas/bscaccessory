﻿using AcCms.Data.Domains;
using AcCms.Models.Enumeration;
using AcCms.Services.Admin.Models;
using AcCms.Services.Admin.Mvc;
using AcCms.Services.Admin.Services;
using AcCms.Services.Common.Authentication;
using Core.Mvc.Models;
using Core.Task;
using System.Web.Mvc;

namespace AcCms.Web.Areas.admin.Controllers
{
    [AdminAuthorize(Enums.Privilege.AdminSuper)]
    [SetModule("Super", "Language", "Language")]
    public class LanguageController : AdminDataControllerBaseWithIdentity<Language, LanguageModelsCreate, LanguageModelsEdit, LanguageModelsView, LanguageModelsFilter, LanguageModelsExcelExport>
    {
        public LanguageController(LanguageService service, WebIdentity user) : base(service, user)
        {
        }
    }
}