﻿using AcCms.Models.Enumeration;
using AcCms.Services.Admin.Mvc;
using AcCms.Services.Admin.Services;
using AcCms.Services.Common.Authentication;
using AcCms.Services.Common.Resources;
using Core.Mvc.Models;
using Core.Resources;
using System.Web.Mvc;

namespace AcCms.Web.Areas.admin.Controllers
{
    public class ResourcesController : AdminMvcControllerBaseWithIdentity
    {
        private readonly AcResourceHelper _resourceHelper;
        private readonly MenuService _menuService;

        public ResourcesController(WebIdentity identity, AcResourceHelper resourceHelper, MenuService menuService) : base(identity)
        {
            _resourceHelper = resourceHelper;
            _menuService = menuService;
        }

        [HttpGet]
        [SetModule("Settings", "WebResource", "Web Resource")]
        [AdminAuthorize(Enums.Privilege.AdminSettings)]
        public ActionResult WebResource()
        {
            if (!Identity.LanguageId.HasValue)
            {
                return RedirectToAction("Index", "Home");
            }

            var model = _resourceHelper.LoadResource<WebResource>(Identity.LanguageId.Value);
            ViewBag.MenuList = _menuService.SelectList(Identity.LanguageId.Value);
            return View("Index", model);
        }

        [HttpPost]
        [SetModule("Settings", "WebResource", "Web Resource")]
        [AdminAuthorize(Enums.Privilege.AdminSettings)]
        public ActionResult WebResource(WebResource model)
        {
            if (ModelState.IsValid)
            {
                _resourceHelper.SaveResourceCached(model, Identity.Id);
                Message.Success = "Module updated successfully.";
            }
            else
            {
                Message.Error = "Please check errors.";
            }
            ViewBag.MenuList = _menuService.SelectList(Identity.LanguageId.Value);
            return View("Index", model);
        }
    }
}