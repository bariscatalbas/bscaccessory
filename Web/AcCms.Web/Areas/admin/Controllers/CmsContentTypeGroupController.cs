﻿using AcCms.Models.Enumeration;
using AcCms.Services.Admin.Mvc;
using AcCms.Services.Common.Authentication;
using Core.Mvc.Models;
using Modules.Cms.AdminModels;
using Modules.Cms.Domains;
using Modules.Cms.Services;

namespace AcCms.Web.Areas.admin.Controllers
{
    [AdminAuthorize(Enums.Privilege.AdminSuper)]
    [SetModule("Setting", "CmsContentTypeGroup", "Cms Content Type Group")]
    public class CmsContentTypeGroupController : AdminDataControllerBaseWithIdentity<CmsContentTypeGroup, CmsContentTypeGroupModelsCreate, CmsContentTypeGroupModelsEdit, CmsContentTypeGroupModelsView, CmsContentTypeGroupModelsFilter, CmsContentTypeGroupModelsExcelExport>
    {
        public CmsContentTypeGroupController(CmsContentTypeGroupService service, WebIdentity user) : base(service, user)
        {
        }
    }
}