﻿using AcCms.Models.Enumeration;
using AcCms.Services.Admin.Models;
using AcCms.Services.Admin.Mvc;
using AcCms.Services.Admin.Services;
using AcCms.Services.Common.Authentication;
using Core.Mvc.Attributes;
using Core.Mvc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AcCms.Data.Domains;

namespace AcCms.Web.Areas.admin.Controllers
{
    [AdminAuthorize(Enums.Privilege.AdminPage)]
    public class ProductController : AdminDataControllerBaseWithIdentity<Product, ProductModelsCreate, ProductModelsEdit, ProductModelsView, ProductModelsFilter, ProductModelsExcelExport>
    {
        private readonly ProductService _productService;
        private readonly ProductStockService _productStockService;
        private readonly ProductImageService _productImageService;

        public ProductController(WebIdentity identity, ProductService productService, ProductStockService productStockService, ProductImageService productImageService) : base(productService, identity)
        {
            _productService = productService;
            _productStockService = productStockService;
            _productImageService = productImageService;
        }

        [SetModule("Product", "ProductStock", "Ürün")]
        public override ActionResult Index(ProductModelsFilter filter)
        {
            ViewBag.IsImageHideFilter = filter.IsImageHide;
            return base.Index(filter);
        }

        [SetModule("Product", "ProductStock", "Stok")]
        public ActionResult ProductStockList(ProductStockModelsFilter filter)
        {
            Module.Url = _.Web.RemovePagingQueryString(Request.Url);
            var model = _productStockService.Index(filter);
            ViewBag.Data = model;
            ViewBag.ProductId = filter.ProductId;
            return View();
        }

        [SetModule("Product", "ProductStock", "Stok")]
        [HttpGet]
        public ActionResult ProductStockCreate(long productId)
        {
            var model = new ProductStockModelsCreate { ProductId = productId }; 
            return View(model);
        }

        [SetModule("Product", "ProductStock", "Stok")]
        [HttpPost, AcParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult ProductStockCreate(ProductStockModelsCreate model, bool continueEditing)
        {
            UploadMedia(model);
            if (ModelState.IsValid)
            {
                var id = _productStockService.Insert(model, Identity.Id);

                Message.Success = "Module created successfully.";

                if (continueEditing)
                {
                    return RedirectToAction("ProductStockEdit", new { id = id });
                }

                return RedirectToAction("ProductStockList", new { productId = model.ProductId });
            }
            Message.Error = "Please check errors.";
            return View(model);
        }

        [SetModule("Product", "ProductStock", "Stok")]
        [HttpGet]
        public ActionResult ProductStockEdit(long id)
        {
            var model = _productStockService.Single(id);
            if (model != null)
            { 
                LoadMedia(model);
                return View(model);
            }
            return RedirectToAction("ProductStockList", new { productId = model.ProductId });
        }

        [SetModule("Product", "ProductStock", "Stok")]
        [HttpPost, AcParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult ProductStockEdit(ProductStockModelsEdit model, bool continueEditing)
        {
            UploadMedia(model);
            if (ModelState.IsValid)
            {
                if (_productStockService.Update(model, Identity.Id))
                {
                    Message.Success = "Module updated successfully.";

                    if (continueEditing)
                    {
                        return RedirectToAction("ProductStockEdit", new { id = model.Id });
                    }

                    return RedirectToAction("ProductStockList", new { productId = model.ProductId });
                }
                else
                {
                    Message.Error = "Module not found";
                }
            }

            Message.Error = "Please check errors.";
            LoadMedia(model);
            return View(model);
        }

        [HttpPost]
        public ActionResult ProductStockDelete(long id)
        {
            var model = _productStockService.Single(id);
            if (_productStockService.Delete(id, Identity.Id))
            {
                Message.Success = "Module deleted successfully.";
                return RedirectToAction("ProductStockList", new { productId = model.ProductId });
            }
            else
            {
                Message.Error = "Module not found";
                return RedirectToAction("ProductStockEdit", new { id = id });
            }
        }


        [SetModule("Product", "ProductImage", "Ürün Görsel")]
        public ActionResult ProductImageList(ProductImageModelsFilter filter)
        {
            Module.Url = _.Web.RemovePagingQueryString(Request.Url);
            var model = _productImageService.Index(filter);
            ViewBag.ProductId = filter.ProductId;
            ViewBag.Data = model;
            return View();
        }

        [SetModule("Product", "ProductImage", "Ürün Görsel")]
        [HttpGet]
        public ActionResult ProductImageCreate(long productId)
        {
            var model = new ProductImageModelsCreate { ProductId = productId };
            return View(model);
        }

        [SetModule("Product", "ProductImage", "Ürün Görsel")]
        [HttpPost, AcParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult ProductImageCreate(ProductImageModelsCreate model, bool continueEditing)
        {
            UploadMedia(model);
            if (ModelState.IsValid)
            {
                var id = _productImageService.Insert(model, Identity.Id);

                Message.Success = "Module created successfully.";

                if (continueEditing)
                {
                    return RedirectToAction("ProductImageEdit", new { id = id });
                }

                return RedirectToAction("ProductImageList", new { productId = model.ProductId });
            }
            Message.Error = "Please check errors.";
            return View(model);
        }

        [SetModule("Product", "ProductImage", "Ürün Görsel")]
        [HttpGet]
        public ActionResult ProductImageEdit(long id)
        {
            var model = _productImageService.Single(id);
            if (model != null)
            {
                LoadMedia(model);
                return View(model);
            }
            return RedirectToAction("ProductImageList", new { productId = model.ProductId });
        }

        [SetModule("Product", "ProductImage", "Ürün Görsel")]
        [HttpPost, AcParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult ProductImageEdit(ProductImageModelsEdit model, bool continueEditing)
        {
            UploadMedia(model);
            if (ModelState.IsValid)
            {
                if (_productImageService.Update(model, Identity.Id))
                {
                    Message.Success = "Module updated successfully.";

                    if (continueEditing)
                    {
                        return RedirectToAction("ProductImageEdit", new { id = model.Id });
                    }

                    return RedirectToAction("ProductImageList", new { productId = model.ProductId });
                }
                else
                {
                    Message.Error = "Module not found";
                }
            }

            Message.Error = "Please check errors.";
            LoadMedia(model);
            return View(model);
        }

        [HttpPost]
        public ActionResult ProductImageDelete(long id)
        {
            var model = _productImageService.Single(id);
            if (_productImageService.Delete(id, Identity.Id))
            {
                Message.Success = "Module deleted successfully.";
                return RedirectToAction("ProductImageList", new { productId = model.ProductId });
            }
            else
            {
                Message.Error = "Module not found";
                return RedirectToAction("ProductImageEdit", new { id = id });
            }
        }
    }
}