﻿using AcCms.Data.Domains;
using AcCms.Models.Enumeration;
using AcCms.Services.Admin.Models;
using AcCms.Services.Admin.Mvc;
using AcCms.Services.Admin.Services;
using AcCms.Services.Common.Authentication;
using Core.Mvc.Models;
using System.Web.Mvc;

namespace AcCms.Web.Areas.admin.Controllers
{
    [AdminAuthorize(Enums.Privilege.AdminStringResrouce)]
    [SetModule("StringResource", "StringResource", "String Resource")]
    public class StringResourceController : AdminDataControllerBaseWithIdentity<StringResource, StringResourceModelsCreate, StringResourceModelsEdit, StringResourceModelsView, StringResourceModelsFilter, StringResourceModelsExcelExport>
    {
        private readonly StringResourceService _stringResourceService;

        public StringResourceController(StringResourceService service, WebIdentity user) : base(service, user)
        {
            _stringResourceService = service;
        }

        public override ActionResult Index(StringResourceModelsFilter filter)
        {
            filter.LanguageId = Identity.LanguageId ?? 0;
            var model = _stringResourceService.Index(filter);

            ViewBag.Data = model;

            return View();
        }

        public override ActionResult Create()
        {
            var languageId = Identity.LanguageId ?? 0;

            var model = new StringResourceModelsCreate
            {
                LanguageId = languageId
            };

            LoadViewData(null);
            return View(model);
        }
    }
}