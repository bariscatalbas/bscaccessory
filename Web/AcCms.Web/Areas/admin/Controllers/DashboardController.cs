﻿using AcCms.Models.Enumeration;
using AcCms.Services.Admin.Mvc;
using AcCms.Services.Admin.Services;
using AcCms.Services.Common.Authentication;
using System.Web.Mvc;

namespace AcCms.Web.Areas.admin.Controllers
{
    [AdminAuthorize(Enums.Privilege.AdminAccess)]
    public class DashboardController : AdminMvcControllerBaseWithIdentity
    {
        private readonly LanguageService _languageService;
        private readonly ReportService _reportService;
        public DashboardController(WebIdentity identity, LanguageService languageService, ReportService reportService) : base(identity)
        {
            _languageService = languageService;
            _reportService = reportService;
        }

        public ActionResult Index()
        {
            ViewBag.TopData = _reportService.DashboardTopReport(Identity.LanguageId.Value);

            ViewBag.Last30DaysSales = _reportService.Last30DaysSales();
            ViewBag.Last12MonthsSales = _reportService.Last12MonthsSales();

            //ViewBag.ProductSalesCount = _reportService.ProductSalesCount(Identity.CurrentLanguageId);
            //ViewBag.Last30DaysProductSalesCount = _reportService.Last30DaysProductSalesCount(Identity.CurrentLanguageId);
            //ViewBag.ProductStockReport = _reportService.ProductStockReport(Identity.CurrentLanguageId);
            ViewBag.YearlyProductSalesCount = _reportService.YearlyProductSalesCount(Identity.LanguageId.Value);
            ViewBag.MonthlyProductSalesCount = _reportService.MonthlyProductSalesCount(Identity.LanguageId.Value);
            ViewBag.DailyProductSalesCount = _reportService.DailyProductSalesCount(Identity.LanguageId.Value);
            return View();
        }

        public ActionResult LanguageSelect()
        {
            var model = _languageService.GetLanguages();
            return View(model);
        }

        public ActionResult UpdateAccountLanguage(long languageId, string returnUrl)
        {
            Identity.ChangeLanguage(languageId);
            Message.Success = "Language Updated for Current User";
            if (!string.IsNullOrWhiteSpace(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

        public ActionResult Error()
        {
            return View();
        }
    }
}