﻿using AcCms.Data.Domains;
using AcCms.Models.Enumeration;
using AcCms.Services.Admin.Models;
using AcCms.Services.Admin.Mvc;
using AcCms.Services.Admin.Services;
using AcCms.Services.Common.Authentication;
using Core.Mvc.Models;

namespace AcCms.Web.Areas.admin.Controllers
{
    [AdminAuthorize(Enums.Privilege.AdminRole)]
    [SetModule("User", "Role", "Role")]
    public class RoleController : AdminDataControllerBaseWithIdentity<Role, RoleModelsCreate, RoleModelsEdit, RoleModelsView, RoleModelsFilter, RoleModelsExcelExport>
    {
        public RoleController(RoleService service, WebIdentity user) : base(service, user)
        {
        }
    }
}