﻿using AcCms.Models.Authentication;
using AcCms.Models.Enumeration;
using AcCms.Services.Admin.Mvc;
using AcCms.Services.Common.Authentication;
using Core.CommonModels;
using Core.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AcCms.Web.Areas.admin.Controllers
{
    public class AuthenticationController : AdminMvcControllerBaseWithIdentity
    {
      private readonly AuthenticationService _authenticationService;
        public AuthenticationController(WebIdentity identity, AuthenticationService authenticationService) : base(identity)
        {
            _authenticationService = authenticationService;
        }

        public ActionResult Login(string returnUrl)
        {
            if (Identity.IsAuthenticated)
            {
                if (Identity.AllowAccess(Enums.Privilege.AdminAccess))
                {
                    return RedirectToAction("Index", "Dashboard");
                }
            }

            ViewBag.ReturnURL = returnUrl;
            ViewBag.CaptchaImage = ResetCaptcha();
            return View();
        }

        [HttpPost]
        public ActionResult Login(LoginModel model, string capchatoken)
        {
            if (Identity.IsAuthenticated)
            {
                if (Identity.AllowAccess(Enums.Privilege.AdminAccess))
                {
                    return RedirectToAction("Index", "Dashboard");
                }
            }

            if (!String.IsNullOrEmpty(capchatoken) && Session["Captcha"] != null && String.Equals(capchatoken, Session["Captcha"].ToString()))
            {
                List<KeyValue> validationErrors;
                if (AcValidationProvider.Validate(ModelState, p => p, out validationErrors))
                {
                    Guid token;
                    var result = _authenticationService.Login(model, out token);
                    if (result == Enums.LoginResult.Failed)
                    {
                        Message.Error = "Wrong email or password.";
                    }
                    else if (result == Enums.LoginResult.RequireEmailValidation)
                    {
                        Message.Error = "Please verify your email address.";
                    }
                    else if (result == Enums.LoginResult.NotActive)
                    {
                        Message.Error = "Your user is inactive.";
                    }
                    else
                    {
                        Identity.SignIn(token);

                        return RedirectToAction("Index", "Dashboard");
                    }
                }
                else
                {
                    Message.Error = "Please check errors.";
                }
            }
            else {
                Message.Error = "Please check captcha.";
            }

            ViewBag.CaptchaImage = ResetCaptcha();
            return View(model);
        }

        public ActionResult LogOut()
        {
            _authenticationService.Logout(Identity.Id, Identity.Token);
            Identity.SignOut();

            return RedirectToAction("Index", "Dashboard");
        }

        [AllowAnonymous]
        public ActionResult RerenderCaptcha()
        {
            return Json(new { result = ResetCaptcha() }, JsonRequestBehavior.AllowGet);
        }
        public string ResetCaptcha()
        {
            var rand = new Random((int)DateTime.Now.Ticks);
            string captcha = rand.Next(100000, 999999).ToString();
            Session["Captcha"] = captcha;

            return _.Web.CaptchaImage(captcha);
        }
    }
}