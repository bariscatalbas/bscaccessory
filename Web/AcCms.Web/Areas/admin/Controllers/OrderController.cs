﻿using AcCms.Models.Enumeration;
using AcCms.Services.Admin.Models;
using AcCms.Services.Admin.Mvc;
using AcCms.Services.Admin.Services;
using AcCms.Services.Common.Authentication;
using ClosedXML.Excel;
using Core.Audit;
using Core.Log;
using Core.Mvc.Models;
using Core.Mvc.Services;
using Core.Provider;
using Core.Settings;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web.Mvc;
using AcCms.Models.Common;
using AcCms.Services.Common.Resources;
using Core.Mvc.Attributes;
using Core.Pagination;
using DocumentFormat.OpenXml.EMMA;

namespace AcCms.Web.Areas.admin.Controllers
{
    [AdminAuthorize(Enums.Privilege.AdminLogs)]
    [SetModule("Order", "Order", "Sipariş Oluştur")]

    public class OrderController : AdminMvcControllerBaseWithIdentity
    {
        private readonly ProductService _productService;
        private readonly OrderService _orderService;

        public OrderController(WebIdentity identity, ProductService productService, OrderService orderService) : base(identity)
        {
            _productService = productService;
            _orderService = orderService;
        }

        [HttpGet]
        public ActionResult Create()
        {
            var model = _productService.GetCreateOrderProducts();
            return View(model);
        }

        [HttpPost]
        public ActionResult Create(List<ProductDetailsModel> model)
        {
            if (model != null)
            {
                if (model.Any(p => p.Amount > 0))
                {
                    var newModel = new List<ProductDetailsModel>();
                    foreach (var item in model)
                    {
                        if (item.Amount > 0)
                        {
                            var orderResult = new ProductDetailsModel
                            {
                                Id = item.Id,
                                Amount = item.Amount,
                                ImageUrl = item.ImageUrl,
                                Code = item.Code,
                                Name = item.Name,
                                Price = item.Price,
                                RemainingStock = item.RemainingStock,
                            };
                            newModel.Add(orderResult);
                        }
                    }

                    Session["OrderResult"] = newModel;

                    return RedirectToAction("Submit");
                }
            }


            Message.Error = "Ürün Seçiniz";
            return View(model);
        }

        [HttpGet]
        public ActionResult Submit()
        {
            var sessionModel = Session["OrderResult"];
            if (sessionModel == null)
            {
                Message.Error = "Ürünlerin seçim süresi doldu. Tekrardan seçim yapınız";
                return RedirectToAction("Create");
            }
            return View();
        }

        [HttpPost]
        public ActionResult Submit(OrderModelsCreate model)
        {
            ModelState.ValidateAndLoadErrors(model, p => p);
            if (ModelState.IsValid)
            {
                var orders = (List<ProductDetailsModel>)Session["OrderResult"];
                var result = _orderService.Insert(model, orders, Identity);
                if (result)
                {
                    Message.Success = "Sipariş oluşturuldu";
                    Session["OrderResult"] = null;
                    return RedirectToAction("Index");
                }
            }

            Message.Error = "Lütfen tüm alanları kontrol ediniz";
            return View();
        }

        public ActionResult Details(long id = 0)
        {
            if (id > 0)
            {
                var model = _orderService.Details(id);
                return View(model);
            }
            return RedirectToAction("Index");
        }

        [HttpPost, AcParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult DetailsStatus(OrderDetailsModel model, bool continueEditing)
        {
            UploadMedia(model);
            if (ModelState.IsValid)
            {
                if (_orderService.StatusUpdate(model, Identity.Id))
                {
                    Message.Success = "Module updated successfully.";
                    return RedirectToAction("Details", new { id = model.Id });
                }
                else
                {
                    Message.Error = "Module not found";
                }
            }

            Message.Error = "Please check errors.";
            LoadMedia(model);

            return RedirectToAction("Details", new { id = model.Id });
        }


        [HttpPost, AcParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult DetailsShipment(OrderDetailsModel model, bool continueEditing)
        {
            UploadMedia(model);
            if (ModelState.IsValid)
            {
                if (_orderService.ShipmentUpdate(model, Identity.Id))
                {
                    Message.Success = "Module updated successfully.";
                    return RedirectToAction("Details", new { id = model.Id });
                }
                else
                {
                    Message.Error = "Module not found";
                }
            }

            Message.Error = "Please check errors.";
            LoadMedia(model);

            return RedirectToAction("Details", new { id = model.Id });
        }

        public virtual ActionResult Index(OrderModelsFilter filter)
        {
            if (filter.Export)
            {
                filter.PageNumber = 1;
                filter.PageSize = int.MaxValue;

                var items = _orderService.Index(filter);
                ModifyIndexData(items);

                DataTable datatable = new DataTable();

                var selectedExportProperties = filter.SelectedExportProperties.Split(',').ToList();
                var selectedExportPropertyNames = selectedExportProperties.Select(p => p.Split(':')[0]).ToList();
                var selectedExportPropertyDisplayNames = selectedExportProperties.Select(p => p.Split(':')[1]).ToList();
                foreach (var propertyDisplayName in selectedExportPropertyDisplayNames)
                {
                    datatable.Columns.Add(propertyDisplayName);
                }

                if (items.Any())
                {
                    var allProperties = items.FirstOrDefault().GetType().GetProperties();
                    var propertiesOrdered = allProperties.Where(p => selectedExportPropertyNames.Contains(p.Name)).ToList();

                    foreach (var item in items)
                    {
                        DataRow row = datatable.NewRow();
                        foreach (var propertyInfo in propertiesOrdered)
                        {
                            MemberInfo propertyMemberInfo = typeof(OrderModelsView).GetProperty(propertyInfo.Name);

                            string displayName = "";
                            var displayNameAttribute = propertyMemberInfo.GetCustomAttribute(typeof(DisplayNameAttribute)) as DisplayNameAttribute;
                            if (displayNameAttribute != null)
                            {
                                displayName = displayNameAttribute.DisplayName;
                            }
                            if (string.IsNullOrWhiteSpace(displayName))
                            {
                                displayName = propertyInfo.Name;
                            }

                            row[displayName] = propertyInfo.GetValue(item);
                        }

                        datatable.Rows.Add(row);
                    }
                }

                var fileName = Module.Title + "_" + DateTime.Now.ToString("yyyyMMdd_HHmmss") + ".xlsx";

                var workbook = new XLWorkbook();
                var worksheet = workbook.Worksheets.Add(datatable, "Data");
                worksheet.Columns().AdjustToContents();

                using (MemoryStream stream = new MemoryStream())
                {
                    workbook.SaveAs(stream);

                    return new ExcelResult
                    {
                        FileName = fileName,
                        Stream = stream
                    };
                }
            }

            Module.Url = _.Web.RemovePagingQueryString(Request.Url);
            var model = _orderService.Index(filter);
            ModifyIndexData(model);
            ViewBag.Data = model;
            return View(filter);
        }

        public virtual void ModifyIndexData(AcPager<OrderModelsView> data) { }
        public virtual void ModifyIndexExportData(AcPager<OrderModelsView> data) { }


        public virtual ActionResult SelectExcelExportColumns()
        {
            List<Tuple<string, string>> model = new List<Tuple<string, string>>();

            var excelExportModel = new OrderModelsView();

            var properties = excelExportModel.GetType().GetProperties();

            foreach (var propertyInfo in properties)
            {
                var propertyInfoType = propertyInfo.PropertyType;
                var propertyInfoUType = Nullable.GetUnderlyingType(propertyInfoType);
                if (propertyInfoUType != null)
                {
                    propertyInfoType = propertyInfoUType;
                }
                var isPropertyEnum = propertyInfoType.IsEnum;
                if ((_.Prop.GetEnumerableType(propertyInfo.PropertyType) == null || _.Prop.GetEnumerableType(propertyInfo.PropertyType) == typeof(char)) && (propertyInfo.PropertyType.Namespace.StartsWith("System") || isPropertyEnum))
                {
                    MemberInfo propertyMemberInfo = typeof(OrderModelsView).GetProperty(propertyInfo.Name);

                    bool preventOnExcelExport = false;
                    var preventOnExcelExportAttribute = propertyMemberInfo.GetCustomAttribute(typeof(AcPreventOnExcelExportAttribute)) as AcPreventOnExcelExportAttribute;
                    if (preventOnExcelExportAttribute != null)
                    {
                        preventOnExcelExport = true;
                    }

                    if (!preventOnExcelExport)
                    {
                        string displayName = "";
                        var displayNameAttribute = propertyMemberInfo.GetCustomAttribute(typeof(DisplayNameAttribute)) as DisplayNameAttribute;
                        if (displayNameAttribute != null)
                        {
                            displayName = displayNameAttribute.DisplayName;
                        }
                        if (string.IsNullOrWhiteSpace(displayName))
                        {
                            displayName = propertyInfo.Name;
                        }

                        var propertyName = new Tuple<string, string>(propertyInfo.Name, displayName);
                        model.Add(propertyName);
                    }
                }
            }

            return View(model);
        }

    }
}