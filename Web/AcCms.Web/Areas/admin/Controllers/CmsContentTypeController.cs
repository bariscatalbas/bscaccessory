﻿using AcCms.Models.Enumeration;
using AcCms.Services.Admin.Mvc;
using AcCms.Services.Common.Authentication;
using Core.Mvc.Models;
using Modules.Cms.AdminModels;
using Modules.Cms.Domains;
using Modules.Cms.Services;
using System.Web.Mvc;

namespace AcCms.Web.Areas.admin.Controllers
{
    [AdminAuthorize(Enums.Privilege.AdminSuper)]
    [SetModule("Setting", "CmsContentType", "Cms Content Type")]
    public class CmsContentTypeController : AdminDataControllerBaseWithIdentity<CmsContentType, CmsContentTypeModelsCreate, CmsContentTypeModelsEdit, CmsContentTypeModelsView, CmsContentTypeModelsFilter, CmsContentTypeModelsExcelExport>
    {
        private readonly CmsContentTypeService _cmsContentTypeService;
        private readonly CmsContentTypeGroupService _cmsContentTypeGroupService;

        public CmsContentTypeController(CmsContentTypeService service, WebIdentity user, CmsContentTypeGroupService cmsContentTypeGroupService) : base(service, user)
        {
            _cmsContentTypeService = service;
            _cmsContentTypeGroupService = cmsContentTypeGroupService;
        }

        public override void LoadViewData(object objectModel)
        {
            ViewBag.CmsContentTypeGroupList = _cmsContentTypeGroupService.SelectList();
        }
		
        public ActionResult LoadFromAssembly()
        {
            _cmsContentTypeService.LoadAllFromAssembly(Identity.Id);

            return RedirectToAction("Index");
        }
    }
}