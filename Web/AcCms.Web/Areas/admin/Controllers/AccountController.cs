﻿using AcCms.Data.Domains;
using AcCms.Models.Enumeration;
using AcCms.Services.Admin.Models;
using AcCms.Services.Admin.Mvc;
using AcCms.Services.Admin.Services;
using AcCms.Services.Common.Authentication;
using Core.Mvc.Models;
using System.Web.Mvc;

namespace AcCms.Web.Areas.admin.Controllers
{
    [AdminAuthorize(Enums.Privilege.AdminAccount)]
    [SetModule("User", "Account", "Account")]
    public class AccountController : AdminDataControllerBaseWithIdentity<Account, AccountModelsCreate, AccountModelsEdit, AccountModelsView, AccountModelsFilter, AccountModelsExcelExport>
    {
        private readonly RoleService _roleService;
        private readonly AccountService _accountService;

        public AccountController(AccountService service, WebIdentity user, RoleService roleService) : base(service, user)
        {
            _roleService = roleService;
            _accountService = service;
        }

        public override void LoadViewData(object objectModel)
        {
            ViewBag.RoleList = _roleService.SelectList();
        }

        public override ActionResult Create(AccountModelsCreate model, bool continueEditing)
        {
            model.Email = (model.Email ?? "").Trim().ToLowerInvariant();
            if (!_accountService.ValidateEmail(model.Email, 0))
            {
                Message.Error = "There is an account with this e-mail address.";
                LoadViewData(model);
                return View(model);
            }

            return base.Create(model, continueEditing);
        }

        public override ActionResult Edit(AccountModelsEdit model, bool continueEditing)
        {
            model.Email = (model.Email ?? "").Trim().ToLowerInvariant();
            if (!_accountService.ValidateEmail(model.Email, model.Id))
            {
                Message.Error = "There is an account with this e-mail address.";
                LoadViewData(model);
                return View(model);
            }

            return base.Edit(model, continueEditing);
        }

        [HttpGet]
        public ActionResult ChangePassword(long id)
        {
            var model = new AccountModelsChangePassword { Id = id };
            return View(model);
        }

        [HttpPost]
        public ActionResult ChangePassword(AccountModelsChangePassword model)
        {
            ModelState.ValidateAndLoadErrors(model, p => p);
            if (ModelState.IsValid)
            {
                var result = _accountService.ChangePassword(model, Identity.Id);

                if (string.IsNullOrWhiteSpace(result))
                {
                    Message.Success = "Password updated successfully.";
                    return RedirectToAction("Index");
                }
                else
                {
                    Message.Error = result;
                }
            }

            return View(model);
        }

    }
}