﻿using AcCms.Models.Enumeration;
using AcCms.Services.Admin.Mvc;
using AcCms.Services.Common.Authentication;
using ClosedXML.Excel;
using Core.Email;
using Core.Mvc.Models;
using Core.Mvc.Services;
using Core.Sms;
using System;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web.Mvc;

namespace AcCms.Web.Areas.admin.Controllers
{
    [AdminAuthorize(Enums.Privilege.AdminQueuedSms)]
    [SetModule("Settings", "QueuedSms", "Queued Sms")]
    public class QueuedSmsController : AdminMvcControllerBaseWithIdentity
    {
        private readonly AcSmsHelper _smsHelper;
        public QueuedSmsController(WebIdentity identity, AcSmsHelper smsHelper) : base(identity)
        {
            _smsHelper = smsHelper;
        }

        public ActionResult Index(AcQueuedSmsAdminModelsFilter filter)
        {
            if (filter.Export)
            {
                filter.PageNumber = 1;
                filter.PageSize = int.MaxValue;

                var items = _smsHelper.GetQueuedSms(filter);

                DataTable datatable = new DataTable();

                var selectedExportProperties = filter.SelectedExportProperties.Split(',').ToList();
                var selectedExportPropertyNames = selectedExportProperties.Select(p => p.Split(':')[0]).ToList();
                var selectedExportPropertyDisplayNames = selectedExportProperties.Select(p => p.Split(':')[1]).ToList();
                foreach (var propertyDisplayName in selectedExportPropertyDisplayNames)
                {
                    datatable.Columns.Add(propertyDisplayName);
                }

                if (items.Any())
                {
                    var allProperties = items.FirstOrDefault().GetType().GetProperties();
                    var propertiesOrdered = allProperties.Where(p => selectedExportPropertyNames.Contains(p.Name)).ToList();

                    foreach (var item in items)
                    {
                        DataRow row = datatable.NewRow();
                        foreach (var propertyInfo in propertiesOrdered)
                        {
                            MemberInfo propertyMemberInfo = typeof(AcQueuedSmsAdminModelsList).GetProperty(propertyInfo.Name);

                            string displayName = "";
                            var displayNameAttribute = propertyMemberInfo.GetCustomAttribute(typeof(DisplayNameAttribute)) as DisplayNameAttribute;
                            if (displayNameAttribute != null)
                            {
                                displayName = displayNameAttribute.DisplayName;
                            }
                            if (string.IsNullOrWhiteSpace(displayName))
                            {
                                displayName = propertyInfo.Name;
                            }

                            row[displayName] = propertyInfo.GetValue(item);
                        }

                        datatable.Rows.Add(row);
                    }
                }

                var fileName = Module.Title + "_" + DateTime.Now.ToString("yyyyMMdd_HHmmss") + ".xlsx";

                var workbook = new XLWorkbook();
                var worksheet = workbook.Worksheets.Add(datatable, "Data");
                worksheet.Columns().AdjustToContents();

                using (MemoryStream stream = new MemoryStream())
                {
                    workbook.SaveAs(stream);

                    return new ExcelResult
                    {
                        FileName = fileName,
                        Stream = stream
                    };
                }
            }

            Module.Url = _.Web.RemovePagingQueryString(Request.Url);
            var model = _smsHelper.GetQueuedSms(filter);
            ViewBag.Data = model;
            return View(filter);
        }

        public ActionResult Details(long id)
        {
            var model = _smsHelper.GetQueuedSmsDetails(id);
            if (model == null)
            {
                Message.Error = "Module not found.";
                return RedirectToAction("Index");
            }

            LoadMedia(model);
            return View(model);
        }

        public ActionResult SendAgain(long id)
        {
            if (_smsHelper.SendQueuedSmsAgain(id, Identity.Id))
            {
                Message.Success = "Mail will be send again.";
            }
            else
            {
                Message.Error = "An error occured, please try again later.";
            }

            return RedirectToAction("Details", new { id = id });
        }
    }
}