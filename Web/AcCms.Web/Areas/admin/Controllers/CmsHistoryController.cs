﻿using AcCms.Models.Enumeration;
using AcCms.Services.Admin.Mvc;
using AcCms.Services.Admin.Services;
using AcCms.Services.Common.Authentication;
using Core.Ioc;
using Core.Mvc.Models;
using Modules.Cms.Interfaces;
using Modules.Cms.Services;
using System;
using System.Web.Mvc;

namespace AcCms.Web.Areas.admin.Controllers
{
    [SetModule("Design", "Cms", "Cms")]
    public class CmsHistoryController : AdminMvcControllerBaseWithIdentity
    {
        public CmsHistoryController(WebIdentity identity) : base(identity)
        {
        }
    }
}