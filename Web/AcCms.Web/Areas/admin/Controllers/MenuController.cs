﻿using AcCms.Models.Enumeration;
using AcCms.Services.Admin.Models;
using AcCms.Services.Admin.Mvc;
using AcCms.Services.Admin.Services;
using AcCms.Services.Common.Authentication;
using Core.Mvc.Attributes;
using Core.Mvc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AcCms.Web.Areas.admin.Controllers
{
    [AdminAuthorize(Enums.Privilege.AdminPage)]
    public class MenuController : AdminMvcControllerBaseWithIdentity
    {
        private readonly MenuService _menuService;

        public MenuController(WebIdentity identity, MenuService menuService) : base(identity)
        {
            _menuService = menuService;
        }

        [SetModule("Design", "Menu", "Menu")]
        public ActionResult Index(MenuModelsFilter filter)
        {
            filter.LanguageId = Identity.LanguageId ?? 0;
            Module.Url = _.Web.RemovePagingQueryString(Request.Url);
            var model = _menuService.Index(filter);
            ViewBag.Data = model;
            return View();
        }

        [SetModule("Design", "Menu", "Menu")]
        [HttpGet]
        public ActionResult Create()
        {
            var model = new MenuModelsCreate { LanguageId = Identity.LanguageId ?? 0 };
            return View(model);
        }

        [HttpPost, AcParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult Create(MenuModelsCreate model, bool continueEditing)
        {
            ModelState.ValidateAndLoadErrors(model, p => p);
            if (ModelState.IsValid)
            {
                var id = _menuService.Insert(model, Identity.Id);
                model.Id = id;

                Message.Success = "Module created successfully.";
                if (continueEditing)
                {
                    return RedirectToAction("Edit", new { id = id });
                }

                return RedirectToAction("Index");
            }
            Message.Error = "Please check errors.";
            return View(model);
        }

        [SetModule("Design", "Menu", "Menu")]
        public ActionResult Edit(long id)
        {
            var model = _menuService.Single(id);
            if (model == null)
            {
                Message.Error = "Module not found";
                return RedirectToAction("Index");
            }

            return View(model);
        }

        [HttpPost, AcParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult Edit(MenuModelsEdit model, bool continueEditing)
        {
            ModelState.ValidateAndLoadErrors(model, p => p);
            if (ModelState.IsValid)
            {
                if (_menuService.Update(model, Identity.Id))
                {
                    Message.Success = "Module updated successfully.";
                }
                else
                {
                    Message.Error = "Module not found";
                }
                if (continueEditing)
                {
                    return RedirectToAction("Edit", new { id = model.Id });
                }

                return RedirectToAction("Index");
            }
            Message.Error = "Please check errors.";
            return View(model);
        }

        [HttpPost]
        public ActionResult Delete(long id)
        {
            if (_menuService.Delete(id, Identity.Id))
            {
                Message.Success = "Module deleted successfully.";
                return RedirectToAction("Index");
            }
            else
            {
                Message.Error = "Module not found";
                return RedirectToAction("Edit", new { id = id });
            }
        }

        [SetModule("Design", "Menu", "Menu Item")]
        public ActionResult Items(long menuId)
        {
            ViewBag.MenuId = menuId;
            var model = _menuService.ItemsIndex(menuId);
            ViewBag.Data = model;
            return View();
        }

        [SetModule("Design", "Menu", "Menu Item")]
        [HttpGet]
        public ActionResult ItemCreate(long menuId)
        {
            var model = new MenuItemCreateModel { MenuId = menuId };
            ViewBag.ParentList = _menuService.GetParentList(menuId, 0);
            return View(model);
        }

        [HttpPost, AcParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult ItemCreate(MenuItemCreateModel model, bool continueEditing)
        {
            UploadMedia(model);
            if (ModelState.IsValid)
            {
                var id = _menuService.ItemInsert(model, Identity.Id);

                Message.Success = "Module created successfully.";

                if (continueEditing)
                {
                    return RedirectToAction("ItemEdit", new { id = id });
                }

                return RedirectToAction("Items", new { menuId = model.MenuId });
            }
            Message.Error = "Please check errors.";
            return View(model);
        }

        [SetModule("Design", "Menu", "Menu Item")]
        [HttpGet]
        public ActionResult ItemEdit(long id)
        {
            var model = _menuService.ItemSingle(id);
            if (model != null)
            {
                ViewBag.ParentList = _menuService.GetParentList(model.MenuId, id);
                LoadMedia(model);
                return View(model);
            }
            return RedirectToAction("Items", new { menuId = model.MenuId });
        }

        [HttpPost, AcParameterBasedOnFormName("save-continue", "continueEditing")]
        public ActionResult ItemEdit(MenuItemEditModel model, bool continueEditing)
        {
            UploadMedia(model);
            if (ModelState.IsValid)
            {
                if (_menuService.ItemUpdate(model, Identity.Id))
                {
                    Message.Success = "Module updated successfully.";

                    if (continueEditing)
                    {
                        return RedirectToAction("ItemEdit", new { id = model.Id });
                    }

                    return RedirectToAction("Items", new { menuId = model.MenuId });
                }
                else
                {
                    Message.Error = "Module not found";
                }
            }

            Message.Error = "Please check errors.";
            LoadMedia(model);
            return View(model);
        }

        [HttpPost]
        public ActionResult ItemDelete(long id)
        {
            var model = _menuService.ItemSingle(id);
            if (_menuService.ItemDelete(id, Identity.Id))
            {
                Message.Success = "Module deleted successfully.";
                return RedirectToAction("Items", new { menuId = model.MenuId });
            }
            else
            {
                Message.Error = "Module not found";
                return RedirectToAction("ItemEdit", new { id = id });
            }
        }
    }
}