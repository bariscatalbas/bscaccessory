﻿using AcCms.Models.Enumeration;
using AcCms.Services.Admin.Mvc;
using AcCms.Services.Admin.Services;
using AcCms.Services.Common.Authentication;
using Core.Ioc;
using Core.Mvc.Models;
using Modules.Cms.Interfaces;
using Modules.Cms.Services;
using System;
using System.Web.Mvc;

namespace AcCms.Web.Areas.admin.Controllers
{
    [SetModule("Design", "Cms", "Cms")]
    public class CmsDesignController : AdminMvcControllerBaseWithIdentity
    {
        private const string AccessNotAllowedErrorText = "You are not allowed to access design";

        public CmsDesignController(WebIdentity identity) : base(identity)
        {
        }

        private CmsDesignService DecideCmsDesignService(string entityName)
        {
            if (entityName == "Page")
            {
                var cmsEntityService = AcEngine.Resolve<CmsPageService>();
                return new CmsDesignService(cmsEntityService);
            }
            else if (entityName == "PagePartial")
            {
                var cmsEntityService = AcEngine.Resolve<CmsPagePartialService>();
                return new CmsDesignService(cmsEntityService);
            }

            return null;
        }

        private bool DecidePrivilege(string entityName, bool isAdmin)
        {
            if (entityName == "Page")
            {
                return isAdmin ? Identity.AllowAccess(Enums.Privilege.AdminPageAdmin) : Identity.AllowAccess(Enums.Privilege.AdminPage, Enums.Privilege.AdminPageAdmin);
            }
            else if (entityName == "PagePartial")
            {
                return isAdmin ? Identity.AllowAccess(Enums.Privilege.AdminPagePartialAdmin) : Identity.AllowAccess(Enums.Privilege.AdminPagePartial, Enums.Privilege.AdminPagePartialAdmin);
            }

            return false;
        }

        public ActionResult Design(string entityName, long entityId)
        {
            if (!DecidePrivilege(entityName, false))
            {
                Message.Error = AccessNotAllowedErrorText;
                return RedirectToAction("Index", entityName);
            }

            var cmsDesignService = DecideCmsDesignService(entityName);
            var cmsVersionId = cmsDesignService.CreateCmsVersion(entityId, Identity.Id);
            return RedirectToAction("Index", new { cmsVersionId, entityName });
        }

        public ActionResult Index(string entityName, long cmsVersionId)
        {
            if (!DecidePrivilege(entityName, false))
            {
                Message.Error = AccessNotAllowedErrorText;
                return RedirectToAction("Index", entityName);
            }

            var cmsDesignService = DecideCmsDesignService(entityName);
            var cmsVersion = cmsDesignService.GetCmsVersion(cmsVersionId);
            if (cmsVersion != null)
            {
                TempData["CmsVersion"] = cmsVersion;
                var model = cmsDesignService.GetCmsVersionContents(cmsVersion.CmsVersionId);
                ViewBag.IsAdmin = DecidePrivilege(entityName, true);
                return View(model);
            }
            else
            {
                Message.Error = "An error occured, please try again later.";
                return RedirectToAction("Index", entityName);
            }
        }

        public ActionResult CreateModal(string entityName, long cmsVersionId, long? parentId = null, string parentKey = "Root")
        {
            if (!DecidePrivilege(entityName, false))
            {
                Message.Error = AccessNotAllowedErrorText;
                return RedirectToAction("Index", entityName);
            }

            var cmsDesignService = DecideCmsDesignService(entityName);
            var model = cmsDesignService.GetContentTypes(parentKey);
            ViewBag.EntityName = entityName;
            ViewBag.CmsVersionId = cmsVersionId;
            ViewBag.ParentId = parentId;
            return View(model);
        }

        public ActionResult Create(string entityName, long cmsVersionId, long? parentId, long contentTypeId)
        {
            if (!DecidePrivilege(entityName, false))
            {
                Message.Error = AccessNotAllowedErrorText;
                return RedirectToAction("Index", entityName);
            }

            var cmsDesignService = DecideCmsDesignService(entityName);
            string contentTypeDataSystemName;

            var model = cmsDesignService.GetCreateModel(contentTypeId, out contentTypeDataSystemName);
            ViewBag.EntityName = entityName;
            ViewBag.CmsVersionId = cmsVersionId;
            ViewBag.ParentId = parentId;
            ViewBag.ContentTypeId = contentTypeId;
            ViewBag.ContentTypeDataSystemName = contentTypeDataSystemName;

            return View(model);
        }

        [HttpPost]
        public ActionResult Create([ModelBinder(typeof(CmsModelBinder))]object model, string entityName, long cmsVersionId, long? parentId, long contentTypeId, string contentTypeDataSystemName)
        {
            if (!DecidePrivilege(entityName, false))
            {
                Message.Error = AccessNotAllowedErrorText;
                return RedirectToAction("Index", entityName);
            }

            UploadMedia(model);
            var cmsDesignService = DecideCmsDesignService(entityName);
            ModelState.ValidateAndLoadErrors(model, p => p);
            if (ModelState.IsValid)
            {
                var id = cmsDesignService.Create(model, cmsVersionId, parentId, contentTypeId, contentTypeDataSystemName, Identity.Id);
                Message.Success = "Module created successfully.";
                return RedirectToAction("Edit", new { cmsVersionId, id, entityName });
            }

            ViewBag.EntityName = entityName;
            ViewBag.CmsVersionId = cmsVersionId;
            ViewBag.ParentId = parentId;
            ViewBag.ContentTypeId = contentTypeId;
            ViewBag.ContentTypeDataSystemName = contentTypeDataSystemName;

            return View(model);
        }

        [HttpGet]
        public ActionResult Edit(string entityName, long cmsVersionId, long id)
        {
            if (!DecidePrivilege(entityName, false))
            {
                Message.Error = AccessNotAllowedErrorText;
                return RedirectToAction("Index", entityName);
            }

            var cmsDesignService = DecideCmsDesignService(entityName);
            string contentTypeDataSystemName;
            var model = cmsDesignService.GetUpdateModel(id, out contentTypeDataSystemName);
            ViewBag.EntityName = entityName;
            ViewBag.CmsVersionId = cmsVersionId;
            ViewBag.ContentTypeDataSystemName = contentTypeDataSystemName;

            LoadMedia(model);
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit([ModelBinder(typeof(CmsModelBinder))] object model, string entityName, long cmsVersionId, string contentTypeDataSystemName)
        {
            if (!DecidePrivilege(entityName, false))
            {
                Message.Error = AccessNotAllowedErrorText;
                return RedirectToAction("Index", entityName);
            }

            UploadMedia(model);
            var cmsDesignService = DecideCmsDesignService(entityName);
            ModelState.ValidateAndLoadErrors(model, p => p);
            if (ModelState.IsValid)
            {
                if (cmsDesignService.Update(model, cmsVersionId, Identity.Id))
                {
                    Message.Success = "Module updated successfully.";
                }
                else
                {
                    Message.Error = "Module not found.";
                }
            }

            ViewBag.EntityName = entityName;
            ViewBag.CmsVersionId = cmsVersionId;
            ViewBag.ContentTypeDataSystemName = contentTypeDataSystemName;

            LoadMedia(model);
            return View(model);
        }

        public ActionResult Delete(string entityName, long cmsVersionContentId, long cmsVersionId)
        {
            if (!DecidePrivilege(entityName, false))
            {
                Message.Error = AccessNotAllowedErrorText;
                return RedirectToAction("Index", entityName);
            }

            var cmsDesignService = DecideCmsDesignService(entityName);
            cmsDesignService.Delete(cmsVersionContentId, Identity.Id);
            return RedirectToAction("Index", new { cmsVersionId, entityName });
        }

        public JsonResult UpdateOrder(string entityName, string orderList, int cmsVersionId)
        {
            if (!DecidePrivilege(entityName, false))
            {
                return Json(new { Success = false, Error = AccessNotAllowedErrorText }, JsonRequestBehavior.AllowGet);
            }

            try
            {
                var cmsDesignService = DecideCmsDesignService(entityName);
                cmsDesignService.UpdateOrder(orderList, cmsVersionId, Identity.Id);
                return Json(new
                {
                    Success = true,
                    Html = ""
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Success = false, Error = "An error occured, please try again later." + " " + _.Web.OpenException(ex) }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ApproveDesign(string entityName, long entityId)
        {
            if (!DecidePrivilege(entityName, true))
            {
                Message.Error = AccessNotAllowedErrorText;
                return RedirectToAction("Index", entityName);
            }

            var cmsDesignService = DecideCmsDesignService(entityName);
            if (cmsDesignService.ApproveCmsDesign(entityId, Identity.Id))
            {
                Message.Success = entityName + " design approved successfully.";
            }
            else
            {
                Message.Error = "An error occured, please try again later.";
            }
            return RedirectToAction("Index", entityName);
        }

        public ActionResult RejectDesign(string entityName, long entityId)
        {
            if (!DecidePrivilege(entityName, true))
            {
                Message.Error = AccessNotAllowedErrorText;
                return RedirectToAction("Index", entityName);
            }

            var cmsDesignService = DecideCmsDesignService(entityName);
            if (cmsDesignService.RejectCmsDesign(entityId, Identity.Id))
            {
                Message.Success = entityName + " design rejected successfully.";
            }
            else
            {
                Message.Error = "An error occured, please try again later.";
            }

            return RedirectToAction("Index", entityName);
        }

        public ActionResult CancelDesign(string entityName, long entityId)
        {
            if (!DecidePrivilege(entityName, false))
            {
                Message.Error = AccessNotAllowedErrorText;
                return RedirectToAction("Index", entityName);
            }

            var cmsDesignService = DecideCmsDesignService(entityName);
            if (cmsDesignService.CancelCmsVersion(entityId, Identity.Id))
            {
                Message.Success = entityName + " design cancelled successfully.";
            }
            else
            {
                Message.Success = "An error occured, please try again later.";
            }
            return RedirectToAction("Index", entityName);
        }

        public ActionResult SendtoApprovalDesign(string entityName, long entityId)
        {
            if (!DecidePrivilege(entityName, false))
            {
                Message.Error = AccessNotAllowedErrorText;
                return RedirectToAction("Index", entityName);
            }

            var cmsDesignService = DecideCmsDesignService(entityName);
            if (cmsDesignService.CmsVersionSendToApproval(entityId, Identity.Id))
            {
                Message.Success = entityName + " design send to approval successfully.";
            }
            else
            {
                Message.Success = "An error occured, please try again later.";
            }
            return RedirectToAction("Index", entityName);
        }

        public ActionResult PublishDesign(string entityName, long entityId)
        {
            if (!DecidePrivilege(entityName, true))
            {
                Message.Error = AccessNotAllowedErrorText;
                return RedirectToAction("Index", entityName);
            }

            var cmsDesignService = DecideCmsDesignService(entityName);
            if (cmsDesignService.CmsVersionPublish(entityId, Identity.Id))
            {
                Message.Success = entityName + " design published successfully.";
            }
            else
            {
                Message.Success = "An error occured, please try again later.";
            }

            return RedirectToAction("Index", entityName);
        }

        public ActionResult UnpublishDesign(string entityName, long entityId)
        {
            if (!DecidePrivilege(entityName, false))
            {
                Message.Error = AccessNotAllowedErrorText;
                return RedirectToAction("Index", entityName);
            }

            var cmsDesignService = DecideCmsDesignService(entityName);

            if (cmsDesignService.CmsVersionUnpublish(entityId, Identity.Id))
            {
                Message.Success = entityName + " design unpublished successfully.";
            }
            else
            {
                Message.Success = "An error occured, please try again later.";
            }

            return RedirectToAction("Index", entityName);
        }

        public ActionResult History(string entityName, long entityId)
        {
            var cmsDesignService = DecideCmsDesignService(entityName);
            var model = cmsDesignService.GetCmsHistory(entityName, entityId);
            return View(model);
        }

        public ActionResult UndoCmsVersion(string entityName, long entityId, int cmsVersion)
        {
            var cmsDesignService = DecideCmsDesignService(entityName);
            if (cmsDesignService.UndoCmsVersion(entityId, cmsVersion, Identity.Id))
            {
                Message.Success = "Version updated successfully.";
            }
            else
            {
                Message.Success = "An error occured, please try again later.";
            }
            return RedirectToAction("Index", entityName);
        }
    }
}