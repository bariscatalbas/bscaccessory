﻿using AcCms.Models.Enumeration;
using AcCms.Services.Admin.Mvc;
using AcCms.Services.Admin.Services;
using AcCms.Services.Common.Authentication;
using AcCms.Services.Common.Settings;
using Core.Mvc.Models;
using Core.Provider;
using Core.Settings;
using System.Web.Mvc;

namespace AcCms.Web.Areas.admin.Controllers
{
    public class SettingsController : AdminMvcControllerBaseWithIdentity
    {
        private readonly AcSettingsHelper _settingsHelper;
        
        public SettingsController(WebIdentity identity, AcSettingsHelper settingsHelper) : base(identity)
        {
            _settingsHelper = settingsHelper;
        }

        [HttpGet]
        [SetModule("Super", "System", "Core Settings")]
        [AdminAuthorize(Enums.Privilege.AdminSettings)]
        public ActionResult Core()
        {
            var model = _settingsHelper.LoadSettings<CoreSettings>();

            ViewBag.AuditProviderList = AcProviderHelper.SelectList("audit");
            ViewBag.MediaProviderList = AcProviderHelper.SelectList("media");
            ViewBag.LogProviderList = AcProviderHelper.SelectList("log");
            ViewBag.EmailProviderList = AcProviderHelper.SelectList("email");
            ViewBag.SmsProviderList = AcProviderHelper.SelectList("sms");
            ViewBag.TaskProviderList = AcProviderHelper.SelectList("task");

            return View("Index", model);
        }

        [HttpPost]
        [SetModule("Super", "System", "Core Settings")]
        [AdminAuthorize(Enums.Privilege.AdminSettings)]
        public ActionResult Core(CoreSettings model)
        {
            ModelState.ValidateAndLoadErrors(model, p => p);
            if (ModelState.IsValid)
            {
                _settingsHelper.SaveSettingsCached(model, Identity.Id);
                Message.Success = "Module updated successfully.";
            }
            else
            {
                Message.Error = "Please check errors.";
            }

            ViewBag.AuditProviderList = AcProviderHelper.SelectList("audit");
            ViewBag.MediaProviderList = AcProviderHelper.SelectList("media");
            ViewBag.LogProviderList = AcProviderHelper.SelectList("log");
            ViewBag.EmailProviderList = AcProviderHelper.SelectList("email");
            ViewBag.SmsProviderList = AcProviderHelper.SelectList("sms");
            ViewBag.TaskProviderList = AcProviderHelper.SelectList("task");
            return View("Index", model);
        }

        [HttpGet]
        [SetModule("Super", "System", "Project Settings")]
        [AdminAuthorize(Enums.Privilege.AdminSuper)]
        public ActionResult Project()
        {
            var model = _settingsHelper.LoadSettings<ProjectSettings>();

            return View("Index", model);
        }

        [HttpPost]
        [SetModule("Super", "System", "Project Settings")]
        [AdminAuthorize(Enums.Privilege.AdminSuper)]
        public ActionResult Project(ProjectSettings model)
        {
            ModelState.ValidateAndLoadErrors(model, p => p);
            if (ModelState.IsValid)
            {
                _settingsHelper.SaveSettingsCached(model, Identity.Id);
                Message.Success = "Module updated successfully.";
            }
            else
            {
                Message.Error = "Please check errors.";
            }

            return View("Index", model);
        }

        [HttpGet]
        [SetModule("Super", "System", "Authentication Settings")]
        [AdminAuthorize(Enums.Privilege.AdminSuper)]
        public ActionResult Authentication()
        {
            var model = _settingsHelper.LoadSettings<AuthenticationSettings>();

            return View("Index", model);
        }

        [HttpPost]
        [SetModule("Super", "System", "Authentication Settings")]
        [AdminAuthorize(Enums.Privilege.AdminSuper)]
        public ActionResult Authentication(AuthenticationSettings model)
        {
            ModelState.ValidateAndLoadErrors(model, p => p);
            if (ModelState.IsValid)
            {
                _settingsHelper.SaveSettingsCached(model, Identity.Id);
                Message.Success = "Module updated successfully.";
            }
            else
            {
                Message.Error = "Please check errors.";
            }

            return View("Index", model);
        }
    }
}