﻿using AcCms.Models.Core;
using AcCms.Models.Email;
using AcCms.Models.Enumeration;
using AcCms.Services.Admin.Mvc;
using AcCms.Services.Common.Authentication;
using AcCms.Services.Common.Services;
using Core.Mvc.Models;
using Core.Pagination;
using Core.Provider;
using Core.SmsTemplates;
using System.Linq;
using System.Web.Mvc;

namespace AcCms.Web.Areas.admin.Controllers
{
    [AdminAuthorize(Enums.Privilege.AdminSmsTemplate)]
    [SetModule("Settings", "SmsTemplate", "Sms Template")]
    public class SmsTemplateController : AdminDataControllerBaseWithIdentity<AcSmsTemplate, AcSmsTemplateAdminModelsCreate, AcSmsTemplateAdminModelsEdit, AcSmsTemplateAdminModelsView, AcSmsTemplateAdminModelsFilter, AcSmsTemplateAdminModelsExcelExport>
    {
        private readonly SmsService _smsService;

        public SmsTemplateController(AcSmsTemplateAdminService service, WebIdentity identity, SmsService smsService) : base(service, identity)
        {
            _smsService = smsService;
        }

        public override ActionResult Index(AcSmsTemplateAdminModelsFilter filter)
        {
            filter.ContainerId = Identity.LanguageId ?? 0;
            return base.Index(filter);
        }

        public override void ModifyIndexData(AcPager<AcSmsTemplateAdminModelsView> data)
        {
            var typeList = _smsService.SmsTemplateTypeSelectList();
            foreach (var item in data)
            {
                item.Type = typeList.Where(p => p.Value == item.Type).Select(p => p.Text).FirstOrDefault() ?? item.Type;
            }
        }

        public override void ModifyIndexExportData(AcPager<AcSmsTemplateAdminModelsExcelExport> data)
        {
            var typeList = _smsService.SmsTemplateTypeSelectList();
            foreach (var item in data)
            {
                item.Type = typeList.Where(p => p.Value == item.Type).Select(p => p.Text).FirstOrDefault() ?? item.Type;
            }
        }

        public override ActionResult Create()
        {
            var languageId = Identity.LanguageId ?? 0;
            var model = new AcSmsTemplateAdminModelsCreate
            {
                ContainerId = languageId,
            };
            LoadViewData(null);
            return View(model);
        }

        public override void LoadViewData(object objectModel)
        {
            ViewBag.SmsProviderCodeList = AcProviderHelper.GetProvidersSelectList("sms");
            ViewBag.TypeList = _smsService.SmsTemplateTypeSelectList();
        }
    }
}