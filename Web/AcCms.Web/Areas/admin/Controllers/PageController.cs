﻿using AcCms.Data.Domains;
using AcCms.Models.Enumeration;
using AcCms.Services.Admin.Models;
using AcCms.Services.Admin.Mvc;
using AcCms.Services.Admin.Services;
using AcCms.Services.Common.Authentication;
using ClosedXML.Excel;
using Core.Audit;
using Core.Log;
using Core.Mvc.Attributes;
using Core.Mvc.Models;
using Core.Mvc.Services;
using Core.Provider;
using Core.Settings;
using System;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web.Mvc;

namespace AcCms.Web.Areas.admin.Controllers
{
    [AdminAuthorize(Enums.Privilege.AdminPage, Enums.Privilege.AdminPageAdmin)]
    [SetModule("Design", "Page", "Page")]
    public class PageController : AdminDataControllerBaseWithIdentity<Page, PageModelsCreate, PageModelsEdit, PageModelsView, PageModelsFilter, PageModelsExcelExport>
    {
        private readonly LanguageService _languageService;
        private readonly PageService _pageService;
        private readonly CmsPageService _cmsPageService;
        
        public PageController(WebIdentity identity, LanguageService languageService, PageService pageService, CmsPageService cmsPageService) : base(pageService, identity)
        {
            _languageService = languageService;
            _pageService = pageService;
            _cmsPageService = cmsPageService;
        }

        public override ActionResult Index(PageModelsFilter filter)
        {
            filter.LanguageId = Identity.LanguageId ?? 0;
            var data = _pageService.Index(filter);
            ViewBag.Data = data;
            ViewBag.EntityName = _cmsPageService.GetEntityName();
            return View(filter);
        }

        [HttpGet]
        public override ActionResult Create()
        {
            var languageId = Identity.LanguageId ?? 0;

            var model = new PageModelsCreate
            {
                LanguageId = languageId,
            };
            LoadViewData(model);
            return View(model);
        }

        public override PageModelsCreate ModifyCreate(PageModelsCreate model)
        {
            model.Url = (model.Url ?? "").Trim().TrimStart('/').TrimEnd('/').ToLowerInvariant();
            return model;
        }

        public override ActionResult CreateReturn(long id)
        {
            return RedirectToAction("Design", "CmsDesign", new { entityId = id, entityName = _cmsPageService.GetEntityName() });
        }

        [AdminAuthorize(Enums.Privilege.AdminPageAdmin)]
        public override ActionResult Edit(long id)
        {
            return base.Edit(id);
        }

        public override PageModelsEdit ModifyEdit(PageModelsEdit model)
        {
            model.Url = (model.Url ?? "").Trim().TrimStart('/').TrimEnd('/').ToLowerInvariant();
            return model;
        }


        [AdminAuthorize(Enums.Privilege.AdminPageAdmin)]
        [HttpPost, AcParameterBasedOnFormName("save-continue", "continueEditing")]
        public override ActionResult Edit(PageModelsEdit model, bool continueEditing)
        {
            return base.Edit(model, continueEditing);
        }

        [HttpPost]
        [AdminAuthorize(Enums.Privilege.AdminPageAdmin)]
        public override ActionResult Delete(long id)
        {
            var page = _pageService.Single(id);

            base.Delete(id);

            return RedirectToAction("Index");
        }

        public ActionResult CmsInfo(long id)
        {
            var model = _pageService.GetCmsInfo(id);
            ViewBag.EntityName = _cmsPageService.GetEntityName();
            return View(model);
        }

        public override void LoadViewData(object objectModel)
        {
        }
    }
}