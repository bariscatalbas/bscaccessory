﻿using AcCms.Models.Core;
using AcCms.Models.Email;
using AcCms.Models.Enumeration;
using AcCms.Services.Admin.Mvc;
using AcCms.Services.Common.Authentication;
using AcCms.Services.Common.Services;
using Core.EmailTemplates;
using Core.Mvc.Base;
using Core.Mvc.Models;
using Core.Pagination;
using Core.Provider;
using DocumentFormat.OpenXml.Math;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AcCms.Web.Areas.admin.Controllers
{
    [AdminAuthorize(Enums.Privilege.AdminEmailTemplate)]
    [SetModule("Settings", "EmailTemplate", "Email Template")]
    public class EmailTemplateController : AdminDataControllerBaseWithIdentity<AcEmailTemplate, AcEmailTemplateAdminModelsCreate, AcEmailTemplateAdminModelsEdit, AcEmailTemplateAdminModelsView, AcEmailTemplateAdminModelsFilter, AcEmailTemplateAdminModelsExcelExport>
    {
        private readonly EmailService _emailService;

        public EmailTemplateController(AcEmailTemplateAdminService service, WebIdentity identity, EmailService emailService) : base(service, identity)
        {
            _emailService = emailService;
        }

        public override ActionResult Index(AcEmailTemplateAdminModelsFilter filter)
        {
            filter.ContainerId = Identity.LanguageId ?? 0;
            return base.Index(filter);
        }

        public override void ModifyIndexData(AcPager<AcEmailTemplateAdminModelsView> data)
        {
            var typeList = _emailService.EmailTemplateTypeSelectList();
            foreach (var item in data)
            {
                item.Type = typeList.Where(p => p.Value == item.Type).Select(p => p.Text).FirstOrDefault() ?? item.Type;
            }
        }

        public override void ModifyIndexExportData(AcPager<AcEmailTemplateAdminModelsExcelExport> data)
        {
            var typeList = _emailService.EmailTemplateTypeSelectList();
            foreach (var item in data)
            {
                item.Type = typeList.Where(p => p.Value == item.Type).Select(p => p.Text).FirstOrDefault() ?? item.Type;
            }
        }

        public override ActionResult Create()
        {
            var languageId = Identity.LanguageId ?? 0;
            var model = new AcEmailTemplateAdminModelsCreate
            {
                ContainerId = languageId,
            };
            LoadViewData(null);
            return View(model);
        }

        public override void LoadViewData(object objectModel)
        {
            ViewBag.EmailProviderCodeList = AcProviderHelper.GetProvidersSelectList("email");
            ViewBag.TypeList = _emailService.EmailTemplateTypeSelectList();
        }

        public ActionResult TestEmail()
        {
            var email = new WelcomeEmailModel
            {
                FirstName = "Barış",
                LastName = "Çatalbaş",
                To = "bariscatalbas92@gmail.com",
                Cc = "bariscatalbas92@gmail.com",
                Bcc = "bariscatalbas92@gmail.com",
                AdminSite = "",
                Website = ""
            };
            _emailService.SendWelcomeEmail(Identity, new LanguageModel { Id = 1 }, email);
            return RedirectToAction("Index");
        }
    }
}