﻿using Core.Mvc.Routing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace AcCms.Web
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(name: "RerenderCaptcha", url: "RerenderCaptcha", defaults: new { controller = "Authentication", action = "RerenderCaptcha" }, namespaces: new[] { "Tvlyzer.Web.Controllers" });

            routes.MapRoute(name: "Error", url: "Error", defaults: new { controller = "Home", action = "Error" });
            routes.MapRoute(name: "Preview", url: "Preview", defaults: new { controller = "Home", action = "Preview" });
            routes.MapRoute(name: "RedirectToLogin", url: "redirectToLogin", defaults: new { controller = "Home", action = "RedirectToLogin" });
            routes.MapRoute(name: "PagePartial", url: "Home/PagePartial", defaults: new { controller = "Home", action = "PagePartial" });
            routes.MapRoute(name: "Widget", url: "Widget/{action}", defaults: new { controller = "Widget" });

            var genericUrlRouteProvider = new GenericUrlRouteProvider();
            genericUrlRouteProvider.RegisterRoutes(routes);
        }
    }
}
