﻿using AcCms.Services.Web.Mvc;
using System.Web;
using System.Web.Mvc;

namespace AcCms.Web
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            filters.Add(new LoadErrorFilterAttribute());
        }
    }
}
