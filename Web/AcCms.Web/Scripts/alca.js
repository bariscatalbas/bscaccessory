﻿
/*
 * Alca Js
 * @version 1.0.0
 * @author Sezgi Güneş
 * @author Can Üzüm
 */


var _window = $(window);
var _document = $(document);
var _html = $("html");
var _body = $("body");
var isMobile = false;
var isResizeinitiated = false;

var isRecaptchaActive = false;
var recaptchaInitialized = false;
var currentData;

this.alca = {
    settings: {
        mobileChangeWidth: 900,
        preventScroll: false,
        initLinkFormatter: false
    },
    string: {
        guid: function () {
            function s4() {
                return Math.floor((1 + Math.random()) * 0x10000)
                    .toString(16)
                    .substring(1);
            }
            return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
                s4() + '-' + s4() + s4() + s4();
        },
        trimUrl: function (x) {
            return x.replace(/^\/+|\/+$/gm, '');
        },
        updateQueryString: function (uri, key, value) {
            var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
            var separator = uri.indexOf('?') !== -1 ? "&" : "?";
            if (uri.match(re)) {
                return uri.replace(re, '$1' + key + "=" + value + '$2');
            }
            else {
                return uri + separator + key + "=" + value;
            }
        }
    },
    math: {
        fromRem: function (val) {
            return val * (_html.getValue("font-size"));
        }
    },
    modules: {
        // a-tabs, a-tab-menu, a-tab-menu-item, a-tab-item
        initTabs: function () {
            $("*[a-tabs]").each(function (i, e) {
                var _container = $(e);
                var _tabMenu = _container.find("*[a-tab-menu]");
                var _tabMenuSelect = _tabMenu.find('select');
                var _tabMenuItem = _container.find("*[a-tab-menu-item]");
                var _tabItem = _container.find("*[a-tab-item]");

                if (_tabMenu.hasAttr("a-fluid")) {
                    var _items = _tabMenu.find("*[a-tab-menu-item]");
                    _items.css("width", 100 / _items.length + "%");
                };

                _tabMenuItem.click(function () {
                    var _this = $(this);

                    if (!_this.hasClass("active")) {
                        _tabMenuItem.removeClass("active");
                        _this.addClass("active");

                        _tabItem.removeClass("active");
                        _tabItem.eq(_tabMenuItem.index(_this)).addClass("active");

                        _tabMenuSelect.val(_this.data('value'));
                    };
                });

                _tabMenuSelect.change(function () {
                    var value = $(this).val();
                    _container.find("*[a-tab-menu-item][data-value='" + value + "']").click();
                });
            });
        },
        // a-accordion, a-accordion-item, a-accordion-content
        initAccordion: function (_container) {
            var _target = $("*[a-accordion]");

            if (_container) {
                _target = _container.find("*[a-accordion]");
            };

            _target.each(function (i, e) {
                var _accordion = $(e);
                var _items = _accordion.find("*[a-accordion-item]");

                _items.click(function (e) {
                    var _this = $(this);

                    if (!$(e.target).hasAttr("a-accordion-content") && !$(e.target).closest("[a-accordion-content]").any()) {
                        if (!_this.hasClass("active")) {
                            _items.filter(".active").removeClass("active").find("*[a-accordion-content]").slideUp(300);

                            _this.addClass("active");
                            _this.find("*[a-accordion-content]").slideDown(300, function () {
                                if (_accordion.attr("a-accordion") == "focus") {
                                    $("html, body").stop().animate({
                                        scrollTop: _this.offset().top
                                    }, 500, "swing");
                                };
                            });
                        }
                        else {
                            _this.find("*[a-accordion-content]").slideUp(300);
                            _this.removeClass("active");
                        }
                    };
                });
            });
        },
        // a-video, a-play-video, a-pause-video
        initVideo: function (_container) {
            var _target = $("*[a-video]");

            if (_container) {
                _target = _container.find("*[a-video]");
            };

            _target.each(function (i, e) {
                var _container = $(e);
                var hasControls = (_container.attr("a-video") == "controls");
                var _video = _container.find("video");
                var _btn = _container.find("*[a-play-video]");
                var _pause = null;

                if (_container.hasAttr("a-pause-video")) {
                    _pause = _container;
                }
                else if (_container.find("*[a-pause-video]").any()) {
                    _pause = _container.find("*[a-pause-video]");
                }

                _btn.click(function () {
                    _container.attr("a-playing", "true");

                    if (hasControls) {
                        _video.attr("controls", "true");
                    };

                    _video[0].play();
                    _btn.hide();
                });

                if (_pause != null) {
                    _pause.click(function (e) {
                        if (!$(e.target).hasAttr("a-play-video") && !$(e.target).closest("*[a-play-video]").any()) {
                            if (hasControls) {
                                _video.removeAttr("controls");
                            };
                            
                            _video[0].pause();
                        };
                    });
                };

                _video[0].onpause = function () {
                    if (!_video[0].seeking) {
                        _btn.show();
                        _container.removeAttr("a-playing");

                        if (hasControls) {
                            _video.removeAttr("controls");
                        };
                    };
                };
            });
        },
        // a-cover-media, a-mobile-image, a-desktop-image
        initResponsiveImages: function (_container) {
            var _targetBg = $("*[a-mobile-image][a-desktop-image]");
            var _targetCover = $("*[a-cover-media]");

            if (_container) {
                _targetBg = _container.find("*[a-mobile-image][a-desktop-image]");
                _targetCover = _container.find("*[a-cover-media]");
            };

            if (_targetBg.any()) {
                alca.resize.minit(function () {
                    _targetBg.each(function (i, e) {
                        var _this = $(e);
                        var attr = _this.attr("a-mobile-image");

                        if (attr != null && attr.length > 0) {
                            _this.css("background-image", "url(" + attr + ")");
                        };
                    });
                });

                alca.resize.dinit(function () {
                    _targetBg.each(function (i, e) {
                        var _this = $(e);
                        var attr = _this.attr("a-desktop-image");

                        if (attr != null && attr.length > 0) {
                            _this.css("background-image", "url(" + attr + ")");
                        };
                    });
                });
            };

            var loadFunc = function (element) {
                alca.modules.initCoverImage(element);
            }

            if (_targetCover.any()) {
                _targetCover.mediaLoad(loadFunc);

                alca.resize.all(function () {
                    _targetCover.each(function (i, e) {
                        setTimeout(function () {
                            loadFunc(e);
                        }, 0);
                    });
                });
            };
        },
        initCoverImages: function (_container) {
            var _targetCover = $("*[a-cover-media]");
            if (_container) {
                _targetCover = _container.find("*[a-cover-media]");
            };

            if (_targetCover.any()) {
                _targetCover.each(function (i, e) {
                    alca.modules.initCoverImage(e);
                });
            };
        },
        initCoverImage: function (element) {
            var _this = $(element);
            var _parent = _this.closest("*[a-cover-target]");

            if (!_parent.any()) {
                _parent = _this.parent();
            };

            var tw;
            var th;
            var pw = _parent.outerWidth();
            var ph = _parent.outerHeight();

            if (_this[0].tagName.toLowerCase() == "img") {
                tw = _this[0].naturalWidth;
                th = _this[0].naturalHeight;
            }
            else if (_this[0].tagName.toLowerCase() == "video") {
                tw = _this[0].videoWidth;
                th = _this[0].videoHeight;
            }

            if (tw / th > pw / ph) {
                _this.css("height", ph + "px").css("width", "auto");
                _this.css("left", (_this.width() - pw) / -2 + "px").css("top", "0");
            }
            else {
                _this.css("width", pw + "px").css("height", "auto");
                _this.css("top", (_this.height() - ph) / -2 + "px").css("left", "0");
            }
        },
        // a-sametab, a-newtab
        initLinks: function (_container) {
            var _target = $("a");
            var extensionsToBlank = [
                ".jpg", ".jpeg", ".png", ".gif", ".bmp", ".tiff",
                ".mp4", ".flv", ".mov", ".avi", ".webm", ".ogg", ".wav", ".wmv", ".3gp",
                ".pdf", ".psd", ".ai", ".xml",
                ".xls", ".xlsx", ".doc", ".docx", ".ppt", ".pptx"
            ];

            if (_container) {
                _target = _container.find("a");
            };

            _target.each(function (i, e) {
                var _this = $(e);
                var extension = e.href.split('.').pop().toLowerCase();

                var notLink = e.href.indexOf('tel:') != 0 || e.href.indexOf('mailto:') != 0 || e.href.indexOf('javascript') != 0;

                if (!notLink && (e.hostname != window.location.hostname || extensionsToBlank.indexOf('.' + extension) > -1)) {
                    _this.attr("target", "_blank");
                };

                if (_this.hasAttr('a-sametab')) {
                    _this.attr("target", "");
                }
                else if (_this.hasAttr('a-newtab')) {
                    _this.attr("target", "_blank");
                }
            });
        },
        //optional: targetSelector
        equalHeight: function (_items, targetSelector) {
            var rowCount = Math.floor(_items.parent().innerWidth() / _items.eq(0).outerWidth());
            var maxHeight = 0;

            if (targetSelector) {
                _items.find(targetSelector).css("height", "");
            }
            else {
                _items.css("height", "");
            }

            _items.each(function (i, e) {
                var _target;

                if (targetSelector) {
                    _target = $(e).find(targetSelector);
                }
                else {
                    _target = $(e);
                }

                var h = _target.outerHeight();

                if (h > maxHeight) {
                    maxHeight = h;
                }

                if (i % rowCount == rowCount - 1 || i == _items.length - 1) {
                    var startIndex = ((i % rowCount == rowCount - 1) ? (i + 1 - rowCount) : Math.floor(_items.length / rowCount) * rowCount);
                    var stopIndex = ((i % rowCount == rowCount - 1) ? i + 1 : _items.length);

                    for (var j = startIndex; j < stopIndex; j++) {
                        if (targetSelector) {
                            _items.eq(j).find(targetSelector).css("height", maxHeight + "px");
                        }
                        else {
                            _items.eq(j).css("height", maxHeight + "px");
                        }
                    };

                    maxHeight = 0;
                }
            });
        },
        // a-fullheight
        initFullHeight: function (_container) {
            var _target = $("*[a-fullheight]");

            if (_container) {
                _target = _container.find("*[a-fullheight]");
            };

            _target.each(function (i, e) {
                alca.modules.fullHeight($(e));
            });
        },
        fullHeight: function (_section, _excludedItems) {
            var excludedHeight = 0;

            if (_excludedItems) {
                _excludedItems.each(function (i, e) {
                    excludedHeight += $(e).outerHeight();
                });
            };

            _section.css("height", _window.height() - excludedHeight + "px");
        },
        // a-read-more(target), a-desktop-lines, a-mobile-lines
        initReadMore: function () {
            $("*[a-read-more]").each(function (i, e) {
                var _this = $(e);
                var _target = _this.parent().find(_this.attr("a-read-more"));

                if (!_target.any()) {
                    _target = _this.closest(_this.attr("a-read-more"));
                }

                alca.resize.all(function () {
                    checkHeight();
                });

                _this.click(function () {
                    if (_this.hasClass("closed")) {
                        _target.css("height", "");
                        _this.removeClass("closed").addClass("opened");
                    }
                    else if (_this.hasClass("opened")) {
                        checkHeight();
                    }
                });

                function checkHeight() {
                    var remainingLines = (isMobile ? _this.attr("a-mobile-lines") : _this.attr("a-desktop-lines")) * 1;

                    _this.removeClass("opened").removeClass("closed");
                    _target.css("height", "");

                    if (_target.height() > _target.getValue("line-height") * remainingLines) {
                        var limit = 0;
                        var _inners = _target.find(">*");

                        if (!_inners.any()) {
                            _inners = _target;
                        }

                        _inners.each(function (ii, ee) {
                            var _elem = $(ee);
                            var lineHeight = _elem.getValue("line-height");

                            if (_elem.height() > remainingLines * lineHeight) {
                                limit += remainingLines * lineHeight + _elem.getValue("margin-top") + _elem.getValue("padding-top");
                                return false;
                            }
                            else {
                                limit += _elem.height() + _elem.getValue("margin-top") + _elem.getValue("margin-bottom") + _elem.getValue("padding-top") + _elem.getValue("padding-bottom");
                                remainingLines -= Math.round(_elem.height() / lineHeight);
                            }
                        });

                        _target.css("height", limit + "px").css("overflow", "hidden");
                        _this.addClass("closed");
                    };
                }
            });
        }
    },
    // a-animate-up, a-animate-target
    animate: {
        scrollUp: function (_container) {
            var _target = $("*[a-animate-up]");

            if (_container) {
                _target = _container.find("*[a-animate-up]");
            };

            _target.css("opacity", "0");
            _target.each(function (i, e) {
                alca.animate.scrollUpItems.push($(e));
            });
        },
        scrollOne: function (_element, offset, animationFunction) {
            if (typeof animationFunction == "function") {
                alca.animate.scrollOneItems.push({ element: _element, offset: offset, animationFunction: animationFunction });
            };
        },
        refresh: function () {
            var ws = _window.scrollTop();
            var wh = _window.innerHeight();

            var itemIndexesToSplice = [];
            for (var i = 0; i < alca.animate.scrollOneItems.length; i++) {
                var item = alca.animate.scrollOneItems[i];
                var elemOffset = item.element.offset().top;
                if (ws + wh > elemOffset + item.offset * wh / 100) {
                    item.animationFunction();
                    itemIndexesToSplice.push(i);
                };
            };

            for (var i = itemIndexesToSplice.length - 1; i >= 0; i--) {
                alca.animate.scrollOneItems.splice(itemIndexesToSplice[i], 1);
            }

            itemIndexesToSplice = [];
            for (var i = 0; i < alca.animate.scrollUpItems.length; i++) {
                var _item = alca.animate.scrollUpItems[i];
                var elemOffset = (_item.hasAttr("a-animate-target") && $(_item.attr("a-animate-target")).any() ? $(_item.attr("a-animate-target")).offset().top : _item.offset().top);
                if (ws + wh > elemOffset + wh / 4) {
                    _item.css("opacity", "1");
                    var delay = (_item.attr("a-animate-up") ? _item.attr("a-animate-up") * 1 : 0);
                    TweenLite.from(_item, .8, { opacity: 0, y: alca.math.fromRem(.3), delay: delay });
                    itemIndexesToSplice.push(i);
                };
            };

            for (var i = itemIndexesToSplice.length - 1; i >= 0; i--) {
                alca.animate.scrollUpItems.splice(itemIndexesToSplice[i], 1);
            }
        },
        scrollOneItems: [],
        scrollUpItems: []
    },
    f: {
        repaintHtml: function () {
            var paint = document.getElementsByTagName("html")[0];
            paint.style.display = 'none';
            paint.offsetHeight;
            paint.style.display = '';
        },
        getScrollBarWidth: function () {
            if (_body.height() > _window.height()) {
                var inner = document.createElement('p');
                inner.style.width = "100%";
                inner.style.height = "200px";

                var outer = document.createElement('div');
                outer.style.position = "absolute";
                outer.style.top = "0px";
                outer.style.left = "0px";
                outer.style.visibility = "hidden";
                outer.style.width = "200px";
                outer.style.height = "150px";
                outer.style.overflow = "hidden";
                outer.appendChild(inner);

                document.body.appendChild(outer);
                var w1 = inner.offsetWidth;
                outer.style.overflow = 'scroll';
                var w2 = inner.offsetWidth;
                if (w1 == w2) w2 = outer.clientWidth;

                document.body.removeChild(outer);

                return (w1 - w2);
            }
            else {
                return 0;
            }
        },
        showLoading: function (show, _container) {
            if (!_container) {
                _container = _body;
            }

            if (show) {
                _container.find('[a-loading]').show();
            }
            else {
                _container.find('[a-loading]').hide();
            }
        },
        lockScroll: function (isLock) {
            if (isLock && !_html.hasClass("lock")) {
                alca.f.lastScrollPosition = _window.scrollTop();
                _html.addClass("lock");
                _body.css("padding-right", alca.f.getScrollBarWidth() + "px");
                _body.css("margin-top", alca.f.lastScrollPosition * (-1) + "px");
                _body.append($("<div class='scrollbar-placeholder' style='position: fixed; height: 100%; top: 0; right: 0; background-color: inherit;'></div>").css("width", alca.f.getScrollBarWidth() + "px"));
            }
            else if (!isLock && _html.hasClass("lock")) {
                _html.removeClass("lock");
                _body.css("padding-right", "");
                _body.css("margin-top", "");
                _body.find(".scrollbar-placeholder").remove();
                _window.scrollTop(alca.f.lastScrollPosition);
            }
        },
        lastScrollPosition: 0
    },
    cookie: {
        set: function (cname, cvalue, exdays) {
            var d = new Date();
            d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
            var expires = "expires=" + d.toUTCString();
            document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
        },
        get: function (cname) {
            var name = cname + "=";
            var decodedCookie = decodeURIComponent(document.cookie);
            var ca = decodedCookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') {
                    c = c.substring(1);
                }
                if (c.indexOf(name) == 0) {
                    return c.substring(name.length, c.length);
                }
            }
            return "";
        }
    },
    form: {
        ajaxCalls: {},
        errorHtml: "<p class='input-error'>{0}</p>",
        errorContainerClass: "form-item",
        errorMessages: {
            filenotselected: 'Lütfen bir dosya seçiniz.',
            filevalidextention: 'Lütfen geçerli formatta bir dosya yükleyin. ({0})',
            required: 'Bu alanı doldurmanız zorunludur.',
            email: 'Lütfen geçerli bir e-posta adresi giriniz.',
            minlength: 'En az {0} karakter girebilirsiniz.',
            maxlength: 'En fazla {0} karakter girebilirsiniz.',
            samewith: 'Girdiğiniz değerler uyuşmuyor.'
        },
        init: function (_container, errorHtml, errorContainerClass) {
            if (!_container) {
                _container = _body;
            }

            var _onlyNumberInputs = _container.find("*[a-onlynumber]");

            _onlyNumberInputs.on('keypress', function (e) {
                var theEvent = e || window.event;
                var key = theEvent.keyCode || theEvent.which;
                key = String.fromCharCode(key);
                var regex = /[0-9]/g;
                if (!regex.test(key)) {
                    theEvent.returnValue = false;
                    if (theEvent.preventDefault) theEvent.preventDefault();
                }
            });

            _onlyNumberInputs.on('keyup change paste', function (e) {
                var value = $(this).val();
                $(this).val(value.replace(/[^0-9]/g, ''));
            });

            var _onlyTextInputs = _container.find("*[a-onlytext]");

            _onlyTextInputs.on('keypress', function (e) {
                var theEvent = e || window.event;
                var key = theEvent.keyCode || theEvent.which;
                key = String.fromCharCode(key);
                var regex = /[a-zA-ZçÇşŞğĞüÜıİöÖ]/g;
                if (!regex.test(key)) {
                    theEvent.returnValue = false;
                    if (theEvent.preventDefault) theEvent.preventDefault();
                }
            });

            _onlyTextInputs.on('keyup change paste', function (e) {
                var value = $(this).val();
                $(this).val(value.replace(/[^a-zA-ZçÇşŞğĞüÜıİöÖ]/g, ''));
            });

            var _onlyTextWithSpaceInputs = _container.find("*[a-onlytextwithspace]");

            _onlyTextWithSpaceInputs.on('keypress', function (e) {
                var theEvent = e || window.event;
                var key = theEvent.keyCode || theEvent.which;
                key = String.fromCharCode(key);
                var regex = /[a-zA-ZçÇşŞğĞüÜıİöÖ\s]/g;
                if (!regex.test(key)) {
                    theEvent.returnValue = false;
                    if (theEvent.preventDefault) theEvent.preventDefault();
                }
            });

            _onlyTextWithSpaceInputs.on('keyup change paste', function (e) {
                var value = $(this).val();
                $(this).val(value.replace(/[^a-zA-ZçÇşŞğĞüÜıİöÖ\s]/g, ''));
            });

            var _onlyEmailInputs = _container.find("*[a-onlytextemail]");

            _onlyEmailInputs.on('keypress', function (e) {
                var theEvent = e || window.event;
                var key = theEvent.keyCode || theEvent.which;
                key = String.fromCharCode(key);
                var regex = /[a-zA-Z0-9@._-]/g;
                if (!regex.test(key)) {
                    theEvent.returnValue = false;
                    if (theEvent.preventDefault) theEvent.preventDefault();
                }
            });

            _onlyEmailInputs.on('keyup change paste', function (e) {
                var value = $(this).val();
                $(this).val(value.replace(/[^a-zA-Z0-9@._-]/g, ''));
            });

            var _targetSubmitButtons = _container.find("*[a-submit]");
            _targetSubmitButtons.click(function () {
                $(this).closest('form').submit();
            });

            var _targetForms = _container.find("*[a-form]");
            _targetForms.each(function (i, e) {
                var _form = $(e);
                _form.on('submit', function () {
                    var _clickedButton = $(document.activeElement);
                    if (_clickedButton.hasAttr('a-preventvalidation')) {
                        if ($(this).hasAttr('a-ajaxcallfunction')) {
                            alca.form.ajaxCalls[$(this).attr('a-ajaxcallfunction')]($(this));

                            return false;
                        }

                        alca.f.showLoading(_container);
                        return true;
                    }
                    else {
                        var _formInputsToValidate = _form.find('*[a-validate]:not(:hidden)');

                        var formIsValid = true;

                        _formInputsToValidate.each(function (childI, childE) {
                            var childElement = $(childE);
                            var itemIsValid = alca.form.validateItem(childElement);

                            if (formIsValid && !itemIsValid) {
                                formIsValid = false;
                            }

                            if (!childElement.hasAttr('a-validated')) {
                                childElement.attr('a-validated', 'true');
                                childElement.on('change keyup paste blur', function () {
                                    alca.form.validateItem(childElement);
                                });
                            }
                        });

                        if (!formIsValid) {
                            _form.find('*[a-error]').first().find('input').focus();
                        }

                        if (formIsValid) {
                            if ($(this).hasAttr('a-ajaxcallfunction')) {
                                alca.form.ajaxCalls[$(this).attr('a-ajaxcallfunction')]($(this));

                                return false;
                            }

                            alca.f.showLoading(_container);

                            return true;
                        }

                        return formIsValid;
                    }
                });
            });
        },
        customValidationFunctions: {},
        validateItem: function (_element) {
            alca.form.clearError(_element);

            if (_element.hasAttr('a-customvalidation')) {
                if (alca.form.customValidationFunctions[_element.attr('a-customvalidation')]) {
                    return alca.form.customValidationFunctions[_element.attr('a-customvalidation')](_element);
                }

                return false;
            }
            else {
                if (_element.attr('type') == "file") {
                    if (_element.val() == '') {
                        var errorMessage = alca.form.getErrorMessage(_element, "filenotselected");
                        alca.form.appendError(_element, errorMessage);
                        return false;
                    }
                    if (_element.hasAttr('a-validextentions')) {
                        var validExtentions = _element.attr('a-validextentions').split(',');
                        var fileName = _element.val();
                        var fileExtention = fileName.replace(/^.*\./, '');
                        if (fileName == fileExtention) {
                            fileExtention = '';
                        }
                        else {
                            fileExtention = fileExtention.toLowerCase();
                        }

                        if (validExtentions.indexOf(fileExtention) < 0) {
                            var errorMessage = alca.form.getErrorMessage(_element, "filevalidextention", validExtentions.join(', '));
                            alca.form.appendError(_element, errorMessage);
                            return false;
                        }
                    }
                }
                if (_element.attr('type') == "checkbox") {
                    if (!_element.is(':checked')) {
                        var errorMessage = alca.form.getErrorMessage(_element, "required");
                        alca.form.appendError(_element, errorMessage);
                        return false;
                    }
                }
                else if (_element.attr('type') == "radio") {
                    var elementName = _element.attr('name');
                    var _allRadioInputsWithSameName = _element.closest('*[a-form]').find('[name="' + elementName + '"]');
                    var noneOfRadiosChecked = true;
                    _allRadioInputsWithSameName.each(function (i, e) {
                        if ($(e).is(':checked')) {
                            noneOfRadiosChecked = false;
                        }
                    });
                    if (noneOfRadiosChecked) {
                        _allRadioInputsWithSameName.each(function (i, e) {
                            var errorMessage = alca.form.getErrorMessage(_element, "required");
                            alca.form.appendError($(e), errorMessage);
                        });
                        return false;
                    }
                    else {
                        _allRadioInputsWithSameName.each(function (i, e) {
                            alca.form.clearError($(e));
                        });
                    }
                }
                else if (_element[0].nodeName.toLowerCase() == "select" && _element.hasAttr('multiple')) {
                    var inputValue = _element.val();

                    if (!_element.hasAttr('a-not-required')) {
                        if (inputValue.length == 0) {
                            var errorMessage = alca.form.getErrorMessage(_element, "required");
                            alca.form.appendError(_element, errorMessage);
                            return false;
                        }
                    }
                }
                else {
                    var inputValue = _element.val();

                    if (!_element.hasAttr('a-not-required')) {
                        if (inputValue.trim() == '') {
                            var errorMessage = alca.form.getErrorMessage(_element, "required");
                            alca.form.appendError(_element, errorMessage);
                            return false;
                        }
                    }
                    if (_element.hasAttr('a-email')) {
                        var emailRegex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
                        if (inputValue.length > 0 && !inputValue.match(emailRegex)) {
                            var errorMessage = alca.form.getErrorMessage(_element, "email");
                            alca.form.appendError(_element, errorMessage);
                            return false;
                        }
                    }
                    if (_element.hasAttr('a-minlength')) {
                        var minLength = _element.attr('a-minlength');
                        if (inputValue.length > 0 && inputValue.length < minLength) {
                            var errorMessage = alca.form.getErrorMessage(_element, "minlength", minLength);
                            alca.form.appendError(_element, errorMessage);
                            return false;
                        }
                    }
                    if (_element.hasAttr('a-maxlength')) {
                        var maxLength = _element.attr('a-maxlength');
                        if (inputValue.length > maxLength) {
                            var errorMessage = alca.form.getErrorMessage(_element, "maxlength", maxLength);
                            alca.form.appendError(_element, errorMessage);
                            return false;
                        }
                    }
                    if (_element.hasAttr('a-samewith')) {
                        var sameElementName = _element.attr('a-samewith');
                        var sameElement = _element.closest('*[a-form]').find('[name="' + sameElementName + '"]');
                        if (inputValue.length > 0 && sameElement.val() != inputValue) {
                            var errorMessage = alca.form.getErrorMessage(_element, "samewith");
                            alca.form.appendError(_element, errorMessage);
                            return false;
                        }
                    }
                }
            }

            return true;
        },
        clearError: function (_element) {
            var errorHtml = alca.form.errorHtml;
            var errorContainerClass = alca.form.errorContainerClass;

            var errorElement = $(errorHtml);
            var errorElementClass = errorElement.attr('class');
            var errorElementClassSelector = errorElementClass.split(' ').join('.');

            _element.closest('.' + errorContainerClass).removeAttr('a-error');
            _element.closest('.' + errorContainerClass).find('.' + errorElementClassSelector).remove();
        },
        appendError: function (_element, errorMessage) {
            alca.form.clearError(_element);

            var errorHtml = alca.form.errorHtml;
            var errorContainerClass = alca.form.errorContainerClass;

            var errorElement = $(errorHtml);

            if (errorMessage && errorMessage != '') {
                var error = errorElement.html(errorElement.html().replace("{0}", errorMessage));
                _element.closest('.' + errorContainerClass).append(error);
            }
            _element.closest('.' + errorContainerClass).attr('a-error', 'true');
        },
        getErrorMessage: function (_element, key, extraParameter) {
            var errorMessage = '';
            if (_element && _element.hasAttr('a-errormessage')) {
                errorMessage = _element.attr('a-errormessage')
            }
            else if (alca.form.errorMessages[key] && alca.form.errorMessages[key] != '') {
                errorMessage = alca.form.errorMessages[key];
            }
            if (extraParameter) {
                errorMessage = errorMessage.replace("{0}", extraParameter);
            }
            return errorMessage;
        }
    },
    request: {
        initRecaptcha: function () {
            isRecaptchaActive = true;
        },
        call: function (url, data, type, callbackFunction, _loadingContainer, preventCaptcha) {
            if (_loadingContainer) {
                alca.f.showLoading(true, _loadingContainer);
            }

            currentData = data;

            var runRecaptcha = isRecaptchaActive && !preventCaptcha;

            if (runRecaptcha) {
                grecaptcha.execute();
            }
            else {
                recaptchaInitialized = true;
            }

            var postData = setInterval(function () {
                if (recaptchaInitialized) {
                    recaptchaInitialized = false;
                    clearInterval(postData);

                    if (!type) {
                        type = "post";
                    }

                    $.ajax({
                        cache: false,
                        url: url,
                        data: currentData,
                        type: type,
                        success: function (response) {
                            if (runRecaptcha) {
                                grecaptcha.reset();
                            }

                            alca.f.showLoading(false, _loadingContainer);
                            callbackFunction(response);
                        },
                        error: function () {
                            if (runRecaptcha) {
                                grecaptcha.reset();
                            }

                            alca.f.showLoading(false, _loadingContainer);
                            callbackFunction({ IsSuccess: false });
                        }
                    });
                }
            }, 500);
        }
    },
    facebook: {
        init: function (appId) {
            window.fbAsyncInit = function () {
                FB.init({
                    appId: appId,
                    cookie: true,
                    xfbml: true,
                    version: 'v2.12'
                });
            };

            (function (d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) { return; }
                js = d.createElement(s); js.id = id;
                js.src = "https://connect.facebook.net/en_US/sdk.js";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));
        },
        login: function () {
            FB.getLoginStatus(function (statusResponse) {
                if (statusResponse.status == "connected") {
                    alca.request.facebookGetUserData();
                }
                else {
                    FB.login(function (loginResponse) {
                        if (loginResponse.status == "connected") {
                            alca.request.facebookGetUserData();
                        }
                    }, { scope: 'email' });
                }
            });
        },
        getUserData: function () {
            FB.api('/me', { fields: 'id, first_name, last_name, email' },
                function (meResponse) {
                    if (!response.email) {
                        alert("E-posta adresiniz alınamadı.");
                    }
                    else {
                        var user = {};
                        user.FacebookToken = response.id;
                        user.Email = response.email;
                        user.FirstName = "";
                        user.LastName = "";
                        if (response.first_name) {
                            user.FirstName = response.first_name;
                        }
                        if (response.last_name) {
                            user.LastName = response.last_name;
                        }

                        alca.request.call("/Customer/FacebokLogin", { model: user }, "post", function () {
                            location.reload();
                        });
                    }
                });
        }
    },
    owl: {
        //options: { elementSelector, name, value, onlyMobile, onlyDesktop }
        responsiveOption: function (options) {
            alca.owl.responsiveOptionList.push(options);
        },
        responsiveOptionList: [],
        refreshOptions: function () {
            for (var i = 0; i < alca.owl.responsiveOptionList.length; i++) {
                var item = alca.owl.responsiveOptionList[i];

                var element = $(item.elementSelector);
                element.each(function (index, elem) {
                    if ($(elem).hasClass("owl-carousel")) {
                        if ((item.onlyMobile && isMobile) || (item.onlyDesktop && !isMobile) || (!item.onlyMobile && !item.onlyDesktop)) {
                            $(elem).each(function () {
                                var owl = $(this).data('owl.carousel');

                                owl.settings[item.name] = alca.math.fromRem(item.value);
                                owl.options[item.name] = alca.math.fromRem(item.value);

                                $(elem).trigger('refresh.owl.carousel');
                            });
                        }
                    };
                });
            };
        }
    },
    jQueryExtensions: {
        init: function () {
            jQuery.fn.extend({
                hasAttr: function (name) {
                    var result = false;
                    this.each(function (i, e) {
                        var attr = $(e).attr(name)
                        if (typeof attr != "undefined" && attr != null) {
                            result = true;
                        };
                    });
                    return result;
                },
                any: function () {
                    return this.length > 0;
                },
                getValue: function (cssProp) {
                    var value = this.css(cssProp).replace("px", "") * 1;
                    return isNaN(value) ? 0 : value;
                },
                mediaLoad: function (func) {
                    var _targetImage = this.filter("img");
                    var _targetVideo = this.filter("video");;

                    if (_targetImage.length == 0) {
                        _targetImage = this.find("img");
                    };
                    if (_targetVideo.length == 0) {
                        _targetVideo = this.find("video");
                    };

                    if (typeof func == "function" && _targetImage.length > 0) {
                        _targetImage.on("load", function () {
                            func(this);
                        }).each(function () {
                            if (this.complete) {
                                func(this);
                            }
                        });
                    };

                    if (typeof func == "function" && _targetVideo.length > 0) {
                        _targetVideo.on("loadedmetadata", function () {
                            func(this);
                        }).each(function () {
                            if (this.readyState >= 2) {
                                func(this);
                            }
                        });
                    };
                }
            });
        }
    },
    resize: {
        width: 0,
        //type: mobile / mobile_init / desktop / desktop_init / all
        m: function (func) {
            if (typeof func == "function") {
                alca.resize.functions["mobile"].push(func);
            };
        },
        minit: function (func) {
            if (typeof func == "function") {
                alca.resize.functions["mobile_init"].push(func);
            };
        },
        d: function (func) {
            if (typeof func == "function") {
                alca.resize.functions["desktop"].push(func);
            };
        },
        dinit: function (func) {
            if (typeof func == "function") {
                alca.resize.functions["desktop_init"].push(func);
            };
        },
        all: function (func) {
            if (typeof func == "function") {
                alca.resize.functions["all"].push(func);
            };
        },
        refresh: function () {
            setTimeout(function () {
                if (_window.width() + alca.f.getScrollBarWidth() <= alca.settings.mobileChangeWidth) {
                    //Mobile Resize
                    if (!isMobile || !isResizeinitiated) {
                        //Mobile init
                        isMobile = true;

                        for (var i = 0; i < alca.resize.functions.mobile_init.length; i++) {
                            alca.resize.functions.mobile_init[i]();
                        };

                        isResizeinitiated = true;
                    };

                    for (var i = 0; i < alca.resize.functions.mobile.length; i++) {
                        alca.resize.functions.mobile[i]();
                    };
                }
                else {
                    //Desktop Resize
                    if (isMobile || !isResizeinitiated) {
                        //Desktop init
                        isMobile = false;

                        for (var i = 0; i < alca.resize.functions.desktop_init.length; i++) {
                            alca.resize.functions.desktop_init[i]();
                        };

                        isResizeinitiated = true;
                    };

                    for (var i = 0; i < alca.resize.functions.desktop.length; i++) {
                        alca.resize.functions.desktop[i]();
                    };
                }

                for (var i = 0; i < alca.resize.functions.all.length; i++) {
                    alca.resize.functions.all[i]();
                };
            }, 0);
        },
        functions: { mobile: [], mobile_init: [], desktop: [], desktop_init: [], all: [] }
    },
    load: {
        add: function (func) {
            if (typeof func == "function") {
                alca.load.functions.push(func);
            };
        },
        refresh: function () {
            var isFontsLoaded = false;
            var isWindowLoaded = false;
            var isFunctionsCalled = false;

            setTimeout(function () {
                if (document.fonts && document.fonts.ready) {
                    if (document.readyState == "complete") {
                        isWindowLoaded = true;
                        callLoadFunctions();
                    }
                    else {
                        _window.on('load', function () {
                            isWindowLoaded = true;
                            callLoadFunctions();
                        });
                    }

                    document.fonts.ready.then(function () {
                        isFontsLoaded = true;
                        callLoadFunctions();
                    });
                }
                else {
                    isFontsLoaded = true;

                    if (document.readyState == "complete") {
                        isWindowLoaded = true;
                        callLoadFunctions();
                    }
                    else {
                        _window.on('load', function () {
                            isWindowLoaded = true;
                            callLoadFunctions();
                        });
                    }
                }
            }, 0);

            function callLoadFunctions() {
                if (isWindowLoaded && isFontsLoaded && !isFunctionsCalled) {
                    isFunctionsCalled = true;
                    for (var i = 0; i < alca.load.functions.length; i++) {
                        alca.load.functions[i]();
                    };
                }
            }
        },
        functions: []
    },
    ready: function () {
        alca.resize.refresh();
        alca.load.refresh();
    },
    init: function () {
        //Custom jQuery functions
        alca.jQueryExtensions.init();

        //Resize options set
        if (_window.width() + alca.f.getScrollBarWidth() <= alca.settings.mobileChangeWidth) {
            isMobile = true;
        }
        else {
            isMobile = false;
        }

        alca.resize.width = _window.width();

        //Modules
        alca.modules.initVideo();
        alca.modules.initTabs();
        alca.modules.initAccordion();
        alca.modules.initResponsiveImages();
        alca.modules.initReadMore();
        alca.modules.initFullHeight();
        alca.animate.scrollUp();

        if (alca.settings.initLinkFormatter) {
            alca.modules.initLinks(_body);
        };


        //Forms
        alca.form.init();

        //Prevent Scroll
        window.addEventListener("mousewheel", function (e) {
            if (alca.settings.preventScroll) {
                preventEvent(e);
            }
        }, { passive: false });

        window.addEventListener("touchmove", function (e) {
            if (alca.settings.preventScroll) {
                preventEvent(e);
            }
        }, { passive: false });

        function preventEvent(e) {
            e = e || window.event;
            if (e.preventDefault)
                e.preventDefault();
            e.returnValue = false;
        }

        //Animate Refresh
        _window.on('scroll', function () {
            alca.animate.refresh();
        });

        alca.load.add(function () {
            alca.animate.refresh();
        });

        //Resize settings
        _window.on('resize', function () {
            waitForFinalEvent(function () {
                alca.resize.refresh();

                if (alca.owl.responsiveOptionList.length > 0) {
                    alca.owl.refreshOptions();
                };

                //Only fire when width changes
                if (alca.resize.width != _window.width()) {
                    alca.resize.width = _window.width();
                    alca.f.repaintHtml();
                };
            }, 250, "window resize");
        });
    }
}

function onFormSubmit(token) {
    var recaptchaData = "ReCaptchaToken=" + token;
    if (typeof currentData == "object") {
        currentData.ReCaptchaToken = token;
    }
    else {
        if (currentData.length > 0) {
            recaptchaData = "&" + recaptchaData;
        }
        currentData += recaptchaData;
    }
    recaptchaInitialized = true;
}

var waitForFinalEvent = (function () {
    var timers = {};
    return function (callback, ms, uniqueId) {
        if (!uniqueId) {
            uniqueId = "Don't call this twice without a uniqueId";
        }
        if (timers[uniqueId]) {
            clearTimeout(timers[uniqueId]);
        }
        timers[uniqueId] = setTimeout(callback, ms);
    };
})();
