﻿_document.ready(function () {
    alca.init();
    initMenu();
    initIndicator();
    scrollLink();
    alca.ready();
});


function initMenu() {
    var _header = $("header"),
      _menu = _header.find(".menu-wrapper"),
      _hamburger = _header.find(".hamburger"),
        _overlay = $(".overlay");
  
    if (_header.any()) {
      _window.on("scroll", function () {
        var ws = _window.scrollTop();
        var offset = _header.outerHeight() - 100;
		
        if (ws > offset && !_header.hasClass("fixed")) {
          _header.addClass("fixed");
        } else if (ws <= offset && _header.hasClass("fixed")) {
          _header.removeClass("fixed");
        }
      });
    }
  
    _hamburger.click(function () {
      if (_body.hasClass("menu")) {
        _body.removeClass("menu");
      } else {
        _body.addClass("menu");
      }
    });
  
    _overlay.click(function () {
      _body.removeClass("menu");
    });
  }

  function initIndicator() {
    var _cont = $(".indicator");
    if (_cont.any()) {
      _window.on("scroll", function () {
        var winScroll = $("body,html").scrollTop();
        var height = $("body").outerHeight() - $(window).height();
  
        var scrolled = (winScroll / height) * 100;
        _cont.css("width", scrolled + "%");
      });
    }
  }

  function scrollLink() {
    var _headerHeight = ($("header").outerHeight()),
      _item = $("a.link-item"),
      _logo = $(".logo a");
  
    alca.resize.minit(function () {
      _item.click(function (event) {
        event.preventDefault();
        var _this = $(this);
        window.scrollTo({
          top: $("section." + _this.attr("href")).offset().top - _headerHeight,
          left: 0,
          behavior: "smooth",
        });
        _body.removeClass("menu");
      });
    });
  
    alca.resize.dinit(function () {
      _item.click(function (event) {
        event.preventDefault();
        var _this = $(this);
        window.scrollTo({
          top: $("section." + _this.attr("href")).offset().top - _headerHeight,
          left: 0,
          behavior: "smooth",
        });
      });
    });
  
    _logo.click(function (event) {
      event.preventDefault();
      window.scrollTo({
        top: 0,
        left: 0,
        behavior: "smooth",
      });
    });
  }