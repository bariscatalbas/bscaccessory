﻿using AcCms.Services.Web.Mvc;
using Modules.Cms.Models;
using System.Collections.Generic;
using System.Web.Mvc;
using AcCms.Models.Common;
using AcCms.Services.Web.Services;

namespace AcCms.Web.Controllers
{
    public class HomeController : ClientControllerBaseWithIdentity
    {

        public HomeController()
        {

        }

        public ActionResult Content(string entityName, long entityId, string url, int? statusCode)
        {
            var model = new List<CmsContentModel>();

            if (entityName == "Page")
            {
                var entity = ContentService.CmsEntity(entityName, entityId);
                if (entity != null)
                {
                    ViewBag.Entity = entity;
                    model = ContentService.CmsContents(Language, entity.EntityName, entity.EntityId, entity.CmsVersion, false);
                }
            }

            if (statusCode.HasValue)
            {
                Response.StatusCode = statusCode.Value;
                Response.TrySkipIisCustomErrors = true;
            }

            //ViewBag.TopMenu = ContentService.me

            return View(entityName, model);
        }

        public ActionResult Preview(string entityName, long entityId, int cmsVersion)
        {
            var model = new List<CmsContentModel>();

            if (entityName == "Page")
            {
                var entity = ContentService.CmsEntity(entityName, entityId, true);
                if (entity != null)
                {
                    ViewBag.Entity = entity;
                    model = ContentService.CmsContents(Language, entity.EntityName, entity.EntityId, cmsVersion, true);
                }
            }

            return View(entityName, model);
        }

        [ChildActionOnly]
        public ActionResult PagePartial(string name)
        {
            var pagePartial = ContentService.GetPagePartialIdByNameLanguageId(name, Language.Id);
            if (pagePartial != null)
            {
                var model = ContentService.CmsContents(Language, pagePartial.EntityName, pagePartial.EntityId, pagePartial.CmsVersion, false);

                return View("PagePartial", model);
            }

            return Content("");
        }

        public ActionResult NotFound()
        {
            return View();
        }

        public ActionResult Error()
        {
            return View();
        }
    }
}