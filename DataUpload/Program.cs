﻿using AcCms.Data;
using ExcelDataReader;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AcCms.Data.Domains;
using static AcCms.Models.Enumeration.Enums;

namespace DataUpload
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Excel Okunuyor...");
            var response = ReadFromExcel();

            Console.WriteLine("Model oluşturuldu...");

            var i = 1;
            var counter = 1;

            foreach (var excelItem in response)
            {
                UpdateOrdersToDeleted(excelItem);

                counter = counter + i;
                Console.WriteLine(i++ + " kayıt.");
            }

            Console.WriteLine("Tamamlandı");
            Console.WriteLine("Çıkmak İçin Bir Tuşa Basın !");
            Console.ReadKey();
        }

        private static void UpdateOrdersToDeleted(ProductItemModel excelItem)
        {
            var context = new DbContext();

            var item = new Product
            {
                IsActive = true,
                Code = excelItem.Code,
                Price = excelItem.Price,
                Name = excelItem.Name,
                CreatedBy = 1,
                ModificationDate = DateTime.Now,
                CreationDate = DateTime.Now,
                ModifiedBy = 1,
                CompanyName = excelItem.CompanyName,
                Deleted = false, 
            };

            if (excelItem.Type.Contains("Bileklik"))
            {
                item.CategoryType = ProductCategory.Bracelet;
            }
            else if (excelItem.Type.Contains("Küpe"))
            {
                item.CategoryType = ProductCategory.Earring;
            }
            else if (excelItem.Type.Contains("Kolye"))
            {
                item.CategoryType = ProductCategory.Necklace;
            }
            else if (excelItem.Type.Contains("Yüzük"))
            {
                item.CategoryType = ProductCategory.Ring;
            }
            else
            {
                item.CategoryType = 0;
            }

            context.Products.Add(item);
            context.SaveChanges();

            if (item.Id > 0)
            {
                var productStock = new ProductStock
                {
                    ProductId = item.Id,
                    Amount = excelItem.Amount,
                    BuyingPrice = excelItem.BuyingPrice,
                    CreatedBy = 1,
                    CreationDate = DateTime.Now,
                    ModifiedBy = 1,
                    ModificationDate = DateTime.Now,
                    Deleted = false,
                };
                context.ProductStocks.Add(productStock);
            }

             
            context.SaveChanges();

        }

        private static List<ProductItemModel> ReadFromExcel()
        {
            var filePath = @"C:\BSC-SON.xlsx";
            var model = new List<ProductItemModel>();
            using (var stream = File.Open(filePath, FileMode.Open, FileAccess.Read))
            {
                using (var reader = ExcelReaderFactory.CreateReader(stream))
                {
                    do
                    {
                        while (reader.Read())
                        {
                            var code = reader.GetValue(0);
                            var type = reader.GetValue(1);
                            var name = reader.GetValue(2);
                            var buyingPrice = reader.GetValue(3);
                            var price = reader.GetValue(4);
                            var amount = reader.GetValue(5) + "";

                            if (name != null)
                            {
                                var newModel = new ProductItemModel
                                {
                                    Type = type.ToString(),
                                    Name = name.ToString(), 
                                    Price = _.To.Decimal(price),
                                    BuyingPrice = _.To.Decimal(buyingPrice),
                                    Code = _.String.StripNonAlphaNumeric(code.ToString()),
                                };

                                if (!string.IsNullOrWhiteSpace(amount))
                                {
                                    if (amount.Contains("+"))
                                    {
                                        var splitList = amount.Split('+').ToList();
                                        foreach (var item in splitList)
                                        {
                                            newModel.Amount += _.To.Int(item);
                                        }
                                    }
                                    else
                                    {
                                        newModel.Amount = _.To.Int(amount);
                                    }
                                }

                                model.Add(newModel);
                            }

                        }
                    } while (reader.NextResult());

                }
            }
            return model;
        }
    }


    public class ProductItemModel
    { 
        public string Type { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string CompanyName { get; set; }
        public decimal Price { get; set; }
        public int Amount { get; set; }
        public decimal BuyingPrice { get; set; }

    }
}