﻿using AcCms.Data;
using Core.Task;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Core.Ioc;
using AcCms.Services.Common.Data;

namespace AcCms.Services.Common.Tasks
{
    public class DeleteGuestAccountsTask : AcTaskBase
    {
        public DeleteGuestAccountsTask() : base()
        {
        }

        public override string Code => "DeleteGuestAccountsTask";

        public override string Name => "Delete Guest Accounts Task";

        public override string SettingsSystemName { get { return typeof(DeleteGuestAccountsTaskSettings).AssemblyQualifiedName; } }

        private DeleteGuestAccountsTaskSettings Settings { get; set; }

        public override bool CanIStartLocal(TaskPrepareModel task, out string logThisAtHistory)
        {
            logThisAtHistory = "";
            Settings = JsonConvert.DeserializeObject<DeleteGuestAccountsTaskSettings>(task.SettingsJson);

            if (!string.IsNullOrWhiteSpace(Settings.RunningHours))
            {
                List<int> runningHours = Settings.RunningHours.Split(',').Select(p => _.To.Int(p.Trim(), -1)).ToList();

                if (runningHours.Contains(DateTime.UtcNow.Hour))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

            return true;
        }

        public override bool Execute(TaskPrepareModel task, out string logThisAtHistory)
        {
            var dbContextFunctions = AcEngine.Resolve<DbContextFunctions>();

            var utcNow = DateTime.UtcNow;
            var deleteBeforeUtc = utcNow.AddMonths(-1 * Settings.MonthsToKeepGuestAccounts);
            var deletedCount = dbContextFunctions.DeleteGuestAccounts(deleteBeforeUtc);
            logThisAtHistory = deletedCount + " accounts deleted";
            return true;
        }

        public class DeleteGuestAccountsTaskSettings
        {
            [DisplayName("Running Hours (In UTC, comma seperated)")]
            public string RunningHours { get; set; }

            [DisplayName("How many months to keep guest Accounts?")]
            public int MonthsToKeepGuestAccounts { get; set; }
        }
    }
}
