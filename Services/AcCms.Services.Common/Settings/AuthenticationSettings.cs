﻿using Core.Mvc.Attributes;
using Core.Settings;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace AcCms.Services.Common.Settings
{
    public class AuthenticationSettings : ISetting
    {
        [DisplayName("Enable Email Validation On Register")]
        public bool EnableEmailValidationOnRegister { get; set; }

        [DisplayName("Password Reminder Token Expiration Day")]
        public int PasswordReminderTokenExpirationDay { get; set; }
    }
}
