﻿using System;

namespace AcCms.Services.Common.Data
{
    public class DbAccountSessionModel
    {
        public Guid Token { get; set; }
        public long AccountId { get; set; }
        public long? ImpersonatedAccountId { get; set; }

        public string LastError { get; set; }
        public string LastSuccess { get; set; }
    }
}
