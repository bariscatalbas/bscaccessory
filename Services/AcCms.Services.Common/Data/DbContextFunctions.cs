﻿using AcCms.Data;
using Core.Ioc;
using System;
using System.Data.SqlClient;
using System.Linq;

namespace AcCms.Services.Common.Data
{
    public class DbContextFunctions : IInjectAsSelf
    {
        private readonly DbContext _dbContext;

        public DbContextFunctions(DbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public DbAccountSessionModel LoadSession(Guid token)
        {
            var parameter1 = new SqlParameter("@token", token);
            var parameter2 = new SqlParameter("@date", DateTime.UtcNow);

            return _dbContext.Database.SqlQuery<DbAccountSessionModel>("LoadSession @token, @date", parameter1, parameter2).FirstOrDefault();
        }

        public int DeleteGuestAccounts(DateTime deleteBeforeUtc)
        {
            return _dbContext.Database.ExecuteSqlCommand("DeleteGuestAccounts @DeleteBeforeUtc", new SqlParameter("@DeleteBeforeUtc", deleteBeforeUtc.ToString("yyyy-MM-dd")));
        }
    }
}
