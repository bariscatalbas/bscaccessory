﻿using AcCms.Models.Authentication;
using Core.Cache;
using Core.Ioc;
using Core.Mvc.Services;
using Omu.ValueInjecter;
using System;

namespace AcCms.Services.Common.Authentication
{
    public class WebIdentity : IdentityBase, IInjectAsSelf
    {
        private const string cookieKey = "o-key-client";

        public WebIdentity()
        {
            var cacheHelper = AcEngine.Resolve<AcCacheHelper>();
            var cookieBasedMembershipProvider = new CookieBasedMembershipProvider(cookieKey, 300);
            var authenticationService = AcEngine.Resolve<AuthenticationService>();

            var identity = cacheHelper
                        .GetPerRequest("identity",
                        () =>
                        {
                            var token = cookieBasedMembershipProvider.GetSessionKey;
                            var identityBase = authenticationService.Identity(token);

                            if (identityBase != null)
                            {
                                if (identityBase.Token != token)
                                {
                                    cookieBasedMembershipProvider.SignIn(identityBase.Token);
                                }
                            }

                            return identityBase;
                        });

            this.InjectFrom(identity);
        }
        public void SignIn(Guid token)
        {
            var cacheHelper = AcEngine.Resolve<AcCacheHelper>();
            var cookieBasedMembershipProvider = new CookieBasedMembershipProvider(cookieKey, 300);
            var authenticationService = AcEngine.Resolve<AuthenticationService>();

            var identity = cacheHelper
                       .SetPerRequest("identity",
                       () =>
                       {
                           var identityBase = authenticationService.Identity(token);

                           if (identityBase != null)
                           {
                               cookieBasedMembershipProvider.SignIn(identityBase.Token);
                           }

                           return identityBase;
                       });

            this.InjectFrom(identity);
        }
        public void ChangeLanguage(long languageId)
        {
            var authenticationService = AcEngine.Resolve<AuthenticationService>();
            authenticationService.ChangeLanguage(Id, languageId);
            this.LanguageId = languageId;

            var _cacheProvider = AcEngine.Resolve<AcCacheHelper>();
            _cacheProvider.SetPerRequest("identity", (IdentityBase)this);
        }

        public void SetMessageBeforeRedirection(string successMessage, string errorMessage)
        {
            var authenticationService = AcEngine.Resolve<AuthenticationService>();
            authenticationService.SetMessageBeforeRedirection(Id, successMessage, errorMessage);
        }
        public void SignOut()
        {
            var cookieBasedMembershipProvider = new CookieBasedMembershipProvider(cookieKey, 300);
            cookieBasedMembershipProvider.SignOut();
        }
    }
}
