﻿using AcCms.Models.Enumeration;
using Core.Ioc;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AcCms.Services.Common.Authentication
{
    public class AdminAuthorizeAttribute : AuthorizeAttribute
    {
        public AdminAuthorizeAttribute(params Enums.Privilege[] privileges)
        {
            Privileges = (privileges != null && privileges.Any()) ? privileges.ToList() : new List<Enums.Privilege>();
        }

        public List<Enums.Privilege> Privileges { get; set; }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            var identity = AcEngine.Resolve<WebIdentity>();
            if (!identity.IsGuest && identity.AllowAccess(Enums.Privilege.AdminAccess))
            {
                if (Privileges.Any())
                {
                    return identity.AllowAccess(Privileges.ToArray());
                }
            }
            return false;
        }
    }
}
