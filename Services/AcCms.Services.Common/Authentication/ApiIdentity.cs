﻿using AcCms.Models.Authentication;
using Core.Ioc;
using Omu.ValueInjecter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AcCms.Services.Common.Authentication
{
    public class ApiIdentity : IdentityBase, IInjectAsSelf
    {
        public Guid GetAuthenticationToken()
        {
            var authenticationToken = _.Web.GetHeaderValue("AuthenticationToken");
            var sessionKey = _.Crypto.DecryptText(authenticationToken);
            return _.To.Guidd(sessionKey);
        }

        public void SetAuthenticationToken()
        {
            var authenticationToken = _.Crypto.EncryptText(Token.ToString());
            _.Web.SetHeaderValue("AuthenticationToken", authenticationToken);
        }

        public ApiIdentity()
        {
            var authenticationService = AcEngine.Resolve<AuthenticationService>();
            var token = GetAuthenticationToken();
            var identity = authenticationService.Identity(token);
            if (identity != null)
            {
                if (identity.Token != token)
                {
                    identity.Token = token;
                }
            }

            this.InjectFrom(identity);
        }

        public void SignIn(Guid token)
        {
            var authenticationService = AcEngine.Resolve<AuthenticationService>();
            var identity = authenticationService.Identity(token);
            if (identity != null)
            {
                this.Token = identity.Token;
            }
            this.InjectFrom(identity);
        }

    }
}
