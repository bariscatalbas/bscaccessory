﻿using AcCms.Data.Domains;
using AcCms.Models.Authentication;
using AcCms.Models.Core;
using AcCms.Models.Email;
using AcCms.Models.Enumeration;
using AcCms.Services.Common.Data;
using AcCms.Services.Common.Resources;
using AcCms.Services.Common.Services;
using AcCms.Services.Common.Settings;
using Core.Data.Interfaces;
using Core.Ioc;
using Core.Resources;
using Core.Settings;
using System;
using System.Linq;

namespace AcCms.Services.Common.Authentication
{
    public class AuthenticationService : IInjectAsSelf
    {
        private readonly IRepository<AccountSession> _accountSessionRepository;
        private readonly IRepository<AccountPermission> _accountPermissionRepository;
        private readonly IRepository<Account> _accountRepository;
        private readonly DbContextFunctions _dbContextFunctions;
        private readonly LanguageService _languageService;
        private readonly EmailService _emailService;

        private readonly AcSettingsHelper _settingsHelper;
        private readonly AcResourceHelper _resourceHelper;
        private readonly AuthenticationSettings _authenticationSettings;

        public AuthenticationService(IRepository<AccountSession> accountSessionRepository, IRepository<AccountPermission> accountPermissionRepository, IRepository<Account> accountRepository,
            DbContextFunctions dbContextFunctions, LanguageService languageService, EmailService emailService, AcSettingsHelper settingsHelper, AcResourceHelper resourceHelper)
        {
            _accountSessionRepository = accountSessionRepository;
            _accountPermissionRepository = accountPermissionRepository;
            _accountRepository = accountRepository;
            _dbContextFunctions = dbContextFunctions;
            _languageService = languageService;
            _emailService = emailService;

            _settingsHelper = settingsHelper;
            _resourceHelper = resourceHelper;
            _authenticationSettings = _settingsHelper.LoadSettingsCached<AuthenticationSettings>();
        }

        public IdentityBase Identity(Guid token)
        {
            IdentityBase identity = null;

            var session = _dbContextFunctions.LoadSession(token);
            if (session.ImpersonatedAccountId != null && _.Configuration.AppSettings("WhichAppAmI") == "Web" && session.ImpersonatedAccountId != session.AccountId)
            {
                identity = _accountRepository
                    .Table
                    .Where(p => p.Id == session.ImpersonatedAccountId)
                    .Select(p => new IdentityBase
                    {
                        Id = p.Id,
                        LanguageId = p.LanguageId,
                        Token = session.Token,
                        IsAuthenticated = true,
                        IsGuest = p.IsGuest,
                        IsEmailValidated = p.IsEmailValidated,
                        Email = p.Email,
                        Phone = p.Phone,
                        FirstName = p.FirstName,
                        LastName = p.LastName,
                        Roles = p.Roles.Select(t => t.RoleId).ToList(),
                        Privileges = p.Roles.SelectMany(t => t.Role.Privileges.Select(q => (Enums.Privilege)q.Privilege)).Distinct().ToList(),
                        LastError = session.LastError,
                        LastSuccess = session.LastSuccess
                    }).FirstOrDefault();
            }
            if (identity == null)
            {
                identity = _accountRepository
                        .Table
                        .Where(p => p.Id == session.AccountId)
                        .Select(p => new IdentityBase
                        {
                            Id = p.Id,
                            LanguageId = p.LanguageId,
                            Token = session.Token,
                            IsAuthenticated = true,
                            IsGuest = p.IsGuest,
                            IsEmailValidated = p.IsEmailValidated,
                            Email = p.Email,
                            Phone = p.Phone,
                            FirstName = p.FirstName,
                            LastName = p.LastName,
                            Roles = p.Roles.Select(t => t.RoleId).ToList(),
                            Privileges = p.Roles.SelectMany(t => t.Role.Privileges.Select(q => (Enums.Privilege)q.Privilege)).Distinct().ToList(),
                            LastError = session.LastError,
                            LastSuccess = session.LastSuccess
                        }).FirstOrDefault();
            }

            if (identity != null)
            {
                if(identity.AllowAccess(Enums.Privilege.AdminAccess) && !identity.LanguageId.HasValue)
                {
                    var language = _languageService.GetLanguage(0, true);
                    identity.LanguageId = language != null ? (long?)language.Id : null;
                }

                var superAdmins = _.Configuration.AppSettings("SuperAdmins");
                if (!string.IsNullOrWhiteSpace(superAdmins))
                {
                    var superAdminEmails = superAdmins.Split(',');

                    if (superAdminEmails.Contains(identity.Email))
                    {
                        identity.Privileges.Add(Enums.Privilege.AdminSuper);
                    }
                }
            }

            return identity;
        }

        public void SetMessageBeforeRedirection(long accountId, string success, string error)
        {
            var accountObj = _accountRepository.Table.FirstOrDefault(p => p.Id == accountId);
            if (accountObj != null)
            {
                accountObj.LastError = error;
                accountObj.LastSuccess = success;
                _accountRepository.Update(accountObj, accountObj.Id);
            }
        }

        public Enums.LoginResult Login(LoginModel model, out Guid token)
        {
            token = Guid.Empty;
            var email = (model.Email ?? "").ToLowerInvariant();
            var account = _accountRepository.Table.FirstOrDefault(p => p.Email == email && !p.Deleted && !p.IsGuest);
            if (account != null)
            {
                var passSalt = account.PasswordSalt;
                var pass = _.Crypto.Password.CreatePasswordHash(model.Password, passSalt);

                if (pass == account.PasswordHash)
                {
                    if (!account.IsActive)
                    {
                        return Enums.LoginResult.NotActive;
                    }
                    if (!account.IsEmailValidated)
                    {
                        return Enums.LoginResult.RequireEmailValidation;
                    }

                    token = Guid.NewGuid();
                    SignIn(account.Id, token);
                    return Enums.LoginResult.Success;
                }
            }

            return Enums.LoginResult.Failed;
        }

        private void SignIn(long accountId, Guid token)
        {
            var session = new AccountSession
            {
                AccountId = accountId,
                Token = token,
                IsActive = true,
                LastActivityDate = DateTime.UtcNow,
                CreationDate = DateTime.UtcNow
            };
            _accountSessionRepository.Insert(session, accountId);
        }

        //public Enums.RegisterResult Register(IdentityBase identity, LanguageModel language, RegisterModel model, out Guid token)
        //{
        //    token = Guid.Empty;
        //    var email = model.Email.Trim().ToLowerInvariant();
        //    var existingAccountWithSameEmail = _accountRepository.Table.FirstOrDefault(p => p.Email == email && !p.Deleted && !p.IsGuest);
        //    if(existingAccountWithSameEmail != null)
        //    {
        //        return Enums.RegisterResult.InvalidEmail;
        //    }
        //    else
        //    {
        //        var passSalt = _.Crypto.Password.CreateSaltKey(25);
        //        var pass = _.Crypto.Password.CreatePasswordHash(model.Password, passSalt);

        //        var account = new Account
        //        {
        //            LastActivityDate = DateTime.UtcNow,
        //            FirstName = model.FirstName,
        //            LastName = model.LastName,
        //            Email = model.Email,
        //            Phone = model.Phone,
        //            IsMale = model.IsMale,
        //            Birthdate = model.Birthdate,
        //            IsEmailValidated = _authenticationSettings.EnableEmailValidationOnRegister ? false : true,
        //            EmailValidationToken = _authenticationSettings.EnableEmailValidationOnRegister ? Guid.NewGuid() : Guid.Empty,
        //            IsGuest = false,
        //            IsActive = true,
        //            PasswordSalt = passSalt,
        //            PasswordHash = pass
        //        };

        //        _accountRepository.Insert(account, identity.Id);
                
        //        if(model.AllowCommunicationEmail)
        //        {
        //            AccountPermission accountPermissionEmail = new AccountPermission
        //            {
        //                AccountId = account.Id,
        //                Permission = Enums.CommunicationPermission.Email,
        //                Date = DateTime.UtcNow
        //            };

        //            _accountPermissionRepository.Insert(accountPermissionEmail, identity.Id);
        //        }

        //        if (model.AllowCommunicationSms)
        //        {
        //            AccountPermission accountPermissionSms = new AccountPermission
        //            {
        //                AccountId = account.Id,
        //                Permission = Enums.CommunicationPermission.Email,
        //                Date = DateTime.UtcNow
        //            };

        //            _accountPermissionRepository.Insert(accountPermissionSms, identity.Id);
        //        }

        //        if (_authenticationSettings.EnableEmailValidationOnRegister)
        //        {
        //            WebResource webResource = _resourceHelper.LoadResourceCached<WebResource>(language.Id);

        //            EmailValidationEmailModel emailModel = new EmailValidationEmailModel
        //            {
        //                FirstName = account.FirstName,
        //                LastName = account.LastName,
        //                Link = (webResource.EmailValidationPageUrl ?? "") + "?token=" + account.EmailValidationToken
        //            };

        //            _emailService.SendEmailValidtionEmail(identity, language, emailModel);

        //            return Enums.RegisterResult.EmailValidationRequired;
        //        }
        //        else
        //        {
        //            token = Guid.NewGuid();
        //            SignIn(account.Id, token);

        //            return Enums.RegisterResult.Success;
        //        }
        //    }
        //}

        public bool ValdateEmail(IdentityBase identity, EmailValidationModel model)
        {
            var account = _accountRepository.Table.FirstOrDefault(p => p.EmailValidationToken == model.EmailValidationToken && !p.Deleted && !p.IsGuest);
            if (account != null)
            {
                account.IsEmailValidated = true;
                _accountRepository.Update(account, identity.Id);

                return true;
            }

            return false;
        }

        //public Enums.PasswordReminderResult SendPasswordReminder(IdentityBase identity, LanguageModel language, PasswordReminderModel model)
        //{
        //    var account = _accountRepository.Table.FirstOrDefault(p => p.Email == model.Email && !p.Deleted && !p.IsGuest);
        //    if(account != null)
        //    {
        //        var passwordReminderToken = Guid.NewGuid();
        //        account.PasswordReminderToken = passwordReminderToken;
        //        account.PasswordReminderTokenExpirationDate = DateTime.UtcNow.AddDays(_authenticationSettings.PasswordReminderTokenExpirationDay);
        //        _accountRepository.Update(account, identity.Id);

        //        WebResource webResource = _resourceHelper.LoadResourceCached<WebResource>(language.Id);

        //        ForgetPasswordEmailModel emailModel = new ForgetPasswordEmailModel
        //        {
        //            Link = _.Configuration.WebSiteUrl + (webResource.ChangePasswordPageUrl ?? "") + "?token=" + account.PasswordReminderToken
        //        };

        //        _emailService.SendForgetPasswordEmail(identity, language, emailModel);

        //        return Enums.PasswordReminderResult.Success;
        //    }
        //    else
        //    {
        //        return Enums.PasswordReminderResult.WrongEmail;
        //    }
        //}

        public Enums.ChangePasswordResult ChangePassword(IdentityBase identity, ChangePasswordModel model)
        {
            var account = _accountRepository.Table.FirstOrDefault(p => p.PasswordReminderToken == model.PasswordReminderToken && !p.Deleted && !p.IsGuest);
            if (account != null)
            {
                var utcNow = DateTime.UtcNow;
                if(utcNow <= account.PasswordReminderTokenExpirationDate)
                {
                    var passSalt = account.PasswordSalt;
                    var pass = _.Crypto.Password.CreatePasswordHash(model.Password, passSalt);
                    account.PasswordHash = pass;
                    _accountRepository.Update(account, identity.Id);

                    return Enums.ChangePasswordResult.Success;
                }
                else
                {
                    return Enums.ChangePasswordResult.TokenExpired;
                }
            }
            else
            {
                return Enums.ChangePasswordResult.WrongToken;
            }
        }

        public void ChangeLanguage(long accountId, long languageId)
        {
            var account = _accountRepository.Table.FirstOrDefault(p => p.Id == accountId);
            if (account != null)
            {
                account.LanguageId = languageId;
                _accountRepository.Update(account, accountId);
            }
        }

        public void Logout(long accountId, Guid token)
        {
            SignOut(accountId, token);
        }

        private void SignOut(long accountId, Guid token)
        {
            var session = _accountSessionRepository.Table.Where(p => p.AccountId == accountId && p.Token == token).FirstOrDefault();
            if (session != null)
            {
                session.IsActive = false;
                _accountSessionRepository.Update(session, accountId);
            }
        }
    }
}
