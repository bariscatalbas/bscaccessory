﻿using AcCms.Models.Enumeration;
using Core.Ioc;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AcCms.Services.Common.Authentication
{
    public class WebAuthorizeAttribute : AuthorizeAttribute
    {
        public WebAuthorizeAttribute(params Enums.Privilege[] privileges)
        {
            Privileges = (privileges != null && privileges.Any()) ? privileges.ToList() : new List<Enums.Privilege>();
        }

        public List<Enums.Privilege> Privileges { get; set; }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            var identity = AcEngine.Resolve<WebIdentity>();
            if (!identity.IsGuest)
            {
                if (!Privileges.Any())
                {
                    return true;
                }
                else
                {
                    return identity.AllowAccess(Privileges.ToArray());
                }
            }
            return false;
        }
    }
}
