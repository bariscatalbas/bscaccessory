﻿using AcCms.Data.Domains;
using AcCms.Models.Core;
using Core.Cache;
using Core.Data.Interfaces;
using Core.Ioc;
using System.Collections.Generic;
using System.Linq;

namespace AcCms.Services.Common.Services
{
    public class LanguageService : IInjectAsSelf
    {
        private readonly IRepository<Language> _languageRepository;
        private readonly AcCacheHelper _cacheHelper;

        public LanguageService(IRepository<Language> languageRepository, AcCacheHelper cacheHelper)
        {
            _languageRepository = languageRepository;
            _cacheHelper = cacheHelper;
        }


        private List<LanguageModel> _languages = null;
        private List<LanguageModel> Languages
        {
            get
            {
                if (_languages == null)
                {
                    _languages = _cacheHelper.GetVersioned<List<LanguageModel>>("AC_CACHE_LANGUAGE",
                           () =>
                           {
                               var list = _languageRepository
                                                    .Table
                                                    .Select(p => new LanguageModel
                                                    {
                                                        Id = p.Id,
                                                        Code = p.Code,
                                                        DisplayOrder = p.DisplayOrder,
                                                        IsDefault = p.IsDefault,
                                                        Name = p.Name,
                                                        Published = p.Published
                                                    }).ToList();
                               return list;
                           });

                }
                return _languages;
            }
        }
        public long DecideLanguageFromUrl(string urlContainerPart, out bool isDefault)
        {
            isDefault = false;
            var language = Languages.FirstOrDefault(p => p.Code == urlContainerPart && p.Published);
            if (language == null)
            {
                language = Languages.Where(p => p.IsDefault && p.Published).OrderBy(p => p.DisplayOrder).FirstOrDefault();
                isDefault = true;
            }
            return language?.Id ?? 0;
        }
        public LanguageModel GetLanguage(long languageId, bool isRequired = false)
        {
            var language = Languages.FirstOrDefault(p => p.Id == languageId);

            if (language == null && isRequired)
            {
                language = Languages.FirstOrDefault(p => p.IsDefault);
                if (language == null)
                {
                    language = Languages.FirstOrDefault();
                }
            }

            return language;
        }

        public LanguageModel GetLanguage(string languageCode, bool isRequired = false)
        {
            var language = Languages.FirstOrDefault(p => p.Code == languageCode);

            if (language == null && isRequired)
            {
                language = Languages.FirstOrDefault(p => p.IsDefault);
                if (language == null)
                {
                    language = Languages.FirstOrDefault();
                }
            }

            return language;
        }
    }
}
