﻿using AcCms.Models.Authentication;
using AcCms.Models.Core;
using AcCms.Models.Email;
using Core.EmailTemplates;
using Core.Ioc;
using Core.Sms;
using Core.SmsTemplates;
using System.Collections.Generic;
using System.Web.Mvc;

namespace AcCms.Services.Common.Services
{
    public class SmsService : IInjectAsSelf
    {
        private readonly AcSmsTemplateHelper _smsTemplateHelper;
        private readonly AcSmsHelper _smsHelper;

        public SmsService(AcSmsTemplateHelper smsTemplateHelper, AcSmsHelper smsHelper)
        {
            _smsTemplateHelper = smsTemplateHelper;
            _smsHelper = smsHelper;
        }

        public List<SelectListItem> SmsTemplateTypeSelectList()
        {
            return new List<SelectListItem>
            {
                new SelectListItem{ Text = "Master Template", Value = "Master"},
                new SelectListItem{ Text = "Welcome Sms", Value = "Welcome"}
            };
        }

        public long SendWelcomeEmail(IdentityBase identity, LanguageModel language, WelcomeSmsModel model)
        {
            return SendEmail(identity, language, model, "Welcome");
        }

        private long SendEmail<T>(IdentityBase identity, LanguageModel language, T model, string type) where T : AcSmsTemplateBaseModel
        {
            var sms = _smsTemplateHelper.FillTemplateWithModel(model, language.Id, type, "Master");
            if (sms != null)
            {
                return _smsHelper.AddSmsToQueue(sms, identity.Id);
            }
            return 0;
        }



    }
}
