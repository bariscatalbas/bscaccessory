﻿using AcCms.Models.Authentication;
using AcCms.Models.Core;
using AcCms.Models.Email;
using Core.Email;
using Core.EmailTemplates;
using Core.Ioc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace AcCms.Services.Common.Services
{
    public class EmailService : IInjectAsSelf
    {
        private readonly AcEmailTemplateHelper _emailTemplateHelper;
        private readonly AcEmailHelper _emailHelper;

        public EmailService(AcEmailTemplateHelper emailTemplateHelper, AcEmailHelper emailHelper)
        {
            _emailTemplateHelper = emailTemplateHelper;
            _emailHelper = emailHelper;
        }

        public List<SelectListItem> EmailTemplateTypeSelectList()
        {
            return new List<SelectListItem>
            {
                new SelectListItem{ Text = "Master Template", Value = "Master"},
                new SelectListItem{ Text = "Welcome Email", Value = "Welcome"},
                new SelectListItem{ Text = "Email Validation Email", Value = "EmailValidation"},
                new SelectListItem{ Text = "Forget Password Email", Value = "ForgetPassword"}
            };
        }

        public long SendWelcomeEmail(IdentityBase identity, LanguageModel language, WelcomeEmailModel model)
        {
            return SendEmail(identity, language, model, "Welcome");
        }

        public long SendEmailValidtionEmail(IdentityBase identity, LanguageModel language, EmailValidationEmailModel model)
        {
            return SendEmail(identity, language, model, "EmailValidation");
        }

        public long SendForgetPasswordEmail(IdentityBase identity, LanguageModel language, ForgetPasswordEmailModel model)
        {
            return SendEmail(identity, language, model, "ForgetPassword");
        }

        private long SendEmail<T>(IdentityBase identity, LanguageModel language, T model, string type) where T : AcEmailTemplateBaseModel
        {
            var email = _emailTemplateHelper.FillTemplateWithModel(model, language.Id, type, "Master");
            if (email != null)
            {
                return _emailHelper.AddEmailToQueue(email, identity.Id);
            }
            return 0;
        }



    }
}
