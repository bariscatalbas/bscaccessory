﻿using AcCms.Data.Domains;
using AcCms.Models.Content;
using AcCms.Models.Core;
using Core.Cache;
using Core.Data.Interfaces;
using Core.Ioc;
using System.Collections.Generic;
using System.Linq;

namespace AcCms.Services.Common.Services
{
    public class StringResourceService : IInjectAsSelf
    {
        private readonly IRepository<StringResource> _stringResourceRepository;
        private readonly AcCacheHelper _cacheHelper;

        public StringResourceService(IRepository<StringResource> stringResourceRepository, AcCacheHelper cacheHelper)
        {
            _stringResourceRepository = stringResourceRepository;
            _cacheHelper = cacheHelper;
        }

        public string GetStringResource(long languageId, string key)
        {
            var stringResouces = _cacheHelper.GetVersioned<Dictionary<string, string>>("AC_CACHE_STRINGRESOURCE_" + languageId,
                           () =>
                           {
                               var stringResources = _stringResourceRepository
                                                        .Table
                                                        .Where(p => p.LanguageId == languageId)
                                                        .Select(p => new StringResourceModel
                                                        {
                                                            Key = p.Key,
                                                            Value = p.Value
                                                        }).ToDictionary(p => p.Key, p => p.Value);

                               return stringResources;
                           });

            string value;
            if (!stringResouces.TryGetValue(key, out value))
            {
                return "";
            }

            return value;
        }
    }
}
