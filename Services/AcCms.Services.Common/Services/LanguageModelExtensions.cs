﻿using AcCms.Models.Core;
using Core.Mvc.Attributes;
using Omu.ValueInjecter;
using System.Linq;

namespace AcCms.Services.Common.Services
{
    public static class LanguageModelExtensions
    {
        public static string Url(this LanguageModel model, string url)
        {
            if (string.IsNullOrWhiteSpace(url))
            {
                return "";
            }
            if (url.StartsWith("http://") || url.StartsWith("https://") || url.StartsWith("/"))
            {
                return url;
            }
            if (model.IsDefault)
            {
                return "/" + url;
            }
            return "/" + model.Code + "/" + url;
        }

        public static void LocalizeObject<T>(this LanguageModel model, T item)
        {
            if (item != null)
            {
                var properties = item.GetType().GetProperties();
                foreach (var propertyInfo in properties)
                {
                    if (!propertyInfo.CanRead || !propertyInfo.CanWrite)
                        continue;

                    object[] attrs = propertyInfo.GetCustomAttributes(true);
                    if (attrs.Any(p => p is AcLocalizeUrlAttribute))
                    {
                        var url = (string)propertyInfo.GetValue(item);
                        url = model.Url(url);
                        propertyInfo.SetValue(item, url);
                    }
                }
            }
        }

        public static T LocalizeClone<T>(this LanguageModel model, T item) where T : new()
        {
            if (item != null)
            {
                T clone = new T();
                clone.InjectFrom(item);

                var properties = clone.GetType().GetProperties();
                foreach (var propertyInfo in properties)
                {
                    if (!propertyInfo.CanRead || !propertyInfo.CanWrite)
                        continue;

                    object[] attrs = propertyInfo.GetCustomAttributes(true);
                    if (attrs.Any(p => p is AcLocalizeUrlAttribute))
                    {
                        var url = (string)propertyInfo.GetValue(clone);
                        url = model.Url(url);
                        propertyInfo.SetValue(clone, url);
                    }
                }

                return clone;
            }
            return default(T);
        }
    }
}
