﻿using Core.Mvc.Attributes;
using Core.Resources;
using Core.UrlRecord;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace AcCms.Services.Common.Resources
{
    public class WebResource : IResource
    {
        [UIHint("hiddenInput")]
        public long ContainerId { get; set; }

        [DisplayName("Home Page Url")]
        [AcLocalizeUrl]
        public string HomePageUrl { get; set; }

        [DisplayName("Page Not Found Page Url")]
        [AcLocalizeUrl]
        public string PageNotFoundUrl { get; set; }

        [DisplayName("Top Menu")]
        [UIHint("DropdownLong")]
        [AcDropdownParameter("MenuList")]
        public long? TopMenuId { get; set; }
        [DisplayName("Bottom Menu")]
        [UIHint("DropdownLong")]
        [AcDropdownParameter("MenuList")]
        public long? BottomMenuId { get; set; }
    }
}
