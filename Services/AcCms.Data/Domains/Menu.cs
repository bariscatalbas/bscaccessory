﻿using Core.Audit;
using Core.Data.Interfaces;
using Core.Data.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AcCms.Data.Domains
{
    public class Menu : EntityBase, ICachedRecord, IAuditable
    {
        public virtual long LanguageId { get; set; }
        [ForeignKey("LanguageId")]
        public virtual Language Language { get; set; }

        [MaxLength(250)]
        public virtual string Name { get; set; }

        public virtual IList<MenuItem> MenuItems { get; set; }

        public string CacheKey() => "AC_CACHE_MENU_" + Id;

        public AuditModel SelectLog(object repository)
        {
            return new AuditModel
            {
                EntityId = this.Id,
                EntityName = "Menu",
                ParentEntityId = 0,
                ParentEntityName = "",
                Details = new
                {
                    LanguageId,
                    Name
                }
            };
        }
    }
}
