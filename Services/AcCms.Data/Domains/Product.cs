﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AcCms.Models.Enumeration;
using Core.Data.Models;
using Core.Media;

namespace AcCms.Data.Domains
{
    public class Product : DeletableEntity
    {
        public virtual bool IsActive { get; set; }
        public virtual long? ImageId { get; set; }
        [ForeignKey("ImageId")]
        public virtual AcMedia Image { get; set; }
        public virtual string Code { get; set; }
        public virtual string Name { get; set; } 
        public virtual decimal Price { get; set; }
        public virtual string CompanyName { get; set; }
        public virtual Enums.ProductCategory CategoryType { get; set; }
        public IList<ProductImage> ProductImages { get; set; }
        public IList<ProductStock> ProductStocks { get; set; } 
    }
}
