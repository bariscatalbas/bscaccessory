﻿using Core.Data.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AcCms.Data.Domains
{
    public class ProductStock : DeletableEntity
    {
        public virtual long ProductId { get; set; }
        [ForeignKey("ProductId")]
        public virtual Product Product { get; set; }
        public virtual int Amount { get; set; }
        public virtual decimal BuyingPrice { get; set; }
    }
}
