﻿using Core.Audit;
using Core.Data.Interfaces;
using Core.Data.Models;
using System.ComponentModel.DataAnnotations.Schema;

namespace AcCms.Data.Domains
{
    public class AccountRole : EntityBase, IAuditable
    {
        public virtual long AccountId { get; set; }
        [ForeignKey("AccountId")]
        public virtual Account Account { get; set; }

        public virtual long RoleId { get; set; }
        [ForeignKey("RoleId")]
        public virtual Role Role { get; set; }

        public AuditModel SelectLog(object repository)
        {
            return new AuditModel
            {
                EntityName = "Account Role",
                EntityId = this.Id,
                ParentEntityId = this.AccountId,
                ParentEntityName = "Account",
                Details = new
                {
                    Id,
                    RoleId,
                    AccountId
                }
            };
        }
    }
}
