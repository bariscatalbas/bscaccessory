﻿using Core.Audit;
using Core.Data.Interfaces;
using Core.Data.Models;
using System.ComponentModel.DataAnnotations;

namespace AcCms.Data.Domains
{
    public class StringResource : EntityBase, ICachedRecord, IAuditable
    {
        public virtual long LanguageId { get; set; }

        [MaxLength(250)]
        public virtual string Key { get; set; }

        public virtual string Value { get; set; }

        public string CacheKey() => "AC_CACHE_STRINGRESOURCE_" + LanguageId;

        public AuditModel SelectLog(object repository)
        {
            return new AuditModel
            {
                EntityId = this.Id,
                EntityName = "StringResource",
                ParentEntityId = 0,
                ParentEntityName = "",
                Details = new
                {
                    Id,
                    LanguageId,
                    Key,
                    Value
                }
            };
        }
    }
}
