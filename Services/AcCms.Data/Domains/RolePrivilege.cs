﻿using Core.Audit;
using Core.Data.Interfaces;
using Core.Data.Models;
using System.ComponentModel.DataAnnotations.Schema;

namespace AcCms.Data.Domains
{
    public class RolePrivilege : EntityBase, IAuditable
    {
        public virtual long RoleId { get; set; }
        [ForeignKey("RoleId")]
        public virtual Role Role { get; set; }

        public virtual int Privilege { get; set; }

        public AuditModel SelectLog(object repository)
        {
            return new AuditModel
            {
                EntityId = this.Id,
                EntityName = "RolePrivilege",
                ParentEntityId = 0,
                ParentEntityName = "",
                Details = new
                {
                    RoleId,
                    Privilege
                }
            };
        }
    }
}
