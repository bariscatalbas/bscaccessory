﻿using Core.Data.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace AcCms.Data.Domains
{
    public class Role : EntityBase
    {
        [MaxLength(250)]
        public virtual string Name { get; set; }

        public virtual IList<RolePrivilege> Privileges { get; set; }
    }
}
