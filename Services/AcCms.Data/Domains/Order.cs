﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AcCms.Models.Enumeration;
using Core.Audit;
using Core.Data.Interfaces;
using Core.Data.Models;

namespace AcCms.Data.Domains
{
    public class Order : DeletableEntity
    {
        [MaxLength(50)]
        public virtual string OrderNo { get; set; }
        public virtual DateTime Date { get; set; }
        [MaxLength(500)]
        public virtual string FirstName { get; set; }
        [MaxLength(500)]
        public virtual string LastName { get; set; }
        [MaxLength(500)]
        public virtual string City { get; set; }
        [MaxLength(500)]
        public virtual string District { get; set; }
        [MaxLength(500)]
        public virtual string Address { get; set; }
        [MaxLength(50)]
        public virtual string Phone { get; set; }
        [MaxLength(50)]
        public virtual string Email { get; set; }
        public virtual Enums.OrderStatus OrderStatus { get; set; }
        public virtual Enums.OrderChannel OrderChannel { get; set; }
        public virtual string Notes { get; set; }
        public virtual DateTime? ShipmentDate { get; set; }
      
        [MaxLength(500)]
        public virtual string ShipmentNumber { get; set; }
        public virtual decimal? ShipmentPrice { get; set; }
        public virtual Enums.ShipmentType? ShipmentType { get; set; }
        public IList<OrderItem> Items { get; set; }
    }
}
