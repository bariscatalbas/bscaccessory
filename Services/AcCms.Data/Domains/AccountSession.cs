﻿using Core.Data.Models;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace AcCms.Data.Domains
{
     public class AccountSession : EntityBase
    {
        public virtual long AccountId { get; set; }
        [ForeignKey("AccountId")]
        public virtual Account Account { get; set; }

        public virtual DateTime CreationDate { get; set; }
        public virtual DateTime LastActivityDate { get; set; }

        public virtual Guid Token { get; set; }
        public virtual bool IsActive { get; set; }

    }
}
