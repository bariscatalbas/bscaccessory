﻿using Core.Audit;
using Core.Data.Interfaces;
using Core.Data.Models;
using Core.Media;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AcCms.Data.Domains
{
    public class MenuItem : EntityBase, ICachedRecord, IAuditable
    {
        public virtual long MenuId { get; set; }
        [ForeignKey("MenuId")]
        public virtual Menu Menu { get; set; }

        public virtual long? ParentId { get; set; }
        [ForeignKey("ParentId")]
        public virtual MenuItem Parent { get; set; }

        public virtual long? MediaId { get; set; }
        [ForeignKey("MediaId")]
        public virtual AcMedia Media { get; set; }

        public virtual int DisplayOrder { get; set; }

        [MaxLength(500)]
        public virtual string Url { get; set; }
        [MaxLength(250)]
        public virtual string Name { get; set; }



        public string CacheKey() => "AC_CACHE_MENU_" + MenuId;

        public AuditModel SelectLog(object repository)
        {
            return new AuditModel
            {
                EntityId = this.Id,
                EntityName = "MenuItem",
                ParentEntityId = this.MenuId,
                ParentEntityName = "Menu",
                Details = new
                {
                    MenuId,
                    ParentId,
                    DisplayOrder,
                    Url,
                    Name
                }
            };
        }
    }
}
