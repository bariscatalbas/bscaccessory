﻿using Core.Audit;
using Core.Data.Interfaces;
using Core.Data.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AcCms.Data.Domains
{
    public class Account : DeletableEntity, IAuditable
    {
        public virtual long? LanguageId { get; set; }
        [ForeignKey("LanguageId")]
        public virtual Language Language { get; set; }

        public virtual DateTime LastActivityDate { get; set; }
        public virtual bool IsGuest { get; set; }
        public virtual bool IsActive { get; set; }
        public virtual bool IsEmailValidated { get; set; }
        public virtual Guid? EmailValidationToken { get; set; }

        [MaxLength(250)]
        public virtual string FirstName { get; set; }
        [MaxLength(250)]
        public virtual string LastName { get; set; }
        [MaxLength(250)]
        public virtual string Email { get; set; }
        [MaxLength(250)]
        public virtual string Phone { get; set; }
        public virtual DateTime? Birthdate { get; set; }
        public virtual bool? IsMale { get; set; }

        [MaxLength(250)]
        public virtual string PasswordHash { get; set; }
        [MaxLength(250)]
        public virtual string PasswordSalt { get; set; }

        public virtual Guid? PasswordReminderToken { get; set; }
        public virtual DateTime? PasswordReminderTokenExpirationDate { get; set; }

        public virtual string LastError { get; set; }
        public virtual string LastSuccess { get; set; }

        public virtual IList<AccountPermission> Permissions { get; set; }
        public virtual IList<AccountSession> Sessions { get; set; }
        public virtual IList<AccountRole> Roles { get; set; }

        public AuditModel SelectLog(object repository)
        {
            return new AuditModel
            {
                EntityId = this.Id,
                EntityName = "Account",
                ParentEntityId = 0,
                ParentEntityName = "",
                Details = new
                {
                    Id,
                    IsGuest,
                    IsActive,
                    FirstName,
                    LastName,
                    Email,
                    Phone,
                    IsMale
                }
            };
        }
    }
}
