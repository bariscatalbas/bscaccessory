﻿using Core.Audit;
using Core.Data.Interfaces;
using Core.Data.Models;
using System.ComponentModel.DataAnnotations;

namespace AcCms.Data.Domains
{
    public class Language : DeletableEntity, IAuditable, ICachedRecord
    {
        [MaxLength(10)]
        public virtual string Code { get; set; }
        [MaxLength(50)]
        public virtual string Name { get; set; }

        public virtual bool Published { get; set; }
        public virtual bool IsDefault { get; set; }
        public virtual int DisplayOrder { get; set; }

        public string CacheKey()
        {
            return "AC_CACHE_LANGUAGE"; // + this.Id; -- cache all languages
        }

        public AuditModel SelectLog(object repository)
        {
            return new AuditModel
            {
                EntityId = this.Id,
                EntityName = "Language",
                ParentEntityId = 0,
                ParentEntityName = "",
                Details = new
                {
                    Code,
                    Name,
                    IsDefault,
                    Published,
                    DisplayOrder
                }
            };
        }
    }
}
