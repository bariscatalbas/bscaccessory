﻿using AcCms.Models.Enumeration;
using Core.Audit;
using Core.Data.Interfaces;
using Core.Data.Models;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace AcCms.Data.Domains
{
    public class AccountPermission : EntityBase, IAuditable
    {
        public virtual long AccountId { get; set; }
        [ForeignKey("AccountId")]
        public virtual Account Account { get; set; }

        public virtual Enums.CommunicationPermission Permission { get; set; }

        public virtual DateTime Date { get; set; }
        public virtual DateTime? CancelledDate { get; set; }

        public AuditModel SelectLog(object repository)
        {
            return new AuditModel
            {
                EntityId = this.Id,
                EntityName = "AccountPermission",
                ParentEntityId = 0,
                ParentEntityName = "",
                Details = new
                {
                    Id,
                    AccountId,
                    Permission,
                    Date,
                    CancelledDate
                }
            };
        }
    }
}
