﻿using AcCms.Models.Enumeration;
using Core.Audit;
using Core.Data.Interfaces;
using Core.Data.Models;
using Core.Media;
using Modules.Cms.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AcCms.Data.Domains
{
    public class PagePartial : CmsEntityBase, ICachedRecord, IAuditable
    {
        public virtual long LanguageId { get; set; }

        [MaxLength(250)]
        public virtual string Name { get; set; }

        [ForeignKey("CreatedBy")]
        public virtual Account CreatedAccount { get; set; }

        [ForeignKey("LockedById")]
        public virtual Account LockedBy { get; set; }

        public override string CmsEntityName() => "PagePartial";

        public string CacheKey() => "AC_CACHE_PAGEPARTIAL_" + Id;

        public AuditModel SelectLog(object repository)
        {
            return new AuditModel
            {
                EntityId = this.Id,
                EntityName = "PagePartial",
                ParentEntityId = 0,
                ParentEntityName = "",
                Details = new
                {
                    LanguageId,
                    Name,
                    LockedById,
                    Published,
                    PendingApproval,
                    ActiveVersion,
                    DesignedVersion,
                    LastVersion,
                    CreationDate,
                    CreatedBy,
                    ModificationDate,
                    ModifiedBy,
                    Deleted
                }
            };
        }
    }
}
