﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Data.Models;
using Core.Media;

namespace AcCms.Data.Domains
{
    public class ProductImage : EntityBase
    {
        public virtual bool IsActive { get; set; }
        public virtual int DisplayOrder { get; set; }
        public virtual long ProductId { get; set; }
        [ForeignKey("ProductId")]
        public virtual Product Product { get; set; }
        public virtual long? ImageId { get; set; }
        [ForeignKey("ImageId")]
        public virtual AcMedia Image { get; set; }
    }
}
