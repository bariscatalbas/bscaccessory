﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AcCms.Models.Enumeration;
using Core.Audit;
using Core.Data.Interfaces;
using Core.Data.Models;

namespace AcCms.Data.Domains
{
    public class OrderItem : EntityBase
    {
        public virtual long OrderId { get; set; }
        [ForeignKey("OrderId")]
        public virtual Order Order { get; set; }
        public virtual long ProductId { get; set; }
        [ForeignKey("ProductId")]
        public virtual Product Product { get; set; } 
        public virtual decimal UnitPrice { get; set; }
        public virtual int Amount { get; set; }
    }
}
