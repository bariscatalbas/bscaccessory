﻿using Core.Audit;
using Core.Data.Interfaces;
using Core.Media;
using Modules.Cms.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AcCms.Data.Domains
{
    public class Page : CmsEntityBase, IUrlRecord, ICachedRecord, IAuditable
    {
        public virtual long LanguageId { get; set; }


        [ForeignKey("CreatedBy")]
        public virtual Account CreatedAccount { get; set; }

        [ForeignKey("LockedById")]
        public virtual Account LockedBy { get; set; }

        [MaxLength(500)]
        public virtual string Url { get; set; }
        [MaxLength(500)]
        public virtual string MetaTitle { get; set; }
        [MaxLength(500)]
        public virtual string MetaDescription { get; set; }
        public virtual long? MetaImageId { get; set; }
        [ForeignKey("MetaImageId")]
        public virtual AcMedia MetaImage { get; set; }
        [MaxLength(500)]
        public virtual string CanonicalUrl { get; set; }

        public long UrlRecordContainerId() => LanguageId;
        public long UrlRecordItemId() => Id;
        public string UrlRecordEntityName() => "Page";

        public override string CmsEntityName() => "Page";

        public string CacheKey() => "AC_CACHE_PAGE_" + Id;

        public AuditModel SelectLog(object repository)
        {
            return new AuditModel
            {
                EntityId = this.Id,
                EntityName = "Page",
                ParentEntityId = 0,
                ParentEntityName = "",
                Details = new
                {
                    LanguageId,
                    LockedById,
                    Published,
                    PendingApproval,
                    ActiveVersion,
                    DesignedVersion,
                    LastVersion,
                    Url,
                    MetaTitle,
                    MetaDescription,
                    CreationDate,
                    CreatedBy,
                    ModificationDate,
                    ModifiedBy,
                    Deleted
                }
            };
        }
    }
}
