﻿using AcCms.Data.Domains;
using Core.Data.Services;
using Core.Ioc;
using Modules.Cms.Domains;
using System.Data.Entity;

namespace AcCms.Data
{
    public class DbContext : AcContext, IInjectAsDbContext, ICmsDbContext
    {
        // Cms Module
        public DbSet<CmsContentType> CmsContentTypes { get; set; }
        public DbSet<CmsContentTypeGroup> CmsContentTypeGroups { get; set; }
        public DbSet<CmsHistory> CmsHistories { get; set; }
        public DbSet<CmsVersion> CmsVersions { get; set; }
        public DbSet<CmsVersionContent> CmsVersionContents { get; set; }

        // Authentication
        public DbSet<Account> Accounts { get; set; }
        public DbSet<AccountPermission> AccountPermissions { get; set; }
        public DbSet<AccountRole> AccountRoles { get; set; }
        public DbSet<AccountSession> AccountSessions { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<RolePrivilege> RolePrivileges { get; set; }


        // Common
        public DbSet<Language> Languages { get; set; }
        public DbSet<StringResource> StringResources { get; set; }

        // Cms
        public DbSet<Page> Pages { get; set; }
        public DbSet<PagePartial> PagePartials { get; set; }


        public DbSet<Menu> Menus { get; set; }
        public DbSet<MenuItem> MenuItems { get; set; }

        //Custom
        public DbSet<Product> Products { get; set; }
        public DbSet<ProductStock> ProductStocks { get; set; }
        public DbSet<ProductImage> ProductImages { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderItem> OrderItems { get; set; } 

    }
}
