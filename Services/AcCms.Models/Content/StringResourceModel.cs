﻿namespace AcCms.Models.Content
{
    public class StringResourceModel
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
}
