﻿using AcCms.Models.Enumeration;
using Modules.Cms.Models;

namespace AcCms.Models.Content
{
    public class PageModel : CmsEntityModelBase
    {
        public string MetaTitle { get; set; }
        public string MetaDescription { get; set; }
        public string MetaImage { get; set; }
        public string CanonicalUrl { get; set; }
    }
}
