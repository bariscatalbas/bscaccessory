﻿using AcCms.Models.Enumeration;
using Modules.Cms.Models;

namespace AcCms.Models.Content
{
    public class PagePartialModel : CmsEntityModelBase
    {
        public string Name { get; set; }
    }
}
