﻿using System;
using Core.Data.Models;

namespace AcCms.Models.Report
{
    public class ReportDashboardTopModel
    {
        public decimal TodayOrderTotal { get; set; }
        public int TodayOrderCount { get; set; }

        public decimal YesterdayOrderTotal { get; set; }
        public int YesterdayOrderCount { get; set; }

        public decimal ThisMonthOrderTotal { get; set; }
        public int ThisMonthOrderCount { get; set; }

        public decimal OrderTotal { get; set; }
        public decimal ThisYearOrderCount { get; set; }
        public int OrderCount { get; set; }
    }


    public class ReportDashboardItemCount
    {
        public long Id { get; set; }
        public string Image { get; set; }
        public string Manufacturer { get; set; }
        public string Name { get; set; }
        public int Count { get; set; }
    }

    public class ReportDashboardItemDate
    {
        public DateTime Date { get; set; }
        public decimal Total { get; set; }
        public int Count { get; set; }
    }
}
