﻿using Modules.Cms.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace AcCms.Models.Cms
{
    public class HtmlDataModel : CmsDataModelBase
    {
        [DisplayName("Html")]
        [UIHint("TextArea")]
        [AllowHtml]
        public string Html { get; set; }

        public override string CmsBaseName
        {
            get
            {
                return "Html";
            }
        }
        public override string CmsBaseCode
        {
            get
            {
                return "Html";
            }
        }
        public override string CmsBaseAllowedEntities
        {
            get
            {
                return null;
            }
        }
        public override string CmsBaseParentKey
        {
            get
            {
                return "Root";
            }
        }
        public override string CmsBaseChildrenKey
        {
            get
            {
                return null;
            }
        }
        public override string CmsBaseGroupCode
        {
            get
            {
                return "basic";
            }
        }
    }
}
