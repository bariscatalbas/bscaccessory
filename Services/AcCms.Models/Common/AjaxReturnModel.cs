﻿using System;

namespace AcCms.Models.Common
{
    public class AjaxReturnModel
    {
        public bool Success { get; set; }
        public string Message { get; set; }
        public string Data { get; set; }
        public string Html { get; set; }
        public string RedirectUrl { get; set; }
    }
}
