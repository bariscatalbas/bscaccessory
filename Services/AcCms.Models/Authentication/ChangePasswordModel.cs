﻿using Core.Validation;
using System;

namespace AcCms.Models.Authentication
{
    public class ChangePasswordModel
    {
        public Guid PasswordReminderToken { get; set; }

        [AcValidateRequired]
        [AcValidateMinLength(6)]
        public string Password { get; set; }
        [AcValidateMustBeSame("Password")]
        public string PasswordConfirm { get; set; }
    }
}
