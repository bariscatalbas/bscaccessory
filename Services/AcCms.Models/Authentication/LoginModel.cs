﻿using Core.Validation;

namespace AcCms.Models.Authentication
{
    public class LoginModel
    {
        [AcValidateRequired]
        public string Email { get; set; }

        [AcValidateRequired]
        public string Password { get; set; }

        public bool RememberMe { get; set; }
    }
}
