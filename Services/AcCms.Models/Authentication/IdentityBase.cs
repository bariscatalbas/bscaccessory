﻿using AcCms.Models.Core;
using AcCms.Models.Enumeration;
using Core.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AcCms.Models.Authentication
{
    public class IdentityBase : EntityBase
    {
        public IdentityBase()
        {
            Privileges = new List<Enums.Privilege>();
            Roles = new List<long>();
        }

        public Guid Token { get; set; }
        
        public long? LanguageId { get; set; }

        public bool IsAuthenticated { get; set; }
        public bool IsEmailValidated { get; set; }
        public bool IsGuest { get; set; }
        

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }


        public string LastError { get; set; }
        public string LastSuccess { get; set; }

        public List<Enums.Privilege> Privileges { get; set; }
        public List<long> Roles { get; set; }

        public bool AllowAccess(params Enums.Privilege[] keys)
        {
            var preventedPrivileges = _.Configuration.AppSettings("PreventedPrivileges");
            var preventedPrivilegesList = (preventedPrivileges ?? "").Split(',').Select(p => _.To.Int(p, -1)).ToList();
            return Privileges.Any(t => keys.Contains(t)) && !(keys.All(t => preventedPrivilegesList.Contains((int)t)));
        }
    }
}
