﻿using Core.Validation;
using System;

namespace AcCms.Models.Authentication
{
    public class RegisterModel
    {
        [AcValidateRequired]
        public string FirstName { get; set; }
        [AcValidateRequired]
        public string LastName { get; set; }

        [AcValidateRequired]
        public string Phone { get; set; }

        public DateTime? Birthdate { get; set; }

        public bool? IsMale { get; set; }

        [AcValidateRequired]
        public string Email { get; set; }
        
        [AcValidateRequired]
        [AcValidateMinLength(6)]
        public string Password { get; set; }
        [AcValidateMustBeSame("Password")]
        public string PasswordConfirm { get; set; }

        public bool AllowCommunicationEmail { get; set; }
        public bool AllowCommunicationSms { get; set; }
    }
}
