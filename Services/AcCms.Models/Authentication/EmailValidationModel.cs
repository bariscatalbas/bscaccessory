﻿using Core.Validation;
using System;

namespace AcCms.Models.Authentication
{
    public class EmailValidationModel
    {
        public Guid EmailValidationToken { get; set; }
    }
}
