﻿using Core.Validation;

namespace AcCms.Models.Authentication
{
    public class PasswordReminderModel
    {
        [AcValidateRequired]
        public string Email { get; set; }
    }
}
