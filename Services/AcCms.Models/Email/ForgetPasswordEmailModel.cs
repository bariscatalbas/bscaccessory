﻿using Core.EmailTemplates;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AcCms.Models.Email
{
    public class ForgetPasswordEmailModel : AcEmailTemplateBaseModel
    {
        public string Link { get; set; }
    }
}
