﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AcCms.Models.Enumeration
{
    public partial class Enums
    {
        public enum ShipmentType : int
        {
            [Description("PTT Kargo")]
            PTT = 10,
            [Description("Elden Teslimat")]
            ManualDelivery = 20,
            [Description("Yurtici Kargo")]
            YK = 30,
            [Description("MNG Kargo")]
            MNG = 40,
            [Description("Diğer")]
            Other = 50,
        }
    }
}
