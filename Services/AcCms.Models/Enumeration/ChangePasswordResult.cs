﻿namespace AcCms.Models.Enumeration
{
    public partial class Enums
    {
        public enum ChangePasswordResult : int
        {
            WrongToken,
            TokenExpired,
            Success
        }
    }
}
