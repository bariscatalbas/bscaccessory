﻿using System.ComponentModel;

namespace AcCms.Models.Enumeration
{
    public partial class Enums
    {
        public enum Privilege : int
        {
            [Description("Admin Access")]
            AdminAccess = 0,

            [Description("Admin Account")]
            AdminAccount = 10,
            [Description("Admin Role")]
            AdminRole = 11,

            [Description("Admin Email Template")]
            AdminEmailTemplate = 41,
            [Description("Admin Sms Template")]
            AdminSmsTemplate = 42,

            [Description("Admin Queued Email")]
            AdminQueuedEmail = 70,
            [Description("Admin Queued SMS")]
            AdminQueuedSms = 71,
            [Description("Admin Logs")]
            AdminLogs = 72,

            [Description("Admin Settings")]
            AdminSettings = 100,
            [Description("Admin String Resource")]
            AdminStringResrouce = 101,

            [Description("Admin Page")]
            AdminPage = 200,
            [Description("Admin Page Admin")]
            AdminPageAdmin = 201,
            [Description("Admin Page Partial")]
            AdminPagePartial = 202,
            [Description("Admin PagePartial Admin")]
            AdminPagePartialAdmin = 203,
       

            [Description("Admin Super")]
            AdminSuper = 1000
        }
    }
}
