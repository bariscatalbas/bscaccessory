﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AcCms.Models.Enumeration
{
    public partial class Enums
    {
        public enum OrderStatus : int
        {
            [Description("Sipariş Alındı")]
            Pending = 10,
            [Description("Hazırlanıyor")]
            Processing = 20, 
            [Description("Kargoya Verildi")]
            Transport = 30,
            [Description("Tamamlandı")]
            Complete = 40,
            [Description("İptal Edildi")]
            Cancelled = 50,
            [Description("İade Edildi")]
            Returned = 60
        }
    }
}
