﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AcCms.Models.Enumeration
{
    public partial class Enums
    {
        public enum ProductCategory : int
        {
            [Description("Belirsiz")]
            Nothing = 0,
            [Description("Yüzük")]
            Ring = 10,
            [Description("Kolye")]
            Necklace = 20,
            [Description("Küpe")]
            Earring = 30,
            [Description("Bileklik")]
            Bracelet = 40,
            [Description("Toka")]
            Buckle = 50,
        }
    }
}
