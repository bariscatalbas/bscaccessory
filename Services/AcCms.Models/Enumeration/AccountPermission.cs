﻿namespace AcCms.Models.Enumeration
{
    public partial class Enums
    {
        public enum CommunicationPermission : int
        {
            Email,
            Sms
        }
    }
}
