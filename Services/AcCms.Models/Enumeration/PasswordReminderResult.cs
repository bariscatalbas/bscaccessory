﻿namespace AcCms.Models.Enumeration
{
    public partial class Enums
    {
        public enum PasswordReminderResult : int
        {
            WrongEmail,
            Success
        }
    }
}
