﻿namespace AcCms.Models.Enumeration
{
    public partial class Enums
    {
        public enum LoginResult : int
        {
            Failed,
            RequireEmailValidation,
            NotActive,
            Success,
        }
    }
}
