﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AcCms.Models.Enumeration
{
    public partial class Enums
    {
        public enum OrderChannel : int
        {
            [Description("Instagram")]
            Instagram = 10,
            [Description("Whatsapp")]
            Whatsapp = 20,
            [Description("Shopier")]
            Shopier = 30, 
            [Description("Gardrops")]
            Gardrops = 40,

        }
    }
}
