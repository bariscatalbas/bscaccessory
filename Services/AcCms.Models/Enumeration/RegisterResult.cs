﻿namespace AcCms.Models.Enumeration
{
    public partial class Enums
    {
        public enum RegisterResult : int
        {
            InvalidEmail = 0,
            Success = 20,
            EmailValidationRequired = 30
        }
    }
}
