﻿using Core.SmsTemplates;

namespace AcCms.Models.Email
{
    public class WelcomeSmsModel : AcSmsTemplateBaseModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
