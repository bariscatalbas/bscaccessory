﻿using AcCms.Services.Admin.Services;
using AcCms.Services.Common.Authentication;
using Core.Data.Models;
using Core.Ioc;
using Core.Media;
using Core.Mvc.Base;
using Core.Pagination;

namespace AcCms.Services.Admin.Mvc
{
    public class AdminDataControllerBaseWithIdentity<TDomain, TCreateModel, TEditModel, TViewModel, TFilterModel, TExcelExportModel>
     : DataControllerBase<TDomain, TCreateModel, TEditModel, TViewModel, TFilterModel, TExcelExportModel>
         where TDomain : EntityBase, new()
         where TCreateModel : EntityBase, new()
         where TEditModel : EntityBase, new()
         where TViewModel : EntityBase, new()
         where TFilterModel : AcFilter, new()
         where TExcelExportModel : EntityBase, new()
    {
        public readonly WebIdentity Identity;

        public AdminDataControllerBaseWithIdentity(DataServiceBase<TDomain, TCreateModel, TEditModel, TViewModel, TFilterModel, TExcelExportModel> service, WebIdentity identity) : base(service, identity)
        {
            Identity = identity;
        }
    }
}
