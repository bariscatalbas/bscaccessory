﻿using AcCms.Services.Common.Authentication;
using Core.Ioc;
using Core.Mvc.Base;

namespace AcCms.Services.Admin.Mvc
{
    public abstract class AdminMvcViewBaseWithIdentity<T> : MvcViewBase<T>
    {

        private WebIdentity _identity;
        public WebIdentity Identity
        {
            get
            {
                if (_identity == null)
                {
                    _identity = AcEngine.Resolve<WebIdentity>();
                }
                return _identity;
            }
        }
    }
}
