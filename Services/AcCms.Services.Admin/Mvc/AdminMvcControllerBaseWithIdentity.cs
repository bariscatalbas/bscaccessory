﻿using AcCms.Services.Common.Authentication;
using Core.Mvc.Base;

namespace AcCms.Services.Admin.Mvc
{
    public class AdminMvcControllerBaseWithIdentity : MvcControllerBase
    {
        public readonly WebIdentity Identity;

        public AdminMvcControllerBaseWithIdentity(WebIdentity identity)
        {
            Identity = identity;
        }
    }
}
