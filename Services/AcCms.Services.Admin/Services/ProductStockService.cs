﻿using AcCms.Data.Domains;
using AcCms.Services.Admin.Models;
using Core.Data.Interfaces;
using Core.Mvc.Base;
using Core.Pagination;
using Omu.ValueInjecter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace AcCms.Services.Admin.Services
{
    public class ProductStockService : DataServiceBase<ProductStock, ProductStockModelsCreate, ProductStockModelsEdit, ProductStockModelsView, ProductStockModelsFilter, ProductStockModelsExcelExport>
    {
        private readonly IRepository<ProductStock> _productStockRepository;

        public ProductStockService(IRepository<ProductStock> repository) : base(repository)
        {
            _productStockRepository = repository;
        }

        public override AcPager<ProductStockModelsView> Index(ProductStockModelsFilter filter)
        {
            return Repository
                        .Table
                        .Where(p => !p.Deleted && p.ProductId == filter.ProductId)
                        .Select(p => new ProductStockModelsView
                        {
                            Id = p.Id,
                            Amount = p.Amount,
                            ProductId = p.ProductId,
                            BuyingPrice = p.BuyingPrice,
                            CreationDate = p.CreationDate
                        }).ToAcPager(filter);
        }

        public override ProductStockModelsEdit Single(long id)
        {
            return Repository.Table.Where(p => p.Id == id)
                    .Select(p => new ProductStockModelsEdit
                    {
                        Id = p.Id,
                        Amount = p.Amount,
                        ProductId = p.ProductId,
                        BuyingPrice = p.BuyingPrice
                    }).FirstOrDefault();
        }
    }
}
