﻿using AcCms.Data.Domains;
using AcCms.Services.Admin.Models;
using Core.Data.Interfaces;
using Core.Mvc.Base;
using Core.Pagination;
using Omu.ValueInjecter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace AcCms.Services.Admin.Services
{
     public class MenuService : DataServiceBase<Menu, MenuModelsCreate, MenuModelsEdit, MenuModelsView, MenuModelsFilter, MenuModelsExcelExport>
    {
        private readonly IRepository<MenuItem> _menuItemRepository;

        public MenuService(IRepository<Menu> repository, IRepository<MenuItem> menuItemRepository) : base(repository)
        {
            _menuItemRepository = menuItemRepository;
        }

        public override AcPager<MenuModelsView> Index(MenuModelsFilter filter)
        {
            return Repository
                        .Table
                        .Where(p => p.LanguageId == filter.LanguageId)
                        .Select(p => new MenuModelsView
                        {
                            LanguageId = p.LanguageId,
                            Id = p.Id,
                            Name = p.Name,
                        }).ToAcPager(filter);
        }

        public override AcPager<MenuModelsExcelExport> ExportIndex(MenuModelsFilter filter)
        {
            return Repository
                        .Table
                        .Where(p => p.LanguageId == filter.LanguageId)
                        .Select(p => new MenuModelsExcelExport
                        {
                            LanguageId = p.LanguageId,
                            Id = p.Id,
                            Name = p.Name
                        }).ToAcPager(filter);
        }

        public override MenuModelsEdit Single(long id)
        {
            return Repository.Table.Where(p => p.Id == id)
                    .Select(p => new MenuModelsEdit
                    {
                        Id = p.Id,
                        LanguageId = p.LanguageId,
                        Name = p.Name
                    }).FirstOrDefault();
        }

        public List<SelectListItem> SelectList(long languageId)
        {
            var list = Repository
                            .Table
                            .Where(p => p.LanguageId == languageId)
                            .OrderBy(p => p.Name)
                            .Select(p => new SelectListItem
                            {
                                Text = p.Name,
                                Value = p.Id.ToString()
                            }).ToList();

            return list;
        }

        public List<MenuItemViewModel> ItemsIndex(long menuId)
        {
            return _menuItemRepository
                        .Table
                        .Where(p => p.MenuId == menuId)
                        .Select(p => new MenuItemViewModel
                        {
                            Id = p.Id,
                            Name = p.Name,
                            Url = p.Url,
                            MenuId = p.MenuId,
                            DisplayOrder = p.DisplayOrder,
                            ParentId = p.ParentId,
                            ParentName = p.Parent.Name,
                        }).ToList();
        }

        public List<SelectListItem> GetParentList(long menuId, long itemId)
        {
            var list = _menuItemRepository
                        .Table
                        .Where(p => p.MenuId == menuId && p.Id != itemId && p.ParentId == null)
                        .OrderBy(p => p.Name)
                        .Select(p => new SelectListItem
                        {
                            Text = p.Name,
                            Value = p.Id.ToString()
                        }).ToList();

            return list;
        }

        public long ItemInsert(MenuItemCreateModel model, long userId)
        {
            var item = new MenuItem();
            item.InjectFrom(model);
            _menuItemRepository.Insert(item, userId);
            return item.Id;
        }

        public MenuItemEditModel ItemSingle(long id)
        {
            var item = _menuItemRepository.Single(id);
            if (item != null)
            {
                var model = new MenuItemEditModel();
                model.InjectFrom(item);
                return model;
            }
            return null;
        }

        public bool ItemUpdate(MenuItemEditModel model, long userId)
        {
            var item = _menuItemRepository.Single(model.Id);
            if (item != null)
            {
                item.InjectFrom(model);
                _menuItemRepository.Update(item, userId);
                return true;
            }
            return false;
        }

        public bool ItemDelete(long id, long userId)
        {
            var item = _menuItemRepository.Single(id);
            if (item != null)
            {
                _menuItemRepository.Delete(item, userId);
                return true;
            }
            return false;
        }


    }
}
