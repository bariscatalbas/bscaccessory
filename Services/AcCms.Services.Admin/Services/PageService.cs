﻿using AcCms.Data.Domains;
using AcCms.Services.Admin.Models;
using Core.Data.Interfaces;
using Core.Mvc.Base;
using Core.Pagination; 
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace AcCms.Services.Admin.Services
{
    public class PageService : DataServiceBase<Page, PageModelsCreate, PageModelsEdit, PageModelsView, PageModelsFilter, PageModelsExcelExport>
    {
        public PageService(IRepository<Page> repository) : base(repository)
        {
        }

        public override AcPager<PageModelsView> Index(PageModelsFilter filter)
        {
            return Repository
                        .Table
                        .Where(p => !p.Deleted && p.LanguageId == filter.LanguageId)
                        .Select(p => new PageModelsView
                        {
                            LanguageId = p.LanguageId,
                            ActiveVersion = p.ActiveVersion,
                            DesignedVersion = p.DesignedVersion,
                            LastVersion = p.LastVersion,
                            Id = p.Id,
                            LockedById = p.LockedById,
                            LockedByUserName = p.LockedBy.Email,
                            MetaDescription = p.MetaDescription,
                            MetaTitle = p.MetaTitle,
                            PendingApproval = p.PendingApproval,
                            Published = p.Published,
                            Url = p.Url,
                            CanonicalUrl = p.CanonicalUrl
                        }).ToAcPager(filter);
        }

        public override AcPager<PageModelsExcelExport> ExportIndex(PageModelsFilter filter)
        {
            return Repository
                        .Table
                        .Where(p => !p.Deleted && p.LanguageId == filter.LanguageId)
                        .Select(p => new PageModelsExcelExport
                        {
                            LanguageId = p.LanguageId,
                            ActiveVersion = p.ActiveVersion,
                            DesignedVersion = p.DesignedVersion,
                            LastVersion = p.LastVersion,
                            Id = p.Id,
                            LockedById = p.LockedById,
                            LockedByUserName = p.LockedBy.Email,
                            MetaDescription = p.MetaDescription,
                            MetaTitle = p.MetaTitle,
                            PendingApproval = p.PendingApproval,
                            Published = p.Published,
                            Url = p.Url,
                            CanonicalUrl = p.CanonicalUrl
                        }).ToAcPager(filter);
        }

        public override PageModelsEdit Single(long id)
        {
            return Repository.Table.Where(p => !p.Deleted && p.Id == id)
                        .Select(p => new PageModelsEdit
                        {
                            Id = p.Id,
                            LanguageId = p.LanguageId,
                            MetaDescription = p.MetaDescription,
                            MetaTitle = p.MetaTitle,
                            Url = p.Url,
                            CanonicalUrl = p.CanonicalUrl,
                        }).FirstOrDefault();
        }

        public List<SelectListItem> SelectList(long languageId)
        {
            return Repository
                        .Table
                        .Where(p => p.LanguageId == languageId && !p.Deleted)
                        .Select(p => new SelectListItem
                        {
                            Text = p.Url,
                            Value = p.Url
                        }).ToList();
        }

        public PageModelsView GetCmsInfo(long id)
        {
            return Repository.Table.Where(p => p.Id == id)
                .Select(p => new PageModelsView
                {
                    LanguageId = p.LanguageId,
                    ActiveVersion = p.ActiveVersion,
                    DesignedVersion = p.DesignedVersion,
                    LastVersion = p.LastVersion,
                    Id = p.Id,
                    LockedById = p.LockedById,
                    LockedByUserName = p.LockedBy.Email,
                    MetaDescription = p.MetaDescription,
                    MetaTitle = p.MetaTitle,
                    PendingApproval = p.PendingApproval,
                    Published = p.Published,
                    Url = p.Url,
                    CanonicalUrl = p.CanonicalUrl,
                }).FirstOrDefault();
        }
    }
}
