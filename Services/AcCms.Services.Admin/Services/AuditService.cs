﻿using AcCommerce.Data.Domains;
using Core.Audit;
using Core.Data.Interfaces;
using Core.Ioc;
using Core.Pagination;
using Core.Provider;

namespace AcCommerce.Services.Admin.Services
{
    public class AuditService : IInjectAsSelf
    {
        private readonly IAuditProvider _auditProvider;

        public AuditService()
        {
            _auditProvider = AcProviderHelper.GetAuditProvider();
        }

        public AcPager<AcAuditLogAdminModelsList> Index(AcAuditLogAdminModelsFilter filter)
        {
            return _auditProvider.GetAuditLogs(filter);
        }

        public AcAuditLogAdminModelsDetails Details(long id)
        {
            return _auditProvider.GetAuditLogDetails(id);
        }

    }
}
