﻿using AcCms.Data.Domains;
using AcCms.Services.Admin.Models;
using Core.Data.Interfaces;
using Core.Mvc.Base;
using Core.Pagination;
using Omu.ValueInjecter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace AcCms.Services.Admin.Services
{
    public class ProductImageService : DataServiceBase<ProductImage, ProductImageModelsCreate, ProductImageModelsEdit, ProductImageModelsView, ProductImageModelsFilter, ProductImageModelsExcelExport>
    {
        private readonly IRepository<ProductImage> _productImageRepository;

        public ProductImageService(IRepository<ProductImage> repository) : base(repository)
        {
            _productImageRepository = repository;
        }

        public override AcPager<ProductImageModelsView> Index(ProductImageModelsFilter filter)
        {
            return Repository
                        .Table
                        .Where(p => p.ProductId == filter.ProductId)
                        .Select(p => new ProductImageModelsView
                        {
                            Id = p.Id,
                            ProductId = p.ProductId,
                            DisplayOrder = p.DisplayOrder,
                            ImageId = p.ImageId,
                            ImageUrl = p.Image.Url,
                            IsActive = p.IsActive
                        }).ToAcPager(filter);
        }


        public override ProductImageModelsEdit Single(long id)
        {
            return Repository.Table.Where(p => p.Id == id)
                    .Select(p => new ProductImageModelsEdit
                    {
                        Id = p.Id,
                        ProductId = p.ProductId,
                        DisplayOrder = p.DisplayOrder,
                        ImageId = p.ImageId, 
                        IsActive = p.IsActive
                    }).FirstOrDefault();
        }
    }
}
