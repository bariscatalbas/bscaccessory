﻿using AcCms.Data.Domains;
using AcCms.Services.Admin.Models;
using Core.Data.Interfaces;
using Core.Mvc.Base;
using Core.Pagination;
using System.Collections.Generic;
using System.Linq;

namespace AcCms.Services.Admin.Services
{
    public class LanguageService : DataServiceBase<Language, LanguageModelsCreate, LanguageModelsEdit, LanguageModelsView, LanguageModelsFilter, LanguageModelsExcelExport>
    {

        public LanguageService(IRepository<Language> repository) : base(repository)
        {
        }

        public override AcPager<LanguageModelsView> Index(LanguageModelsFilter filter)
        {
            return Repository
                        .Table
                        .Where(p => !p.Deleted)
                        .Select(p => new LanguageModelsView
                        {
                            Code = p.Code,
                            Name = p.Name,
                            DisplayOrder = p.DisplayOrder,
                            Id = p.Id,
                            IsDefault = p.IsDefault,
                            Published = p.Published,
                        })
                        .OrderBy(p => p.DisplayOrder)
                        .ToAcPagerSorted(filter);
        }

        public override AcPager<LanguageModelsExcelExport> ExportIndex(LanguageModelsFilter filter)
        {
            return Repository
                        .Table
                        .Where(p => !p.Deleted)
                        .Select(p => new LanguageModelsExcelExport
                        {
                            Code = p.Code,
                            Name = p.Name,
                            DisplayOrder = p.DisplayOrder,
                            Id = p.Id,
                            IsDefault = p.IsDefault,
                            Published = p.Published
                        })
                        .OrderBy(p => p.DisplayOrder)
                        .ToAcPagerSorted(filter);
        }

        public override LanguageModelsEdit Single(long id)
        {
            return Repository.Table.Where(p => !p.Deleted && p.Id == id)
                    .Select(p => new LanguageModelsEdit
                    {
                        Id = p.Id,
                        Code = p.Code,
                        DisplayOrder = p.DisplayOrder,
                        IsDefault = p.IsDefault,
                        Name = p.Name,
                        Published = p.Published
                    }).FirstOrDefault();
        }

        public List<LanguageModelsView> GetLanguages()
        {
            return Repository.Table.Where(p => !p.Deleted).Select(p => new LanguageModelsView
            {
                Id = p.Id,
                Code = p.Code,
                DisplayOrder = p.DisplayOrder,
                IsDefault = p.IsDefault,
                Name = p.Name,
                Published = p.Published
            }).ToList();
        }

        public override void ModifyInsert(Language item, LanguageModelsCreate model, long identityId)
        {
            if (model.IsDefault)
            {
                var defaults = Repository.Table.Where(p => !p.Deleted && p.IsDefault).ToList();
                foreach (var defaultItem in defaults)
                {
                    defaultItem.IsDefault = false;
                    Repository.Update(defaultItem, identityId);
                }
            }
        }

        public override void ModifyUpdate(Language item, LanguageModelsEdit model, long identityId)
        {
            if (model.IsDefault)
            {
                var defaults = Repository.Table.Where(p => !p.Deleted && p.IsDefault && p.Id != model.Id).ToList();
                foreach (var defaultItem in defaults)
                {
                    defaultItem.IsDefault = false;
                    Repository.Update(defaultItem, identityId);
                }
            }
        }
    }
}
