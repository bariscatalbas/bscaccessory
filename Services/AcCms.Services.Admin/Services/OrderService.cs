﻿using AcCms.Data.Domains;
using AcCms.Services.Admin.Models;
using Core.Data.Interfaces;
using Core.Mvc.Base;
using Core.Pagination;
using Omu.ValueInjecter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using AcCms.Data;
using AcCms.Models.Enumeration;
using AcCms.Services.Common.Authentication;
using Core.Data.Services;
using Core.Ioc;

namespace AcCms.Services.Admin.Services
{
    public class OrderService : IInjectAsSelf
    {
        private readonly IRepository<Product> _productRepository;
        private readonly IRepository<Order> _orderRepository;
        private readonly IRepository<OrderItem> _orderItemRepository;

        public OrderService(IRepository<Product> repository, IRepository<Order> orderRepository, IRepository<OrderItem> orderItemRepository)
        {
            _productRepository = repository;
            _orderRepository = orderRepository;
            _orderItemRepository = orderItemRepository;
        }

        public AcPager<OrderModelsView> Index(OrderModelsFilter filter, bool failedOrders = false)
        {

            var query = _orderRepository.Table;

            if (!string.IsNullOrWhiteSpace(filter.Email))
            {
                query = query.Where(p => p.Email.Contains(filter.Email));
            }

            if (filter.DateStart.HasValue)
            {
                var utcDateStart = _.Date.ConvertToUtcTime(filter.DateStart.Value);
                query = query.Where(p => p.Date >= utcDateStart);
            }
            if (filter.DateEnd.HasValue)
            {
                var utcDateEnd = _.Date.ConvertToUtcTime(filter.DateEnd.Value);
                query = query.Where(p => p.Date <= utcDateEnd);
            }

            var orders = query.Select(p => new OrderModelsView()
            {
                Id = p.Id,
                Email = p.Email,
                Date = p.Date,
                Phone = p.Phone,
                LastName = p.LastName,
                FirstName = p.FirstName,
                OrderStatus = p.OrderStatus,
                OrderNo = p.OrderNo,
                OrderChannel = p.OrderChannel,
                ShipmentType = p.ShipmentType,
                Notes = p.Notes,
                TotalPrice = p.Items.Where(q => q.OrderId == p.Id).Select(q => q.UnitPrice * q.Amount).Sum() + p.ShipmentPrice.Value,
                Address = p.Address,
                District = p.District,
                ShipmentPrice = p.ShipmentPrice,
                City = p.City,
                ShipmentDate = p.ShipmentDate,
                ShipmentNumber = p.ShipmentNumber,
            }).OrderByDescending(p => p.Date).ToAcPager(filter);

            foreach (var order in orders)
            {
                order.Date = _.Date.ConvertUtcToTrTime(order.Date);
            }

            return orders;
        }

        public OrderDetailsModel Details(long id)
        {

            var query = _orderRepository.Table.Where(p => p.Id == id).Select(p => new OrderDetailsModel()
            {
                Id = p.Id,
                Email = p.Email,
                Date = p.Date,
                Phone = p.Phone,
                LastName = p.LastName,
                FirstName = p.FirstName,
                OrderStatus = p.OrderStatus,
                OrderNo = p.OrderNo,
                OrderChannel = p.OrderChannel,
                ShipmentType = p.ShipmentType,
                Notes = p.Notes,
                TotalPrice = p.Items.Where(q => q.OrderId == p.Id).Select(q => q.UnitPrice * q.Amount).Sum() + p.ShipmentPrice.Value,
                Address = p.Address,
                District = p.District,
                ShipmentPrice = p.ShipmentPrice,
                City = p.City,
                ShipmentDate = p.ShipmentDate,
                ShipmentNumber = p.ShipmentNumber,
                Items = p.Items.Select(q => new OrderItemListModel
                {
                    Id = q.Id,
                    Amount = q.Amount,
                    Price = q.UnitPrice,
                    Name = q.Product.Name,
                    ImageUrl = q.Product.Image.Url,
                    Code = q.Product.Code,
                    RemainingStock = q.Amount,
                }).ToList(),
            }).FirstOrDefault();

            query.Date = _.Date.ConvertUtcToTrTime(query.Date);


            return query;
        }

        public bool Insert(OrderModelsCreate model, List<ProductDetailsModel> orders, WebIdentity identity)
        {
            if (orders.Any())
            {
                var order = new Order
                {
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    Email = model.Email,
                    Phone = model.Phone,
                    City = model.City,
                    District = model.District,
                    Address = model.Address,
                    Date = DateTime.UtcNow,
                    OrderChannel = model.OrderChannel,
                    OrderStatus = Enums.OrderStatus.Processing,
                    Notes = model.Notes,
                    ShipmentPrice = model.ShipmentPrice,
                };
                _orderRepository.Insert(order, identity.Id);

                var single = _orderRepository.Single(order.Id);
                single.OrderNo = CreateOrderNumber(order.Id);
                _orderRepository.Update(single, identity.Id);

                foreach (var item in orders)
                {
                    var orderItem = new OrderItem();
                    orderItem.OrderId = order.Id;
                    orderItem.ProductId = item.Id;
                    orderItem.Amount = item.Amount;
                    orderItem.UnitPrice = item.Price;
                    _orderItemRepository.Insert(orderItem, identity.Id);
                }

                return true;
            }
            return false;
        }

        public OrderModelsEdit Single(long id)
        {
            var item = _orderRepository.Single(id);
            if (item != null)
            {
                var model = new OrderModelsEdit();
                model.InjectFrom(item);
                return model;
            }
            return null;
        }

        public bool ShipmentUpdate(OrderDetailsModel model, long userId)
        {
            var item = _orderRepository.Single(model.Id);
            if (item != null)
            {
               
                    item.ShipmentType = model.ShipmentType;
                    item.ShipmentNumber = model.ShipmentNumber;
                    item.ShipmentDate = DateTime.UtcNow;
                    item.OrderStatus = Enums.OrderStatus.Transport;

                    _orderRepository.Update(item, userId);
                    return true;
             
            }
            return false;
        }

        public bool StatusUpdate(OrderDetailsModel model, long userId)
        {
            var item = _orderRepository.Single(model.Id);
            if (item != null)
            {
                item.OrderStatus = model.OrderStatus;
                _orderRepository.Update(item, userId);
                return true;
            }
            return false;
        }

        public string CreateOrderNumber(long id)
        {
            return (DateTime.Now.Year - 2000).ToString("00") + DateTime.Now.Month.ToString("00") + id.ToString("000000");
        }

    }
}
