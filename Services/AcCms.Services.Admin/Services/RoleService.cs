﻿using AcCms.Data.Domains;
using AcCms.Models.Enumeration;
using AcCms.Services.Admin.Models;
using Core.Data.Interfaces;
using Core.Mvc.Base;
using Core.Pagination;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace AcCms.Services.Admin.Services
{
    public class RoleService : DataServiceBase<Role, RoleModelsCreate, RoleModelsEdit, RoleModelsView, RoleModelsFilter, RoleModelsExcelExport>
    {
        private readonly IRepository<RolePrivilege> _rolePrivilegeRepository;

        public RoleService(IRepository<Role> repository, IRepository<RolePrivilege> rolePrivilegeRepository) : base(repository)
        {
            _rolePrivilegeRepository = rolePrivilegeRepository;
        }

        public override AcPager<RoleModelsView> Index(RoleModelsFilter filter)
        {
            var preventedPrivileges = _.Configuration.AppSettings("PreventedPrivileges");
            var preventedPrivilegesList = (preventedPrivileges ?? "").Split(',').Select(p => _.To.Int(p, -1)).ToList();

            var model = Repository
                   .Table
                   .Select(p => new RoleModelsView
                   {
                       Id = p.Id,
                       Name = p.Name,
                       PrivilegeNames = p.Privileges.Where(t => !preventedPrivileges.Contains(t.Privilege.ToString())).OrderBy(t => t.Privilege).Select(t => t.Privilege.ToString()).ToList()
                   }).ToAcPager(filter);

            foreach (var item in model)
            {
                item.PrivilegeNames = item.PrivilegeNames.Select(p => _.Enum.Description((Enums.Privilege)Convert.ToInt32(p))).ToList();
            }

            return model;
        }

        public override AcPager<RoleModelsExcelExport> ExportIndex(RoleModelsFilter filter)
        {
            var preventedPrivileges = _.Configuration.AppSettings("PreventedPrivileges");
            var preventedPrivilegesList = (preventedPrivileges ?? "").Split(',').Select(p => _.To.Int(p, -1)).ToList();

            var model = Repository
                   .Table
                   .Select(p => new RoleModelsExcelExport
                   {
                       Id = p.Id,
                       Name = p.Name,
                       PrivilegeNames = p.Privileges.Where(t => !preventedPrivileges.Contains(t.Privilege.ToString())).OrderBy(t => t.Privilege).Select(t => t.Privilege.ToString()).ToList()
                   }).ToAcPager(filter);

            foreach (var item in model)
            {
                item.PrivilegeNames = item.PrivilegeNames.Select(p => _.Enum.Description((Enums.Privilege)Convert.ToInt32(p))).ToList();
            }

            return model;
        }

        public override RoleModelsCreate CreateModel()
        {
            return new RoleModelsCreate
            {
                SelectedPrivileges = _.Enum.SelectList<Enums.Privilege>().Where(p => Convert.ToInt32(p.Value) != (int)Enums.Privilege.AdminSuper).Select(p => new RolePrivilegeModel { Privilege = (Enums.Privilege)(Convert.ToInt32(p.Value)) }).ToList()
            };
        }

        public override RoleModelsEdit Single(long id)
        {
            var preventedPrivileges = _.Configuration.AppSettings("PreventedPrivileges");
            var preventedPrivilegesList = (preventedPrivileges ?? "").Split(',').Select(p => _.To.Int(p, -1)).ToList();

            var model = base.Single(id);
            if (model != null)
            {
                var currentPrivileges = _rolePrivilegeRepository.Table.Where(t => !preventedPrivileges.Contains(t.Privilege.ToString())).Where(p => p.RoleId == id).Select(p => p.Privilege).ToList();

                model.SelectedPrivileges = _.Enum.SelectList<Enums.Privilege>().Where(p => Convert.ToInt32(p.Value) != (int)Enums.Privilege.AdminSuper && !preventedPrivileges.Contains(p.Value.ToString())).Select(p => new RolePrivilegeModel { Privilege = (Enums.Privilege)(Convert.ToInt32(p.Value)), Selected = currentPrivileges.Contains(Convert.ToInt32(p.Value)) }).ToList();
            }
            return model;
        }

        public override long Insert(RoleModelsCreate model, long userId)
        {
            var roleId = base.Insert(model, userId);
            if (roleId > 0 && model.SelectedPrivileges != null)
            {
                foreach (var privilege in model.SelectedPrivileges.Where(p => p.Selected))
                {
                    _rolePrivilegeRepository.Insert(new RolePrivilege
                    {
                        Privilege = (int)privilege.Privilege,
                        RoleId = roleId
                    }, userId);
                }
            }
            return roleId;
        }

        public override bool Update(RoleModelsEdit model, long userId)
        {
            if (base.Update(model, userId))
            {
                var currentPrivileges = _rolePrivilegeRepository.Table.Where(p => p.RoleId == model.Id).ToList();
                var selectedPrivileges = model.SelectedPrivileges.Where(p => p.Selected).Select(p => (int)p.Privilege).ToList();

                // Delete  Privileges
                var privilegesToDelete = currentPrivileges.Where(p => !selectedPrivileges.Contains((int)p.Privilege)).ToList();
                foreach (var privilege in privilegesToDelete)
                {
                    _rolePrivilegeRepository.Delete(privilege, userId);
                }

                // New Privileges
                var privilegesToAdd = selectedPrivileges.Where(p => !currentPrivileges.Any(t => (int)t.Privilege == p)).ToList();
                foreach (var privilege in privilegesToAdd)
                {
                    _rolePrivilegeRepository.Insert(new RolePrivilege
                    {
                        Privilege = privilege,
                        RoleId = model.Id
                    }, userId);
                }

                return true;
            }
            return false;
        }

        public override bool Delete(long id, long userId)
        {
            if (base.Delete(id, userId))
            {
                var currentPrivileges = _rolePrivilegeRepository.Table.Where(p => p.RoleId == id).ToList();
                foreach (var privilege in currentPrivileges)
                {
                    _rolePrivilegeRepository.Delete(privilege, userId);
                }
                return true;
            }
            return false;
        }

        public List<SelectListItem> SelectList()
        {
            var list = Repository
                .Table
                .Select(p => new
                {
                    p.Id,
                    p.Name
                }).AsEnumerable()
                .Select(p => new SelectListItem
                {
                    Value = p.Id.ToString(),
                    Text = p.Name
                }).ToList();

            return list;
        }
    }
}
