﻿using AcCms.Data.Domains;
using AcCms.Services.Admin.Models;
using Core.Data.Interfaces;
using Core.Mvc.Base;
using Core.Pagination;
using Omu.ValueInjecter;
using System;
using System.Linq;

namespace AcCms.Services.Admin.Services
{
    public class AccountService : DataServiceBase<Account, AccountModelsCreate, AccountModelsEdit, AccountModelsView, AccountModelsFilter, AccountModelsExcelExport>
    {
        private readonly IRepository<Account> _accountRepository;
        private readonly IRepository<AccountRole> _accountRoleRepository;

        public AccountService(IRepository<Account> repository, IRepository<AccountRole> accountRoleRepository) : base(repository)
        {
            _accountRepository = repository;
            _accountRoleRepository = accountRoleRepository;
        }

        public override AcPager<AccountModelsView> Index(AccountModelsFilter filter)
        {
            var query = _accountRepository.Table.Where(p => !p.Deleted && !p.IsGuest);

            if (filter.RoleId.HasValue)
            {
                query = query.Where(p => p.Roles.Any(t => t.RoleId == filter.RoleId));
            }
            if (!string.IsNullOrWhiteSpace(filter.FirstName))
            {
                query = query.Where(p => p.FirstName.Contains(filter.FirstName));
            }
            if (!string.IsNullOrWhiteSpace(filter.LastName))
            {
                query = query.Where(p => p.LastName.Contains(filter.LastName));
            }
            if (!string.IsNullOrWhiteSpace(filter.Email))
            {
                query = query.Where(p => p.Email.Contains(filter.Email));
            }
            if (!string.IsNullOrWhiteSpace(filter.Phone))
            {
                query = query.Where(p => p.Phone.Contains(filter.Phone));
            }

            return query
                        .Select(p => new AccountModelsView
                        {
                            Id = p.Id,
                            Phone = p.Phone,
                            Email = p.Email,
                            FirstName = p.FirstName,
                            LastName = p.LastName,
                            RoleNames = p.Roles.Select(t => t.Role.Name).ToList(),
                        })
                        .OrderByDescending(p => p.Id)
                        .ToAcPagerSorted(filter);
        }

        public override AcPager<AccountModelsExcelExport> ExportIndex(AccountModelsFilter filter)
        {
            var query = _accountRepository.Table.Where(p => !p.Deleted && !p.IsGuest);

            if (filter.RoleId.HasValue)
            {
                query = query.Where(p => p.Roles.Any(t => t.RoleId == filter.RoleId));
            }
            if (!string.IsNullOrWhiteSpace(filter.FirstName))
            {
                query = query.Where(p => p.FirstName.Contains(filter.FirstName));
            }
            if (!string.IsNullOrWhiteSpace(filter.LastName))
            {
                query = query.Where(p => p.LastName.Contains(filter.LastName));
            }
            if (!string.IsNullOrWhiteSpace(filter.Email))
            {
                query = query.Where(p => p.Email.Contains(filter.Email));
            }
            if (!string.IsNullOrWhiteSpace(filter.Phone))
            {
                query = query.Where(p => p.Phone.Contains(filter.Phone));
            }

            return query
                        .Select(p => new AccountModelsExcelExport
                        {
                            Id = p.Id,
                            Phone = p.Phone,
                            Email = p.Email,
                            FirstName = p.FirstName,
                            LastName = p.LastName,
                            IsActive = p.IsActive,
                            IsEmailValidated = p.IsEmailValidated,
                            RoleNames = p.Roles.Select(t => t.Role.Name).ToList()
                        })
                        .OrderByDescending(p => p.Id)
                        .ToAcPagerSorted(filter);
        }

        public override AccountModelsEdit Single(long id)
        {
            return Repository
                        .Table
                        .Where(p => !p.Deleted && p.Id == id)
                        .Select(p => new AccountModelsEdit
                        {
                            Id = p.Id,
                            Email = p.Email,
                            FirstName = p.FirstName,
                            LastName = p.LastName,
                            Phone = p.Phone,
                            IsActive = p.IsActive,
                            IsEmailValidated = p.IsEmailValidated,
                            RoleIds = p.Roles.Select(t => t.RoleId).ToList()
                        }).FirstOrDefault();
        }

        public override long Insert(AccountModelsCreate model, long userId)
        {
            var obj = new Account();
            obj.InjectFrom(model);
            obj.PasswordSalt = _.Crypto.Password.CreateSaltKey(25);
            obj.LastActivityDate = DateTime.UtcNow;
            Repository.Insert(obj, userId);
            var id = obj.Id;

            if (id > 0)
            {
                foreach (var item in model.RoleIds)
                {
                    _accountRoleRepository.Insert(new AccountRole
                    {
                        AccountId = id,
                        RoleId = item
                    }, userId);
                }
            }
            return id;
        }

        public override bool Update(AccountModelsEdit model, long userId)
        {
            if (base.Update(model, userId))
            {
                var currentRoles = _accountRoleRepository.Table.Where(p => p.AccountId == model.Id).ToList();
                var selectedRoles = model.RoleIds;

                var rolesToDelete = currentRoles.Where(p => !selectedRoles.Contains(p.RoleId)).ToList();
                foreach (var itemToDelete in rolesToDelete)
                {
                    _accountRoleRepository.Delete(itemToDelete, userId);
                }

                foreach (var itemToAdd in selectedRoles.Where(p => !currentRoles.Any(t => t.RoleId == p)))
                {
                    _accountRoleRepository.Insert(new AccountRole
                    {
                        AccountId = model.Id,
                        RoleId = itemToAdd,
                    }, userId);
                }

                return true;
            }
            return false;
        }

        public override bool Delete(long id, long userId)
        {
            if (base.Delete(id, userId))
            {
                var currentRoles = _accountRoleRepository.Table.Where(p => p.AccountId == id).ToList();
                foreach (var itemToDelete in currentRoles)
                {
                    _accountRoleRepository.Delete(itemToDelete, userId);
                }
                return true;
            }
            return false;
        }

        public bool ValidateEmail(string email, long accountId = 0)
        {
            var existingAccountWithSameEmail = _accountRepository.Table.FirstOrDefault(p => p.Email == email && p.Id != accountId && !p.Deleted && !p.IsGuest);
            if (existingAccountWithSameEmail != null)
            {
                return false;
            }

            return true;
        }

        public string ChangePassword(AccountModelsChangePassword model, long accountId)
        {
            var account = Repository.Table.FirstOrDefault(p => p.Id == model.Id);
            if (account == null)
            {
                return "Account not found.";
            }
            if (account.Deleted)
            {
                return "Account not found.";
            }

            account.PasswordSalt = _.Crypto.Password.CreateSaltKey(25);
            account.PasswordHash = _.Crypto.Password.CreatePasswordHash(model.Password, account.PasswordSalt);
            Repository.Update(account, accountId);
            return "";

        }
    }
}
