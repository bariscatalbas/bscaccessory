﻿using AcCms.Data.Domains;
using AcCms.Services.Admin.Models;
using Core.Data.Interfaces;
using Core.Mvc.Base;
using Core.Pagination;
using Omu.ValueInjecter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace AcCms.Services.Admin.Services
{
    public class PagePartialService : DataServiceBase<PagePartial, PagePartialModelsCreate, PagePartialModelsEdit, PagePartialModelsView, PagePartialModelsFilter, PagePartialModelsExcelExport>
    {
        public PagePartialService(IRepository<PagePartial> repository) : base(repository)
        {
        }

        public override AcPager<PagePartialModelsView> Index(PagePartialModelsFilter filter)
        {
            return Repository
                        .Table
                        .Where(p => !p.Deleted && p.LanguageId == filter.LanguageId)
                        .Select(p => new PagePartialModelsView
                        {
                            LanguageId = p.LanguageId,
                            ActiveVersion = p.ActiveVersion,
                            DesignedVersion = p.DesignedVersion,
                            LastVersion = p.LastVersion,
                            Id = p.Id,
                            LockedById = p.LockedById,
                            LockedByUserName = p.LockedBy.Email,
                            Name = p.Name,
                            PendingApproval = p.PendingApproval,
                            Published = p.Published
                        }).ToAcPager(filter);
        }

        public override AcPager<PagePartialModelsExcelExport> ExportIndex(PagePartialModelsFilter filter)
        {
            return Repository
                        .Table
                        .Where(p => !p.Deleted && p.LanguageId == filter.LanguageId)
                        .Select(p => new PagePartialModelsExcelExport
                        {
                            LanguageId = p.LanguageId,
                            ActiveVersion = p.ActiveVersion,
                            DesignedVersion = p.DesignedVersion,
                            LastVersion = p.LastVersion,
                            Id = p.Id,
                            LockedById = p.LockedById,
                            LockedByUserName = p.LockedBy.Email,
                            Name = p.Name,
                            PendingApproval = p.PendingApproval,
                            Published = p.Published
                        }).ToAcPager(filter);
        }

        public override PagePartialModelsEdit Single(long id)
        {
            return Repository.Table.Where(p => !p.Deleted && p.Id == id)
                        .Select(p => new PagePartialModelsEdit
                        {
                            Id = p.Id,
                            LanguageId = p.LanguageId,
                            Name = p.Name
                        }).FirstOrDefault();
        }

        public PagePartialModelsView GetCmsInfo(long id)
        {
            return Repository.Table.Where(p => p.Id == id)
                .Select(p => new PagePartialModelsView
                {
                    LanguageId = p.LanguageId,
                    ActiveVersion = p.ActiveVersion,
                    DesignedVersion = p.DesignedVersion,
                    LastVersion = p.LastVersion,
                    Id = p.Id,
                    LockedById = p.LockedById,
                    LockedByUserName = p.LockedBy.Email,
                    Name = p.Name,
                    PendingApproval = p.PendingApproval,
                    Published = p.Published
                }).FirstOrDefault();
        }
    }
}
