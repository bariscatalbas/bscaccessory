﻿using AcCms.Data.Domains;
using Core.Data.Interfaces;
using Core.Ioc;
using Modules.Cms.Interfaces;
using Modules.Cms.Models;
using Omu.ValueInjecter;
using System.Linq;

namespace AcCms.Services.Admin.Services
{
    public class CmsPackageService : ICmsEntityAdminService, IInjectAsSelf
    {
        private readonly IRepository<Package> _packageRepository;

        public CmsPackageService(IRepository<Package> packageRepository)
        {
            _packageRepository = packageRepository;
        }

        public CmsEntityUpdateModel GetEntity(long entityId)
        {
            return _packageRepository.Table.Where(p => p.Id == entityId && !p.Deleted).Select(p => new CmsEntityUpdateModel
            {
                Id = p.Id,
                ActiveVersion = p.ActiveVersion,
                DesignedVersion = p.DesignedVersion,
                LastVersion = p.LastVersion,
                LockedById = p.LockedById,
                PendingApproval = p.PendingApproval,
                Published = p.Published
            }).FirstOrDefault();
        }

        public string GetEntityName()
        {
            return "Package";
        }

        public void UpdateEntity(CmsEntityUpdateModel entity, long identityId)
        {
            var item = _packageRepository.Table.FirstOrDefault(p => p.Id == entity.Id && !p.Deleted);
            if (item != null)
            {
                item.InjectFrom(entity);
                _packageRepository.Update(item, identityId);
            }
        }
    }
}
