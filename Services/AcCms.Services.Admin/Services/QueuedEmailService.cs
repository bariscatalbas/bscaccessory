﻿using AcCommerce.Data.Domains;
using AcCommerce.Services.Admin.Models;
using Core.Data.Interfaces;
using Core.Email;
using Core.Ioc;
using Core.Mvc.Base;
using Core.Pagination;
using System.Linq;

namespace AcCommerce.Services.Admin.Services
{
    public class QueuedEmailService : IInjectAsSelf
    {
        private readonly AcEmailHelper _emailHelper;

        public QueuedEmailService(AcEmailHelper emailHelper)
        {
            _emailHelper = emailHelper;
        }

        public AcPager<AcQueuedEmailAdminModelsList> Index(AcQueuedEmailAdminModelsFilter filter)
        {
            return _emailHelper.GetQueuedEmails(filter);
        }

        public AcQueuedEmailAdminModelsDetails Details(long queuedEmailId)
        {
            return _emailHelper.GetQueuedEmailDetails(queuedEmailId);
        }

        public bool SendAgain(long queuedEmailId, long identityId)
        {
            return _emailHelper.SendQueuedEmailAgain(queuedEmailId, identityId);
        }
    }
}
