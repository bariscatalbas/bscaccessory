﻿using AcCms.Data.Domains;
using AcCms.Services.Admin.Models;
using Core.Data.Interfaces;
using Core.Mvc.Base;
using Core.Pagination;
using Omu.ValueInjecter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using AcCms.Data;
using AcCms.Models.Enumeration;
using AcCms.Models.Report;
using AcCms.Services.Common.Authentication;
using Core.Data.Services;
using Core.Ioc;

namespace AcCms.Services.Admin.Services
{
    public class ReportService : IInjectAsSelf
    {
        private readonly IRepository<Order> _orderRepository;
        private readonly IRepository<OrderItem> _orderItemRepository;
        private readonly IRepository<Product> _productRepository; 
        private readonly IRepository<Account> _accountRepository;

        public ReportService(IRepository<Order> orderRepository, IRepository<OrderItem> orderItemRepository, IRepository<Product> productRepository,
             IRepository<Account> accountRepository)
        {
            _orderRepository = orderRepository;
            _orderItemRepository = orderItemRepository;
            _productRepository = productRepository; 
            _accountRepository = accountRepository;
        }

        public ReportDashboardTopModel DashboardTopReport(long languageId)
        {
            var today = _.Date.ConvertToUtcTime(DateTime.Now.Date);
            var yesterday = today.AddDays(-1);

            var thisMonthStart = _.Date.ConvertToUtcTime(new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1));
            var thisYearStart = _.Date.ConvertToUtcTime(new DateTime(DateTime.Now.Year, 1, 1));

            return new ReportDashboardTopModel
            {
                OrderCount = _orderRepository
                                .Table
                                .Where(p => !p.Deleted && p.OrderStatus != Enums.OrderStatus.Cancelled)
                                .Count(),
                //OrderTotal = _orderRepository
                //                .Table
                //                .Where(p => !p.Deleted && p.Valid && p.OrderStatus != OrderStatusEnum.Canceled)
                //                .Sum(p => (decimal?)(p.CurrencyRate > 0 ? p.Total / p.CurrencyRate : 0)) ?? 0,

                ThisYearOrderCount = _orderRepository
                                         .Table
                                         .Where(p => !p.Deleted && p.OrderStatus != Enums.OrderStatus.Cancelled 
                                                     && p.Date >= thisYearStart)
                                         .Count(),
                //TodayOrderTotal = _orderRepository
                //                .Table
                //                .Where(p => !p.Deleted && p.Valid && p.OrderStatus != OrderStatusEnum.Canceled
                //                && p.CreationDate >= today)
                //                .Sum(p => (decimal?)(p.CurrencyRate > 0 ? p.Total / p.CurrencyRate : 0)) ?? 0,
                TodayOrderCount = _orderRepository
                                .Table
                                .Where(p => !p.Deleted && p.OrderStatus != Enums.OrderStatus.Cancelled 
                                && p.Date >= today)
                                .Count(),
                //YesterdayOrderTotal = _orderRepository
                //                .Table
                //                .Where(p => !p.Deleted && p.Valid && p.OrderStatus != OrderStatusEnum.Canceled
                //                && p.CreationDate >= yesterday && p.CreationDate < today)
                //                .Sum(p => (decimal?)(p.CurrencyRate > 0 ? p.Total / p.CurrencyRate : 0)) ?? 0,
                YesterdayOrderCount = _orderRepository
                                .Table
                                .Where(p => !p.Deleted && p.OrderStatus != Enums.OrderStatus.Cancelled 
                                && p.Date >= yesterday && p.Date < today)
                                .Count(),
                //ThisMonthOrderTotal = _orderRepository
                //                .Table
                //                .Where(p => !p.Deleted && p.Valid && p.OrderStatus != OrderStatusEnum.Canceled
                //                && p.CreationDate >= thisMonthStart)
                //                .Sum(p => (decimal?)(p.CurrencyRate > 0 ? p.Total / p.CurrencyRate : 0)) ?? 0,
                ThisMonthOrderCount = _orderRepository
                                .Table
                                .Where(p => !p.Deleted &&  p.OrderStatus != Enums.OrderStatus.Cancelled 
                                && p.Date >= thisMonthStart)
                                .Count(),
            };
        }

        public List<ReportDashboardItemCount> ProductSalesCount(long languageId)
        {
            return _orderItemRepository
                        .Table
                        .Where(p => !p.Order.Deleted  && p.Order.OrderStatus != Enums.OrderStatus.Cancelled)
                        .Select(p => new
                        {
                            Id = p.ProductId,
                            Name = p.Product.Name,
                            p.Amount
                        })
                        .GroupBy(p => new { p.Id, p.Name })
                        .Select(p => new ReportDashboardItemCount
                        {
                            Id = p.Key.Id,
                            Name = p.Key.Name,
                            Count = p.Sum(t => t.Amount)
                        })
                        .OrderByDescending(p => p.Count)
                        .ToList();
        }

        public List<ReportDashboardItemCount> Last30DaysProductSalesCount(long languageId)
        {
            var last30DaysStart = _.Date.ConvertToUtcTime(DateTime.Now.Date.AddDays(-30));
            return _orderItemRepository
                        .Table
                        .Where(p => !p.Order.Deleted && p.Order.OrderStatus != Enums.OrderStatus.Cancelled 
                        && p.Order.Date >= last30DaysStart)
                        .Select(p => new
                        {
                            Id = p.ProductId,
                            Name = p.Product.Name,
                            p.Amount
                        })
                        .GroupBy(p => new { p.Id, p.Name })
                        .Select(p => new ReportDashboardItemCount
                        {
                            Id = p.Key.Id,
                            Name = p.Key.Name,
                            Count = p.Sum(t => t.Amount)
                        })
                        .OrderByDescending(p => p.Count)
                        .ToList();
        }
      
        public List<ReportDashboardItemCount> YearlyProductSalesCount(long languageId)
        {
            var thisYear = _.Date.ConvertToUtcTime(new DateTime(DateTime.Now.Year, 1, 1));
            return _orderItemRepository
                        .Table
                        .Where(p => !p.Order.Deleted && p.Order.OrderStatus != Enums.OrderStatus.Cancelled && p.Order.Date >= thisYear)
                        .Select(p => new
                        {
                            Id = p.ProductId,
                            Name = p.Product.Name,
                            p.Amount
                        })
                        .GroupBy(p => new { p.Id, p.Name })
                        .Select(p => new ReportDashboardItemCount
                        {
                            Id = p.Key.Id,
                            Name = p.Key.Name,
                            Count = p.Sum(t => t.Amount)
                        })
                        .OrderByDescending(p => p.Count)
                        .ToList();
        }

        public List<ReportDashboardItemCount> MonthlyProductSalesCount(long languageId)
        {
            var thisMonth = _.Date.ConvertToUtcTime(new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1));
            return _orderItemRepository
                        .Table
                        .Where(p => !p.Order.Deleted && p.Order.OrderStatus != Enums.OrderStatus.Cancelled  && p.Order.Date >= thisMonth)
                        .Select(p => new
                        {
                            Id = p.ProductId,
                            Name = p.Product.Name,
                            p.Amount
                        })
                        .GroupBy(p => new { p.Id, p.Name })
                        .Select(p => new ReportDashboardItemCount
                        {
                            Id = p.Key.Id,
                            Name = p.Key.Name,
                            Count = p.Sum(t => t.Amount)
                        })
                        .OrderByDescending(p => p.Count)
                        .ToList();
        }

        public List<ReportDashboardItemCount> DailyProductSalesCount(long languageId)
        {
            var today = _.Date.ConvertToUtcTime(new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day));
            return _orderItemRepository
                        .Table
                        .Where(p => !p.Order.Deleted && p.Order.OrderStatus != Enums.OrderStatus.Cancelled && p.Order.Date >= today)
                        .Select(p => new
                        {
                            Id = p.ProductId,
                            Name = p.Product.Name,
                            p.Amount
                        })
                        .GroupBy(p => new { p.Id, p.Name })
                        .Select(p => new ReportDashboardItemCount
                        {
                            Id = p.Key.Id,
                            Name = p.Key.Name,
                            Count = p.Sum(t => t.Amount)
                        })
                        .OrderByDescending(p => p.Count)
                        .ToList();
        }

        public List<ReportDashboardItemDate> Last30DaysSales()
        {
            var date = _.Date.ConvertToUtcTime(DateTime.Now.Date);

            var list = new List<ReportDashboardItemDate>();
            for (int i = 0; i < 30; i++)
            {
                var nextDate = date.AddDays(1);
                list.Add(new ReportDashboardItemDate
                {
                    Date = _.Date.ConvertToUserTime(date, DateTimeKind.Utc, TimeZoneInfo.Local),
                    Count = _orderRepository.Table.Count(p => !p.Deleted && p.OrderStatus != Enums.OrderStatus.Cancelled && p.OrderStatus != Enums.OrderStatus.Returned
            
                    && p.Date >= date && p.Date < nextDate),
                    Total = _orderRepository.Table
                                    .Where(p => !p.Deleted  && p.OrderStatus != Enums.OrderStatus.Cancelled && p.OrderStatus != Enums.OrderStatus.Returned
                                    && p.Date >= date && p.Date < nextDate)
                                    .Sum(p => (decimal?)p.Items.Select(q=> q.Amount * q.UnitPrice).Sum()) ?? 0
                });
                date = date.AddDays(-1);
            }
            return list.OrderBy(p => p.Date).ToList();
        }

        public List<ReportDashboardItemDate> Last12MonthsSales()
        {
            var thisMonth = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            var date = _.Date.ConvertToUtcTime(thisMonth);

            var list = new List<ReportDashboardItemDate>();
            for (int i = 0; i < 12; i++)
            {
                var localDate = _.Date.ConvertToUserTime(date, DateTimeKind.Utc, TimeZoneInfo.Local);
                var nextDate = date.AddDays(DateTime.DaysInMonth(localDate.Year, localDate.Month));
                list.Add(new ReportDashboardItemDate
                {
                    Date = localDate,
                    Count = _orderRepository.Table.Count(p => !p.Deleted && p.OrderStatus != Enums.OrderStatus.Cancelled && p.OrderStatus != Enums.OrderStatus.Returned
                    && p.Date >= date && p.Date < nextDate),
                    Total = _orderRepository.Table
                                    .Where(p => !p.Deleted && p.OrderStatus != Enums.OrderStatus.Cancelled
                                    && p.OrderStatus != Enums.OrderStatus.Returned 
                                    && p.Date >= date && p.Date < nextDate)
                                    .Sum(p => (decimal?)p.Items.Select(q => q.Amount * q.UnitPrice).Sum()) ?? 0
                });
                date = date.AddDays(DateTime.DaysInMonth(date.Year, date.Month) * -1);
            }
            return list.OrderBy(p => p.Date).ToList();
        }
    }
}
