﻿using Core.EventTrigger;
using Core.Ioc;
using Core.Task;

namespace AcCms.Services.Admin.Services
{
    public static class EntityEventService
    {
        public static void HandleEvents(object source, AcEventTriggerArguments arguments)
        {
            AcTaskHelper taskHelper = AcEngine.Resolve<AcTaskHelper>();

            if (arguments.EventName == "Repository.PostInsert")
            {
            }
            else if (arguments.EventName == "Repository.PostUpdate")
            {
                
            }
            else if (arguments.EventName == "Repository.PostDelete")
            {
            }
        }
    }
}
