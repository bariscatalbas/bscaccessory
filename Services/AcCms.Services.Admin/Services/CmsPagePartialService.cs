﻿using AcCms.Data.Domains;
using Core.Data.Interfaces;
using Core.Ioc;
using Modules.Cms.Interfaces;
using Modules.Cms.Models;
using Omu.ValueInjecter;
using System.Linq;

namespace AcCms.Services.Admin.Services
{
    public class CmsPagePartialService : ICmsEntityAdminService, IInjectAsSelf
    {
        private readonly IRepository<PagePartial> _pagePartialRepository;

        public CmsPagePartialService(IRepository<PagePartial> pagePartialRepository)
        {
            _pagePartialRepository = pagePartialRepository;
        }

        public CmsEntityUpdateModel GetEntity(long entityId)
        {
            return _pagePartialRepository.Table.Where(p => p.Id == entityId && !p.Deleted).Select(p => new CmsEntityUpdateModel
            {
                Id = p.Id,
                ActiveVersion = p.ActiveVersion,
                DesignedVersion = p.DesignedVersion,
                LastVersion = p.LastVersion,
                LockedById = p.LockedById,
                PendingApproval = p.PendingApproval,
                Published = p.Published
            }).FirstOrDefault();
        }

        public string GetEntityName()
        {
            return "PagePartial";
        }

        public void UpdateEntity(CmsEntityUpdateModel entity, long identityId)
        {
            var pagePartial = _pagePartialRepository.Table.FirstOrDefault(p => p.Id == entity.Id && !p.Deleted);
            if (pagePartial != null)
            {
                pagePartial.InjectFrom(entity);
                _pagePartialRepository.Update(pagePartial, identityId);
            }
        }
    }
}
