﻿using AcCms.Data.Domains;
using AcCms.Services.Admin.Models;
using Core.Data.Interfaces;
using Core.Mvc.Base;
using Core.Pagination;
using Omu.ValueInjecter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using AcCms.Data;
using AcCms.Services.Common.Authentication;
using Core.Data.Services;

namespace AcCms.Services.Admin.Services
{
    public class ProductService : DataServiceBase<Product, ProductModelsCreate, ProductModelsEdit, ProductModelsView, ProductModelsFilter, ProductModelsExcelExport>
    {
        private readonly IRepository<Product> _productRepository;

        public ProductService(IRepository<Product> repository) : base(repository)
        {
            _productRepository = repository;
        }

        public override AcPager<ProductModelsView> Index(ProductModelsFilter filter)
        {
            var context = (DbContext)Repository.Context;

            var query = Repository
                .Table
                .Where(p => !p.Deleted);

            if (filter.IsActiveOnly)
            {
                query = query.Where(p => p.IsActive == true);
            }

            if (!string.IsNullOrWhiteSpace(filter.Name))
            {
                query = query.Where(p => p.Name.Contains(filter.Name));
            }

            if (filter.CategoryType.HasValue && filter.CategoryType > 0)
            {
                query = query.Where(p => p.CategoryType == filter.CategoryType);
            }

            if (!string.IsNullOrWhiteSpace(filter.CompanyName))
            {
                query = query.Where(p => p.CompanyName.Contains(filter.CompanyName));
            }

            return query
                        .Select(p => new ProductModelsView
                        {
                            Id = p.Id,
                            Name = p.Name,
                            IsActive = p.IsActive,
                            Code = p.Code,
                            CreationDate = p.CreationDate,
                            Price = p.Price,
                            CategoryType = p.CategoryType,
                            CompanyName = p.CompanyName,
                            ImageId = p.ImageId,
                            ImageUrl = p.Image.Url,
                            OrderCount = context
                                .OrderItems
                                .Where(q => q.ProductId == p.Id)
                                .Select(q => q.Amount)
                                .DefaultIfEmpty(0).Sum(),
                            ProductImages = p.ProductImages
                                .Where(q => q.IsActive)
                                .Select(q => new ProductImageModelsList
                                {
                                    Id = q.Id,
                                    ImageUrl = q.Image.Url
                                }).ToList(),
                            ProductStocks = p.ProductStocks
                                .Where(q => !q.Deleted)
                                .Select(q => new ProductStockModelsList
                                {
                                    Id = q.Id,
                                    Amount = q.Amount,
                                    BuyingPrice = q.BuyingPrice
                                }).ToList(),
                        }).ToAcPager(filter);
        }


        public override ProductModelsEdit Single(long id)
        {
            return Repository.Table.Where(p => p.Id == id)
                    .Select(p => new ProductModelsEdit
                    {
                        Id = p.Id,
                        Name = p.Name,
                        IsActive = p.IsActive,
                        CategoryType = p.CategoryType,
                        CompanyName = p.CompanyName,
                        Code = p.Code,
                        Price = p.Price,
                        ImageId = p.ImageId, 
                    }).FirstOrDefault();
        }

        public List<SelectListItem> SelectList(long languageId)
        {
            var list = Repository
                            .Table
                            .Where(p => !p.Deleted && p.IsActive)
                            .OrderBy(p => p.Name)
                            .Select(p => new SelectListItem
                            {
                                Text = p.Name,
                                Value = p.Id.ToString()
                            }).ToList();

            return list;
        }

        //public List<ProductDetailsModel> GetProducts()
        //{
        //    var context = (DbContext)Repository.Context;
        //    var products = _productRepository
        //                    .Table
        //                    .Where(p => !p.Deleted)
        //                    .Select(p => new ProductDetailsModel
        //                    {
        //                        Id = p.Id,
        //                        Name = p.Name,
        //                        IsActive = p.IsActive,
        //                        Code = p.Code,
        //                        CreationDate = p.CreationDate,
        //                        Price = p.Price,
        //                        OrderCount = context
        //                            .OrderItems
        //                            .Where(q => q.ProductId == p.Id)
        //                            .Select(q => q.Amount)
        //                            .DefaultIfEmpty(0).Sum(),
        //                        ProductImages = p.ProductImages
        //                            .Where(q => q.IsActive)
        //                            .Select(q => new ProductImageModelsList
        //                            {
        //                                Id = q.Id,
        //                                ImageUrl = q.Image.Url
        //                            }).ToList(),
        //                        ProductStocks = p.ProductStocks
        //                            .Where(q => !q.Deleted)
        //                            .Select(q => new ProductStockModelsList
        //                            {
        //                                Id = q.Id,
        //                                Amount = q.Amount,
        //                                BuyingPrice = q.BuyingPrice
        //                            }).ToList(),
        //                    }).ToList();

        //    return products;
        //}

        public List<ProductDetailsModel> GetCreateOrderProducts()
        {
            var context = (DbContext)_productRepository.Context;

            var query = _productRepository
                .Table
                .Where(p => !p.Deleted);

            var products = query.Select(p => new ProductDetailsModel
            {
                Id = p.Id,
                Name = p.Name,
                Code = p.Code,
                Price = p.Price, 
                ImageUrl = p.Image.Url,
                RemainingStock = p.ProductStocks
                    .Where(q => !q.Deleted)
                    .Select(q => q.Amount)
                    .DefaultIfEmpty(0).Sum() - context
                    .OrderItems
                    .Where(q => q.ProductId == p.Id)
                    .Select(q => q.Amount)
                    .DefaultIfEmpty(0).Sum(),
            });

            if (products.Any())
            {
                products = products.Where(p => p.RemainingStock > 0);
            }

            return products.ToList();
        }
    }
}
