﻿using Core.Ioc;
using Core.Log;
using Core.Pagination;
using Core.Provider;
using Core.Settings;

namespace AcCommerce.Services.Admin.Services
{
    public class LogService : IInjectAsSelf
    {
        private readonly ILogProvider _logProvider;
        private readonly AcSettingsHelper _settingHelper;
        private readonly CoreSettings _coreSettings;
        public LogService(AcSettingsHelper settingHelper)
        {
            _settingHelper = settingHelper;
            _coreSettings = _settingHelper.LoadSettingsCached<CoreSettings>();
                
            _logProvider = AcProviderHelper.GetProvider<ILogProvider>("log", _coreSettings.LogProviderCode);
        }

        public AcPager<AcLogAdminModelsList> Index(AcLogAdminModelsFilter filter)
        {
            return _logProvider.GetLogs(filter);
        }

        public AcLogAdminModelsDetails Details(long id)
        {
            return _logProvider.GetLogDetails(id);
        }

    }
}
