﻿using AcCms.Data.Domains;
using AcCms.Services.Admin.Models;
using Core.Data.Interfaces;
using Core.Mvc.Base;
using Core.Pagination;
using System.Linq;

namespace AcCms.Services.Admin.Services
{
    public class StringResourceService : DataServiceBase<StringResource, StringResourceModelsCreate, StringResourceModelsEdit, StringResourceModelsView, StringResourceModelsFilter, StringResourceModelsExcelExport>
    {
        private readonly IRepository<StringResource> _stringResourceRepository;

        public StringResourceService(IRepository<StringResource> repository) : base(repository)
        {
            _stringResourceRepository = repository;
        }

        public override AcPager<StringResourceModelsView> Index(StringResourceModelsFilter filter)
        {
            var query = _stringResourceRepository.Table.Where(p => p.LanguageId == filter.LanguageId);

            if (!string.IsNullOrWhiteSpace(filter.Key))
            {
                query = query.Where(p => p.Key.Contains(filter.Key));
            }
            if (!string.IsNullOrWhiteSpace(filter.Value))
            {
                query = query.Where(p => p.Value.Contains(filter.Value));
            }

            return query
                .Select(p => new StringResourceModelsView
                {
                    Id = p.Id,
                    LanguageId = p.LanguageId,
                    Key = p.Key,
                    Value = p.Value
                }).ToAcPager(filter);
        }

        public override AcPager<StringResourceModelsExcelExport> ExportIndex(StringResourceModelsFilter filter)
        {
            var query = _stringResourceRepository.Table.Where(p => p.LanguageId == filter.LanguageId);

            if (!string.IsNullOrWhiteSpace(filter.Key))
            {
                query = query.Where(p => p.Key.Contains(filter.Key));
            }
            if (!string.IsNullOrWhiteSpace(filter.Value))
            {
                query = query.Where(p => p.Value.Contains(filter.Value));
            }

            return query
                .Select(p => new StringResourceModelsExcelExport
                {
                    Id = p.Id,
                    LanguageId = p.LanguageId,
                    Key = p.Key,
                    Value = p.Value
                }).ToAcPager(filter);
        }
    }
}
