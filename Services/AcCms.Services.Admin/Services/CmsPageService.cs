﻿using AcCms.Data.Domains;
using Core.Data.Interfaces;
using Core.Ioc;
using Modules.Cms.Interfaces;
using Modules.Cms.Models;
using Omu.ValueInjecter;
using System.Linq;

namespace AcCms.Services.Admin.Services
{
    public class CmsPageService : ICmsEntityAdminService, IInjectAsSelf
    {
        private readonly IRepository<Page> _pageRepository;

        public CmsPageService(IRepository<Page> pageRepository)
        {
            _pageRepository = pageRepository;
        }

        public CmsEntityUpdateModel GetEntity(long entityId)
        {
            return _pageRepository.Table.Where(p => p.Id == entityId && !p.Deleted).Select(p => new CmsEntityUpdateModel
            {
                Id = p.Id,
                ActiveVersion = p.ActiveVersion,
                DesignedVersion = p.DesignedVersion,
                LastVersion = p.LastVersion,
                LockedById = p.LockedById,
                PendingApproval = p.PendingApproval,
                Published = p.Published
            }).FirstOrDefault();
        }

        public string GetEntityName()
        {
            return "Page";
        }

        public void UpdateEntity(CmsEntityUpdateModel entity, long identityId)
        {
            var page = _pageRepository.Table.FirstOrDefault(p => p.Id == entity.Id && !p.Deleted);
            if (page != null)
            {
                page.InjectFrom(entity);
                _pageRepository.Update(page, identityId);
            }
        }
    }
}
