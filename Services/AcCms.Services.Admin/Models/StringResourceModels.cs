﻿using Core.Data.Models;
using Core.Mvc.Attributes;
using Core.Pagination;
using Core.Validation;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace AcCms.Services.Admin.Models
{
    public class StringResourceModelsCreate : EntityBase
    {
        [AcPreventOnExcelExport]
        [UIHint("Hidden")]
        public long LanguageId { get; set; }

        [AcValidateRequired]
        [AcValidateMaxLength(250)]
        [DisplayName("Key")]
        public string Key { get; set; }

        [AllowHtml]
        [AcValidateRequired]
        [DisplayName("Value")]
        public string Value { get; set; }
    }

    public class StringResourceModelsEdit : StringResourceModelsCreate
    {

    }

    public class StringResourceModelsView : StringResourceModelsCreate
    {
    }

    public class StringResourceModelsFilter : AcFilter
    {
        [UIHint("HiddenInput")]
        public long LanguageId { get; set; }

        [DisplayName("Key")]
        public string Key { get; set; }

        [DisplayName("Value")]
        public string Value { get; set; }
    }

    public class StringResourceModelsExcelExport : StringResourceModelsView
    {
    }
}
