﻿using Core.Data.Models;
using Core.Mvc.Attributes;
using Core.Pagination;
using Core.Validation;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace AcCms.Services.Admin.Models
{
    public class PagePartialModelsCreate : EntityBase
    {
        [UIHint("HiddenInput")]
        public long LanguageId { get; set; }

        [DisplayName("Name")]
        [AcValidateRequired]
        [AcValidateMaxLength(250)]
        public string Name { get; set; }
    }

    public class PagePartialModelsEdit : PagePartialModelsCreate
    {

    }

    public class PagePartialModelsView : PagePartialModelsCreate
    {
        public bool Published { get; set; }
        public bool PendingApproval { get; set; }

        public int ActiveVersion { get; set; }
        public int DesignedVersion { get; set; }
        public int LastVersion { get; set; }

        public long? LockedById { get; set; }
        public string LockedByUserName { get; set; }
    }

    public class PagePartialModelsFilter : AcFilter
    {
        [UIHint("HiddenInput")]
        public long LanguageId { get; set; }
    }

    public class PagePartialModelsExcelExport : PagePartialModelsView
    {

    }
}
