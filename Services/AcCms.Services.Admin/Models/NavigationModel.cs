﻿namespace AcCms.Services.Admin.Models
{
    public class NavigationModel
    {
        public long Id { get; set; }
        public int ActiveItem { get; set; }
    }
}
