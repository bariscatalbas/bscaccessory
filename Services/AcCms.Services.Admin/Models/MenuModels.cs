﻿using Core.Data.Models;
using Core.Mvc.Attributes;
using Core.Pagination;
using Core.Validation;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AcCms.Services.Admin.Models
{
    public class MenuModelsCreate : EntityBase
    {
        [AcPreventOnExcelExport]
        [UIHint("HiddenInput")]
        [AcValidateRequired]
        public long LanguageId { get; set; }

        [DisplayName("Name")]
        [AcValidateMaxLength(250)]
        [AcValidateRequired]
        public string Name { get; set; }
    }

    public class MenuModelsEdit : MenuModelsCreate
    {

    }
    public class MenuModelsView : MenuModelsCreate
    {

    }
    public class MenuModelsFilter : AcFilter
    {
        [UIHint("HiddenInput")]
        public long LanguageId { get; set; }
    }
    public class MenuModelsExcelExport : MenuModelsView
    {
    }

    public class MenuItemCreateModel : EntityBase
    {
        [UIHint("HiddenInput")]
        public long MenuId { get; set; }

        [DisplayName("Parent")]
        [UIHint("DropdownLong")]
        [AcDropdownParameter("ParentList")]
        public long? ParentId { get; set; }

        [DisplayName("Display Order")]
        public int DisplayOrder { get; set; }

        [DisplayName("Name")]
        [AcValidateRequired]
        [MaxLength(250)]
        public string Name { get; set; }

        [DisplayName("Url")]
        [AcValidateRequired]
        [MaxLength(500)]
        public string Url { get; set; }

        [AcPreventOnExcelExport]
        [AcValidateRequired]
        [AcMediaModule("Menu")]
        [UIHint("Image")]
        [DisplayName("Media")]
        public long? MediaId { get; set; }
    }

    public class MenuItemEditModel : MenuItemCreateModel
    {

    }

    public class MenuItemViewModel : MenuItemCreateModel
    {
        public string ParentName { get; set; }
    }
}
