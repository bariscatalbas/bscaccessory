﻿using AcCms.Models.Enumeration;
using Core.Data.Models;
using Core.Mvc.Attributes;
using Core.Pagination;
using Core.Validation;
using System.Collections.Generic;
using System.ComponentModel;

namespace AcCms.Services.Admin.Models
{
    public class RoleModelsCreate : EntityBase
    {
        public RoleModelsCreate()
        {
            SelectedPrivileges = new List<RolePrivilegeModel>();
        }

        [AcValidateRequired]
        [AcValidateMaxLength(250)]
        [DisplayName("Name")]
        public string Name { get; set; }

        [DisplayName("Privileges")]
        [AcPreventOnExcelExport]
        public List<RolePrivilegeModel> SelectedPrivileges { get; set; }
    }

    public class RoleModelsEdit : RoleModelsCreate
    {

    }

    public class RoleModelsView : RoleModelsCreate
    {
        public RoleModelsView()
        {
            PrivilegeNames = new List<string>();
        }

        public List<string> PrivilegeNames { get; set; }
    }

    public class RoleModelsFilter : AcFilter
    {

    }

    public class RoleModelsExcelExport : RoleModelsView
    {
        [DisplayName("Privilege Names")]
        public string PrivilageNamesValues
        {
            get
            {
                return string.Join(", ", this.PrivilegeNames);
            }
        }

    }

    public class RolePrivilegeModel
    {
        public bool Selected { get; set; }
        public Enums.Privilege Privilege { get; set; }
    }
}
