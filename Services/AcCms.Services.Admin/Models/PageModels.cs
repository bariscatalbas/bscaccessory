﻿using Core.Data.Models;
using Core.Mvc.Attributes;
using Core.Pagination;
using Core.Validation;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace AcCms.Services.Admin.Models
{
    public class PageModelsCreate : EntityBase
    {
        [UIHint("HiddenInput")]
        public long LanguageId { get; set; }

        [DisplayName("Url")]
        [AcValidateRequired]
        [AcValidateMaxLength(250)]
        [AcValidateNonAlphaNumeric(additionalAllowedValues: "/")]
        public string Url { get; set; }

        [DisplayName("Meta Title")]
        [AcValidateMaxLength(500)]
        public string MetaTitle { get; set; }

        [DisplayName("Meta Description")]
        [AcValidateMaxLength(500)]
        public string MetaDescription { get; set; }

        //[DisplayName("Meta Image")]
        //[AcMediaModule("Page")]
        //[UIHint("Image")]
        //public long? MetaImageId { get; set; }

        [DisplayName("Canonical Url")]
        [AcValidateMaxLength(500)]
        public string CanonicalUrl { get; set; }
    }

    public class PageModelsEdit : PageModelsCreate
    {

    }

    public class PageModelsView : PageModelsCreate
    {
        public bool Published { get; set; }
        public bool PendingApproval { get; set; }

        public int ActiveVersion { get; set; }
        public int DesignedVersion { get; set; }
        public int LastVersion { get; set; }

        public long? LockedById { get; set; }
        public string LockedByUserName { get; set; }
    }

    public class PageModelsFilter : AcFilter
    {
        [UIHint("HiddenInput")]
        public long LanguageId { get; set; }
    }

    public class PageModelsExcelExport : PageModelsView
    {

    }
}
