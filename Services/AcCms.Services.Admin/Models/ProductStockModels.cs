﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Data.Models;
using Core.Pagination;
using Core.Validation;

namespace AcCms.Services.Admin.Models
{
    public class ProductStockModelsCreate : EntityBase
    {
        [UIHint("HiddenInput")]
        public long ProductId { get; set; }
        [DisplayName("Ürün Adeti")]
        public int Amount { get; set; }
        [DisplayName("Alım Fiyatı")]
        public decimal BuyingPrice { get; set; }
    }

    public class ProductStockModelsEdit : ProductStockModelsCreate
    {

    }

    public class ProductStockModelsView : ProductStockModelsCreate
    {
        public DateTime CreationDate { get; set; }
    }

    public class ProductStockModelsFilter : AcFilter
    {
        [UIHint("HiddenInput")]
        public long ProductId { get; set; }
    }

    public class ProductStockModelsExcelExport : ProductStockModelsView
    {

    }
}
