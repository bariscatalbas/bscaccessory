﻿using Core.Data.Models;
using Core.Mvc.Attributes;
using Core.Pagination;
using Core.Validation;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace AcCms.Services.Admin.Models
{
    public class AccountModelsCreate : EntityBase
    {
        public AccountModelsCreate()
        {
            RoleIds = new List<long>();
        }

        [DisplayName("First Name")]
        [AcValidateMaxLength(250)]
        public string FirstName { get; set; }

        [DisplayName("Last Name")]
        [AcValidateMaxLength(250)]
        public string LastName { get; set; }

        [DisplayName("Email")]
        [AcValidateMaxLength(250)]
        [AcValidateRequired]
        public string Email { get; set; }

        [DisplayName("Phone")]
        [AcValidateMaxLength(250)]
        public string Phone { get; set; }

        [DisplayName("Is Active?")]
        public bool IsActive { get; set; }

        [DisplayName("Is Email Validated?")]
        public bool IsEmailValidated { get; set; }

        [UIHint("DropdownMultiple")]
        [AcDropdownParameter("RoleList")]
        [DisplayName("Roles")]
        public List<long> RoleIds { get; set; }
    }

    public class AccountModelsEdit : AccountModelsCreate
    {

    }

    public class AccountModelsView : AccountModelsCreate
    {
        public AccountModelsView()
        {
            RoleNames = new List<string>();
        }

        public List<string> RoleNames { get; set; }
    }

    public class AccountModelsFilter : AcFilter
    {
        [DisplayName("Role")]
        [UIHint("DropdownLong")]
        [AcDropdownParameter("RoleList")]
        public long? RoleId { get; set; }

        [DisplayName("First Name")]
        public string FirstName { get; set; }

        [DisplayName("Last Name")]
        public string LastName { get; set; }

        [DisplayName("Email")]
        public string Email { get; set; }

        [DisplayName("Phone")]
        public string Phone { get; set; }
    }

    public class AccountModelsExcelExport : AccountModelsView
    {
        [DisplayName("Roles")]
        public string RoleNamesValue
        {
            get
            {
                return string.Join(", ", this.RoleNames);
            }
        }
    }

    public class AccountModelsChangePassword
    {
        [ScaffoldColumn(false)]
        public long Id { get; set; }

        [AcValidateRequired]
        [DisplayName("Password")]
        public string Password { get; set; }
    }
}
