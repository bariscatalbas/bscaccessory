﻿using Core.Data.Models;

namespace AcCms.Services.Admin.Models
{
    public class MediaProviderModelsView : EntityBase
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string SystemName { get; set; }
        public string SettingsSystemName { get; set; }
    }
}
