﻿using Core.Data.Models;
using Core.Pagination;
using Core.Validation;
using System.ComponentModel;

namespace AcCms.Services.Admin.Models
{
    public class LanguageModelsCreate : EntityBase
    {
        public string PageTitle { get; set; }

        [DisplayName("Code")]
        [AcValidateMaxLength(10)]
        public string Code { get; set; }

        [DisplayName("Name")]
        [AcValidateMaxLength(50)]
        public string Name { get; set; }

        [DisplayName("Is Published?")]
        public bool Published { get; set; }

        [DisplayName("Is Default Language?")]
        public bool IsDefault { get; set; }

        [DisplayName("Display Order")]
        public int DisplayOrder { get; set; }
    }

    public class LanguageModelsEdit : LanguageModelsCreate
    {

    }

    public class LanguageModelsView : LanguageModelsCreate
    {
    }

    public class LanguageModelsFilter : AcFilter
    {
        [DisplayName("Code")]
        public string Code { get; set; }
        
        [DisplayName("Name")]
        public string Name { get; set; }
    }

    public class LanguageModelsExcelExport : LanguageModelsView
    {

    }

}
