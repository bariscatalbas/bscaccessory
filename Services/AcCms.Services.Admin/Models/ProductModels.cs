﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AcCms.Models.Enumeration;
using Core.Data.Models;
using Core.Mvc.Attributes;
using Core.Pagination;
using Core.Validation;

namespace AcCms.Services.Admin.Models
{
    public class ProductModelsCreate : EntityBase
    {
        public ProductModelsCreate()
        {
            IsActive = true;
        }
        [DisplayName("Aktif mi?")]
        public bool IsActive { get; set; }
        [DisplayName("Görsel")]
        [AcMediaModule("Product")]
        [UIHint("Image")]
        public long? ImageId { get; set; }
        [DisplayName("Ürün Tipi")]
        public Enums.ProductCategory CategoryType { get; set; }
        [AcValidateMaxLength(500)]
        [DisplayName("Firma")]
        public string CompanyName { get; set; }
        [AcValidateMaxLength(500)]
        [AcValidateRequired]
        [DisplayName("Stok Kodu")]
        public string Code { get; set; }
        [AcValidateMaxLength(500)]
        [AcValidateRequired]
        [DisplayName("Ürün Adı")]
        public string Name { get; set; }
        [DisplayName("Ürün Fiyatı")]
        public decimal Price { get; set; }

    }

    public class ProductModelsEdit : ProductModelsCreate
    {

    }

    public class ProductModelsView : ProductModelsCreate
    {
        public ProductModelsView()
        {
            ProductImages = new List<ProductImageModelsList>();
            ProductStocks = new List<ProductStockModelsList>();
        }
        public DateTime CreationDate { get; set; }
        public int OrderCount { get; set; }
        public string ImageUrl { get; set; }
        public List<ProductImageModelsList> ProductImages { get; set; }
        public List<ProductStockModelsList> ProductStocks { get; set; }
    }

    public class ProductModelsFilter : AcFilter
    {
        [DisplayName("Ürün Adı")]
        public string Name { get; set; }
        [DisplayName("Ürün Tipi")] 
        [UIHint("Enum")]  
        public Enums.ProductCategory? CategoryType { get; set; }
        [DisplayName("Sadece Aktif Olan Ürünler")]
        public bool IsActiveOnly { get; set; }
        [DisplayName("Firma")]
        public string CompanyName { get; set; }
        [DisplayName("Görselleri Gizle")]
        public bool IsImageHide { get; set; }
    }

    public class ProductModelsExcelExport : ProductModelsView
    {

    }

    public class ProductImageModelsList : EntityBase
    {
        public string ImageUrl { get; set; }
    }

    public class ProductStockModelsList : EntityBase
    {
        public int Amount { get; set; }
        public decimal BuyingPrice { get; set; }
    }

    public class ProductDetailsModelsFilter : AcFilter
    {
        public string Name { get; set; }
    }

    public class ProductDetailsModel : EntityBase
    {
        [AcValidateMaxLength(500)] 
        [DisplayName("Stok Kodu")]
        public string Code { get; set; }
        [AcValidateMaxLength(500)] 
        [DisplayName("Ürün Adı")]
        public string Name { get; set; }
        [DisplayName("Ürün Fiyatı")]
        [AcValidateRequired]
        public decimal Price { get; set; }
        [DisplayName("Kalan Stok")]
        public int RemainingStock { get; set; }
        [DisplayName("Ürün Görseli")]
        public string ImageUrl { get; set; }
        [DisplayName("Adet")]
        [AcValidateRequired]
        public int Amount { get; set; }
    }

    public class OrderModelsCreate : EntityBase
    {
        [DisplayName("Satış Kanalı")]
        [AcValidateRequired("Zorunludur")]
        public Enums.OrderChannel OrderChannel { get; set; }
        [AcValidateMaxLength(500)]
        [AcValidateRequired("Zorunludur")]
        [DisplayName("Adı")]
        public string FirstName { get; set; }
        [AcValidateMaxLength(500)]
        [AcValidateRequired("Zorunludur")]
        [DisplayName("Soyadı")]
        public string LastName { get; set; }
        [AcValidateMaxLength(500)]
        [AcValidateRequired("Zorunludur")]
        [DisplayName("Şehir")]
        public string City { get; set; }
        [AcValidateMaxLength(500)]
        [AcValidateRequired("Zorunludur")]
        [DisplayName("İlçe")]
        public string District { get; set; }
        [AcValidateMaxLength(500)]
        [AcValidateRequired("Zorunludur")]
        [DisplayName("Adres")]
        [UIHint("TextArea")]
        public string Address { get; set; }
        [AcValidateMaxLength(50)]
        [DisplayName("Telefon")]
        public string Phone { get; set; }
        [AcValidateMaxLength(50)]
        [DisplayName("E-posta")]
        public string Email { get; set; }
        [DisplayName("Kargo Ücreti")]
        public decimal? ShipmentPrice { get; set; }
        [DisplayName("Not")]
        [UIHint("TextArea")]
        public string Notes { get; set; }
        
    }

    public class OrderModelsEdit : OrderModelsView
    {

    }

    public class OrderModelsView : EntityBase
    {
        public string OrderNo { get; set; }
        public DateTime Date { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string City { get; set; }
        public string District { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        [DisplayName("Sipariş Durumu")]
        public Enums.OrderStatus OrderStatus { get; set; }
        public Enums.OrderChannel OrderChannel { get; set; }
        public string Notes { get; set; }
        public DateTime? ShipmentDate { get; set; }
        [DisplayName("Kargo Numarası")]
        public string ShipmentNumber { get; set; }
        [DisplayName("Kargo Fiyatı")]
        public decimal? ShipmentPrice { get; set; }
        [DisplayName("Kargo Tipi")]
        public Enums.ShipmentType? ShipmentType { get; set; }
        public decimal? TotalPrice { get; set; }
    }

    public class OrderDetailsModel : OrderModelsView
    {
        public OrderDetailsModel()
        {
            Items= new List<OrderItemListModel>();
        }
        public List<OrderItemListModel> Items { get; set; }
    }

    public class OrderModelsFilter : AcFilter
    {
        [DisplayName("Email")]
        public string Email { get; set; }

        [DisplayName("Date Start")]
        public DateTime? DateStart { get; set; }

        [DisplayName("Date End")]
        public DateTime? DateEnd { get; set; }
    }
    public class OrderItemListModel : EntityBase
    {
        [DisplayName("Stok Kodu")]
        public string Code { get; set; }
        [DisplayName("Ürün Adı")]
        public string Name { get; set; }
        [DisplayName("Ürün Fiyatı")]
        public decimal Price { get; set; }
        [DisplayName("Kalan Stok")]
        public int RemainingStock { get; set; }
        [DisplayName("Ürün Görseli")]
        public string ImageUrl { get; set; }
        [DisplayName("Adet")]
        public int Amount { get; set; }
    }

}
