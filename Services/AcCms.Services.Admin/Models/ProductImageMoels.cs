﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Data.Models;
using Core.Mvc.Attributes;
using Core.Pagination;
using Core.Validation;

namespace AcCms.Services.Admin.Models
{
    public class ProductImageModelsCreate : EntityBase
    {
        public ProductImageModelsCreate()
        {
            IsActive = true;
        }
        [UIHint("HiddenInput")]
        public long ProductId { get; set; }
        [DisplayName("Görsel Aktif mi?")]
        public bool IsActive { get; set; }
        [DisplayName("Ürün Sırası")]
        public int DisplayOrder { get; set; }
        [DisplayName("Görsel")]
        [AcMediaModule("Product")]
        [UIHint("Image")]
        public long? ImageId { get; set; }
    }

    public class ProductImageModelsEdit : ProductImageModelsCreate
    {

    }

    public class ProductImageModelsView : ProductImageModelsCreate
    {
        public string ImageUrl { get; set; }
    }

    public class ProductImageModelsFilter : AcFilter
    {
        [UIHint("HiddenInput")]
        public long ProductId { get; set; }
    }

    public class ProductImageModelsExcelExport : ProductImageModelsView
    {

    }
}
