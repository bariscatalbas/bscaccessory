﻿using AcCms.Services.Common.Resources;
using AcCms.Services.Common.Services;
using Core.Ioc;
using Core.Mvc.Routing;
using Core.Resources;
using System.Collections.Generic;

namespace AcCms.Services.Web.Routing
{
    public class AcRouteProvider : IRouteProvider, IInjectAsInterface
    {
        public Dictionary<string, object> DecideErrorPage(long containerId)
        {
            var dictionary = new Dictionary<string, object>
            {
                ["controller"] = "Home",
                ["action"] = "Error",
                ["languageId"] = containerId,
            };
            return dictionary;
        }

        public string Decide404Page(long containerId)
        {
            var resourceProvider = AcEngine.Resolve<AcResourceHelper>();
            var webResource = resourceProvider.LoadResourceCached<WebResource>(containerId);
            return webResource.PageNotFoundUrl;
        }

        public string DecideContainer(string url, out long containerId)
        {
            var containerPart = (url ?? "").Trim().Trim('/').Split('/')[0];
            var languageProvider = AcEngine.Resolve<LanguageService>();

            bool isDefault;
            containerId = languageProvider.DecideLanguageFromUrl(containerPart, out isDefault);
            if (containerId == 0)
            {
                return "";
            }

            return isDefault ? url : url.Substring(containerPart.Length).TrimStart('/');
        }

        public string DecideHomePage(long containerId)
        {
            var resourceProvider = AcEngine.Resolve<AcResourceHelper>();
            var webResource = resourceProvider.LoadResourceCached<WebResource>(containerId);
            return webResource.HomePageUrl;
        }

        public Dictionary<string, object> DecideNoContainerPage()
        {
            var dictionary = new Dictionary<string, object>();
            dictionary["controller"] = "Home";
            dictionary["action"] = "Error";
            return dictionary;
        }

        public Dictionary<string, object> DecideRoute(long containerId, long entityId, string entityName, string url, string parameters)
        {
            var dictionary = new Dictionary<string, object>();
            switch (entityName)
            {
                case "Page":
                    dictionary["controller"] = "Home";
                    dictionary["action"] = "Content";
                    dictionary["entityId"] = entityId;
                    dictionary["entityName"] = "Page";
                    dictionary["languageId"] = containerId;
                    dictionary["url"] = url;
                    break;
                case "Container":
                    dictionary["controller"] = "Home";
                    dictionary["action"] = "Content";
                    dictionary["entityId"] = entityId;
                    dictionary["entityName"] = "Container";
                    dictionary["languageId"] = containerId;
                    dictionary["url"] = url;
                    break;
                case "Collection":
                    dictionary["controller"] = "Home";
                    dictionary["action"] = "Content";
                    dictionary["entityId"] = entityId;
                    dictionary["entityName"] = "Collection";
                    dictionary["languageId"] = containerId;
                    dictionary["url"] = url;
                    break;

                case "NotFound":
                    dictionary["controller"] = "Home";
                    dictionary["action"] = "NotFound";
                    dictionary["languageId"] = containerId;
                    dictionary["url"] = url;
                    break;


            }

            return dictionary;
        }
    }
}
