﻿using AcCms.Services.Common.Authentication;
using Core.Ioc;
using System;
using System.Web.Mvc;

namespace AcCms.Services.Web.Mvc
{
    public class LoadErrorFilterAttribute : FilterAttribute, IAuthorizationFilter
    {
        public void OnAuthorization(AuthorizationContext filterContext)
        {
            if (filterContext == null)
            {
                throw new ArgumentNullException("filterContext");
            }
            var identity = AcEngine.Resolve<WebIdentity>();

            if (!string.IsNullOrWhiteSpace(identity.LastSuccess))
            {
                filterContext.Controller.TempData["__SUCCESS_MESSAGE__"] = identity.LastSuccess;
            }

            if (!string.IsNullOrWhiteSpace(identity.LastError))
            {
                filterContext.Controller.TempData["__ERROR_MESSAGE__"] = identity.LastError;
            }
        }
    }
}
