﻿using AcCms.Models.Core;
using AcCms.Services.Common.Authentication;
using AcCms.Services.Common.Resources;
using AcCms.Services.Common.Services;
using AcCms.Services.Web.Services;
using Core.Ioc;
using Core.Mvc.Base;

namespace AcCms.Services.Web.Mvc
{
    public abstract class ClientViewBaseWithIdentity<T> : MvcViewBase<T>
    {
        private WebIdentity _clientIdentity;
        public WebIdentity Identity
        {
            get
            {
                if (_clientIdentity == null)
                {
                    _clientIdentity = AcEngine.Resolve<WebIdentity>();
                    return _clientIdentity;
                }
                return _clientIdentity;
            }
        }

        private StringResourceService _stringResourceService;
        public StringResourceService StringResourceService
        {
            get
            {
                if (_stringResourceService == null)
                {
                    _stringResourceService = AcEngine.Resolve<StringResourceService>();
                }
                return _stringResourceService;
            }
        }

        private ContentService _contentService;
        public ContentService ContentService
        {
            get
            {
                if (_contentService == null)
                {
                    _contentService = AcEngine.Resolve<ContentService>();
                }
                return _contentService;
            }
        }

        private LanguageService _languageService;
        public LanguageService LanguageService
        {
            get
            {
                if (_languageService == null)
                {
                    _languageService = AcEngine.Resolve<LanguageService>();
                }
                return _languageService;
            }
        }

        private long _languageId;
        private LanguageModel _language;
        public LanguageModel Language
        {
            get
            {
                if (_language == null)
                {
                    if (_languageId == 0)
                    {
                        _languageId = GetValue<long>("languageId", 0);
                    }
                    _language = LanguageService.GetLanguage(_languageId);
                }
                return _language;
            }
        }

        private WebResource _webResource;
        public WebResource WebResource
        {
            get
            {
                if (_webResource == null)
                {
                    _webResource = ContentService.GetWebResource(Language);
                }
                return _webResource;
            }
        }

        public string Term(string key)
        {
            return StringResourceService.GetStringResource(Language.Id, key);
        }

        public string ProductThumb(string productUrl)
        {
            return _.Io.AppendTextToFileName(productUrl, "_w500", "thumbs/");
        }
    }
}
