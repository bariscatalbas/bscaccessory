﻿using AcCms.Data.Domains;
using AcCms.Models.Content;
using AcCms.Models.Core;
using AcCms.Services.Common.Resources;
using AcCms.Services.Common.Services;
using Core.Cache;
using Core.Data.Interfaces;
using Core.Resources;
using Modules.Cms.Models;
using Modules.Cms.Services; 
using System.Collections.Generic;
using System.Linq; 

namespace AcCms.Services.Web.Services
{
    public class ContentService
    {
        private readonly IRepository<Page> _pageRepository;
        private readonly IRepository<PagePartial> _pagePartialRepository;
        private readonly IRepository<MenuItem> _menuItemRepository;

        private readonly AcCacheHelper _cacheHelper;
        private readonly AcResourceHelper _resourceHelper;
        private readonly CmsService _cmsService;

        public ContentService(IRepository<Page> pageRepository, IRepository<PagePartial> pagePartialRepository, AcCacheHelper cacheHelper,
            AcResourceHelper resourceHelper, CmsService cmsService, IRepository<MenuItem> menuItemRepository)
          
        {
            _pageRepository = pageRepository;
            _pagePartialRepository = pagePartialRepository;
            _menuItemRepository = menuItemRepository;

            _cacheHelper = cacheHelper;
            _resourceHelper = resourceHelper;
            _cmsService = cmsService;
        }

        public PageModel CmsEntity(string entityName, long entityId, bool loadUnpublished = false)
        {
            if (entityName == "Page")
            {
                return _cacheHelper.GetVersioned("AC_CACHE_PAGE_" + entityId,
                        () =>
                        {
                            return _pageRepository
                                    .Table
                                    .Where(p => p.Id == entityId && (loadUnpublished || p.Published) && !p.Deleted)
                                    .Select(p => new PageModel
                                    {
                                        EntityId = p.Id,
                                        EntityName = "Page",
                                        CmsVersion = p.ActiveVersion,
                                        MetaTitle = p.MetaTitle,
                                        MetaDescription = p.MetaDescription,
                                        MetaImage = p.MetaImage.Url,
                                        CanonicalUrl = p.CanonicalUrl,
                                    }).FirstOrDefault();
                        });
            }
            return null;
        }
        public List<CmsContentModel> CmsContents(LanguageModel language, string entityName, long entityId, int cmsVersion, bool skipCache)
        {
            return _cacheHelper.Get("AC_CACHE_CMS_" + entityName + "_" + entityId + "_" + cmsVersion,
                        () =>
                        {
                            var contents = _cmsService.GetContents(entityName, entityId, cmsVersion);
                            foreach (var content in contents)
                            {
                                language.LocalizeObject(content.Data);
                            }

                            return contents;
                        }, 300, skipCache);
        }
        public PagePartialModel GetPagePartialIdByNameLanguageId(string name, long languageId)
        {
            return _cacheHelper.GetVersioned("AC_CACHE_PAGEPARTIAL_" + name + "_" + languageId,
                        () =>
                        {
                            return _pagePartialRepository
                                       .Table
                                       .Where(p => p.Name == name && p.LanguageId == languageId && p.Published && !p.Deleted)
                                       .Select(p => new PagePartialModel
                                       {
                                           Name = p.Name,
                                           EntityId = p.Id,
                                           EntityName = "PagePartial",
                                           CmsVersion = p.ActiveVersion
                                       }).FirstOrDefault();
                        });
        }
        public WebResource GetWebResource(LanguageModel language)
        {
            var webResouce = _resourceHelper.LoadResourceCached<WebResource>(language.Id);
            var localizedWebResource = language.LocalizeClone(webResouce);
            return localizedWebResource;
        }
        public List<MenuModel> GetMenu(long? menuId)
        {
            if (menuId.HasValue)
            {
                return _cacheHelper.GetVersioned("AC_CACHE_MENU_" + menuId.Value,
                    () =>
                    {
                        var menu = _menuItemRepository
                                .Table
                                .Where(p => p.MenuId == menuId.Value)
                                .OrderBy(p => p.DisplayOrder)
                                .Select(p => new MenuModel
                                {
                                    ParentId = p.ParentId,
                                    Name = p.Name,
                                    Url = p.Url,
                                    MediaUrl = p.Media.Url
                                }).ToList();

                        return menu;
                    });

            }

            return new List<MenuModel>();
        }
    }
}
